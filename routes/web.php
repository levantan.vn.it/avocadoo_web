<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\TermsController;
use Illuminate\Support\Facades\Route;

Route::get('', function () {
    return redirect()->route('admin.dashboard');
});

Route::get('webview/address', function () {
    return view('webview.address');
});
Route::get('/policy','PageController@policy')->name('policy.index');
Route::get('/terms','PageController@term')->name('terms.index');


Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
