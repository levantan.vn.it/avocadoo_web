<?php

namespace App\Notifications;

use App\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\User;
use Illuminate\Notifications\DatabaseNotification;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\AndroidConfig;
use NotificationChannels\Fcm\Resources\AndroidFcmOptions;
use NotificationChannels\Fcm\Resources\AndroidNotification;
use NotificationChannels\Fcm\Resources\ApnsConfig;
use NotificationChannels\Fcm\Resources\ApnsFcmOptions;
use Illuminate\Support\Str;

class OrderSendNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $order;
    public $action_name;
    public $title;
    public $content;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Order $order = null, string $action_name)
    {
        $this->order = Order::forFirstProduct()->with('firstProduct')->find($order->id);
        $this->action_name = $action_name;
        list('title' => $title, 'content' => $content) = order_notify_lang($action_name);
        $this->title = $title;
        $this->content = $content;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', FcmChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Notification User')
            ->line('The introduction to the notification')
            ->line($this->title)
            ->line($this->content)
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'action_name' => $this->action_name,
            'order_id'    =>  $this->order->id,
            'images' => optional(optional($this->order)->firstProduct)->avatar,
            'order_icon' => $this->getOrderIcon()
        ];
    }

    private function getOrderIcon()
    {
        return 'order_icon/' . strtolower($this->action_name) . '.png';
    }


    public function getDataNotification($notifiable)
    {
        return [
            'title' =>  $this->title,
            'content' =>  $this->content,
            'order_id'    =>  "{$this->order->id}",
            'action_name' => $this->action_name
        ];
    }

    public function getDataReplace($notifiable)
    {
        return [
            ':ORDER_ID' => "#ZMB{$this->order->id}",
            ':DELIVERY_NAME' => $this->order->delivery_method,
            ':DELIVERY_ID' => "#ZMB{$this->order->delivery_id}"
        ];
    }

    public function toFcm($notifiable)
    {
        $notify = DatabaseNotification::where([
            'notifiable_id' => $notifiable->id,
            'notifiable_type' => 'App\User',
            'type' => 'App\Notifications\OrderSendNotification'
        ])->latest()->first();
        $notify = optional($notify);

        $data = $this->getDataNotification($notifiable);
        $dataReplace = $this->getDataReplace($notifiable);

        foreach ($dataReplace as $key => $text) {
            $data['title'] = Str::replaceFirst($key, $text, $data['title']);
            $data['content'] = Str::replaceFirst($key, $text, $data['content']);
        }

        $data['notification_id'] = $notify->id;

        $image = optional(optional($this->order)->firstProduct)->avatar;

        return FcmMessage::create()
            ->setData($data)
            ->setNotification(
                \NotificationChannels\Fcm\Resources\Notification::create()
                    ->setTitle($data['title'])
                    ->setBody($data['content'])
                    ->setImage($image)
            )
            ->setAndroid(
                AndroidConfig::create()
                    ->setFcmOptions(AndroidFcmOptions::create()->setAnalyticsLabel('analytics'))
                    ->setNotification(AndroidNotification::create()->setColor('#0A0A0A')->setClickAction('FLUTTER_NOTIFICATION_CLICK')->setImage($image))
            )->setApns(
                ApnsConfig::create()
                    ->setFcmOptions(ApnsFcmOptions::create()->setAnalyticsLabel('analytics_ios'))
            );
    }
}
