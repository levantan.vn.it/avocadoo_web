<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryCondition extends Model
{
    protected $table = "delivery_condition";
    protected $fillable = [
        'title',
        'price'
    ];
}
