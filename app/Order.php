<?php

namespace App;

use App\Notifications\OrderSendNotification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\DatabaseNotification as Notification;
use Illuminate\Support\Facades\Log;

class Order extends Model
{
    public static function boot()
    {
        parent::boot();

        static::created(function (Order $order) {
            $user = $order->user()->first();
            if ($order->isUnPaid()) {
                $user->notify(new OrderSendNotification($order, config('constant.ORDER_UNPAID')));
            }
        });
        static::updated(function (Order $order) {
            $user = $order->user()->first();
            if ($order->isProccessing()) {
                $user->notify(new OrderSendNotification($order, config('constant.ORDER_PAID')));
            }
            if ($order->isDelivering()) {
                $user->notify(new OrderSendNotification($order, config('constant.ORDER_DELIVERING')));
            }
            if ($order->isCompletedDelivery()) {
                $user->notify(new OrderSendNotification($order, config('constant.ORDER_ARRIVED')));
            }
            if ($order->isCancel()) {
                $user->notify(new OrderSendNotification($order, config('constant.ORDER_CANCELED')));
                $order->cancelAction();
            }
            // if ($order->isRefunded()) {
            //     $user->notify(new OrderSendNotification($order, config('constant.ORDER_REFUND')));
            // }
        });

        static::deleted(function (Order $order) {
            try {
                Notification::where('type', 'App\Notifications\OrderSendNotification')->where('data', 'like', "%\"order_id\":{$order->id},%")->delete();
            } catch (\Exception $e) {
                Log::error("Failed delete notification: ", [$e]);
            }
        });
    }

    public function cancelAction()
    {
        $this->productSkus()->each(function (ProductSku $product) {
            $product->updateQty($product->pivot->qty, true);
        });
    }

    public function isUnPaid()
    {
        return $this->status == static::UNPAID;
    }
    public function isProccessing()
    {
        return $this->status == static::PROCESSING;
    }
    public function isDelivering()
    {
        return $this->status == static::DELIVERING;
    }
    public function isCompletedDelivery()
    {
        return $this->status == static::COMPLETED_DELIVERY;
    }
    public function isCompleted()
    {
        return $this->status == static::COMPLETED;
    }
    public function isCancel()
    {
        return $this->status == static::CANCEL;
    }
    public function isDeleted()
    {
        return $this->status == static::DELETED;
    }
    public function isRefunded()
    {
        return $this->status == static::REFUND;
    }

    public function isConfirmDelivery()
    {

        return $this->confirm_admin;
    }

    protected $table = 'orders';

    const UNPAID = 1;
    const PROCESSING = 2;
    const DELIVERING = 3;
    const COMPLETED_DELIVERY = 4;
    const COMPLETED = 5;
    const CANCEL = 6;
    const DELETED = 7;
    const REFUND = 8;
    const CONFIRM_DELIVERY = 9;
    const LIST_STATUS = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    const LIST_PROCESS = [1, 2, 3, 4, 9];
    const NOTIFICATION = 'App\Notifications\OrderSendNotification';

    protected $casts = [
        'status' => 'integer'
    ];

    protected $fillable = [
        'id',
        'displayname',
        'phone',
        'email',
        'store_name',
        'delivery_address',
        'sub_total',
        'delivery_fee',
        'total',
        'delivery_method',
        'delivery_id',
        'shipper_name',
        'shipper_phone',
        'delivery_date',
        'status',
        'user_id',
        'images',
        'receiver',
        'phone',
        'interprovincial_type',
        'note',
        'reason_cancel',
        'confirm_admin',
        'receiver',
        'delivery_confirm_date'
    ];

    protected $dates = [
        'delivery_confirm_date'
    ];

    public function productSkus()
    {
        return $this->belongsToMany(ProductSKU::class, 'order_product', 'order_id', 'product_sku_id')
            ->withTrashed()
            ->withTimestamps()
            ->withPivot(['variant', 'qty', 'subtotal']);
    }


    public function OrderProduct()
    {
        return $this->hasMany(OrderProduct::class, 'order_id')->with('product');
    }

    public function getFilePdfAttribute()
    {
        return $this->invoice_images ? my_asset($this->invoice_images) : '';
    }
    public function getParseSerializeAttribute()
    {
        try {
            return $this->sku ? serialize($this->sku) : [];
        } catch (\Exception $e) {
            return [];
        }
    }

    public function getImageFirstAttribute()
    {
        return $this->images ? $this->images->first() : [];
    }
    public function getParseVariantAttribute()
    {
        try {
            return $this->variant ? unserialize($this->variant) : '';
        } catch (\Exception $e) {
            return [];
        }
    }
    public function changeStatusDelivery()
    {
        if ($this->status == 2) {
            return $this->status = 3;
        }
    }

    public function checkStatus()
    {
        if ($this->status == 1) {
            return '미결제';
        } else if ($this->status == 2) {
            return '배송준비중';
        } else if ($this->status == 3) {
            return '배송중';
        } else if ($this->status == 4 || $this->status == 9) {
            return '배송완료';
        } else if ($this->status == 5) {
            return '주문완료';
        } else if ($this->status == 6) {
            return '주문취소';
        } else if ($this->status == 8) {
            return '환불금';
        } else {
            return '삭제';
        }
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'order_product', 'order_id', 'product_id')
            ->withTrashed()
            ->withTimestamps()
            ->withPivot(['variant', 'qty', 'subtotal']);
    }

    public function allProduct()
    {
        $products = Product::where('status', '1')->get();
        return $products;
    }

    public function transaction()
    {
        return $this->hasOne(Transaction::class);
    }

    public function firstProduct()
    {
        return $this->belongsTo(Product::class, 'product_id')->withTrashed();
    }

    public function scopeForFirstProduct($query)
    {
        return $query->addSelect([
            'product_id' => OrderProduct::select('product_id')
                ->whereColumn('order_id', 'orders.id')
                ->orderBy('id', 'asc')
                ->take(1)
        ]);
    }

    public function scopeWithAllRelation($query)
    {
        return $query->with(['OrderProduct' => function ($q) {
            $q->with(['product' => function ($query) {
                $query->withoutGlobalScope('skuMinPrice')->withTrashed();
            }, 'productSku' => function ($query) {
                $query->withTrashed();
            }]);
        }, 'transaction']);
    }
}
