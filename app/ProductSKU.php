<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProductSKU extends Model
{
    use SoftDeletes;
    protected $table = 'product_sku';

    public static function boot()
    {
        parent::boot();

        static::deleted(function (ProductSKU $productSKU) {
            Log::debug($productSKU);
            if (!OrderProduct::where('product_sku_id', $productSKU->id)->exists() && Cart::where('product_sku_id', $productSKU->id)->exists()) {
                $productSKU->forceDelete();
            }
        });
    }

    protected $fillable = [
        'product_id',
        'sku',
        // 'title',
        'weight',
        'price',
        'qty',
        'status',
        'sale',
        'variant'
    ];


    const STATUS_ACTIVE = '1';
    const STATUS_INACTIVE = '2';

    public function product()
    {
        return $this->belongsTo(Product::class);
    }


    public function scopeActiveInStock($query)
    {
        return $query->where([
            'product_sku.status' => static::STATUS_ACTIVE,
            ['product_sku.qty', '>', 0]
        ])->whereNotNull('variant');
    }

    public function getParseVariantAttribute($value)
    {
        try {
            return $this->variant ? unserialize($this->variant) : null;
        } catch (\Exception $e) {
            return null;
        }
    }

    public function updateQty($qty, $refund = false)
    {
        if ($qty < 0) return false;
        if ($refund) {
            $this->update([
                // 'sales' => DB::raw("IF(sales > {$qty}, sales - {$qty}, 0)"),
                'qty' => DB::raw("qty + {$qty}")
            ]);
            $this->product()->update([
                'total_sale' => DB::raw("IF(total_sale > {$qty}, total_sale - {$qty}, 0)")
            ]);
        } else {
            $this->update([
                // 'sales' => DB::raw("IF(qty < {$qty}, IF(qty > 0, sales + qty, sales), sales + {$qty})"),
                'qty' => DB::raw("IF(qty < {$qty}, 0, qty - {$qty})")
            ]);
            $this->product()->update([
                'total_sale' => DB::raw("total_sale + {$qty}")
            ]);
        }
    }
}
