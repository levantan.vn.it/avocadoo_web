<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $fillable = [
        'title',
        'type',
        'parent_id',
        'images',
        'id_images',
        'main_image',
        'main_image_id'
    ];

    protected $appends = [
        'imageUrlMain',
        'imageUrl',
        'banner'
    ];

    public function products(){
    	return $this->hasMany(Product::class);
    }

    public function childrenCategories()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function parentCategory()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function getBannerAttribute()
    {
        return my_asset($this->images);

    }

    public function isActive(){
        return intval($this->status) === 1;
    }
    public function isInActive(){
        return intval($this->status) === 2;
    }

    public function image(){
        return $this->belongsTo(File::class, 'id_images');
    }

    public function getImageUrlAttribute(){
        return my_asset($this->images);
    }
    public function getImageUrlMainAttribute(){
        return my_asset($this->main_image);
    }



}
