<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notification_admin';
    protected $fillable = ['title','type','content','status','images','id_images'];
    public function statusText()
    {
        if ($this->status == 1) {
            return __('notifications.active');
        } elseif ($this->status == 2) {
            return __('notifications.inactive');
        }
        return "";
    }

    public function typeText() {
        if($this->type == 1){
            return __('notifications.announcement');
        }elseif($this->type == 2) {
            return __('notifications.promotion');
        } elseif($this->type == 3) {
            return __('notifications.system');
        }
        return "";
    }

}
