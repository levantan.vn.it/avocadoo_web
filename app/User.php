<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $table = 'users';
    protected $appends = ['avatar_url'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'displayname', 'email', 'password', 'fullname', 'phone', 'birthday', 'gender', 'verify_phone', 'status', 'business_name', 'tax_invoice',
        'representative', 'store_name', 'store_address', 'avatar', 'kakao_id', 'naver_id', 'document', 'document_id', 'city_id', 'district_id', 'ward_id', 'store_detailed_address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function deviceFCMs()
    {
        return $this->hasMany('App\UserDeviceFcms', 'user_id');
    }

    /**
     * ------------------------------
     * Notification TO
     * ------------------------------
     */
    /**
     * Specifies the user's FCM token
     *
     * @return array
     */
    public function routeNotificationForFcm()
    {
        return $this->deviceFCMs()->pluck('token')->toArray();
    }


    /**
     * Get all of the payment method for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments()
    {
        return $this->hasMany(UserPayment::class, 'user_id');
    }

    /**
     * Get all of the preimary payment method for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function paymentPrimary()
    {
        return $this->hasOne(UserPayment::class, 'user_id')->whereType(1);
    }

    /**
     * Get all of the delivery_books for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deliveryBooks()
    {
        return $this->hasMany(DeliveryBook::class, 'user_id');
    }

    /**
     * Get all of the primary delivery_books for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deliveryBookPrimary()
    {
        return $this->hasOne(DeliveryBook::class, 'user_id')->where('delivery_books.type', DeliveryBook::PRIMARY);
    }

    public function genderText()
    {
        if ($this->gender == 1) {
            return __('users.male');
        } elseif ($this->sex == 2) {
            return __('users.female');
        }
        return "";
    }

    public function statusText()
    {
        if ($this->status == 1) {
            return __('users.active');
        } elseif ($this->status == 2) {
            return __('users.deactive');
        } elseif ($this->status == 3) {
            return __('users.block');
        }
        return "";
    }

    public function isActive()
    {
        return intval($this->status) === 1;
    }

    public function isInActive()
    {
        return intval($this->status) === 2;
    }


    public function likeProducts()
    {
        return $this->belongsToMany(Product::class, 'like_products', 'user_id', 'product_id');
    }


    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function transaction()
    {
        return $this->hasMany(Transaction::class);
    }

    public function getAvatarUrlAttribute()
    {
        return my_asset($this->avatar);
    }

    public function carts()
    {
        return $this->hasMany(Cart::class, 'user_id');
    }

    public function searches()
    {
        return $this->belongsToMany(Search::class, 'user_search', 'user_id', 'search_id')->withPivot('last_search_at');
    }

    public function fk_district()
    {
        return $this->hasOne(District::class, 'id', 'district_id');
    }

    public function fk_ward()
    {
        return $this->hasOne(Ward::class, 'id', 'ward_id');
    }

    public function fk_city()
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }

    public function getFullAddressAttribute()
    {
        $list = [$this->store_address];
        if (!empty($this->fk_ward->name)) {
            $list[] = $this->fk_ward->name;
        }
        if (!empty($this->fk_district->name)) {
            $list[] = $this->fk_district->name;
        }
        if (!empty($this->fk_city->name)) {
            $list[] = $this->fk_city->name;
        }
        return implode(", ", $list);
    }
}
