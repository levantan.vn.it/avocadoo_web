<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SupplierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'location' => 'required|max:255',
            'email' => 'required|max:255|email:filter',
            'phone' => 'required|min:11|regex:/[0-9]{6,20}/',
            'representative' => 'required|max:255'
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if (!empty($validator->errors()->all())) {
            $validator->errors()->add('SupplierRequest', 'is-invalid');
            }
        });
    }

    public function attributes()
    {
        return [
            'name' => '이름',
            'address' => '주소',
            'location' => '배송지',
            'email' => '이메일',
            'phone' => '전화번호',
            'representative' => '대표자명'
        ];
    }
}
