<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'displayname' => 'required|max:255',
            'fullname' => '',
            'email' => 'required|email:rfc,dns',
            'phone' => 'required|min:11',
        ];
    }
        /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if (!empty($validator->errors()->all())) {
            $validator->errors()->add('UserRequest', 'is-invalid');
            }
        });
    }
   /**
 * Get custom attributes for validator errors.
 *
 * @return array
 */
    public function attributes()
    {
        return [
            'displayname' => '화면에 표시될 이름',
            'email' => 'Email',
            'phone' => '폰번호',
        ];
    }
}
