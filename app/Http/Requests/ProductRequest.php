<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // $variant = [];
        // $title= $this->variant['title'] ?? [];
        // for ($i=0; $i < count($title); $i++) {
        //     if (!empty($title[$i])) {
        //         $variant = array_merge($variant, [
        //             "variant.title.{$i}"=> 'required|string|max:100',
        //             "variant.value.{$i}"=> 'required|array|min:1',
        //             "variant.value.{$i}.*"=> 'required|string',
        //         ]);
        //     }
        // }
        $rules= [
            'name' => 'required|string|max:255',
            'category_id' => 'required|exists:categories,id',
            'supplier_id' => 'required|exists:suppliers,id',
            // 'variant'=>'required|array',
            'variant.title.*'=>'required',
            'variant.value.*.*'=>'required',

            // 'variant[title][]'=>'required'
            // 'value' => ['required'],

            // 'variant' => 'required|array',
            // 'title'     => 'required|array',
            // 'title.*' =>  'required',
            // // 'value.*' =>  'required',
        ];
        // if ($this->value ) {
        //     $rules[ 'value' ]= ['min']
        // }
        return  array_merge($rules);
    }


    public function attributes()
    {
        return [
            'name' =>'이름',
            'category_id' =>'카테고리 ID',
            'supplier_id' =>'공급자 ID',
            'variant.title.*'=>'변형 제목',
            'variant.value.*.*'=> '변형 값',
        ];
    }

    // protected function prepareForValidation()
    // {
    //     $value = $this->variant['value'] ?? [];
    //     $title = $this->variant['title'] ?? [];

    //     $value = collect($value)->map(function($item){
    //         return is_array($item) ? collect($item)->filter(function($i){
    //             return $i;
    //         }): [];
    //     });

    //     $this->merge([
    //         'variant'=> [
    //             'title'=> $title,
    //             'value'=> $value
    //         ]
    //     ]);
    // }


}
