<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CollectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:100',
            'section' => 'required',
            'short_description' => 'required|string|min:5',
            'start_date' => 'required|date|',
            'end_date' => 'required|date|after:start_date',
            'brand_id' => 'nullable',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if (!empty($validator->errors()->all())) {
            $validator->errors()->add('CollectionRequest', 'is-invalid');
            }
        });
    }

    public function attributes()
    {
        return [
            'name' => __('collections.title'),
            'section' => __('collections.section'),
            'short_description' => __('collections.sort_descript'),
            'start_date' => __('collections.start_date'),
            'end_date' => __('collections.end_date'),
        ];
    }
}
