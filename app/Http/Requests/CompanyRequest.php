<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'city_id' => 'bail|nullable|exists:cities,id',
            // 'district_id' => 'bail|nullable|exists:districts,id',
            // 'ward_id' => 'bail|nullable|exists:wards,id',
            'company_name' => 'required|string|max:255',
            'represent_name' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'business_number' => 'required|numeric',
            'email' => 'required|string|email|max:255',
            'hotline' => 'required|numeric|min:10',
            'description' => 'required|string|max:255',
            'tax_code' => 'required|string|max:191'
        ];
    }

    public function attributes()
    {
        return [
            'city_id' => __('settings.city'),
            'district_id' => __('settings.district'),
            'ward_id' => __('settings.ward'),
            'address' => __('settings.address'),
            'business_number' => __('settings.register_number'),
            'email' => __('settings.email'),
            'hotline' => __('settings.hotline'),
            'description' => __('settings.description'),
        ];
    }
}
