<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules= [
            'name' => 'required|string|max:255',
            'category_id' => 'required|exists:categories,id',
            'supplier_id' => 'required|exists:suppliers,id',
            'description'=> 'required',
            'variant.title.*'=>'required',
            'variant.value.*.*'=>'required',
            'id_files' => 'required'
            
        ];
        return  $rules;
    }


    public function attributes()
    {
        return [
            'name' =>'이름',
            'category_id' =>'카테고리 ID',
            'supplier_id' =>'공급자 ID',
            'description' =>'제품 설명',
            'variant.title.*'=>'변형 제목',
            'variant.value.*.*'=> '변형 값',
            'id_files'=>'상품 이미지',
            // 'variant[title][]'=>'표제'
        ];
    }
}
