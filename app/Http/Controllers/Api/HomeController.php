<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\BannerCollection;
use App\Http\Resources\CategoryCollection;
use App\Http\Resources\CategoryDetailCollection;
use App\Http\Resources\BrandCollection;
use App\Http\Resources\BrandDetailCollection;
use App\Http\Resources\CollectionCollection;
use App\Http\Resources\CollectionDetailCollection;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use App\UserDeviceFcms;
use JWTAuth;
use App\File;
use App\EntityImage;
use App\Banner;
use App\Category;
use App\Supplier;
use App\Setting;
use App\Collection;
use App\City;
use App\District;
use App\Ward;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{

    public function banners(Request $request)
    {
        $banners = new Banner();
        if (!empty($request->position)) {
            $banners = $banners->where('position', $request->position);
        }
        $banners = $banners->where('start_date', '<=', date('Y-m-d'))->where('end_date', '>=', date('Y-m-d'))->where('status', '<>', 3)->orderBy('id', 'desc')->get();
        return new BannerCollection($banners);
    }

    public function categories(Request $request)
    {
        $categories = new Category();
        if (!empty($request->parent_id)) {
            $categories = $categories->where('parent_id', $request->parent_id);
        } else {
            $categories = $categories->where('parent_id', 0);
        }
        $categories = $categories->where('status', 1)->orderBy('id', 'desc')->get();
        return new CategoryCollection($categories);
    }

    public function categoryDetail(Request $request, $id)
    {
        $category = Category::where('id', $id)->first();
        if (empty($category) || $category->status != 1) {
            return response()->json([
                'message' => __('api_not_found.Category'),
                'success' => false
            ], 500);
        }
        return new CategoryDetailCollection($category);
    }

    public function brands(Request $request)
    {
        return new BrandCollection(Supplier::where('status', 1)->orderBy('id', 'desc')->get());
    }

    public function brandDetail(Request $request, $id)
    {
        $brand = Supplier::withCount([
            'products' => function ($query) {
                $query->countActiveInStock()->select(DB::raw('COUNT(DISTINCT `products`.id)'));
            }
        ])->where('id', $id)->first();
        if (empty($brand) || $brand->status != 1) {
            return response()->json([
                'message' => __('api_not_found.Supplier'),
                'success' => false
            ], 500);
        }

        return BrandDetailCollection::make($brand);
    }

    public function settings(Request $request)
    {
        $setting = Setting::select('value', 'updated_at')->where('type', $request->key)->first();
        if ($request->key == 'company_infor') {
            if ($setting) {
                $data = json_decode($setting->value, true);
                // $data['full_address'] = full_adress_company($setting);
            }
            $data = (object)($data ?? []);
            return response()->json([
                'message' => "OK",
                'success' => true,
                'data'  =>  $data,
                'updated_at'    =>  !empty($setting->updated_at) ? date('Y년 m월 d일', strtotime($setting->updated_at)) : "",
            ], 200);
        }
        $content = $setting->value;
        if($request->key == 'term_service'){
            $setting2 = Setting::select('value', 'updated_at')->where('type', 'term_service2')->first();
            $content .= $setting2->value ?? "";
            $setting3 = Setting::select('value', 'updated_at')->where('type', 'term_service3')->first();
            $content .= $setting3->value ?? "";
            $setting4 = Setting::select('value', 'updated_at')->where('type', 'term_service4')->first();
            $content .= $setting4->value ?? "";
            $setting5 = Setting::select('value', 'updated_at')->where('type', 'term_service5')->first();
            $content .= $setting5->value ?? "";
            $setting6 = Setting::select('value', 'updated_at')->where('type', 'term_service6')->first();
            $content .= $setting6->value ?? "";
            $setting7 = Setting::select('value', 'updated_at')->where('type', 'term_service7')->first();
            $content .= $setting7->value ?? "";
            return response()->json([
                'message' => "OK",
                'success' => true,
                'data'  =>  $content,
                'updated_at'    =>  !empty($setting->updated_at) ? date('Y년 m월 d일', strtotime($setting->updated_at)) : "",
            ], 200);
        }
        return response()->json([
            'message' => "OK",
            'success' => true,
            'data'  =>  $setting->value ?? "",
            'updated_at'    =>  !empty($setting->updated_at) ? date('Y년 m월 d일', strtotime($setting->updated_at)) : "",
        ], 200);
    }

    public function collections(Request $request)
    {
        $collections = new Collection();
        if (!empty($request->section)) {
            $collections = $collections->where('section', $request->section);
        }
        $collections = $collections->where('status', 1)->where('start_date', '<=', date('Y-m-d'))->where('end_date', '>=', date('Y-m-d'))->orderBy('id', 'desc')->get();
        return new CollectionCollection($collections);
    }

    public function collectionDetail(Request $request, $id)
    {
        $collection = Collection::where('id', $id)->with(['products' => function ($query) use ($request) {
            $query->with(['supplier', 'productSku', 'skus' => function ($query) {
                $query->activeInStock();
            }])->when(true, function ($query) use ($request) {
                $query->activeInStock();
                if ($request->category_id) {
                    $query->whereIn('category_id', is_array($request->category_id) ? $request->category_id : [$request->category_id]);
                }

                if ($request->min_price && $request->min_price >= 0) {
                    $query->forSkuMinPrice($request->min_price)->where('product_sku.price', '>=', intval($request->min_price));
                }

                if ($request->max_price && $request->max_price > 0) {
                    $query->where('product_sku.price', '<=', intval($request->max_price));
                }

                if ($request->order) {
                    $query->orderBy('product_sku.price', preg_match('/(asc|desc)/i', $request->sort) ? $request->sort : 'asc');
                }

                $brand = null;

                if ($request->keyword) {
                    $query->where('products.name', 'like', '%' . $request->keyword . '%');
                }

                if ($request->brand && is_array($request->brand)) {
                    $brand = array_unique($request->brand);
                    $brand = array_filter($brand, function ($b) {
                        return $b;
                    });
                }

                if (!empty($brand)) {
                    $query->whereIn('products.supplier_id', $request->brand);
                }
            })->latest('products.created_at');
        }])->where('start_date', '<=', date('Y-m-d'))->where('end_date', '>=', date('Y-m-d'))->first();
        if (empty($collection) || $collection->status != 1) {
            return response()->json([
                'message' => __('api_not_found.Collection'),
                'success' => false
            ], 500);
        }
        return new CollectionDetailCollection($collection);
    }

    public function cities(Request $request)
    {
        $cities = City::select('id', 'name')->get();
        return response()->json([
            'message' => "OK",
            'success' => true,
            'data'  =>  $cities
        ], 200);
    }

    public function districts(Request $request, $city_id)
    {
        $districts = District::select('id', 'name')->where('city_id', $city_id)->get();
        return response()->json([
            'message' => "OK",
            'success' => true,
            'data'  =>  $districts
        ], 200);
    }

    public function wards(Request $request, $district_id)
    {
        $wards = Ward::select('id', 'name')->where('district_id', $district_id)->get();
        return response()->json([
            'message' => "OK",
            'success' => true,
            'data'  =>  $wards
        ], 200);
    }

    public function deliveryCompany(Request $request)
    {
        return response()->json([
            'success' => true,
            'data'  =>  [
                'company_name'  =>  get_setting('company_shipping_name'),
                'company_logo'  =>  my_asset(api_asset(get_setting('company_logo'))),
                'company_delivery_time'  =>  get_setting('company_delivery_time'),
            ]
        ], 200);
    }
}
