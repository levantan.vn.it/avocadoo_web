<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\UserCollection;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use App\UserDeviceFcms;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\File;
use App\EntityImage;
use App\Http\Resources\NotificationResource;
use App\Message;
use App\Order;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function info($id)
    {
        return new UserCollection(User::where('id', $id)->first());
    }

    public function user(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        return new UserCollection($user);
    }

    public function changePassword(Request $request)
    {
        $request->validate([
            'password' => 'bail|required|string|min:10|max:20|confirmed',
            'current_password' => 'bail|required|string|min:10|max:20'
        ]);
        $user = JWTAuth::parseToken()->authenticate();
        if (!Hash::check($request->current_password, $user->password)) {
            return response()->json(['message' => __('auth.current_password_not_correct'), 'user' => null, 'error' => 'current_password'], 401);
        }
        $user->password = bcrypt($request->password);
        $user->save();
        return response()->json([
            'message' => __('auth.change_password_success'),
            'data'  =>  $user
        ], 200);
    }

    /**
     * [deleteDocument delete business document]
     * @param  [type] $id [id of file]
     */
    public function deleteDocument($index)
    {
        $user = JWTAuth::parseToken()->authenticate();
        try {
            $document = explode(",", $user->document);
            $document_id = explode(",", $user->document_id);
            $index = $index - 1;
            if (empty($document[$index])) {
                return response()->json([
                    'message' => 'error'
                ], 500);
            }
            unset($document[$index]);
            unset($document_id[$index]);
            $user->document = implode(",", $document);
            $user->document_id = implode(",", $document_id);
            $user->save();
            return response()->json([
                'message' => __('auth.update_success')
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'error'
            ], 500);
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json([
                'message' => 'error'
            ], 500);
        }
    }



    public function updateAvatar(Request $request)
    {
        $request->validate([
            'avatar' => 'required|image|file|max:20480',
        ]);
        $user = JWTAuth::parseToken()->authenticate();
        if ($request->hasFile('avatar')) {
            if (!empty($user->avatar)) {
                Storage::delete($user->avatar);
            }

            $user->avatar = $request->file('avatar')->store('uploads/avatar');
            $user->save();
        }
        return response()->json([
            'message' => __('members.change_avatar'),
            'data'  =>  new UserCollection($user)
        ]);
    }

    public function addFcmToken(Request $request)
    {
        $request->validate([
            'token' => 'bail|required|max:191',
            'device' => 'bail|required|string|in:ios,android',
        ]);
        $user = JWTAuth::parseToken()->authenticate();
        try {
            $check = UserDeviceFcms::where('token', $request->token)->first();
            if (!empty($check)) {
                if ($check->user_id != $user->id) {
                    $check->user_id = $user->id;
                    $check->save();
                }
                return response()->json([
                    'message' => __('auth.update_success')
                ]);
            }
            $request->merge(['user_id' => $user->id]);
            UserDeviceFcms::create($request->all());
            return response()->json([
                'message' => __('auth.update_success')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'error'
            ], 500);
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json([
                'message' => 'error'
            ], 500);
        }
    }

    public function deleteFcmToken($token)
    {
        try {
            UserDeviceFcms::where('token', $token)->delete();
            return response()->json([
                'message' => __('auth.update_success')
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'error'
            ], 500);
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json([
                'message' => 'error'
            ], 500);
        }
    }

    public function updateProfile(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $request->validate([
            'displayname' => 'bail|required|string|max:100',
            'email' => 'bail|required|string|email|max:100|unique:users,email,' . $user->id,
            'fullname' => 'bail|required|string|max:100',
            'phone' => 'bail|required|string|max:20|unique:users,phone,' . $user->id,
            'birthday' => 'bail|nullable|date',
            'gender' => 'bail|nullable|in:1,2',

        ]);
        try {
            $user->displayname = $request->displayname;
            $user->fullname = $request->fullname;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->birthday = $request->birthday ?? null;
            $user->gender = $request->gender ?? null;

            $user->save();
            return response()->json([
                'message' => __('auth.update_success'),
                'data'  =>  new UserCollection($user)
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'error'
            ], 500);
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json([
                'message' => 'error'
            ], 500);
        }
    }

    public function updateBusiness(Request $request)
    {
        $request->validate([
            'business_name' => 'bail|required|max:100',
            'tax_invoice' => 'bail|required|max:100',
            'representative' => 'bail|required|max:100',
            'store_name' => 'bail|required|max:100',
            'store_address' => 'bail|required|max:255',
            'post_code' => 'bail|nullable',
            'store_detailed_address' => 'bail|nullable|max:255',
        ]);
        $user = JWTAuth::parseToken()->authenticate();
        try {
            $user->business_name = $request->business_name;
            $user->tax_invoice = $request->tax_invoice;
            $user->representative = $request->representative;
            $user->store_name = $request->store_name;
            $user->store_address = $request->store_address;
            $user->post_code = $request->post_code;
            $user->store_detailed_address = $request->store_detailed_address;
            $documents = [];
            $id_files = [];
            if ($request->hasFile('business_document')) {
                $type = array(
                    "jpg" => "image",
                    "jpeg" => "image",
                    "png" => "image",
                    "svg" => "image",
                    "webp" => "image",
                    "gif" => "image",
                    "mp4" => "video",
                    "mpg" => "video",
                    "mpeg" => "video",
                    "webm" => "video",
                    "ogg" => "video",
                    "avi" => "video",
                    "mov" => "video",
                    "flv" => "video",
                    "swf" => "video",
                    "mkv" => "video",
                    "wmv" => "video",
                    "wma" => "audio",
                    "aac" => "audio",
                    "wav" => "audio",
                    "mp3" => "audio",
                    "zip" => "archive",
                    "rar" => "archive",
                    "7z" => "archive",
                    "doc" => "document",
                    "txt" => "document",
                    "docx" => "document",
                    "pdf" => "document",
                    "csv" => "document",
                    "xml" => "document",
                    "ods" => "document",
                    "xlr" => "document",
                    "xls" => "document",
                    "xlsx" => "document"
                );
                $files = $request->file('business_document');
                foreach ($files as $file) {
                    $upload = new File;
                    $upload->file_original_name = null;
                    $arr = explode('.', $file->getClientOriginalName());

                    for ($i = 0; $i < count($arr) - 1; $i++) {
                        if ($i == 0) {
                            $upload->file_original_name .= $arr[$i];
                        } else {
                            $upload->file_original_name .= "." . $arr[$i];
                        }
                    }
                    $upload->file_name = $file->store('uploads/business_document');
                    $upload->member_id = $user->id;
                    $upload->user_id = $user->id;
                    $upload->extension = strtolower($file->getClientOriginalExtension());
                    if (isset($type[$upload->extension])) {
                        $upload->type = $type[$upload->extension];
                    } else {
                        $upload->type = "others";
                    }
                    $upload->file_size = $file->getSize();
                    $upload->save();
                    $documents[] = $upload->file_name;
                    $id_files[] = $upload->id;
                }
            }
            if (!empty($documents)) {
                $curent_doc = $user->document;
                $documents = implode(",", $documents);
                if(!empty($curent_doc)){
                    $documents = $curent_doc . "," . $documents;
                }
                $user->document = $documents;
                $curent_doc_id = $user->document_id;
                $document_id = implode(",", $id_files);
                if(!empty($curent_doc_id)){
                    $document_id = $curent_doc_id . "," . $document_id;
                }
                $user->document_id = $document_id;
            }

            $user->save();

            return new UserCollection($user);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'error'
            ], 500);
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json([
                'message' => 'error'
            ], 500);
        }
    }

    /**
     * Amount of unread notification
     * @return jsonResponse
     */
    public function countNotificationUnread()
    {
        $user = JWTAuth::parseToken()->authenticate();
        return response()->json([
            'success' => true,
            'status' => 200,
            'data' => [
                'total' => $user->unreadNotifications()->count()
            ]
        ]);
    }

    /**
     * List notification
     */
    public function notifications()
    {
        $user = JWTAuth::parseToken()->authenticate();
        // Get order notification
        $notifications = $user->notifications()->paginate(request('pageSize'));
        // Mark read notification
        // $notifications->markAsRead();
        $orderIds = $notifications->where('type', Order::NOTIFICATION)->pluck('data.order_id')->unique()->all();
        $orders = Order::forFirstProduct()->with('firstProduct')->whereIn('id', $orderIds)->get();
        $notifications->each(function ($notify, $key) use ($user, $orders) {
            $data = $notify->data;
            $action_name = $data['action_name'] ?? '';
            $notify->body_data = collect($data)->except([
                'title',
                'content',
                'action_name',
                'images'
            ])->all();
            switch ($notify->type) {
                case Order::NOTIFICATION:
                    $message = order_notify_lang($action_name);
                    $order_id = $data['order_id'] ?? 0;
                    $notify['data'] = [
                        'title' => $message['title'] ?? '',
                        'content' => $message['content'] ?? '',
                        'action_name' => $action_name
                    ];
                    $order = optional($orders->where('id', $order_id)->first());
                    $notify->images = optional($order->firstProduct)->avatar ?? null;
                    $notify->body_data = [
                        ':ORDER_ID' => $order_id,
                        ':DELIVERY_NAME' => $order->delivery_method,
                        ':DELIVERY_ID' => $order->delivery_id
                    ];
                    $notify->order_icon = static_asset($data['order_icon'] ?? null);
                    break;
                default:
                    $notify->images = my_asset($data['images'] ?? null);
                    $data['images'] = $notify->images;
                    $notify['data'] = $data;
                    break;
            }
        });




        return NotificationResource::collection($notifications);
    }

    /**
     * Mark read all notifications
     */
    public function markAsReadAllNotification()
    {
        $user = JWTAuth::parseToken()->authenticate();
        try {
            $updated =  $user->unreadNotifications()->update(['read_at' => now()]);
            return response()->json([
                'success' => true,
                'status' => 200,
                'data' => [
                    'updated' => $updated
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'status' => 500,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Mark read notifications
     */
    public function markAsReadNotification($id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        try {
            return response()->json([
                'success' => true,
                'status' => 200,
                'data' => [
                    'updated' => $user->unreadNotifications()->where('id', $id)->update(['read_at' => now()])
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'status' => 500,
                'message' => $e->getMessage()
            ], 500);
        }
    }
}
