<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\BrandCollection;
use App\Supplier;

class BrandController extends Controller
{
    public function index()
    {
        return BrandCollection::collection(Supplier::all());
    }
}
