<?php

namespace App\Http\Controllers\Api;

use App\File;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class UploadImageController extends Controller
{
    public function upload(Request $request) {

        if($request->hasFile('file')){
            $user = JWTAuth::parseToken()->authenticate();
            $token = JWTAuth::parseToken()->getToken()->__toString();
            $user_id = $user->id;
            $path = [];
            foreach ($request->file('file') as $index => $item) {
                $path[] = my_asset($request->file[$index]->store('uploads/message/'.$user_id));
            }
            if($path) {
                return response()->json([
                    'message' => 'Upload success',
                    'url' => $path,
                    'token' => $token
                ],200);
            }else {
                return response()->json([
                    'message' => 'Upload fails',
                ],422);
            }

        }
    }
}
