<?php

namespace App\Http\Controllers;

use App\CompanyInfo;
use App\DeliveryManagement;
use App\DeliveryCondition;
use App\Faq;
use App\Setting;
use App\City;
use Illuminate\Http\Request;
use App\Http\Requests\CompanyRequest;
use App\Http\Requests\DeliveryRequest;
use App\Http\Requests\DistanceFixedAreaRequest;
use App\Http\Requests\DistanceRequest;
use App\Http\Requests\InterprovincialRequest;
use App\Http\Requests\PrivacyPoliciesRequest;
use App\Http\Requests\TermServiceRequest;
use App\Http\Requests\DeliveryConditionRequest;
use App\Role;
use App\Rules\TrimSpace;

class SettingsController extends Controller
{
    //Settings Homepage
    public function index() {
        return view('backend.settings.index');
    }

    //Company Information Page
    public function companyInfo() {
        $setting = Setting::where('type','company_infor')->first();
        if($setting) {
            $setting = json_decode($setting->value,true);
        }
        $setting = (object)($setting ?? []);
        return view('backend.settings.company.index',compact('setting'));
    }

    //Create and Update Company Information

    public function updateCompany(CompanyRequest $request) {
        $setting = Setting::where('type','company_infor')->first();
        $data = json_encode($request->validated());
        if($setting) {
            $setting->update([
                'value' => $data
            ]);
        } else {
            $setting = Setting::create([
                'type' => 'company_infor',
                'value' => $data
            ]);
        }
            // dd(($setting));
        flash(__('settings.msg_company_success'))->success();
        return redirect()->route('settings.company');

    }

    //Term of Use page
    public function termService(Request $request) {
        if(empty($request->key)){
            $service = Setting::where('type','term_service')->first();
        }else{
            $service = Setting::where('type',$request->key)->first();
        }

        return view('backend.settings.term-service.index',compact('service'));
    }
    //Term of Use Create and Update
    public function termServiceUpdate(TermServiceRequest $request) {
        if(empty($request->key)){
            $service = Setting::where('type','term_service')->first();
        }else{
            $service = Setting::where('type',$request->key)->first();
        }
        if($service) {
            $service->value = $request->description;
            $service->save();
        }else {
            $service = new Setting;
            $service->type = $request->key;
            $service->value = $request->description;
            $service->save();
        }
        flash(__('settings.msg_terms_success'))->success();
        return redirect()->route('settings.service',['key'=>$request->key]);
    }

    //Privacy and Policies page
    public function privacyPolicies() {
        $policies = Setting::where('type','policy_service')->first();
        return view('backend.settings.privacy-policies.index',compact('policies'));
    }

    //Privacy and Policies create and update
    public function privacyPoliciesUpdate(PrivacyPoliciesRequest $request) {
        $policies = Setting::where('type','policy_service')->first();
        if($policies) {
            $policies->value = $request->description;
            $policies->save();
        }else {
            $policies = new Setting;
            $policies->type = 'policy_service';
            $policies->value = $request->description;
            $policies->save();
        }
        flash(__('settings.msg_policy_success'))->success();
        return redirect()->route('settings.policies');
    }

    //FAQ page

    public function faqIndex() {
        $faqs = Faq::where('status',1)->orderBy("id","asc")->get();
        return view('backend.settings.faq.index',compact('faqs'));
    }

    //Create Faq question
    public function storeTitleFaq(Request $request){
        $request->validate([
            'title' => 'required|string|max:255'
        ]);
        $faq = new Faq;
        $faq->title = $request->title;
        $faq->save();
        flash(__('settings.msg_question_success'))->success();
        return redirect()->route('settings.faq');
    }
    public function updateFaqTitle(Request $request,$id) {
        $request->validate([
                'title' => 'required|string|max:255'
                ]
        );
        $faq = Faq::find($id);
        $faq->title = $request->title;
        if($faq->save()){
            return response()->json(['data' => $faq], 200);
        } else {
            return response()->json(false, 422);
        }
    }
    //Update Faq answer
    public function updateAnswerFaq(Request $request,$id) {
        $request->validate([
            'content'=> ['required','string','min:10', new TrimSpace]
        ],
        [
            'content.required' => __('settings.faq_content_required')
        ]
        );

        $faq = Faq::find($id);
        $faq->content = $request->content;
        if($faq->save()){
            return response()->json(['data' => $faq, 'success' => true, 'message' => 'success'], 200);
        }
        return response()->json(['success' => false, 'message' => 'error'], 422);
    }

    public function destroyFaq($id) {
        $faq = Faq::where('status','1')->findOrFail($id);
        if($faq->delete()){
            flash(__('settings.msg_deletefaq_success'))->success();
            return response()->json(true);
        }
        return response()->json(false);
    }

    //Role page
    public function roleIndex() {
        $roles = Role::all();
        return view('backend.settings.role.index',compact('roles'));
    }

    //Create Role title
    public function roleStore(Request $request) {
        $request->validate(
            [
                'name' => 'required|string|max:255'
            ],
            [
               'name.required' => __('settings.role_name_required')
            ]
            );
        Role::create($request->all());
        flash(__('settings.msg_title_role'))->success();
        return redirect()->route('settings.role');
    }

    public function roleUpdateTitle(Request $request,$id) {
        $request->validate([
                'name' => 'required|string|max:255'
                ],
                [
                   'name.required' => __('settings.role_name_required')
                ]
        );
        $role = Role::find($id);
        $role->name = $request->name;
        if($role->save()){
            return response()->json(['data' => $role], 200);
        } else {
            return response()->json(false, 422);
        }
    }


    //Update Module List
    public function roleUpdate(Request $request,$id) {
        $role = Role::findOrFail($id);
        $input = $request->item;
        $input = json_encode($input);
        // dd($input);

        if($role->update(['permissions' => $input])) {
           return response()->json(['data' => $role, 'success' => true, 'message' => 'success'], 200);
        }
        else {
            return response()->json(['success' => false, 'message' => 'error'], 422);
        }
    }

    public function destroyRole($id) {
        $faq = Role::findOrFail($id);
        if($faq->delete()){
            flash(__('settings.msg_deleterole_success'))->success();
            return response()->json(true);
        }
        return response()->json(false);
    }

    //Base on weight
    public function weightManagement() {
        $weights = DeliveryManagement::where('type_delivery','1')->get();
        // $weights = json_decode($data);
        return view('backend.settings.delivery.weight',compact('weights'));
    }

    public function updateOrInsert(Request $request) {

        $request->validate([
            'fixed_price' => 'bail|required|numeric'

        ]);
        update_setting('fixed_price',$request->fixed_price);
        return $request->ajax() ? response()->json(): redirect()->back();
        $datas = collect($request->delivery);
        $deliveryUpdate = collect($datas->whereNotNull('id')->all());
        $dataCreates = array_values($datas->whereNull('id')->all());
        // $dataCreates = $datas->whereNull('id')->all();
        DeliveryManagement::whereIn('id',$deliveryUpdate->pluck('id')->toArray())->each(function($delivery) use ($deliveryUpdate){
            $delivery->update($deliveryUpdate->where('id',$delivery->id)->first());
        });
        DeliveryManagement::insert($dataCreates);
        return $request->ajax() ? response()->json(): redirect()->back();
    }
    public function destroyRange($id) {
        $data = DeliveryManagement::findOrFail($id);
        if($data->delete()){
            $data->deliveryDistrict()->sync([]);
            $data->deliveryCity()->sync([]);
            flash(__('settings.msg_row_delete'))->success();
            return response()->json(true);
        }
        return response()->json(false);
    }

    //Base on distance (Km)
    public function distanceManagement() {
        $distances = DeliveryManagement::where('type_delivery','2')->where('distance_type','1')->get();
        // $distances = json_decode($data);
        $distancefixeds = DeliveryManagement::where('type_delivery','2')->with('deliveryDistrict')->where('distance_type','2')->get();

        $company_info = get_setting('company_infor');
        $company_info_value= [];
        if(!empty($company_info)) {
            $company_info_value = json_decode($company_info,true);
        }

        return view('backend.settings.delivery.distance',compact('distances','distancefixeds','company_info_value'));
    }

    public function updateOrInsertDistance(DistanceRequest $request) {
        $datas = collect($request->distance);
        $update = collect($datas->whereNotNull('id')->all());
        $create = array_values($datas->whereNull('id')->all());
        DeliveryManagement::whereIn('id',$update->pluck('id')->toArray())->each(function($distance) use ($update){
            $distance->update($update->where('id',$distance->id)->first());
        });
        DeliveryManagement::insert($create);
        return $request->ajax() ? response()->json(): redirect()->back();
    }

    //Base on distance (fixed area)

    public function updateOrInsertArea(DistanceFixedAreaRequest $request) {
        $datas = collect($request->area);
        $update = collect($datas->whereNotNull('id')->all());

        if(!empty($update) && count($update)){
            foreach ($update as $key) {
                $deliveryManagement = DeliveryManagement::where('id',$key['id'])->first();
                $item = DeliveryManagement::where('id',$key['id'])->update([
                    'price' =>  $key['price']
                ]);
                $deliveryManagement->deliveryDistrict()->sync($key['district_id']);
            }
        }

        $create = array_values($datas->whereNull('id')->all());
        if(!empty($create) && count($create)){
            foreach ($create as $key) {
                $item = DeliveryManagement::create([
                    'type_delivery' =>  $key['type_delivery'],
                    'distance_type' =>  $key['distance_type'],
                    'price' =>  $key['price']
                ]);
                $item->deliveryDistrict()->sync($key['district_id']);
            }
        }

        return $request->ajax() ? response()->json(['update' => $update,'create' => $create]): redirect()->back();
    }


    // Base on Interprovincial Delivery
    public function interprovincialManagement() {
        $cars = DeliveryManagement::where('type_delivery','3')->where('interprovincial_type','1')->get();
        // $cars = json_decode($data1);
        $airplanes = DeliveryManagement::where('type_delivery','3')->where('interprovincial_type','2')->get();
        $cities = City::all();
        // $airplanes = json_decode($data2);
        return view('backend.settings.delivery.interprovincial',compact('cars','airplanes','cities'));
    }

    public function updateOrInsertInter(InterprovincialRequest $request) {
        $datas = collect($request->inter);
        $update = collect($datas->whereNotNull('id')->all());
        if(!empty($update) && count($update)){
            foreach ($update as $key) {
                $deliveryManagement = DeliveryManagement::where('id',$key['id'])->first();
                $item = DeliveryManagement::where('id',$key['id'])->update([
                    'price' =>  $key['price']
                ]);
                $deliveryManagement->deliveryCity()->sync($key['city_id']);
            }
        }
        $create = array_values($datas->whereNull('id')->all());
        if(!empty($create) && count($create)){
            foreach ($create as $key) {
                $item = DeliveryManagement::create([
                    'type_delivery' =>  $key['type_delivery'],
                    'interprovincial_type' =>  $key['interprovincial_type'],
                    'price' =>  $key['price']
                ]);
                $item->deliveryCity()->sync($key['city_id']);
            }
        }

        $airplane = collect($request->airplane);
        $update = collect($airplane->whereNotNull('id')->all());
        if(!empty($update) && count($update)){
            foreach ($update as $key) {
                $deliveryManagement = DeliveryManagement::where('id',$key['id'])->first();
                $item = DeliveryManagement::where('id',$key['id'])->update([
                    'price' =>  $key['price']
                ]);
                $deliveryManagement->deliveryCity()->sync($key['city_id']);
            }
        }
        $create = array_values($airplane->whereNull('id')->all());
        if(!empty($create) && count($create)){
            foreach ($create as $key) {
                $item = DeliveryManagement::create([
                    'type_delivery' =>  $key['type_delivery'],
                    'interprovincial_type' =>  $key['interprovincial_type'],
                    'price' =>  $key['price']
                ]);
                $item->deliveryCity()->sync($key['city_id']);
            }
        }
        return $request->ajax() ? response()->json(['update' => $update,'create' => $create]): redirect()->back();
    }


    //Company Information Page
    public function deliveryCompany() {
        return view('backend.settings.delivery.company');
    }

    //Create and Update Company Information

    public function deliveryCompanyPost(Request $request) {
        $request->validate([
            'company_shipping_name' => 'bail|required|string|max:100',
            'company_delivery_time' => 'bail|required|max:100',

        ]);
        if(empty($request->id_files)){
            flash(__('settings.please_choose_logo'))->error();
            return redirect()->route('settings.delivery-company');
        }
        update_setting('company_shipping_name',$request->company_shipping_name);
        update_setting('company_logo',$request->id_files);
        update_setting('company_delivery_time',$request->company_delivery_time);
        flash(__('settings.msg_company_success'))->success();
        return redirect()->route('settings.delivery-company');

    }

    //Base on weight
    public function deliveryCondition() {
        $deliveries = DeliveryCondition::all();
        return view('backend.settings.delivery.condition',compact('deliveries'));
    }

    public function destroyCondition($id) {
        $data = DeliveryCondition::findOrFail($id);
        if($data->delete()){
            flash(__('settings.msg_row_delete'))->success();
            return response()->json(true);
        }
        return response()->json(false);
    }

    public function deliveryConditionPost(DeliveryConditionRequest $request) {

        $datas = collect($request->delivery);
        $deliveryUpdate = collect($datas->whereNotNull('id')->all());
        $dataCreates = array_values($datas->whereNull('id')->all());
        // $dataCreates = $datas->whereNull('id')->all();
        DeliveryCondition::whereIn('id',$deliveryUpdate->pluck('id')->toArray())->each(function($delivery) use ($deliveryUpdate){
            $delivery->update($deliveryUpdate->where('id',$delivery->id)->first());
        });
        DeliveryCondition::insert($dataCreates);
        return $request->ajax() ? response()->json(): redirect()->back();
    }



}
