<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\SupplierRequest;
use App\Http\Requests\AddSupplierRequest;
use App\Http\Requests\CreateSupplierRequest;
use App\Supplier;
use App\Exports\SupplierExport;
use Maatwebsite\Excel\Facades\Excel;
use App\File;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $data = $this->getData($request, 0);

        return view('backend.suppliers.index', compact('data'));
    }

    private function getData($request, $is_export = 0){
        $search = $request->search;
        $data = new Supplier;
        if($search){
            $data = $data->where(function($q) use($search){
                $q->Where(\DB::raw("CONCAT('#',id)"), 'like', '%'.$search.'%')
                ->orWhere('name', 'LIKE', "%$search%");
            });
        }
        if($request->joined_date){
            $data = $data->whereDate('created_at', $request->joined_date);
        }
        if($request->status){
            $data = $data->Where('status', $request->status);
        }
        if($is_export){
            $data = $data->get();
        }else{
            $data = $data->paginate();
        }
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return  view('backend.suppliers.create');
    }

    public function supplieraddinfo(AddSupplierRequest $request){

        $data =$request->validated();
        return redirect()->back()->withInput();
        return redirect()->route('suppliers.index');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SupplierRequest $request, $id)
    {
        $data =$request->validated();
        $suppliers= Supplier::where('id', $id)->update($data);
        flash(__('suppliers.noti_updated'))->success();
         return redirect()->route('suppliers.detail',$id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     $supplier = Supplier::find($id);
    //     if($supplier){
    //         $supplier->delete();
    //         return response()->json(true);
    //     }
    //     flash(__('suppliers.noti_deleted'))->success();
    //     return response()->json(false, 404);
    // count($supplier->products }
    public function destroy($id)
    {
        $supplier = Supplier::find($id);
        if((optional($supplier->products))->count()){
            return response()->json(false, 404);
        }
        else{
            $supplier->delete();
            flash(__('suppliers.noti_deleted'))->success();
            return response()->json(true);

        }
    }

    public function detail($id)
    {
        $data = Supplier::findOrFail($id);
        $images = $data->images;
        return view('backend.suppliers.detail', [
            'data' => $data,
            'images'    =>  $images
        ]);

    }

       public function export(Request $request)
    {
        $data = $this->getData($request, 1);
        return Excel::download(new SupplierExport($data), '브랜드관리.xlsx');
    }

    public function toggleStatus($id)
    {
        $data = Supplier::findOrFail($id);
        $data->update([
            'status'=>$data->isActive() ? 2 : 1
        ]);
          flash(__('suppliers.noti_status'))->success();
        return response()->json([], 200);
    }

    public function validateInfo(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'location' => 'required|max:255',
            'email' => 'required|max:255|email:filter',
            'phone' => 'required|max:20|regex:/[0-9]{6,20}/',
            'representative' => 'required|max:255'
        ]);
        return response()->json();
    }

    public function createinfo(AddSupplierRequest $request){
        // $ids = explode(',', $request->id_images);
        // $files = File::whereIn('id', $ids)->get('file_name');
        // $images = implode(',', $files->pluck('file_name')->toArray());
        // $request->merge([
        //     'images'=>$images
        // ]);

        // $ids_main = explode(',', $request->main_image);
        // $files = File::whereIn('id', $ids_main)->get('file_name');
        // $main_image_id = implode(',', $files->pluck('file_name')->toArray());
        // $request->merge([
        //     'main_image_id'=>$main_image_id
        // ]);


        // $data = Supplier::create($request->all());
        $supplier = new Supplier;
        $supplier->name = $request->name;
        $supplier->address = $request->address;
        $supplier->location = $request->location;
        $supplier->email = $request->email;
        $supplier->phone = $request->phone;
        $supplier->representative = $request->representative;
        // add image
        $images = [];
        if(!empty($request->id_files)){
            $id_files = explode(",", $request->id_files);
            if(!empty($id_files) && count($id_files)){
                foreach ($id_files as $value) {
                    $url = api_asset($value);
                    if(!empty($url)){
                        $images[] = $url;
                    }

                }
            }
        }
        $supplier->images = implode(",", $images);
        $supplier->id_images = ltrim($request->id_files,"0,");
        // supplier main image

        $images_main = [];
        if(!empty($request->id_main_files)){
            $id_main_files = explode(",", $request->id_main_files);
            if(!empty($id_main_files) && count($id_main_files)){
                foreach ($id_main_files as $value) {
                    $url = api_asset($value);
                    if(!empty($url)){
                        $images_main[] = $url;
                    }

                }
            }
        }

        $supplier->main_image = implode(",", $images_main);
        $supplier->main_image_id = ltrim($request->id_main_files,"0,");

        $supplier->save();

        return redirect()->route('suppliers.index');

    }

    public function checkCreate(AddSupplierRequest $request){
        return response()->json(true, 200);
    }
}
