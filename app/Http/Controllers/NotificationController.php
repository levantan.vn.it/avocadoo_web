<?php

namespace App\Http\Controllers;

use App\Exports\NotificationExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\ImageRequest;
use App\Http\Requests\NotiContentRequest;
use App\Http\Requests\NotificationRequest;
use App\Http\Requests\NotiInfoRequest;
use App\Notification;
use App\Notifications\AdminSendNotification;
use App\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $search = null;
        $notifications = $this->getData($request,$search,1);
        return view('backend.notifications.index',compact('notifications'));
    }

    public function export(Request $request){
        $data=$this->getData($request,1,0);
        return Excel::download(new NotificationExport($data),'알림관리.xlsx');
    }

    private function getData($request,$search = null,$is_paginate = 0)
    {
        $search = $request->search;
        $data = Notification::orderBy('id','desc');
        if($search){
            $data = $data->where(function($q) use($search){
                $q->where('id','like', "%".$search."%")
                ->orWhere('title', 'like', "%".$search."%");
            });
        }
        if($request->joined_date){
            $data = $data->whereDate('created_at', $request->joined_date);
        }
        if($request->status){
            $data = $data->where('status', $request->status);
        }
        if($is_paginate){
            $data = $data->paginate(10);
        }else{
            $data = $data->get();
        }
        return $data;
    }
    public function detail($id)
    {
        $notification = Notification::findOrFail($id);
        return view('backend.notifications.detail',compact('notification'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.notifications.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NotificationRequest $request)
    {
        $images = '';
        $id_files = $request->id_files;
        if(!empty($request->id_files)){
                    $images = api_asset($id_files);
        };
        $newRequest = $request->validated();
        $newRequest['images'] = $images;
        $newRequest['id_images'] = $id_files;
        Notification::create($newRequest);
        flash(__('notifications.noti_push_success'))->success();
        return redirect()->route('notifications.index');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $notification = Notification::findOrFail($id);
        if($notification){
            $notification->delete();
            flash(__('notifications.msg_delete_success'))->success();
            return response()->json(true);
        }
        return response()->json(false, 404);
    }

    public function changeStatus(Request $request,$id) {
        $notification = Notification::findOrFail($id);
        $notification->status = $request->status;
        $notification->save();
        flash(__('notifications.msg_status_success'))->success();
        return response()->json(true);
        
        
    }
    public function updateInfor(NotiInfoRequest $request,$id) {
        $notification = Notification::findOrFail($id);
        if($notification) {
            $notification->type = $request->type;
            $notification->created_at = $request->created_at;
            $notification->title = $request->title;
            $notification->save();
            return response()->json(['success' => true, 'message' => 'success'], 200);
        }
        return response()->json(['success' => false, 'message' => 'err'], 422);
    }

    public function updateDescript(NotiContentRequest $request,$id) {
        $notification = Notification::findOrFail($id);
        if($notification) {
            $notification->content = $request->content;
            $notification->save();
            return response()->json(['success' => true, 'message' => 'success'], 200);
        }
        return response()->json(['success' => false, 'message' => 'error'], 422);
    }

    public function sendNotification($id) {
        $notification = Notification::findOrFail($id);
        $users = User::where('status','1')->get();
        foreach(($users ?? []) as $user) {

            $user->notify(new AdminSendNotification(
                $notification->id,
                $notification->title,
                $notification->content,
                $notification->images,
                $notification->created_at
                )
            );
        }

        flash(__('notifications.msg_push_success'))->success();
        return back();
    }

    public function checkNotiInfor(NotiInfoRequest $request) {
        return response()->json(true,200);
    }

    public function checkNotiDescript(NotiContentRequest $request) {
        return response()->json(true,200);
    }
    public function validateImage(ImageRequest $request) {
        return response()->json();
    }
    public function validated(NotificationRequest $request) {
        return response()->json();
    }
}
