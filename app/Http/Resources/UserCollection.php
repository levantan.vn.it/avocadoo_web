<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
class UserCollection extends JsonResource
{
    public function toArray($request)
    {
        $documents = get_asset_from_list($this->document);
        return [
            'id' => (integer) $this->id,
            'displayname' => $this->displayname,
            'fullname' => $this->fullname,
            'email' => $this->email,
            'phone' => $this->phone,
            'avatar' => my_asset($this->avatar),
            'birthday' => $this->birthday,
            'gender' => $this->gender,
            'business_name' => $this->business_name,
            'tax_invoice' => $this->tax_invoice,
            'representative' => $this->representative,
            'store_name' => $this->store_name,
            'store_address' => $this->store_address,
            'store_detailed_address'    =>  $this->store_detailed_address,
            'post_code' => $this->post_code,
            'kakao_id' => $this->kakao_id,
            'naver_id' => $this->naver_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'document'   =>  $documents
        ];
    }

    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }
}
