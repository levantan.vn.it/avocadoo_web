<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return is_null($this->resource) ? [] : [
            'id' => $this->id,
            'displayname' => $this->displayname,
            'receiver'  =>  $this->receiver,
            'phone' => $this->phone,
            'email' => $this->email,
            'delivery_address' => $this->delivery_address,
            'store_name' => $this->store_name,
            'sub_total' => $this->sub_total,
            'delivery_fee' => $this->delivery_fee,
            'total' => $this->total,
            'delivery_method' => $this->delivery_method,
            'delivery_id' => $this->delivery_id,
            'shipper_name' => $this->shipper_name,
            'shipper_phone' => $this->shipper_phone,
            'delivery_date' => $this->delivery_confirm_date,
            'status' => $this->status,
            'user_id' => $this->user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'reason_cancel' =>  $this->reason_cancel,
            'products' => OrderProductDetailResource::collection($this->OrderProduct),
            'transaction' => OrderTransactionResource::make($this->transaction),
            'delivery_billed' => get_asset_from_list($this->images),
            'invoice_billed' => get_asset_from_list($this->invoice_images),
            'confirm_admin'  =>  !empty($this->confirm_admin) ? true : false,
            'delivery_invoice' => get_asset_from_list($this->upload_file_pdf)
        ];
    }

    public function __construct($resource)
    {
        $this->resource = $resource;
        $this->additional([
            'success' => true,
            'status' => 200
        ]);
    }

    public static function collection($resource)
    {
        return parent::collection($resource)->additional([
            'success' => true,
            'status' => 200
        ]);
    }
}
