<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return empty($this->resource) ? [] : [
            'id' => $this->id,
            'product_id' => $this->product_id,
            'variant' => optional($this->productSku)->parse_variant,
            'product_sku_id' => optional($this->productSku)->id,
            'qty' => $this->qty,
            'delivery_condition_id' => $this->delivery_condition_id,
            'qty_in_stock' => optional($this->productSku)->qty ?? 0,
            'price' => optional($this->productSku)->price,
            'sale' => optional($this->productSku)->sale,
            'sku' => optional($this->productSku)->sku,
            'product_name' => $this->product->name,
            'photo' => $this->product->photos,
            'weight' => optional($this->productSku)->weight,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => optional($this->productSku)->deleted_at ?? null,
            'delivery_condition'    =>  $this->delivery_condition

        ];
    }

    public function __construct($resource)
    {
        if ($resource) {
            $resource->loadMissing([
                'product',
                'productSku'
            ]);
        }
        $this->resource = $resource;
        $this->additional([
            'success' => true,
            'status' => 200
        ]);
    }

    public static function collection($resource)
    {
        if ($resource) {
            $resource->loadMissing([
                'product',
                'productSku'
            ]);
        }

        return parent::collection($resource)->additional([
            'success' => true,
            'status' => 200
        ]);
    }
}
