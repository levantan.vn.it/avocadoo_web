<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return is_null($this->resource) ? [] : [
            'id' => $this->id,
            'name' => $this->product->name,
            'variant' => $this->parse_variant,
            'qty' => $this->qty,
            'subtotal' => $this->subtotal,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->product->deleted_at,
            'name' => $this->product->name,
            'description' => $this->product->description,
            'sku' => ProductSkuResource::collection($this->product->skus),
            'photo' => $this->product->photos
        ];
    }

    public static function collection($resource)
    {
        return parent::collection($resource)->additional([
            'success' => true,
            'status' => 200
        ]);
    }
}
