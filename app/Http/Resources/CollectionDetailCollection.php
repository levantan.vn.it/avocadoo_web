<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ProductCollectionNoData;

class CollectionDetailCollection extends JsonResource
{
    public function toArray($request)
    {
        $images = get_asset_from_list($this->images);
        return [
            'id' => $this->id,
            'name' => $this->name,
            'section' => $this->section,
            'images'  =>  $images,
            'product' => ProductCollectionNoData::make($this->products)

        ];
    }

    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }
}
