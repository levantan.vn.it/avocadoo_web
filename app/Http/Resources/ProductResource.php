<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return is_null($this->resource) ? [] : [
            'id'            => $this->id,
            'name'          => $this->name,
            'description'   => $this->description,
            'brand_name'    => optional($this->supplier)->name,
            'photo'         => get_asset_from_list($this->images),
            'category_id'   => $this->category_id,
            'price'         => $this->min_price,
            'sale'          => $this->sale,
            'total_sale'    => optional($this->skus)->sum('sale')
        ];
    }

    public static function collection($resource)
    {
        return parent::collection($resource)->additional([
            'success' => true,
            'status' => 200
        ]);
    }
}
