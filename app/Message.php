<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    const DEFAULT_TYPE = 'TEXT';

    const NOTIFICATION = 'App\Notifications\MessageNotification';


    protected $fillable = ['sender_id', 'receiver_id', 'type', 'product_id', 'message'];

    // Relation ship
    public function sender()
    {
        return $this->belongsTo('App\User');
    }
    public function receiver()
    {
        return $this->belongsTo('App\User');
    }
}
