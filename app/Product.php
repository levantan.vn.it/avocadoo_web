<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;

class Product extends Model
{
    use SoftDeletes;
    protected $table = 'products';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('skuMinPrice', function (Builder $query) {
            return $query->addSelect([
                'min_price_sku_id' => ProductSKU::select('id')
                    ->whereColumn('product_id', 'products.id')
                    ->where('price', '>=', 0)
                    ->where('product_sku.status', ProductSKU::STATUS_ACTIVE)
                    ->whereNotNull('sku')
                    ->whereNotNull('variant')
                    ->orderBy('price', 'asc')
                    ->take(1)
            ]);
        });

        static::saved(function (Product $product) {
            Search::updateOrCreate([
                'query' => $product->name
            ]);
        });

        static::deleted(function (Product $product) {
            if (!OrderProduct::where('product_id', $product->id)->exists() && !Cart::where('product_id', $product->id)->exists()) {
                $product->forceDelete();
                $product->productSku()->forceDelete();
            } else {
                DB::enableQueryLog();
                $product->skus()->delete();
                Log::debug(DB::getQueryLog());
            }
        });
    }

    protected $fillable = [
        'name',
        'category_id',
        'supplier_id',
        'variant',
        'title',
        'sku',
        'status',
        'description',
        'updated_at',
        'created_at',
        'images',
        'id_images',
        'search_total',
    ];
    protected $appends = [
        'minPrice',
        'minPriceSku'
    ];

    public function checkStatus()
    {
        if ($this->status == 1) {
            return '재고 있음';
        } else {
            return '재고 없음';
        }
    }


    public function changeStatus()
    {
        if ($this->status == 1) {
            return '재고 없음';
        } else {
            return '재고 있음';
        }
    }

    public function isActive()
    {
        return intval($this->status) === 1;
    }

    public function isInActive()
    {
        return intval($this->status) === 2;
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function skus()
    {
        return $this->hasMany(ProductSKU::class, 'product_id');
    }
    public function collections()
    {
        return $this->belongsToMany(Collection::class, 'collection_products', 'product_id', 'collection_id');
    }

    public function getMinPriceSkuAttribute()
    {
        return $this->productSku->sku ?? '';
    }

    public function getMinSkuItemAttribute()
    {
        return optional(optional($this->skus())->orderBy('price', 'asc'))->first();
    }

    public function getValueSkuAttribute()
    {
        return $this->skus ? $this->skus->sku : 0;
    }

    public function getSaleAttribute()
    {
        return $this->productSku->sale ?? 0;
    }

    public function getMinPriceAttribute()
    {
        return $this->productSku->price ?? 0;
    }

    public function userLikes()
    {
        return $this->belongsToMany(Product::class, 'like_products');
    }

    public function getParseVariantAttribute()
    {
        try {
            return $this->variant ? unserialize($this->variant) : [];
        } catch (\Exception $e) {
            return [];
        }
    }

    public function getParseVariantTitleAttribute()
    {
        if (empty($this->parse_variant)) return [];

        list('title' => $title, 'value' => $value) = $this->parse_variant;

        return $title;
    }

    public function getParseVariantValueAttribute()
    {
        if (empty($this->parse_variant)) return [];

        list('title' => $title, 'value' => $value) = $this->parse_variant;

        return $value;
    }

    public function getPhotosAttribute()
    {
        return get_asset_from_list($this->images) ?? [];
    }

    public function getAvatarAttribute()
    {
        return $this->photos[0] ?? null;
    }

    public function productSku()
    {
        return $this->belongsTo(ProductSKU::class, 'min_price_sku_id');
    }

    public function authLike()
    {
        return $this->hasOne(LikeProduct::class, 'product_id')->where('like_products.user_id', auth('api')->id());
    }


    public function scopeActiveInStock($query, $active = false)
    {
        if ($active) {
            $query->where('products.status', 1);
        }
        return $query->select(['products.*'])->leftJoin('product_sku', 'products.id', 'product_sku.product_id')
            ->whereNotNull('product_sku.id')
            ->where('product_sku.status', ProductSKU::STATUS_ACTIVE)
            ->where('product_sku.qty', '>', 0)
            ->whereNotNull('product_sku.sku')
            ->whereNotNull('product_sku.variant')
            ->groupBy('product_sku.product_id')
            ->distinct();
    }

    public function scopeCountActiveInStock($query, $active = false)
    {
        if ($active) {
            $query->where('products.status', 1);
        }

        return $query->select(['products.*'])->leftJoin('product_sku', 'products.id', 'product_sku.product_id')
            ->whereNotNull('product_sku.id')
            ->where('product_sku.status', ProductSKU::STATUS_ACTIVE)
            ->where('product_sku.qty', '>', 0)
            ->whereNotNull('product_sku.sku')
            ->whereNotNull('product_sku.variant')
            ->distinct();
    }

    public function scopeForSkuMinPrice($query, $price = 0)
    {
        return $query->withoutGlobalScope('skuMinPrice')->addSelect([
            'min_price_sku_id' => ProductSKU::select('id')
                ->whereColumn('product_id', 'products.id')
                ->where('price', '>=', $price)
                ->where('product_sku.status', ProductSKU::STATUS_ACTIVE)
                ->whereNotNull('sku')
                ->whereNotNull('variant')
                ->orderBy('price', 'asc')
                ->take(1)
        ]);
    }
}
