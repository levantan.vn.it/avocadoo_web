<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App;

class Role extends Model
{
    protected $table = 'roles';
    protected $fillable = ['name','permissions'];
    // protected $appends = ['renderPer,allRoles'];
    
    public function getTranslation($field = '', $lang = false){
        $lang = $lang == false ? App::getLocale() : $lang;
        $role_translation = $this->hasMany(RoleTranslation::class)->where('lang', $lang)->first();
        return $role_translation != null ? $role_translation->$field : $this->$field;
    }

    public function role_translations(){
      return $this->hasMany(RoleTranslation::class);
    }

    public function team_members() {
      return $this->hasOne(TeamMember::class,'role_id');
    }

    // public function getPermissAttribute(){
    //   $permissions = json_decode($this->permissions,true);
    //   $transper = [
    //     1 => 'Dashboard',
    //     2 => 'Users',
    //     3 => 'Orders',
    //     4 => 'Suppliers',
    //     5 => 'Products',
    //     6 => 'Categories',
    //     7 => 'Collections',
    //     8 => 'Banners',
    //     9 => 'Notifications',
    //     10 => 'Messages',
    //     11 => 'Team Members',
    //     12 => 'Settings',
    //   ];
    //   $result = [];

    //   foreach($permissions as $permission){
    //     array_push($result,$transper[$permission]);  
    //   }
    //   return $result;
    // }
    public function allRoles() {
      $roles = Role::all();
      $rls = json_decode($roles);
      return $rls;
      
    }
}
