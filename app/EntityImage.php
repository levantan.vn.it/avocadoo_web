<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EntityImage extends Model
{
    protected $table = 'entity_images';

    const IMAGE = 'image';
    const DOCUMENT = 'document';

    protected $appends = ['file_url'];
    protected $fillable = [
        'file_original_name', 'file_name', 'extension', 'type', 'file_size', 'entity_type', 'entity_id', 'zone'
    ];

    public function getFileUrlAttribute()
    {
        return my_asset($this->file_name);
    }

    public function entity()
    {
        return $this->morphTo();
    }
}
