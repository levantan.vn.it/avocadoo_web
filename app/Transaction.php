<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';
    protected $fillable = [
        'order_id',
        'user_id',
        'money',
        'method',
        'status',
        'transaction_id'
    ];

    public function moneyFunction()
    {
        return $this->money * $this->user_id;
    }

    public function statusTran()
    {
        if ($this->status == 1) {
            return __('users.success');
        } elseif ($this->status == 2) {
            return __('users.failed');
        }
        return "";
    }
    //card
    public function getCardBrand()
    {
        //visa
        $visa_regex = "/^4[0-9]{0,}$/";
        $vpreca_regex = "/^428485[0-9]{0,}$/";
        $postepay_regex = "/^(402360|402361|403035|417631|529948){0,}$/";
        $cartasi_regex = "/^(432917|432930|453998)[0-9]{0,}$/";
        $entropay_regex = "/^(406742|410162|431380|459061|533844|522093)[0-9]{0,}$/";
        $o2money_regex = "/^(422793|475743)[0-9]{0,}$/";
        // MasterCard
        $mastercard_regex = "/^(5[1-5]|222[1-9]|22[3-9]|2[3-6]|27[01]|2720)[0-9]{0,}$/";
        $maestro_regex = "/^(5[06789]|6)[0-9]{0,}$/";
        $kukuruza_regex = "/^525477[0-9]{0,}$/";
        $yunacard_regex = "/^541275[0-9]{0,}$/";
        // American Express
        $amex_regex = "/^3[47][0-9]{0,}$/";
        // Diners Club
        $diners_regex = "/^3(?:0[0-59]{1}|[689])[0-9]{0,}$/";
        //Discover
        $discover_regex = "/^(6011|65|64[4-9]|62212[6-9]|6221[3-9]|622[2-8]|6229[01]|62292[0-5])[0-9]{0,}$/";
        //JCB
        $jcb_regex = "/^(?:2131|1800|35)[0-9]{0,}$/";
        //ordering matter in detection, otherwise can give false results in rare cases
        if (preg_match($jcb_regex, $this->method)) {
            return  __('users.JCB');
        }
        if (preg_match($amex_regex, $this->method)) {
            return  __('users.american_Express');
        }
        if (preg_match($diners_regex, $this->method)) {
            return  __('users.diners_Club');
        }
        if (preg_match($visa_regex, $this->method)) {
            return  __('users.visa_Card');
        }
        if (preg_match($mastercard_regex, $this->method)) {
            return  __('users.masterCard');
        }
        if (preg_match($discover_regex, $this->method)) {
            return  __('users.discover');
        }
        if (preg_match($maestro_regex, $this->method)) {
            if ($this->method[0] == '5') { //started 5 must be mastercard
                return  __('users.masterCard');
            }
            return  __('users.maestro'); //maestro is all 60-69 which is not something else, thats why this condition in the end
        }
        return __('users.unknown'); //unknown for this system
    }

    public function getCardNumber()
    {
        return '**** **** **** ' . substr($this->method, strlen($this->method) - 4);
    }

    public function getCardIcon()
    {
        $card_brand = $this->getCardBrand();
        return static_asset('');
    }
    

}
