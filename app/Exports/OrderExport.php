<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithMapping;


class OrderExport implements FromCollection,WithHeadings,ShouldAutoSize,WithMapping
{

    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return $this->data;
    }

        /**
     * Set header columns
     *
     * @return array
     */
    public function headings(): array
    {
        return [
            '주문 ID',
            '주문일	',
            '회원 ID',
            'Email',
            '상품',
            '총 금액',
            '상태',
        ];
    }
        /**
     * get data
     *
     * @return array
     */
    public function map($orders): array
    {
        if(!empty($orders->email))
            $email=$orders->email;
        else 
            $email=optional($orders->user)->email;

        return [
            $orders->id,
            $orders->created_at->format('Y/m/d'),
            $orders->user_id,
            $email,
            optional($orders->products)->count('id'),
            optional($orders->OrderProduct)->sum('subtotal'),
            $orders->checkStatus(),
        ];
    }


}
