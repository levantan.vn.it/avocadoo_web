<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithMapping;


class CollectionsExport implements FromCollection,WithHeadings,ShouldAutoSize,WithMapping
{

    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return $this->data;
    }
        /**
     * Set header columns
     *
     * @return array
     */
    public function headings(): array
    {
        return [
            '컬렉션 ID	',
            '컬렉션 타이틀	',
            '섹션',
            '상품',
            '생성일',
            '상태',
        ];
    }
    /**
     * Mapping data
     *
     * @return array 
     */
    public function map($collection): array
    {
        return [
            $collection->id,
            $collection->name,
            $collection->sectionText(),
            count($collection->products),
            date('Y/m/d',strtotime($collection->created_at)),
            $collection->statusText(),
        ];
    }

}
