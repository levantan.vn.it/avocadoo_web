<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class BannersExport implements FromCollection,WithMapping,WithHeadings,ShouldAutoSize
{

    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return $this->data;
    }
            /**
     * Set header columns
     *
     * @return array
     */

    public function headings(): array

    {
        return [
            '배너 ID',
            '위치',
            '연결상품 또는 컬렉션',
            '시작일',
            '종료일',
            '상태',
        ];
    }

        /**
     * Mapping data
     *
     * @return array
     */
    public function map($banner): array
    {
        return [
            $banner->id,
            optional($banner)->checkPosition(),
            optional($banner)->collection_id,
            Date('Y/m/d', strtotime($banner->start_date)),
            Date('Y/m/d', strtotime($banner->end_date)),
            optional($banner)->checkStatus(),
        ];
    }
}
