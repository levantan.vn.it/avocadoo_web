<?php

use Illuminate\Database\Seeder;
use App\Category;
use App\File;
class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 5; $i++) {
            $file = File::inRandomOrder()->first();
            Category::create([
                'title'   =>  'Category '.$i,
                'type' =>  1,
                'image' =>  $file->file_name
            ]);
        }
        for ($i=5; $i < 10; $i++) {
            $category_id = Category::where('type',1)->inRandomOrder()->first();
            $file = File::inRandomOrder()->first();
            Category::create([
                'title'   =>  'Category '.$i,
                'type' =>  2,
                'parent_id' =>  $category_id->id,
                'image' =>  $file->file_name
            ]);
        }
    }
}
