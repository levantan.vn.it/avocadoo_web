<?php

use App\Product;
use App\Search;
use Illuminate\Database\Seeder;

class SaveProductNameToSearchQuery extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::select('name')->get()->each(function ($product) {
            Search::updateOrCreate([
                'query' => $product->name
            ]);
        });
    }
}
