<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableDeliveryBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_books', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('location_name',255)->nullable();
            $table->bigInteger('collection_id')->default(0)->nullable();
            $table->string('address',255)->nullable();
            $table->string('receiver',255)->nullable();
            $table->string('phone',20)->nullable();
            $table->enum('type',[0,1])->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_books');
    }
}
