<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVariantColumnToProductSkuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_sku', function (Blueprint $table) {
            $table->text('variant')->nullable();
        });

        Schema::table('carts', function (Blueprint $table) {
            $table->dropColumn('variant');
            $table->bigInteger('product_sku_id');
            $table->foreign('product_sku_id')->references('id')->on('product_sku')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('carts', function (Blueprint $table) {
            $table->text('variant')->nullable();
            $table->dropForeign(['product_sku_id']);
            $table->dropColumn('product_sku_id');
        });

        Schema::table('product_sku', function (Blueprint $table) {
            $table->dropColumn('variant');
        });
    }
}
