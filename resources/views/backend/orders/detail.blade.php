@extends('backend.layouts.app')
@section('title')
@lang('orders.order-detail')
@endsection
@section('content')
<div class="mb-4">
    <a href="{{url()->previous()}}">
        <button type="submit" class="btn-back">
            <img src="{{ static_asset('assets/img/icons/back-admin.svg') }}" class="img-search mr-2" alt="Search">
        </button>
    </a>
</div>
<div class="row m-0 ">

    <div class="hihi">
        @if (!empty($order->products[0]->images))
        @php
              $product_img = explode(',',optional($order->products[0])->images);
        @endphp
        <img src="{{ my_asset($product_img[0]) }}" class="img-ava3 mr-3" alt="{{$order->displayname}}">
        @else
        <img class="img-ava3 mr-3" src="/public/assets/img/icons/no-img.png" alt="{{ $order->displayname }}">
        @endif
    </div>
    <div class="avt-detail ml-1">
      <p class="mb-2 ">{{$order->created_at->format('Y/m/d')}}</p>
      <h5 class="mb-2 font-weight-bold">#{{$order->id}}</h5>
      <div class="process">
        @if ($order->isUnPaid())
        <label  class=" text-white br-50 bnt-active lb-status-or Unpaid-color">{{$order->checkStatus()}}</label>
        <p class="procall">. . . . . . . . . . .</p>
        <button  class="  br-50 bnt-active mark-add-reason Unpaid-color btn-status-or"><span><i class="fas fa-caret-right"></i> @lang('orders.cancel_order')</span> </button>
        @endif
        @if ($order->isProccessing())
        <label  class=" text-white br-50 bnt-active lb-status-or Processing-color">{{$order->checkStatus()}}</label>
        <p class="procall">. . . . . . . . . . .</p>
        {{-- button nhập thông tin mã vận chuyển, tên tài xế và ngày vận chuyển => status = 3  --}}
        <button  class="Processing-color br-50 bnt-active oder-infor btn-status-or"><span><i class="fas fa-caret-right"></i> @lang('orders.call_delivery')</span> </button>
        @endif
        @if ($order->isDelivering())
        <label  class=" text-white br-50 bnt-active lb-status-or Processing-color">{{$order->checkStatus()}}</label>
        <p class="procall">. . . . . . . . . . .</p>
        {{-- Button xác nhận giao hàng thành công.=> status = 4 --}}
        <button   class="Processing-color br-50 bnt-active mark-billed btn-status-or"><span><i class="fas fa-caret-right"></i> @lang('orders.delivered')</span> </button>
        @endif
        @if ($order->isCompletedDelivery())
        <label  class=" text-white br-50 bnt-active lb-status-or Processing-color">{{$order->checkStatus()}}</label>
        <p class="procall">. . . . . . . . . . .</p>
        {{-- Sẽ có nút complete order sẽ bị deactive  --}}
        <button disabled class="Processing-color br-50 bnt-active mark-billed btn-status-or"><span><i class="fas fa-caret-right"></i> @lang('orders.completed')</span> </button>
        @endif
        @if ($order->isCompleted())
          @if(!empty($order->invoice_images)||!empty($order->upload_file_pdf))
          <label  class=" text-white br-50 bnt-active lb-status-or Comp-color">@lang('orders.update-pdf-success')</label>
          @else
            <label  class=" text-white br-50 bnt-active lb-status-or Comp-color">{{$order->checkStatus()}}</label>
            <p class="procall">. . . . . . . . . . .</p>
                @if ($order->isConfirmDelivery() == 1)
                    {{-- Nếu admin xác nhận thì sẽ hiển thị send invoice --}}
                    <button  class="Comp-color br-50 bnt-active  btn-status-or clickSendVoices"><span><i class="fas fa-caret-right"></i> @lang('orders.make')</span> </button>
                @else
                    {{-- Sẽ hiển thị nút completed order khi bấm thì sẽ đổi => confirm_admin = 1 --}}
                    <button class="Comp-color br-50 bnt-active click-confirm-admin btn-status-or"><span><i class="fas fa-caret-right"></i> @lang('orders.completed')</span> </button>
                @endif
          @endif
        @endif
        @if ($order->isCancel())
        <label  class=" text-white br-50 bnt-active lb-status-or Comp-color">{{$order->checkStatus()}}</label>
        <!-- <p class="procall">. . . . . . . . . . .</p> -->
            {{-- Sẽ có nút active lại => status = 1 khi chưa payment và = 2 khi đã payment --}}
            {{-- Nếu order đã pay thì hiển thị nút refund --}}
            @if(!empty($order->transaction()->first()))
            <!-- <button  class="btn btn-info br-50 Processing-color bnt-active change-status-order-cancel btn-status-or"><span><i class="fas fa-caret-right"></i> @lang('orders.active-order')</span> </button> -->
            <button type="button" class="Comp-color ml-5 br-50 bnt-active refundConfirm btn-status-or"><span><i class="fas fa-caret-right"></i> @lang('orders.refund')</span> </button>
            @else
            {{-- <button  class="btn btn-info br-50 bnt-active Unpaid-color change-status-unpain btn-status-or"><span><i class="fas fa-caret-right"></i> @lang('orders.unpaid')</span> </button> --}}
            @endif
        @endif
        @if ($order->isDeleted())
        <label  class=" text-white br-50 bnt-active lb-status-or Comp-color">{{$order->checkStatus()}}</label>
        @endif
        @if ($order->isRefunded())
        <label  class=" text-white br-50 bnt-active lb-status-or Comp-color">{{$order->checkStatus()}}</label>
        @endif
        </div>
    </div>
    <div class="icon-lock col-md">
        <button type="button" class="btn btn-info br-10 btn-order-penban click-changeStatusOrder" ><i class="fas fa-pen "></i></button>
        @if($order->isUnPaid()||$order->isProccessing()||$order->isDelivering()||$order->isCompletedDelivery())
            <button  class="  btn btn-secondary mark-add-reason br-10 ml-1 btn-order-penban"><i class="fas fa-ban"></i></button>
            <!-- <button type="button" class="btn btn-secondary br-10 ml-1 click-change-status-cancel btn-order-penban" ><i class="fas fa-ban"></i></button> -->
        @endif
        @if($order->isCancel())
            <button type="button" class="btn btn-secondary br-10 ml-1 click-deleteOrder p-13-19" ><i class="fas fa-trash-alt"></i></button>
        @endif
    </div>
</div>
<div class="mt-1 ml-0 mr-0">
    <div class=" row ml-0 mr-0">
      <div class="col-md-3 detail-content1">
        <div class="all-content1">
          <div>
            <label class="font-weight-800">@lang('orders.od_id')</label>
            <p class="pb-1">#{{$order->id}}</p>
          </div>
          <div>
            <label class="font-weight-800"> @lang('orders.od_date')</label>
            <p class="pb-1">{{$order->created_at->format('Y/m/d')}}</p>
          </div>
          <div>
            <label class="font-weight-800">@lang('orders.us_id')</label>
            <p class="pb-1">@if($order->user_id)#{{$order->user_id}}@else @endif</p>
          </div>
          <div>
            <label class="font-weight-800">@lang('orders.user_name')</label>
            <p class="pb-1">{{$order->displayname}}</p>
          </div>
          <div>
            <label class="font-weight-800">@lang('orders.phone')</label>
            <p class="pb-1">
            @if($order->phone)
              <?php
              $phones=$order->phone;
              echo (substr($phones,-12,-8) . "-" . substr($phones,-8,-4) . "-" . substr($phones,-4))
              ?>
            @else

            @endif
            </p>
          </div>
          <div>
            <label class="font-weight-800">@lang('orders.email')</label>
            <p class="pb-1 p-tv">
              <?php
              if (empty($order->email))
                echo optional($order->user)->email;
              else
                echo optional($order)->email;
              ?>
            </p>
          </div>
          <div>
          <label class="font-weight-800">@lang('orders.delivery')</label>
          <p class="pb-1 p-tv">{{$order->delivery_address}}</p>
        </div>
        <div>
          <label class="font-weight-800">@lang('orders.note') </label>
          <p class="pb-1 p-tv">{{$order->note}}</p>
      </div>
    </div>
  </div>
  <div class="col-md-3 detail-content1">
    <div class="all-content1">
      <div>
        <label class="font-weight-800">@lang('orders.billed') </label>
        <div class="d-flex bd-highlight mb-0">
          <div class="pb-3 bd-highlight ">
            <label> @lang('orders.or_sub') </label> <br>
            <label> @lang('orders.or_fee')</label> <br>
            <label> @lang('orders.or_total')</label>
          </div>
          <div class="ml-auto pb-3 bd-highlight text-end">
            <label> {{format_price($order->sub_total)}} @lang('orders.pri')</label> <br>
            <label> {{format_price($order->delivery_fee)}} @lang('orders.pri')</label> <br>
            <label> <strong>{{format_price($order->total)}} @lang('orders.pri')</strong></label>
          </div>
        </div>
      </div>
    <div>
      <label class="font-weight-800">@lang('orders.delivery_method')</label>
      <div class="d-flex bd-highlight mb-2" >
        <div class=" bd-highlight">@lang('orders.delivery-detail') </div>
        <div class="ml-auto bd-highlight">{{$order->delivery_method}} @lang('orders.day-delivery')</div>
      </div>
      @if ($order->status==3)
      <div class="d-flex bd-highlight mb-0" >
          <div class="pb-1 bd-highlight">@lang('orders.delivery_id') </div>
          <div class="ml-auto pb-1 bd-highlight font-weight-800">
              @if(empty($order->delevery_id))
                  #{{optional($order)->delivery_id}}
              @else
              @endif
          </div>
      </div>
      @endif
      @if ($order->status==4)
      <div class="d-flex bd-highlight mb-0" >
          <div class="pb-1 bd-highlight">@lang('orders.delivery_id') </div>
          <div class="ml-auto pb-1 bd-highlight font-weight-800">
              @if(empty($order->delevery_id))
                  #{{optional($order)->delivery_id}}
              @else
              @endif
          </div>
      </div>
      <div class="all-ImgContent4  mt-1 row mr-0 ml-0">
            @if(!empty($images) && count($images))
                @foreach($images as $item)
                  @if(my_asset($item))
                  <img src="{{ my_asset($item)}}" class="img-item24 h-40 mb-2" alt="">
                  @endif
                @endforeach
            @endif
      </div>
      @endif
      @if ($order->status==5)
      <div class="d-flex bd-highlight mb-0" >
          <div class="pb-1 bd-highlight">@lang('orders.delivery_id') </div>
          <div class="ml-auto pb-1 bd-highlight font-weight-800">
              @if(empty($order->delevery_id))
                  #{{optional($order)->delivery_id}}
              @else
              @endif
          </div>
      </div>
      <div class="all-ImgContent4  mt-1 row mr-0 ml-0">
            @if(!empty($images) && count($images))
                  @foreach($images as $item)
                    @if(my_asset($item))
                    <img src="{{ my_asset($item)}}" class="img-item24 h-40 mb-2" alt="">
                    @endif
                  @endforeach
            @endif
      </div>
      @endif
    </div>
    <div>
      <label class="font-weight-800">@lang('orders.payment_method') </label>
      <div class="d-flex bd-highlight mb-0 justify-content-space-between">
          <!-- <div class="pb-2 bd-highlight"> -->
              <!-- <label> @lang('orders.or_visa')</label> <br> -->
              <label> {{optional($order->transaction)->getCardBrand()}}</label> <br>
              <label >
                  @if(!empty($order->transaction->method))
                  **** **** ****  {{ substr(optional($order->transaction)->method, -4) }}
                  @else
                  @endif
                </label>
          <!-- </div> -->
      </div>
      <div class="d-flex bd-highlight mb-0 justify-content-space-between">
          <!-- <div class="ml-auto pb-2 bd-highlight text-end" > -->
                <label> @lang('orders.or_transaction')</label>
                <label>
                  @if(!empty($order->transaction->id))
                    <strong>#{{optional($order->transaction)->id}} </strong>
                  @else
                  @endif
                </label>
          <!-- </div> -->
      </div>
    </div>
    @if($order->isCompleted())
        @if ($order->isConfirmDelivery() == 1)
          <div>
                  @php
                      $pdf_tax = explode(',',optional($order)->invoice_images);
                  @endphp
                  @if(!empty($order->invoice_images))
                    <p class="custom-pdf-title">@lang('orders.tax-invoice')</p>
                    <div class="row m-0">
                        @foreach($pdf_tax as $key=>$item)
                        <div class="mr-2">
                            <a class="custom-file-pdf" target="_blank" href="{{ my_asset($item) }}"><i class="fas fa-file-pdf"></i></a>
                        </div>
                        @endforeach
                    </div>
                  @endif
          </div>    
          <div>
                  @php
                      $pdf_delivery = explode(',',optional($order)->	upload_file_pdf);
                  @endphp
                  @if(!empty($order->	upload_file_pdf))
                    <p class="custom-pdf-title">@lang('orders.shipping')</p>
                    <div class="row m-0">
                        @foreach($pdf_delivery as $key=>$item)
                        <div class="mr-2">
                            <a class="custom-file-pdf" target="_blank" href="{{ my_asset($item) }}"><i class="fas fa-file-pdf"></i></a>
                        </div>
                        @endforeach
                    </div>
                  @endif
          </div>
        @endif    
    @endif
  </div>
</div>
  <div class="col-md detail-content2 ">
    <div class="all-content1">
      <div>
        <p class="font-weight-800">@lang('orders.product') </p>
          <div class="">
          @if(count($order->OrderProduct) && !empty($order->OrderProduct))
           @foreach ($order->OrderProduct as $item)
                <div class="d-flex bd-highlight h-70px">
                  <div class="bd-highlight">
                  @if (!empty($item->product->images))
                  @php
                    $pro_img = explode(',',optional($item->product)->images);
                  @endphp
                  <img src="{{ my_asset($pro_img[0]) }}" class="img-table mr-2" alt="{{$item->product->displayname}}">
                  @else
                  <img class="img-table mr-2" src="/public/assets/img/icons/no-img.png" alt="{{ $item->product->displayname }}">
                  @endif
                  </div>
                  <div class="bd-highlight">
                      <label class="m-0">
                        <td>SKU: <b>{{ $item->sku }}</b> </td>
                      </label> <br>
                      <label class="m-0"> {{optional($item->product)->name}} </label> <br>
                      <!-- <label class="m-0"> @if($item->size){{$item->size}} Box @else @endif </label> -->
                      <label class="m-0"> @if($item->productSku){{format_price(optional($item->productSku)->price)}} @lang('orders.pri')/{{optional($item->productSku)->weight}}kg  Box @else @endif </label>

                  </div>
                  <div class="ml-auto pt-2 bd-highlight text-end">
                      <label class="pt-2 mb-0">@lang('orders.quantity'): <b>x {{optional($item)->qty}}</b></label> <br>
                      <label >@lang('orders.sub-total'):<strong> {{format_price(optional($item)->subtotal)}} @lang('orders.pri') </strong></label>
                  </div>
              </div>
            @endforeach
          @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('modal')
  @include('templates.popup.order-information-popup')
  @include('templates.popup.billed-popup')
  @include('templates.popup.order-reason-cancel')
  @include('templates.popup.send-invoices')
  @include('templates.popup.order-edit-info')
@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript">

      // $(document).on('click', '.click-confirm-admin', function() {
      //       var delete_id = {{$order->id}};
      //       Swal.fire({
      //       title: "{{__('orders.cancel-order-status')}}",
      //       text: "{{__('notifications.confirm_popup')}}",
      //       // icon: 'error',
      //       confirmButtonText: "{{__('notifications.yes')}}",
      //       cancelButtonText: "{{__('notifications.no')}}",
      //       showCancelButton: true,
      //       showCloseButton: true,

      //   }).then((result) => {
      //       if (result.isConfirmed) {
      //           var data = {
      //               "_token": "{{ csrf_token() }}",
      //               "id": delete_id,
      //           };
      //           $.ajax({
      //               method: "POST",
      //               url: '{{route ("orders.changestatusAdminConfirm",0)}}' + delete_id,
      //               data: data,
      //               success: function(response) {
      //                   /* Read more about isConfirmed, isDenied below */
      //                   // Swal.fire('저장!', '', 'success').then(() => {
      //                       location.reload();
      //                   // });
      //               },
      //               error: function(err) {
      //                   // Swal.fire('변경 내용이 저장되지 않음', '', 'info');
      //               }
      //           });
      //       }
      //   });
      // });
      
      $(document).on('click', '.change-status-unpain', function() {
          Swal.fire({
            title: "{{__('orders.change-status-orders')}}",
            text: "{{__('notifications.confirm_popup')}}",
            // icon: 'error',
            confirmButtonText: "{{__('notifications.yes')}}",
            cancelButtonText: "{{__('notifications.no')}}",
            showCancelButton: true,
            showCloseButton: true,

          }).then((result) => {
              if (result.isConfirmed) {
                  $.post('{{ route("orders.changeStatusUnpaid",$order) }}').then(() => {
                          location.reload();
                  }).catch((e) => {
                      console.log('ee: ', e)
                  })
              };
          });
      });
      
      $(document).on('click', '.click-confirm-admin', function() {
          Swal.fire({
            title: "{{__('orders.confirm-admin')}}",
            text: "{{__('notifications.confirm_popup')}}",
            // icon: 'error',
            confirmButtonText: "{{__('notifications.yes')}}",
            cancelButtonText: "{{__('notifications.no')}}",
            showCancelButton: true,
            showCloseButton: true,

          }).then((result) => {
              if (result.isConfirmed) {
                  $.post('{{ route("orders.changestatusAdminConfirm",$order) }}').then(() => {
                      // Swal.fire('저장!', '', 'success').then(() => {
                          location.reload();
                      // });
                  }).catch((e) => {
                      console.log('ee: ', e)
                  })
              };
          });
      });
      $('.refundConfirm').on('click',function(){
        Swal.fire({
            title: '{{__('orders.refund')}}',
            text: '{{__('orders.title_confirm_refund')}}',
            // icon: 'error',
            confirmButtonText: '예',
            cancelButtonText: '아니오',
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": {{$order->id}},
                };
                $.ajax({
                    method: "POST",
                    url: '{{route ("orders.refund")}}',
                    data: data,
                    success: function(response) {
                        Swal.fire(response.message, '', 'success');
                        location.reload();
                    },
                    error: function(err) {
                        jsonValue = jQuery.parseJSON( err.responseText );
                        Swal.fire(jsonValue.message, '', 'error');
                    }
                });
            };
        });
    });
      //add image popup send voices
      $(document).on('click','.clickSendVoices',function(){
          $('.send-invoices').addClass('show');
      });
      $(document).on('click', '.click-changeDelivered', function() {
          Swal.fire({
            title: "{{__('orders.change-delivery-success')}}",
            text: "{{__('notifications.confirm_popup')}}",
            // icon: 'error',
            confirmButtonText: "{{__('notifications.yes')}}",
            cancelButtonText: "{{__('notifications.no')}}",
            showCancelButton: true,
            showCloseButton: true,

          }).then((result) => {
              if (result.isConfirmed) {
                  $.post('{{ route("orders.changeStatusOrder",$order) }}').then(() => {
                      // Swal.fire('저장!', '', 'success').then(() => {
                          location.reload();
                      // });
                  }).catch((e) => {
                      console.log('ee: ', e)
                  })
              };
          });
      });



      $(document).on('click', '.change-status-order-cancel', function() {
          Swal.fire({
            title: "{{__('orders.back-order')}}",
            text: "{{__('notifications.confirm_popup')}}",
            // icon: 'error',
            confirmButtonText: "{{__('notifications.yes')}}",
            cancelButtonText: "{{__('notifications.no')}}",
            showCancelButton: true,
            showCloseButton: true,

          }).then((result) => {
              if (result.isConfirmed) {
                  $.post('{{ route("orders.changeStatusOrder",$order) }}').then(() => {
                      // Swal.fire('저장!', '', 'success').then(() => {
                          location.reload();
                      // });
                  }).catch((e) => {
                      console.log('ee: ', e)
                  })
              };
          });
      });
      //delete product use sweetalert2
      $(document).on('click', '.click-deleteOrder', function() {
            var delete_id = {{$order->id}};
            Swal.fire({
              title: "{{__('orders.change-status-orders')}}",
            text: "{{__('notifications.confirm_popup')}}",
            // icon: 'error',
            confirmButtonText: "{{__('notifications.yes')}}",
            cancelButtonText: "{{__('notifications.no')}}",
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": delete_id,
                };
                $.ajax({
                    method: "POST",
                    url: '{{route ("orders.changeStatusCancel",0)}}' + delete_id,
                    data: data,
                    success: function(response) {
                        /* Read more about isConfirmed, isDenied below */
                        // Swal.fire('저장!', '', 'success').then(() => {
                            location.reload();
                        // });
                    },
                    error: function(err) {
                        // Swal.fire('변경 내용이 저장되지 않음', '', 'info');
                    }
                });
            }
        });
      });

      $(document).on('click','.clickAddProduct',function(){
        
      })

      $(document).on('click', '.click-change-status-cancel', function() {
            var delete_id = {{$order->id}};
            Swal.fire({
              title: "{{__('orders.cancel-order-status')}}",
            text: "{{__('notifications.confirm_popup')}}",
            // icon: 'error',
            confirmButtonText: "{{__('notifications.yes')}}",
            cancelButtonText: "{{__('notifications.no')}}",
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": delete_id,
                };
                $.ajax({
                    method: "POST",
                    url: '{{route ("orders.changeStatusAllCancel",0)}}' + delete_id,
                    data: data,
                    success: function(response) {
                        /* Read more about isConfirmed, isDenied below */
                        // Swal.fire('저장!', '', 'success').then(() => {
                            location.reload();
                        // });
                    },
                    error: function(err) {
                        // Swal.fire('변경 내용이 저장되지 않음', '', 'info');
                    }
                });
            }
        });
      });



        // $('.img-ava2').click(
        //   o(){
        //     $('div .img-ava2').addClass('test')
        //     console.log('fa')
        // });
        $('.oder-infor').click(function() {
            let url = $(this).attr('href');
            $('.mark-oder-infors').addClass('show');
        });
        $('.click-changeStatusOrder').click(function(){
            $('.click-change-status-order').addClass('show');
        })
        $('.mark-add-reason').click(function(){
            let url = $(this).attr('href');
            $('.mark-add-reason-cancel').addClass('show');
        });
        $('.mark-billed').click(function() {
            let url = $(this).attr('href');
            $('.mark-billeds').addClass('show');
        });


        //Show image

        // $(function() {
        //     $(document).on("change",".uploadFile", function()
        //     {
        //         var uploadFile = $(this);
        //         var files = !!this.files ? this.files : [];
        //         if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
        //         if (/^image/.test( files[0].type)){ // only image file
        //             var reader = new FileReader(); // instance of the FileReader
        //                 reader.readAsDataURL(files[0]); // read the local file

        //                 reader.onloadend = function(){ // set image data as background of div
        //                     //alert(uploadFile.closest(".upimage").find('.imagePreview').length);
        //     uploadFile.closest(".imgUp").find('.imgPreview').css("background-image", "url("+this.result+")");
        //         }
        //     }
        //   });
        // });
        @if ($errors->has('shipper_name')||$errors->has('shipper_phone')||$errors->has('delivery_date'))
            $('.oder-infor').click();
        @endif

        @if($errors->has('displayname')||$errors->has('phone')||$errors->has('email')||$errors->has('store_name')||$errors->has('delivery_address'))
            $('.click-changeStatusOrder').click();
        @endif

        @if($errors->has('reason_cancel'))
          $('.mark-add-reason').click();
        @endif

        //Date picker popup infomation
        $('#click_dateInfomation').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        autoUpdateInput: false,
        drops:'up',
        minDay:"2000/01/01",
        locale: {
            format: 'YYYY/MM/DD',
            applyLabel: "대다",
            cancelLabel: "취소",
                "monthNames":[
                    "일월" , // Tháng 1
                    "이월" , // Tháng 2
                    "삼월" , // Tháng 3
                    "사월" , // Tháng 4
                    "오월" , // Tháng 5
                    "유월" , // Tháng 6
                    "칠월" , // Tháng 7
                    "팔월" , // Tháng 8
                    "구월" , // Tháng 9
                    "시월" , // Tháng 10
                    "십일월" , // Tháng 11
                    "십이월" , // Tháng 12
                ],
                daysOfWeek:[
                    "월요일" ,// Thứ 2
                    "화요일" ,// Thứ 3
                    "수요일" ,// Thứ 4
                    "목요일" ,// Thứ 5
                    "금요일" ,// Thứ 6
                    "토요일" ,// Thứ 7
                    "일요일" ,//  Chủ nhật.
                ]
        }
        });

        $('#click_dateInfomation').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('YYYY/MM/DD'));
        });

        $('#click_dateInfomation').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });
</script>

@endsection
