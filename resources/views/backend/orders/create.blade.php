@extends('backend.layouts.app')
@section('title')
@lang('orders.create_or')
@endsection
@section('css')
<link href="{{ static_asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
@endsection
@section('content')

<div class="mb-4">
    <a href="{{route('orders.index')}}">
        <button type="submit" class="btn-back">
            <img src="{{ static_asset('assets/img/icons/back-admin.svg') }}" class="img-search mr-2" alt="Search">
        </button>
    </a>
</div>
<div class="row m-0 ">
   <div class="icon-lock col-md">
    <!-- <button type="submit" form="create" class="btn-info btn click-orderCre ">@lang('orders.create-order')</button> -->
    <a href="javascript:void(0)" data-action="{{route('orders.validatedOrder')}}" class="btn-info btn btn-create-order-now">@lang('orders.create-order')</a>
   </div>
</div>
<div class="create-collection row mt-3">
    <div class="col-md-3 mb-3 clickAddInfoOrder">
        <button class="btn-create-collection mark-add-order">
            <i class="fas fas-create-collection fa-plus-circle img-redo mr-2"></i>
            <p class="mt-3">@lang('orders.add-info')</p>
        </button>
    </div>
    <div class="col-md-3 mb-3 d-none clickDoneInfoOrder">
        <a href="javascript:void(0)" class="content1-img clickEditInfoOrder">
            <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail2 alt="Search">
        </a>
        <button class="btn-create-collection ">
            <i class="text-success fas fas-create-collection fa-check img-redo mr-2"></i>
            <p class="mt-3">@lang('banners.add-info')</p>
        </button>
    </div>
    <div class="col-md-3 mb-3 clickAddDeliveryOrder">
        <button class="btn-create-collection mark-add-delivery">
            <i class="fas fas-create-collection fa-plus-circle img-redo mr-2"></i>
            <p class="mt-3">@lang('orders.add-delivery')</p>
        </button>
    </div>
    <div class="col-md-3 mb-3 d-none clickDoneDeliveryOrder">
        <a href="javascript:void(0)" class="content1-img clickEditDeliveryOrder">
            <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail2 alt="Search">
        </a>
        <button class="btn-create-collection ">
            <i class="text-success fas fas-create-collection fa-check img-redo mr-2"></i>
            <p class="mt-3">@lang('banners.add-info')</p>
        </button>
    </div>
    <div class="col-md mb-3 clickAddProductOrder">
        <button class="btn-create-collection edit-product-collect" data-action="{{route('orders.create')}}">
            <i class="fas fas-create-collection fa-plus-circle img-redo mr-2"></i>
            <p class="mt-3">@lang('orders.add-products')</p>
        </button>
    </div>
    <div class="col-md mb-3 d-none clickDoneProductOrder">
        <a href="javascript:void(0)" class="content1-img clickEditProductOrder">
            <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail2 alt="Search">
        </a>
        <button class="btn-create-collection ">
            <i class="text-success fas fas-create-collection fa-check img-redo mr-2"></i>
            <p class="mt-3">@lang('banners.add-info')</p>

        </button>
    </div>
</div>
@endsection
@section('modal')
    <form action="{{route('orders.store')}}" class="click-createOrder" method="post" name="create">
        @csrf
        @include('templates.popup.order-add-popup')
        @include('templates.popup.order-add-delivery')
        @include('templates.popup.order-add-products')
    </form>
@endsection
@section('script')
<script src="{{ static_asset('plugins/select2/js/select2.min.js') }}" ></script>
<script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript">

        // //change value delivery_address
        // $(document).on('change','#delivery_address',function(){
        //     $('.get-value-address').val($(this).val());
        //     console.log($(this).val());
        // })

        // validate add product order
        $(document).on('click','.btn-create-order-now',function(){
            var url = $(this).data('action');
            console.log(url);
            const parent= $(this).parents('body').find('.click-createOrder');
            $.ajax({
                url:url,
                data: parent.serialize(),
                type:"POST",
                statusCode:{
                    422: ({responseJSON})=>{
                        parent.find('.error').remove();
                        const tr = Object.entries(responseJSON.errors);
                        console.log(tr);
                        const arr_info = ['user_id','phone','displayname','receiver','delivery_address'];
                        const arr_delivery=['delivery_fee','delivery_method'];
                        const arr_pro=['id'];
                        for (const [key,val] of tr) {
                                    if(arr_info.includes(key)) {
                                        $('.mark-add-orders').addClass('show');
                                    }
                                    if(arr_delivery.includes(key)) {
                                        $('.mark-add-deliveries').addClass('show');
                                    }
                                    if(arr_pro.includes(key)) {
                                        $('.order-add-products').addClass('show');
                                    }
                                    // if(arr_product.includes(key)) {
                                    //     $('.edit-product-collection').addClass('show');
                                    // }
                                    if(key == 'delivery_method') {
                                    parent.find(`[name="${key}"]`).parents('.dropdown').after(`
                                    <div class="error">${val}</div>
                                    `)
                                    }else if(key == 'delivery_address') {
                                            parent.find(`[name="${key}"]`).parent().children('.dropdown').after(`
                                            <div class="error">${val}</div>
                                    `)
                                    }else if(key == 'user_id') {
                                            parent.find(`[name="${key}"]`).parents('.dropdown').after(`
                                            <div class="error">${val}</div>
                                    `)
                                    // }else if(key == 'id') {
                                    //         parent.find(`[name="${key}"]`).parents('.dropdown').after(`
                                    //         <div class="error">${val}</div>
                                    // `)
                                    }else {
                                        parent.find(`[name="${key}"]`).after(`
                                        <div class="error">${val}</div>
                                    `)
                                    }
                                    
                                }
                  
                    }

                },
                success : function(res) {
                     $('.click-createOrder').submit();
                },
                error : function(err) {
                    console.log(err)
                }
            });
        })

        $(document).on('click','.clickAddProduct',function(){
            $('p.validate-product.error.mt-1').remove();
            let count = $(this).parents('.all-product').children('#product-list').children().children().children().children('.select-input:checked').length;
            console.log(count);
            if(count<1){
                $('#product-list').append(`<p class='validate-product error mt-1'>@lang('orders.validate-product')</p>`)
            }
            else {
                $('.clickDoneProductOrder').removeClass('d-none');
                $('.clickAddProductOrder').addClass('d-none');
                $('.order-add-products').removeClass('show');
                $('.validate-product').remove();
            }
        })

        $('#collection_id option:first').html("선택 안됨");
        //validate ajax
        $(document).on('click','.click-validate',function(){
            var url = $(this).data('action');
            console.log(url);
            var user_id = $('#order_userId3').val();
            var delivery_address = $('#delivery_address').val();
            var displayname = $('#order-userName').val();
            var receiver=$('#receiver').val();
            var phone=$('#phone').val();
            data = {
                "token" : "{{csrf_token()}}",
                "user_id": user_id,
                "delivery_address": delivery_address,
                "displayname" : displayname,
                "receiver" : receiver,
                "phone": phone,
            };
            $.ajax({
                url: url,
                data: data,
                type: "POST",
                beforeSend: function(){
                    $('.mark-add-orders .error').remove();
                },
                success: function(res){
                    $('.mark-add-orders').removeClass('show');
                    $('.mark-add-orders .error').hide();
                    document.querySelector('.mark-add-orders').classList.remove('show');
                    $('.clickAddInfoOrder').addClass('d-none');
                    $('.clickDoneInfoOrder').removeClass('d-none');

                },
                error : function(err) {
                    // var response= JSON.parse(err.responseText);
                    // showErrorMsg(response.errors);
                    const {errors} = err.responseJSON;
                    console.log(err);
                    Object.keys(errors).forEach(item=>{
                        let errorElement = $(` <p class="error mt-1"> ${errors[item]}</p>`);
                        $(`[name="${item}"]`).parent().append(errorElement);
                    });
                }
            })

        });

        // $(document).on('click','.clickAddProduct',function(){
        //     var url = $(this).data('action');
        //     var id = $('.select-input').val();
        //     console.log(url);
        //     data = {
        //         "token" : "{{csrf_token()}}",
        //         "id.*": id,
        //     };
        //     $.ajax({
        //         url: url,
        //         data: data,
        //         type: "POST",
        //         beforeSend: function(){
        //             $('.order-add-products .error').remove();
        //         },
        //         success: function(res){
        //             $('.order-add-products').removeClass('show');
        //             // $('.error').hide();
        //             // document.querySelector('.order-add-products').classList.remove('show');
        //             // $('.clickAddValidateDetail').addClass('d-none');
        //             // $('.clickDoneValidateDetail').removeClass('d-none');
        //         },
        //         error : function(err) {
        //             // var response= JSON.parse(err.responseText);
        //             // showErrorMsg(response.errors);
        //             const {errors} = err.responseJSON;
        //             console.log(errors);
        //             Object.keys(errors).forEach(item=>{
        //                 let errorElement = $(` <p class="error mt-1 ml-2"> ${errors[item]}</p>`);
        //                 if(item.includes('.')){
        //                     let newArr = item.split('.');
        //                     newArr = newArr.map( (item,index) => index ? `[${item}]`: item);
        //                     item = newArr.join('');
        //                 }
        //                 $(`[name="${item}"]`).parent().append(errorElement);

        //             });
        //         }
        //     })

        // });

        // data = $('#product-list :checkbox:checked').val();
            

        $(document).on('click','.click-validateDelivery',function(){
            var url = $(this).data('action');
            var delivery_fee = $('#order_delivery_fee').val();
            var delivery_method = $('#delivery_method').val();

            data = {
                "token" : "{{csrf_token()}}",
                "delivery_fee": delivery_fee,
                "delivery_method": delivery_method,
        
            };
            $.ajax({
                url: url,
                data: data,
                type: "POST",
                beforeSend: function(){
                    $('.mark-add-deliveries .error').remove();
                },
                success: function(res){
                    $('.mark-add-deliveries').removeClass('show');
                    $('.mark-add-deliveries .error').hide();
                    document.querySelector('.mark-add-deliveries').classList.remove('show');
                    $('.clickAddDeliveryOrder').addClass('d-none');
                    $('.clickDoneDeliveryOrder').removeClass('d-none');

                },
                error : function(err) {
                    // var response= JSON.parse(err.responseText);
                    // showErrorMsg(response.errors);
                    const {errors} = err.responseJSON;
                    console.log(err);
                    Object.keys(errors).forEach(item=>{
                        let errorElement = $(` <p class="error mt-1"> ${errors[item]}</p>`);
                        $(`[name="${item}"]`).parent().append(errorElement);
                    });
                }
            })
        });

        $(document).on('click','.clickEditInfoOrder',function(){
            $('.mark-add-orders').addClass('show');
        });

        $(document).on('click','.click-orderCre',function(){
            $('.click-createOrder').submit();
        });

        $(document).on('click','.clickEditDeliveryOrder',function(){
            $('.mark-add-deliveries').addClass('show');
        });

        // $(document).on('click','.clickAddProduct',function(){
        //     $('.clickDoneProductOrder').removeClass('d-none');
        //     $('.clickAddProductOrder').addClass('d-none');
        //     $('.order-add-products').removeClass('show');
        // });

        $(document).on('click','.clickEditProductOrder',function(){
            $('.order-add-products').addClass('show');
        });

        $('#delivery_method').change(function(){
            $('#order_delivery_fee').val($(this).find('option:selected').data('title'))
            console.log($(this).find('option:selected'));
        });

        $('.mark-add-order').click(function() {
            $('.mark-add-orders').addClass('show');
        });

        $('.mark-add-delivery').click(function() {
            let url = $(this).attr('href');
            $('.mark-add-deliveries').addClass('show');
        });

        $('.edit-product-collect').click(function() {
            let url = $(this).attr('href');
            $('.order-add-products').addClass('show');
        });

        $(document).on('click','.btn-search',function(){
            // $(this).parents('.modal-product').find('.all-product').removeClass('d-none');
            const url = $(this).data('action');
            const val = $('#search-product').val();
            console.log(url,val);
            $.get(url+'?search='+val,function(data){
                console.log(data);
                $('.all-product').html(data)
            })
        });
        // $(document).on('click','.dropdown-menu li',function(){
        //     console.log('test');
        // });
        // //select2
        $(function(){
            $('#order_userId3').select2({      
                language: {
                    searching: function() {
                        return "";
                    }
                },
                ajax: {
                    url: '',
                    dataType: 'json',
                    processResults: function (data) {
                        let data1 = {
                            results: data.data.map((item)=>{
                                return {
                                id: item.id,
                                text: '#'+item.id,
                            
                                }
                            })
                        };
                        return data1;
                    },
                },
                type:'GET',
                });
                let deliveryBooks = [];
                $(document).on('change','#order_userId3', function(){

                    const id = this.value;
                    console.log(id);
                    if(id){
                        $.get("{{ route('orders.create',['user_id'=>0])}}" + id).then(function(data){
                            $('#order-userName').val(data.displayname)
                            deliveryBooks = data.delivery_books ?? [];
                            let options = (deliveryBooks ?? []).map(({id, address})=>(
                                `<option class="click-get-value"  value="${id}">${address}</option>`
                            ))
                            $('#delivery_address').html(options);
                            deliveryBookPrimary = deliveryBooks.find((data)=>data.type ==1) ?? deliveryBooks[0] ?? {};
                            $('#delivery_address').val(deliveryBookPrimary.id).change();
                        })
                        $.get("{{route('orders.create',['user_id'=>0])}}"+id).then(function(data){
                            $('#order-phoneNumber').val(data.phone)
                        })
                        $.get("{{route('orders.create',['user_id'=>0])}}"+id).then(function(data){
                            $('#delivery-bookRec').val(data.receiver)
                        })
                    }

                });
                $('#delivery_address').change(function(){
                    
                    delivery = deliveryBooks.find((data)=>data.id == this.value) ?? {};
                    $('.get-value-address').val(delivery.address);
                    console.log((delivery.address));
                    $('#receiver').val(delivery.receiver);
                    $('#phone').val(delivery.phone);
                })
                // $(document).on('change','#',function(){
                //     const id = this.value;
                //     if(id){
                //         $.get("{{route('orders.create',['user_id'=>0])}}"+id).then(function(data){
                //             $('#order-phoneNumber').val(data.phone)
                //         })
                //     }
                // })


        });






        //Show image
        $(".imgAdd").click(function(){
            $(this).closest(".row").find('.imgAdd').before('<div class="col-sm-2 imgUp"><div class="imagePreviews"></div><label class="btn btn-add-image-supplier">Upload<input type="file" class="uploadFile img" value="Upload Photo" style="width:0px;height:0px;overflow:hidden;"></label><i class="fa fa-times del"></i></div>');
            });
            $(document).on("click", "i.del" , function() {
                $(this).parent().remove();
        });

        $(function() {
            $(document).on("change",".uploadFile", function()
            {
                var uploadFile = $(this);
                var files = !!this.files ? this.files : [];
                if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

                if (/^image/.test( files[0].type)){ // only image file
                    var reader = new FileReader(); // instance of the FileReader
                        reader.readAsDataURL(files[0]); // read the local file

                        reader.onloadend = function(){ // set image data as background of div
                            //alert(uploadFile.closest(".upimage").find('.imagePreview').length);
            uploadFile.closest(".imgUp").find('.imagePreviews').css("background-image", "url("+this.result+")");
                }
            }
        });
        });     
        
        @if($errors->has('delivery_method')||$errors->has('displayname')||$errors->has('receiver')||$errors->has('phone'))
            $('.mark-add-order').click();
        @endif
        @if($errors->has('delivery_method')||$errors->has('delivery_fee'))
            $('.mark-add-delivery').click();
        @endif
        @if($errors->has('id'))
            $('.edit-product-collect').click();
        @endif



</script>

@endsection


