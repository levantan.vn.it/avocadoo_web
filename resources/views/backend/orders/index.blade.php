@extends('backend.layouts.app')
@section('title')
@lang('orders.order')
@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection

@section('content')

<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="align-items-center">
        <h1 class="h3"><strong>@lang('orders.order')</strong></h1>
	</div>
</div>
<form class=""  action="" method="GET">
    <div class="row mb-2">
        <div class="col-md-12 d-flex res-FormControl">
            <img src="{{ static_asset('assets/img/icons/search.svg') }}" class="img-search3 mr-2" alt="Search">
            <input type="text" class="form-control res-placeholder" id="search3" name="search" value="{{request()->get('search')}}"  placeholder="@lang('orders.search_id')">
        </div>
        {{--<div class="col-md-3 text-md-right res-FormControl">
            <a href="{{ route('orders.create') }}" class="btn btn-circle btn-info btn-add-product d-flex">
                <!-- <img src="{{ static_asset('assets/img/icons/add.png') }}" class="img-search2 mr-2" alt="Search"> -->
                <i class="fas fa-plus-circle img-redo mr-2"></i>
                <span class="custom-FontSize">@lang('orders.add')</span>
            </a>
        </div>--}}
    </div>
    <div class="row gutters-5 mb-5">
        <div class="col-md-4">
            {{-- <input type="text" onfocus="(this.type='date')" class="form-control custom-placeholder" id="joined_date38" name="date" value="{{ request('date') }}" placeholder="@lang('products.add-date')"> --}}
            {{-- <input type="text" onfocus="(this.type='date')" class="form-control custom-placeholder" id="joined_date" name="created_at"  value="{{ request('joined_date') }}" placeholder="@lang('orders.od_date')"> --}}
            <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' autocomplete="off" class="form-control custom-placeholder res-FormControl"  name="created_at" id="clickDate" placeholder="@lang('orders.od_date')" value="{{ (request()->get('created_at')) }}">
            <div class="custom-down"><i class="fas fa-chevron-down"></i></div>
        </div>
        <div class="col-md-4 res-status">
            <select class="form-control form-control-sm aiz-selectpicker res-FormControl mb-md-0 font-weight-800" id="status" name="status">
                <option value="">@lang('orders.od_state')</option>
                <option value="1" @if (request('status') == (1)) selected @endif>@lang('orders.unpaid')</option>
                <option value="2" @if (request('status') == (2)) selected @endif>@lang('orders.processing')</option>
                <option value="3" @if (request('status') == (3)) selected @endif>@lang('orders.delivering')</option>
                <option value="4" @if (request('status') == (4)) selected @endif>@lang('orders.delivered')</option>
                <option value="5" @if (request('status') == (5)) selected @endif>@lang('orders.completed')</option>
                <option value="6" @if (request('status') == (6)) selected @endif>@lang('orders.cancel_order')</option>
                <option value="7" @if (request('status') == (7)) selected @endif>@lang('orders.delete-status')</option>
            </select>
        </div>
        <div class="col-md all-btn-search">
            <button type="submit" class="pl-0 pr-0 btn btn-info w-32 mr-2 d-flex  btn-responsive justify-content-center">
                <i class="fas fa-search img-redo mr-2"></i>
                <span class="custom-FontSize">@lang('orders.search')</span>
            </button>
            <a href="{{ route('orders.index') }}" class="pl-0 pr-0 w-32 btn btn-info ml-2 mr-2 d-flex btn-responsive justify-content-center">
                <i class="fas fa-redo-alt img-redo mr-2"></i>
                <span class="custom-FontSize">@lang('orders.reset')</span>
            </a>
            <?php
                $request = request()->all();
                $newRequest = http_build_query($request);
            ?>
            <a href="{{ route('orders.export') . '?' . $newRequest }}" class="pl-0 pr-0 btn btn-info w-34 ml-2 d-flex  btn-responsive justify-content-center">
                <i class="fas fa-cloud-download-alt img-redo mr-2"></i>
                <span class="custom-FontSize">@lang('orders.excel')</span>
            </a>
        </div>
    </div>

    <div class="card">
        <div class="card-body-table ">
            <table class="table aiz-table mb-0 table-hover">
                <thead>
                    <tr >
                        <th>@lang('orders.od_id')</th>
                        <th>@lang('orders.od_date')</th>
                        <th>@lang('orders.us_id')</th>
                        <th>@lang('orders.email')</th>
                        <th>@lang('orders.product')</th>
                        <th>@lang('orders.price')</th>
                        <th>@lang('orders.od_state')</th>
                        <th class="text-right">@lang('')</th>
                    </tr>
                </thead>
                <tbody> 
                @if(count($orders ?? []))
                @foreach($orders as $item)
                    <tr >
                        <th>#{{$item->id}}</th>
                        <td class="align-middle">{{$item->created_at->format('Y/m/d')}}</td>
                        <td class="align-middle">#{{$item->user_id}}</td>
                        <td class="align-middle">@if(!empty($item->email)) {{$item->email}} @else{{optional($item->user)->email}}@endif</td>
                        <td class="align-middle">{{optional($item->products)->count('id')}} @lang('orders.product')</td>
                        <td class="align-middle">{{format_price(optional($item->OrderProduct)->sum('subtotal'))}} @lang('orders.pri')</td>
                        <td class="align-middle">{{$item->checkStatus()}}</td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-drop" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-h"></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="{{route('orders.detail', ['id'=>$item->id])}}">@lang('orders.view')</a>
                                <!-- <a class="dropdown-item" href="#">@lang('orders.active')</a> -->
                                <a class="dropdown-item click-delete" data-id="{{$item->id}}" href="#">@lang('orders.del')</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
    <div class="aiz-pagination paginationOrder">
        {{ $orders->appends(request()->input())->links() }}
    </div> 
</form>


<div class="modal fade" id="confirm-ban">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title h6">{{translate('Confirmation')}}</h5>
                <button type="button" class="close" data-dismiss="modal"></button>
            </div>
            <div class="modal-body">
                <p>{{translate('Do you really want to ban this Customer?')}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">{{translate('Cancel')}}</button>
                <a type="button" id="confirmation" class="btn btn-primary">{{translate('Proceed!')}}</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="confirm-unban">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title h6">{{translate('Confirmation')}}</h5>
                <button type="button" class="close" data-dismiss="modal"></button>
            </div>
            <div class="modal-body">
                <p>{{translate('Do you really want to unban this Customer?')}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">{{translate('Cancel')}}</button>
                <a type="button" id="confirmationunban" class="btn btn-primary">{{translate('Proceed!')}}</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
    @include('modals.delete_modal')
@endsection

@section('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <script type="text/javascript">
      $('.custom-drop').click(function(){
        // alert();
        $(this).find('.custom-ul').toggle('d-none');
    })
    // doubole click
    // $('[data-redirect]').on('dblclick', function() {
    //     let url = $(this).attr('data-redirect');
    //     if (url && url.length) {
    //         location.href = url;
    //     }
    // });
    $(document).ready(function() {
    //$('#container').removeClass('mainnav-lg').addClass('mainnav-sm');
    });

    function sort_customers(el){
        $('#sort_customers').submit();
    }
    function confirm_ban(url)
    {
        $('#confirm-ban').modal('show', {backdrop: 'static'});
        document.getElementById('confirmation').setAttribute('href' , url);
    }

    function confirm_unban(url)
    {
        $('#confirm-unban').modal('show', {backdrop: 'static'});
        document.getElementById('confirmationunban').setAttribute('href' , url);
    }
    // delete order
    $(document).on('click', '.click-delete', function() {
        var delete_id = $(this).attr('data-id');
        // alert(delete_id);
        Swal.fire({
            title: "{{__('orders.delete-order')}}",
            text: "{{__('notifications.confirm_popup')}}",
            // icon: 'error',
            confirmButtonText: "{{__('notifications.yes')}}",
            cancelButtonText: "{{__('notifications.no')}}",
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": delete_id,
                };
                $.ajax({
                    method: "DELETE",
                    url: '{{route ("orders.deleteOrder",0)}}' + delete_id,
                    data: data,
                    success: function(response) {
                        /* Read more about isConfirmed, isDenied below */
                        // Swal.fire('저장!', '', 'success').then(() => {
                            location.reload()
                        // });
                    },
                    error: function(err) {
                        // Swal.fire('변경 내용이 저장되지 않음', '', 'info');
                    }
                });
            }
        });
    });
     $('#clickDate').daterangepicker({
        autoUpdateInput: false,
        singleDatePicker: true,
        showDropdowns: true,
        minDate: '1921/01/01',
        locale: {
            format: 'YYYY/MM/DD',
            applyLabel: "대다",
            cancelLabel: "취소",
                "monthNames":[
                    "일월" , // Tháng 1
                    "이월" , // Tháng 2
                    "삼월" , // Tháng 3
                    "사월" , // Tháng 4
                    "오월" , // Tháng 5
                    "유월" , // Tháng 6
                    "칠월" , // Tháng 7
                    "팔월" , // Tháng 8
                    "구월" , // Tháng 9
                    "시월" , // Tháng 10
                    "십일월" , // Tháng 11
                    "십이월" , // Tháng 12
                ],
                daysOfWeek:[
                    "월요일" ,// Thứ 2
                    "화요일" ,// Thứ 3
                    "수요일" ,// Thứ 4
                    "목요일" ,// Thứ 5
                    "금요일" ,// Thứ 6
                    "토요일" ,// Thứ 7
                    "일요일" ,//  Chủ nhật.
                ]
            }
    });
    $('#clickDate').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('YYYY/MM/DD'));
    });



    $('input[name="created_at"]').val('{{ request()->get('created_at') }}');


    </script>
@endsection
