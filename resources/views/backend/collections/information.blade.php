@extends('backend.layouts.app')
@section('title')
    {{__('collections.collection_detail')}}
@endsection
@section('content')
<div class="mb-4">
    {{-- @dump($errors) --}}
        <div class="backpage mb-5">
            <a href="{{url()->previous()}}" class="back btn"><i class="fas fa-chevron-left"></i></a>
        </div>
</div>
@php
@endphp
<div class="header-collection">
    <div class="row m-0">
        <div>
            @if(!empty($data->main_image))
                <img class="img-ava2 mr-2" src="{{ static_asset($data->main_image) }}" alt="">
            @else
                @if($data->images)
                @php
                    $avatar = explode(',',$data->images);
                @endphp
                <img src="{{ static_asset($avatar[0]) }}" class="img-ava2 mr-2" alt="Search">
                @else
                <img src="{{ static_asset('assets/img/placeholder.jpg') }}" class="img-ava2 mr-2" alt="Search">
                @endif
            @endif    
    
        </div>
        <div class="ml-3">
            <p class="mb-2 font-weight-800">#{{$data->id}}</p>
            <p class="mb-2 font-weight-800">{{$data->name}}</p>
            <label class="label-status {{$data->status == 1 ? 'active' : 'inactive'}}">{{$data->statusText()}}</label>
        </div>
    </div>
    <div class="all-BtnReset d-flex">
        <form action="{{ route('collections.status',$data->id)}}" method="POST" class="mr-2">
            @csrf
            <input type="hidden" id="status" name="status" value="{{ optional($data)->status == 1 ? 2 : 1}}">
            {{-- <button type="button" title="@lang('members.unlock')" class="btn btn-info btn-unlock" data-id="{{optional($data)->id}}"><i class="fas fa-unlock-alt"></i></i></i></button> --}}
            <a href="javascript:void(0)" title="{{optional($data)->status == 1 ? __('members.lock') : __('members.unlock')}}" 
                class="btn btn-info btn-lock change-status" data-id="{{optional($data)->id}}">
                <i class="{{optional($data)->status == 1 ? "fas fa-lock img-redo" : "fas fa-unlock-alt img-redo"}} "></i></a>
        </form>
        <a href="javascript:void(0)"  class="btn btn-info delete-collection" data-id="{{optional($data)->id}}"><i class="fas fa-trash-alt"></i></a>
    </div>
</div>
<div class="mt-3 row ml-0 mr-0">
    <div class="col-md-3 detail-content1">
        <a href="javascript:void(0)" class="content1-img edit-info-collect">
            <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail" alt="">
        </a>
        <div class="all-content1">
            <div>
                <p class="font-weight-800">@lang('collections.id')</p>
                <p>#{{$data->id}}</p>
            </div>
            <div>
                <p class="font-weight-800">@lang('collections.title')</p>
                <p>{{$data->name}}</p>
            </div>
            <div>
                <p class="font-weight-800">@lang('collections.sort_descript')</p>
                <p>{{optional($data)->short_description}}</p>
            </div>
            <div>
                <p class="font-weight-800">@lang('collections.section')</p>
                <p>{{$data->sectionText()}}</p>
            </div>
            <div>
                <p class="font-weight-800">@lang('collections.products')</p>
                <p>{{count($data->products)}} @lang('collections.products')</p>
            </div>
            <div>
                <p class="font-weight-800">@lang('collections.start_date')</p>
                <p>{{date('Y/m/d',strtotime(optional($data)->start_date))}}</p>
            </div>
            <div>
                <p class="font-weight-800">@lang('collections.end_date')</p>
                <p>{{date('Y/m/d',strtotime(optional($data)->end_date))}}</p>
            </div>
            <div>
                <p class="font-weight-800">@lang('collections.created-date')</p>
                <p>{{date('Y/m/d',strtotime($data->created_at))}}</p>
            </div>
        </div>
    </div>
    <div class="col-md-4 detail-content1">

        <a href="javascript:void(0)" class="content1-img edit-img-coll">
            <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail" alt="">
        </a>
        <div class="all-content1">
            <p class="font-weight-800">@lang('collections.img-cl')</p>
            @if($data->images)
            @php
                $images = explode(',',$data->images);
            @endphp
            {{-- @dump($images) --}}
            <div class="image-large">
                <img src="{{ static_asset($images[0]) }}" class="large" alt="Search">
            </div>
            <div class="list-image mt-3">
                @foreach($images as $index => $image)
                <div class="item-small {{$index == 0 ? 'd-none' : ''}}">
                    <img src="{{ static_asset($image) }}" class="small " alt="Search">
                </div>
                @endforeach
            </div>
            @else
            <div class="no-image">@lang('collections.no_image')</div>
            @endif
        </div>
    </div>

    <div class="col-md detail-content2">
            <a href="javascript:void(0)" class="content1-img edit-product-collect">
                <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail" alt="">
        </a>
        <div class="all-content1">
            <div class="product-title">
                <p class="font-weight-800">@lang('collections.products')</p>
            </div>
            <div class="overflow-auto">
                @if(count($products) && !empty($products))
                @php
                    $products = json_decode($products);
                @endphp
                @foreach($products as $product)
                <table class="mb-2">
                    <tbody>
                        <tr>
                            <td rowspan="3">@if($product->images)
                                @php
                                    $pro_img = explode(',',$product->images);
                                @endphp
                                <img class="img-table mr-4" src="{{static_asset($pro_img[0])}}" alt="img" >
                                @else
                                <img class="img-table mr-4" src="{{static_asset('/assets/img/placeholder.jpg')}}" alt="img" >
                                @endif
                            </td>
                            <td>SKU: <b>{{$product->minPriceSku}}</b></td>
                        </tr>
                        <tr>
                            <td>{{$product->name}} </td>
                        </tr>
                        @php
                        $min_price = collect($product->skus)->min('price');
                        @endphp
                        <tr>
                            <td>{{format_price($min_price)}}원</td>
                        </tr>
                    </tbody>
                </table>
                 @endforeach
                 @else
                 <div class="no-product">
                     @lang('collections.no_pro')
                 </div>
                 @endif
            </div>
        </div>
    </div>
</div>
@endsection
@section('modal')
    @include('templates.popup.collection-info-popup',['edit' => true])
    @include('templates.popup.collection-product-popup',['edit' => true])
    @include('templates.popup.collection-img-popup',['edit' => true])
@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript">
        // $(document).ready(function(){
        //     @error('Collectionquest')
        //         $('.edit-info-collection').addClass('show');
        //     @enderror
	 	// });
        $('.edit-info-collect').click(function() {
            let url = $(this).attr('href');
            $('.edit-info-collection').addClass('show');
        });
        $('.edit-img-coll').click(function() {
            let url = $(this).attr('href');
            $('.edit-img-colls').addClass('show');
        });
        $('.edit-product-collect').click(function() {
            let url = $(this).attr('href');
            $('.edit-product-collection').addClass('show');
        });
       //Show image
       $('#input-file').change(function () {
            $('.label-image-add-col').before(`<img id="output" src="${window.URL.createObjectURL(this.files[0])}" width="120" height="120">'`);
        })

        $(document).on('click','.btn-search',function(){
            $(this).parents('.modal-product').find('.product-collection').addClass('d-none');
            // $(this).parents('.modal-product').find('.all-product').removeClass('d-none');
            const url = $(this).data('action');
            const val = $('#search-product').val();
            console.log(url+'?search='+val)
            $('#product-list').load(url+'?search='+val)
        })
        $(document).on('click','.close-model-product',function(){
            // location.reload();
            $('.edit-product-collection').removeClass('show');
            $('#product-list').empty();
            $('.product-collection').removeClass('d-none');
        })
        $(document).on('click','.btn-cancel-product',function(){
            // location.reload();
            $('.edit-product-collection').removeClass('show');
            $('#product-list').empty();
            $('.product-collection').removeClass('d-none');
        })
        $(document).on('click','.a-delete-products',function(){
            $(this).parent('.form_delete').submit();
        })
        $('#start_date').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            drops:'up',
            autoUpdateInput: false,
            minDate: '1921/01/01',
            locale: {
            format: 'YYYY/MM/DD',
            applyLabel: "대다",
            cancelLabel: "취소",
                "monthNames":[
                    "일월" , // Tháng 1
                    "이월" , // Tháng 2
                    "삼월" , // Tháng 3
                    "사월" , // Tháng 4
                    "오월" , // Tháng 5
                    "유월" , // Tháng 6
                    "칠월" , // Tháng 7
                    "팔월" , // Tháng 8
                    "구월" , // Tháng 9
                    "시월" , // Tháng 10
                    "십일월" , // Tháng 11
                    "십이월" , // Tháng 12
                ],
                daysOfWeek:[
                    "월요일" ,// Thứ 2
                    "화요일" ,// Thứ 3
                    "수요일" ,// Thứ 4
                    "목요일" ,// Thứ 5
                    "금요일" ,// Thứ 6
                    "토요일" ,// Thứ 7
                    "일요일" ,//  Chủ nhật.
                ]
            }
        });

        $('#start_date').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('YYYY/MM/DD'));
        });

        $('#start_date').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });

        $('#start_date').val('{{ date('Y/m/d',strtotime(optional($data)->start_date)) }}');
        $('#end_date').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            drops:'up',
            autoUpdateInput: false,
            minDate: '1921/01/01',
            locale: {
                format: 'YYYY/MM/DD',
            applyLabel: "대다",
            cancelLabel: "취소",
                "monthNames":[
                    "일월" , // Tháng 1
                    "이월" , // Tháng 2
                    "삼월" , // Tháng 3
                    "사월" , // Tháng 4
                    "오월" , // Tháng 5
                    "유월" , // Tháng 6
                    "칠월" , // Tháng 7
                    "팔월" , // Tháng 8
                    "구월" , // Tháng 9
                    "시월" , // Tháng 10
                    "십일월" , // Tháng 11
                    "십이월" , // Tháng 12
                ],
                daysOfWeek:[
                    "월요일" ,// Thứ 2
                    "화요일" ,// Thứ 3
                    "수요일" ,// Thứ 4
                    "목요일" ,// Thứ 5
                    "금요일" ,// Thứ 6
                    "토요일" ,// Thứ 7
                    "일요일" ,//  Chủ nhật.
                ]
            }
        });

        $('#end_date').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('YYYY/MM/DD'));
        });

        $('#end_date').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });

        $('#end_date').val('{{ date('Y/m/d',strtotime(optional($data)->end_date)) }}');
        $(document).on('click','.btn-addimg',function(){
            // $('.'+$(this).data('text')).addClass('done');
            var url = $(this).data('action');
            const parent = $(this).parents('form');
            $.ajax({
                url : url,
                data: parent.serialize(),
                type : "POST",
                statusCode : {
                    200 : () => {
                        $('.'+$(this).data('text')).addClass('done');
                        parent.find('.error').remove();
                    },
                    422 : ({responseJSON}) => {
                        parent.find('.error').remove();
                        const e = Object.entries(responseJSON.errors);
                        // console.log(e);
                        for (const [key,val] of e) {
                            parent.find(`[name="${key}"]`).parents('.row').after(`
                                <div class="error">${val}</div>
                            `)
                        }
                    }
                },
                success : function(res) {
                    $('#form-image-upload').submit();
                },
                error : function(err) {
                    console.log(err)
                }
            })

        })
        $('#created_at').val('{{ date('Y/m/d',strtotime(optional($data)->created_at)) }}');


        $(document).on('click','.btn-updateinfo',function(){
            var url = $(this).data('action');
            console.log(url)
            var name = $('#name').val();
            var section = $('#section').val();
            var short_description = $('#short_description').val();
            var date1 = $('#start_date').val();
            var start_date=date1.replace(/\//g,'-');
            var date2 = $('#end_date').val();
            var end_date=date2.replace(/\//g,'-');
            var date3 = $('#created_at').val();
            var created_at=date3.replace(/\//g,'-');
            var brand_id = $('#brand_id').val();
            var data = {
                "token" : "{{csrf_token()}}",
                "name" : name,
                "section": section,
                "short_description": short_description,
                "start_date": start_date,
                "end_date": end_date,
                "created_at": created_at,
                "brand_id" : brand_id
            }
            $.ajax({
                url : url,
                data: data,
                type : "POST",
                success : function(res) {
                    $('.edit-info-collection').removeClass('show');
                    $('.error').hide();
                    AIZ.plugins.notify('success', "{{__('collections.msg_infor_success')}}");
                    setTimeout(
                    function() {
                        window.location.reload(true);
                    }, 2000);
                }
                ,
                error : function(err) {
                    var response = JSON.parse(err.responseText);
                    console.log(response.errors)
                    printErrorMsg(response.errors)
                }
            })

            function printErrorMsg (msg) {
            $.each( msg, function( key, value ) {
            console.log(key);
              $('.'+key+'_err').text(value);
                });
            }
        })

        $(document).on('click', '.change-status', function() {
            var status_id = $(this).attr('data-id');
            var status = $('#status').val();
            console.log(status);
            Swal.fire({
                title: '@lang('collections.status_change')',
                text: '@lang('collections.confirm_popup')',
                // icon: 'error',
                confirmButtonText: '@lang('collections.yes')',
                cancelButtonText: '@lang('collections.no')',
                showCancelButton: true,
                showCloseButton: true,

            }).then((result) => {
                if (result.isConfirmed) {
                    var data = {
                        "_token": "{{ csrf_token() }}",
                        "id": status_id,
                        "status" : status
                    };
                    $.ajax({
                        type: "POST",
                        url: '/admin/collections/status/'+status_id,
                        data: data,
                        success: function (response){
                            /* Read more about isConfirmed, isDenied below */
                                location.reload();

                        },
                        error : function(err) {
                            Swal.fire('Changes are not saved', '', 'info');
                        }
                    });
                }
            });
        });

        $(document).on('click', '.delete-collection', function() {
        let delete_id= $(this).attr('data-id');

        Swal.fire({
            title: '@lang('collections.delete_coll')',
            text: '@lang('collections.confirm_popup')',
            // icon: 'error',
            confirmButtonText: '@lang('collections.yes')',
            cancelButtonText: '@lang('collections.no')',
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": delete_id,
                };

                $.ajax({
                    type: "DELETE",
                    url: '/admin/collections/'+delete_id,
                    data: data,
                    success: function (response){
                        /* Read more about isConfirmed, isDenied below */
                        location.href = '/admin/collections';
                    },
                    error : function(err) {
                        Swal.fire('Changes are not saved', '', 'info');
                    }
                });
            }
        });
    });
</script>

@endsection
