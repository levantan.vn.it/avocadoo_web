@extends('backend.layouts.app')
@section('title')
@lang('collections.collection')
@endsection
@section('content')

<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="align-items-center">
		<h1 class="h3">@lang('collections.collection')</h1>
	</div>
</div>
<form class="" id="sort_customers" action="" method="GET">
    <div class="row mb-2 md-3">
        <div class="col-md-9">
            <img src="{{ static_asset('assets/img/icons/search.svg') }}" class="img-search3 mr-2" alt="Search">
            <input type="text" class="form-control res-placeholder" id="search" name="search" value="{{request('search')}}" placeholder="@lang('collections.search-collection')">

        </div>
        <div class="col-md-3 text-md-right res-FormControl">
            <a href="{{ route('collections.create') }}" class="btn btn-circle btn-info btn-add-product d-flex">
                <i class="fas fa-plus-circle img-redo mr-2"></i>
                <span>@lang('collections.add')</span>
            </a>
        </div>
    </div>
    <div class="row gutters-5 mb-5">
        <div class="col-md-4">
            <input type="text" onkeypress="return event.charCode >=48 && event.charCode<=57" autocomplete="off"
            class="form-control" id="joined_date" name="joined_date"  value="{{ request('joined_date') }}"
            placeholder="@lang('collections.created-date')">
            <div class="custom-down"><i class="fas fa-chevron-down"></i></div>
        </div>
        <div class="col-md-4 res-status">
            <select class="form-control form-control-sm aiz-selectpicker mb-2 mb-md-0" id="status" name="status">
                <option value="">@lang('collections.status')</option>
                <option value="1" {{request()->status == 1 ? 'selected' : ''}} >@lang('collections.active')</option>
                <option value="2" {{request()->status == 2 ? 'selected' : ''}}>@lang('collections.inactive')</option>
            </select>

        </div>

        <div class="col-md all-btn-search">
            <button type="submit" class="pl-0 pr-0 btn btn-info w-32 mr-2 d-flex  btn-responsive justify-content-center">
                <i class="fas fa-search img-redo mr-2"></i>
                <span >@lang('collections.search')</span>
            </button>
            <a href="{{ route('collections.index') }}" class="pl-0 pr-0 w-32 btn btn-info ml-2 mr-2 d-flex btn-responsive justify-content-center">
                <i class="fas fa-redo-alt img-redo mr-2"></i>
                <span >@lang('collections.reset')</span>
            </a>
            <?php
                $request = request()->all();
                $newRequest = http_build_query($request);
            ?>
            <a href="{{route('collections.export') . '?' . $newRequest}}" class="pl-0 pr-0 btn btn-info w-34 ml-2 d-flex  btn-responsive justify-content-center">
                <i class="fas fa-cloud-download-alt img-redo mr-2"></i>
                <span>@lang('collections.excel')</span>
            </a>
        </div>

    </div>
</form>

<div class="card">
    <div class="card-body-table">
        <table class="table-collection aiz-table mb-0">
            <thead>
                <tr>
                    <th><strong>@lang('collections.id')</strong></th>
                    <th><strong>@lang('collections.img')</strong></th>
                    <th><strong>@lang('collections.title')</strong></th>
                    <th><strong>@lang('collections.section')</strong></th>
                    <th><strong>@lang('collections.products')</strong></th>
                    <th><strong>@lang('collections.created-date')</strong></th>
                    <th><strong>@lang('collections.status')</strong></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @if(!empty($data) && count($data))
                    @foreach($data as $index => $item)
                    <tr>
                        <td><strong>#{{ $item->id }}</strong></td>
                        <td>
                        @if(!empty($item->main_image))
                        <img class="img-ava mr-2" src="{{ static_asset($item->main_image) }}" alt="">
                        @else
                            @php
                                $images = explode(",", $item->images);
                            @endphp
                            @if($images && $images[0])
                                <img class="img-ava mr-2" src="{{ static_asset($images[0]) }}" alt="">
                            @else
                                <img class="empty_image mr-2" src="{{no_asset()}}" alt="">
                            @endif
                        @endif
                        </td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->sectionText()}}</td>
                        <td>{{count($item->products)}} {{__('collections.products')}}</td>
                        <td>{{date('Y/m/d',strtotime($item->created_at))}}</td>
                        <td>{{$item->statusText()}}</td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-drop" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-h"></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                  <a class="dropdown-item"  href="{{ route('collections.information',optional($item)->id) }}">@lang('collections.view')</a>
                                  <div class="changeStatus">
                                    <form  class="change_status_form"  class="mr-2">
                                        @csrf
                                        <input type="hidden" id="status-change" name="status" value="{{ $item->status == 1? '2':'1'}}">
                                        <a class="dropdown-item toggle-active change-status" data-id="{{optional($item)->id}}" href="javascript:void(0)" type="sum" >
                                            @if ($item->status==2)
                                                @lang('suppliers.active')
                                            @else
                                                @lang('suppliers.inactive')
                                            @endif
                                        </a>
                                    </form>
                                </div>
                                  <a class="dropdown-item col-delete" data-id ="{{$item->id}}" href="javascript:void(0)" >@lang('collections.del')</a>
                                </div>
                              </div>
                        </td>
                    </tr>
                    @endforeach
                @endif
            </tbody>
        </table>

    </div>
</div>

<div class="aiz-pagination paginationOrder">
    {{ $data->appends(request()->input())->links() }}
</div>

@endsection

@section('modal')
    @include('modals.delete_modal')
@endsection


@section('script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
    $(document).on('click', 'a.dropdown-item.col-delete', function() {
        let delete_id= $(this).attr('data-id');

        Swal.fire({
            title: '@lang('collections.delete_coll')',
            text: '@lang('collections.confirm_popup')',
            // icon: 'error',
            confirmButtonText: '@lang('collections.yes')',
            cancelButtonText: '@lang('collections.no')',
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": delete_id,
                };

                $.ajax({
                    type: "DELETE",
                    url: '/admin/collections/'+delete_id,
                    data: data,
                    success: function (response){
                        /* Read more about isConfirmed, isDenied below */
                        location.reload();
                    },
                    error : function(err) {
                        Swal.fire('Changes are not saved', '', 'info');
                    }
                });
            }
        });
    });
    $('#joined_date').daterangepicker({
        autoUpdateInput: false,
        minDate: '1921/01/01',
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'YYYY/MM/DD',
            applyLabel: "대다",
            cancelLabel: "취소",
                "monthNames":[
                    "일월" , // Tháng 1
                    "이월" , // Tháng 2
                    "삼월" , // Tháng 3
                    "사월" , // Tháng 4
                    "오월" , // Tháng 5
                    "유월" , // Tháng 6
                    "칠월" , // Tháng 7
                    "팔월" , // Tháng 8
                    "구월" , // Tháng 9
                    "시월" , // Tháng 10
                    "십일월" , // Tháng 11
                    "십이월" , // Tháng 12
                ],
                daysOfWeek:[
                    "월요일" ,// Thứ 2
                    "화요일" ,// Thứ 3
                    "수요일" ,// Thứ 4
                    "목요일" ,// Thứ 5
                    "금요일" ,// Thứ 6
                    "토요일" ,// Thứ 7
                    "일요일" ,//  Chủ nhật.
                ]
        }
    });
    $('#joined_date').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('YYYY/MM/DD'));
    });

    $('#joined_date').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
    $('#joined_date').val('{{ request()->get('joined_date') }}');
        // Change status
    $(document).on('click', '.change-status', function() {
        var status_id = $(this).attr('data-id');
        var status = $('#status-change').val();
        Swal.fire({
            title: '@lang('collections.status_change')',
            text: '@lang('collections.confirm_popup')',
            // icon: 'error',
            confirmButtonText: '@lang('collections.yes')',
            cancelButtonText: '@lang('collections.no')',
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            console.log(result);
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": status_id,
                    "status" : status
                };
                $.ajax({
                    type: "POST",
                    url: '/admin/collections/status/'+status_id,
                    data: data,
                    success: function (response){
                        /* Read more about isConfirmed, isDenied below */
                            location.reload();

                    },
                    error : function(err) {
                        Swal.fire('Changes are not saved', '', 'info');
                    }
                });
            }
        });
    });
    </script>
@endsection

