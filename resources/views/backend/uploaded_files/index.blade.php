@extends('backend.layouts.app')
@section('title')
@lang('upload_file.title')
@endsection
@section('content')
<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="row align-items-center">
		<div class="col-md-6">
			<h1 class="h3 font-weight-bold">@lang('upload_file.all_upload')</h1>
		</div>
		<div class="col-md-6 text-md-right">
			<a href="{{ route('uploaded-files.create') }}" class="btn btn-primary">
				<span>@lang('upload_file.upload_new')</span>
			</a>
		</div>
	</div>
</div>

<div class="card">
    <form id="sort_uploads" action="">
        <div class="card-header row gutters-5">
            <div class="col-md-3">
                <h5 class="mb-0 h6">@lang('upload_file.all_file')</h5>
            </div>
            <div class="col-md-3 ml-auto mr-0">
                <select class="form-control form-control-xs aiz-selectpicker" name="sort" onchange="sort_uploads()">
                    <option value="newest" @if($sort_by == 'newest') selected="" @endif>@lang('upload_file.sort_newest')</option>
                    <option value="oldest" @if($sort_by == 'oldest') selected="" @endif>@lang('upload_file.sort_oldest')</option>
                    <option value="smallest" @if($sort_by == 'smallest') selected="" @endif>@lang('upload_file.sort_smallest')</option>
                    <option value="largest" @if($sort_by == 'largest') selected="" @endif>@lang('upload_file.sort_largest')</option>
                </select>
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control form-control-xs" name="search" placeholder="@lang('upload_file.search_file')" value="{{ $search }}">
            </div>
            <div class="col-auto">
                <button type="submit" class="btn btn-primary">@lang('upload_file.search')</button>
            </div>
        </div>
    </form>
    <div class="card-body">
    	<div class="row gutters-5 card-item">
            @if (!empty($all_uploads) && count($all_uploads))
    		@foreach($all_uploads as $key => $file)
    			@php
    				$file_name = $file->file_original_name;
    			@endphp
    			<div class="col-auto w-150px w-lg-220px">
    				<div class="aiz-file-box">
    					<div class="dropdown-file" >
    						<a class="dropdown-link" data-toggle="dropdown">
    							<i class="la la-ellipsis-v"></i>
    						</a>
    						<div class="dropdown-menu dropdown-menu-right">
    							<a href="javascript:void(0)" class="dropdown-item" onclick="detailsInfo(this)" data-id="{{ $file->id }}">
    								<i class="las la-info-circle mr-2"></i>
    								<span>@lang('upload_file.detail_info')</span>
    							</a>
    							<a href="{{ my_asset($file->file_name) }}" target="_blank" download="{{ $file_name }}.{{ $file->extension }}" class="dropdown-item">
    								<i class="la la-download mr-2"></i>
    								<span>@lang('upload_file.down')</span>
    							</a>
    							<a href="javascript:void(0)" class="dropdown-item" onclick="copyUrl(this)" data-url="{{ my_asset($file->file_name) }}">
    								<i class="las la-clipboard mr-2"></i>
    								<span>@lang('upload_file.copy_link')</span>
    							</a>
    							<a href="javascript:void(0)" class="dropdown-item confirm-alert" data-href="{{ route('uploaded-files.destroy', $file->id ) }}" data-target="#delete-modal">
    								<i class="las la-trash mr-2"></i>
    								<span>@lang('upload_file.delete')</span>
    							</a>
    						</div>
    					</div>
    					<div class="card card-file aiz-uploader-select c-default" title="{{ $file_name }}.{{ $file->extension }}">
    						<div class="card-file-thumb">
    							@if($file->type == 'image')
    								<img src="{{ my_asset($file->file_name) }}" class="img-fit">
    							@elseif($file->type == 'video')
    								<i class="las la-file-video"></i>
    							@else
    								<i class="las la-file"></i>
    							@endif
    						</div>
    						<div class="card-body">
    							<h6 class="d-flex">
    								<span class="text-truncate title">{{ $file_name }}</span>
    								<span class="ext">.{{ $file->extension }}</span>
    							</h6>
    							<p>{{ formatBytes($file->file_size) }}</p>
    						</div>
    					</div>
    				</div>
    			</div>
    		@endforeach
            @else
            <div class="text_nothing_found">
                <p>@lang('upload_file.nothing_found')</p>
            </div>
        @endif
    	</div>
    </div>
</div>
<div class="aiz-pagination paginationOrder">
    {{ $all_uploads->appends(request()->input())->links() }}
</div>
@endsection
@section('modal')
<div id="delete-modal" class="modal fade">
    <div class="modal-dialog modal-dialog-centered modal_delete">
        <div class="modal-content">
            <div class="modal-header border-0">
                <h4 class="modal-title h3 font-weight-bold modal_title">@lang('upload_file.confirm_delete')</h4>
                <button type="button" class="close ml-0" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body text-center">
                <p class="mt-1 text_delete_uploadfile">@lang('upload_file.question_delete')</p>
                <a href="" class="btn mt-2 btn-primary comfirm-link  text-light">@lang('upload_file.delete')</a>
                <button type="button" class="btn mt-2 button_cancel_uploadfile text-light" data-dismiss="modal">@lang('upload_file.cancel')</button>
            </div>
        </div>
    </div>
</div>
<div id="info-modal" class="modal fade">
	<div class="modal-dialog modal-dialog-right">
			<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title h6">@lang('upload_file.file_info')</h5>
				<button type="button" class="close" data-dismiss="modal">
				</button>
			</div>
			<div class="modal-body c-scrollbar-light position-relative" id="info-modal-content">
				<div class="c-preloader text-center absolute-center">
                    <i class="las la-spinner la-spin la-3x opacity-70"></i>
                </div>
			</div>
		</div>
	</div>
</div>

@endsection
@section('script')
	<script type="text/javascript">
		function detailsInfo(e){
            $('#info-modal-content').html('<div class="c-preloader text-center absolute-center"><i class="las la-spinner la-spin la-3x opacity-70"></i></div>');
			var id = $(e).data('id')
			$('#info-modal').modal('show');
			$.post('{{ route('uploaded-files.info') }}', {_token: AIZ.data.csrf, id:id}, function(data){
                $('#info-modal-content').html(data);
				// console.log(data);
			});
		}
		function copyUrl(e) {
			var url = $(e).data('url');
			var $temp = $("<input>");
		    $("body").append($temp);
		    $temp.val(url).select();
		    try {
			    document.execCommand("copy");
			    AIZ.plugins.notify('success', '@lang('upload_file.link_copy')' );
			} catch (err) {
			    AIZ.plugins.notify('danger', '@lang('upload_file.opps')');
			}
		    $temp.remove();
		}
        function sort_uploads(el){
            $('#sort_uploads').submit();
        }
	</script>
@endsection
