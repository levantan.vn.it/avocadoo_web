@extends('backend.layouts.app')
@section('title')
    @lang('message.message')}
@endsection
@section('css')
    <link rel="stylesheet" href="{{ asset('public/css/chat.css?v='.time()) }}">
    <style>
        #message #sidepanel #contacts ul li.contact .wrap img {
            width: 40px;
            height: 40px;
            object-fit: cover;
        }

        #message .content .messages ul li img {
            width: 22px;
            height: 22px;
        }

    </style>
@endsection

@section('content')
<div class="aiz-titlebar text-left mt-2 mb-3">
    <div class="align-items-center">
        <h1 class="h3"><strong>@lang('message.message')</strong> </h1>
    </div>
</div>
<div id="message">

</div>
@endsection
@section('scripts')
    <script src="{{ asset('public/js/app.js?v='.time()) }}"></script>
    <script src="{{ asset('public/js/chat.js') }}"></script>
    <script>window.Laravel = {csrfToken: '{{ csrf_token() }}'}</script>

@endsection
