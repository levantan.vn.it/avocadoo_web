@extends('backend.layouts.app')
@section('title')
@lang('settings.delivery_title')
@endsection
@section('content')
<div class="backpage mb-5">
    <a href="{{route('settings.index')}}" class="back btn"><i class="fas fa-chevron-left"></i></a>
</div>
<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="align-items-center">
		<h1 class="h3 setting-title"><strong>@lang('settings.delivery_title')</strong></h1>
	</div>
</div>
<div class="company-information setting-page delivery-page">
    <div class="deli-formula">
        <div class="wrapper">
            <label class="box">Delivery Fee</label>
            <span>=</span>
            <label class="box">Weight</label>
            <span>+</span>
            <label class="box">Distance</label>
            <span>|</span>
            <label class="box">Interprovincial</label>

        </div>
    </div>
    <div class="setting-list row">
        <div class="col-md-3 left">
            <div class="card p-3">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" id="weight-tab" data-toggle="tab" href="#weight" role="tab" aria-controls="weight" aria-selected="true">Weight Formula</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="distance-tab" data-toggle="tab" href="#distance" role="tab" aria-controls="distance" aria-selected="false">Distance Formula</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="interprovincial-tab" data-toggle="tab" href="#interprovincial" role="tab" aria-controls="interprovincial" aria-selected="false">Interprovincial delivery</a>
                    </li>
                  </ul>
            </div>
        </div>
        <div class="col-md-9 right">
            <div class="card" id="delivery">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active weight_tabs" id="weight" role="tabpanel" aria-labelledby="weight-tab">
                        <p class="deli_title">Delivery base on Weight</p>
                        <form action="{{route('settings.updateOrInsert')}}" method="post">
                            @csrf
                            <div class="list-weight-range">
                                @if(count($weights) && !empty($weights))
                                @foreach($weights as $index => $weight)
                                <div class="weight-range">
                                    <input type="hidden" name="delivery[{{$index}}][id]" value="{{$weight->id}}">
                                    <label class="font-600">{{$index == 0 ? 'First' : 'Next'}}</label>
                                    <div class="row weight_row">
                                        <div class="col-md-6 field">
                                            @if(optional($weight)->delivery_from == 0)
                                            {{-- <input type="number"  name="delivery[{{$index}}][delivery_from]" value="{{optional($weight)->delivery_from}}" id=""> --}}
                                            <input type="number"  name="delivery[{{$index}}][delivery_to]" value="{{optional($weight)->delivery_to}}" id="">
                                                    <span class="unit">kg</span>
                                                    @error('delivery_to')
                                                    <p class="error mt-1">{{$message}}</p>
                                                    @enderror
                                            @else
                                            <div class="to-from row">
                                                <div class="col-md-6 field">
                                                    <input type="number"  name="delivery[{{$index}}][delivery_from]" value="{{optional($weight)->delivery_from}}" id="">
                                                    <span class="unit">kg</span>
                                                    @error('delivery_from')
                                                    <p class="error mt-1">{{$message}}</p>
                                                    @enderror
                                                </div>
                                                {{-- <span>-</span> --}}
                                                <div class="field col-md-6">
                                                    <input type="number"  name="delivery[{{$index}}][delivery_to]" value="{{optional($weight)->delivery_to}}" id="">
                                                    <span class="unit">kg</span>
                                                    @error('delivery_to')
                                                    <p class="error mt-1">{{$message}}</p>
                                                    @enderror
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                        <div class="col-md-6 field">
                                            <input type="number" name="delivery[{{$index}}][price]" id="" value="{{optional($weight)->price}}">
                                            <span class="unit">원</span>
                                            @error('price')
                                                    <p class="error mt-1">{{$message}}</p>
                                                    @enderror
                                        </div>
                                        <a href="javascript:void(0)" data-id="{{optional($weight)->id}}" class="btn-del btn-del-now"><i class="fas fa-trash"></i></a>
                                    </div>
                                </div>
                                @endforeach
                                @endif
                            </div>
                            <div class="addnew-range">
                                <a class="btn-add-range" href="javascript:void(0)"><i class="fas fa-plus-circle"></i> Add weight range</a>
                            </div>
                            <button type="submit"> SAVE</button>
                        </form>
                    </div>
                    <div class="tab-pane fade distance_tabs" id="distance" role="tabpanel" aria-labelledby="distance-tab">
                        <p class="deli_title">Delivery base on Distance</p>
                        <ul class="nav nav-tabs" id="tabChild" role="tablist">
                            <li class="nav-item">
                              <a class="nav-link active" id="kilo-tab" data-toggle="tab" href="#kilo" role="tab" aria-controls="kilo" aria-selected="true"><i class="far fa-dot-circle checked"></i><i class="far fa-circle none"></i>Kilometers</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="fixed-tab" data-toggle="tab" href="#fixed" role="tab" aria-controls="fixed" aria-selected="false"><i class="far fa-dot-circle checked"></i><i class="far fa-circle none"></i>Fixed Area</a>
                            </li>
                          </ul>
                          <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="kilo" role="tabpanel" aria-labelledby="kilo-tab">
                                <form action="" method="post">
                                    <div class="list-weight-range">
                                        <div class="weight-range">
                                            <label class="font-600">First</label>
                                            <div class="row weight_row">
                                                <div class="col-md-6 field">
                                                    <div class="to-from row">
                                                        <div class="col-md-6 field">
                                                            <input type="number"  name="delivery_from" id="">
                                                            <span class="unit">km</span>
                                                        </div>
                                                        {{-- <span>-</span> --}}
                                                        <div class="col-md-6 field">
                                                            <input type="number"  name="delivery_to" id="">
                                                            <span class="unit">km</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 field">
                                                    <input type="number" name="price" id="">
                                                    <span class="unit">원</span>
                                                </div>
                                                <a href="javascript:void(0)" class="btn-del"><i class="fas fa-trash"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="addnew-range">
                                        <a class="btn-add-range" href="javascript:void(0)"><i class="fas fa-plus-circle"></i> Add distance range</a>
                                    </div>
                                    <button type="submit"> SAVE</button>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="fixed" role="tabpanel" aria-labelledby="fixed-tab">
                                <form action="" method="post">
                                    <div class="list-weight-range">
                                        <div class="weight-range">
                                            <label class="font-600">Area</label>
                                            <div class="row weight_row">
                                                <div class="col-md-6 field">
                                                    <input type="text" data-role="tagsinput" class="tags"  name="list_area" id="">
                                                </div>
                                                <div class="col-md-6 field">
                                                    <input type="number" name="price" id="">
                                                    <span class="unit">원</span>
                                                </div>
                                                <a href="javascript:void(0)" class="btn-del"><i class="fas fa-trash"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="addnew-range">
                                        <a class="btn-add-range" href="javascript:void(0)"><i class="fas fa-plus-circle"></i> Add new area</a>
                                    </div>
                                    <button type="submit"> SAVE</button>
                                </form>
                            </div>
                          </div>
                    </div>
                    <div class="tab-pane fade interpro_tabs" id="interprovincial" role="tabpanel" aria-labelledby="interprovincial-tab">
                        <p class="deli_title">Interprovincial delivery</p>

                            <div class="bycar">
                                <p class="subtitle"><i class="far fa-check-square mr-2"></i>By Car</p>
                                <form action="" method="post">
                                <div class="list-weight-range">
                                    <div class="weight-range">
                                        <label class="font-600">City</label>
                                        <div class="row weight_row">
                                            <div class="col-md-6 field">
                                                <input type="text" data-role="tagsinput" class="tags"  name="list_city" id="">
                                            </div>
                                            <div class="col-md-6 field">
                                                <input type="number" name="price" id="">
                                                <span class="unit">원</span>
                                            </div>
                                            <a href="javascript:void(0)" class="btn-del"><i class="fas fa-trash"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="addnew-range">
                                    <a class="btn-add-range" href="javascript:void(0)"><i class="fas fa-plus-circle"></i> Add city/province</a>
                                </div>
                                <button type="submit"> SAVE</button>
                                </form>
                            </div>
                            <div class="byairplane">
                                <p class="subtitle"><i class="far fa-check-square mr-2"></i>By Airplane</p>
                                <form action="" method="post">
                                    <div class="list-weight-range">
                                        <div class="weight-range">
                                            <label class="font-600">City</label>
                                            <div class="row weight_row">
                                                <div class="col-md-6 field">
                                                    <input type="text" data-role="tagsinput" class="tags"  name="list_city" id="">
                                                </div>
                                                <div class="col-md-6 field">
                                                    <input type="number" name="price" id="">
                                                    <span class="unit">원</span>
                                                </div>
                                                <a href="javascript:void(0)" class="btn-del"><i class="fas fa-trash"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="addnew-range">
                                        <a class="btn-add-range" href="javascript:void(0)"><i class="fas fa-plus-circle"></i> Add city/province</a>
                                    </div>
                                    <button type="submit"> SAVE</button>
                                </form>
                            </div>

                    </div>
                  </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('script')
<script src="{{static_asset('plugins/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(function () {
        $('.tags').tagsinput();

    });
    })
    let i = 1000;

    $(document).on('click','.weight_tabs .btn-add-range',function(){
        $('.weight_tabs .list-weight-range .weight-range:last-child').after(`
        <div class="weight-range"><label class="font-600">Next</label>
            <div class="row weight_row">
                <div class="col-md-6 field">
                    <div class="to-from row">
                        <div class="col-md-6 field">
                            <input type="number"  name="delivery[${i}][delivery_from]" value="" id="">
                             <span class="unit">kg</span>
                             @error('delivery_from')
                             <p class="error mt-1">{{$message}}</p>
                             @enderror
                             </div> <div class="field col-md-6">
                                <input type="hidden" name="delivery[${i}][type_delivery]" value="1" id="">
                                <input type="number"  name="delivery[${i}][delivery_to]" value="" id="">
                                 <span class="unit">kg</span></div> </div> </div> <div class="col-md-6 field">
                                    @error('delivery_to')
                                    <p class="error mt-1">{{$message}}</p>
                                    @enderror
                                    <input type="number" name="delivery[${i}][price]" id="" value="">
                                     <span class="unit">원</span>
                                     @error('price')
                                    <p class="error mt-1">{{$message}}</p>
                                    @enderror
                                     </div> <a href="javascript:void(0)" class="btn-del btn-remove">
                                        <i class="fas fa-trash"></i></a> </div> </div>`)

        i++;
    })
    $(document).on('click','.weight_tabs .btn-remove',function(){
        $(this).parents('.weight_tabs .weight-range').remove();
    })
    $(document).on('click', '.btn-del-now', function() {
        let delete_id= $(this).attr('data-id');
        Swal.fire({
            title: 'Delete this column',
            text: 'Do you want to continue',
            // icon: 'error',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": delete_id,
                };
                console.log(data)
                $.ajax({
                    type: "DELETE",
                    url: '/admin/settings/delivery-management/destroy/'+delete_id,
                    data: data,
                    success: function (response){
                        /* Read more about isConfirmed, isDenied below */
                        if (result.isConfirmed) {
                            Swal.fire('Done!', '', 'success')
                            location.reload();
                        }
                        else if (result.isDenied) {
                            Swal.fire('Changes are not saved', '', 'info')
                        }
                    }
                });
            }
        });
    });
</script>
@endsection
