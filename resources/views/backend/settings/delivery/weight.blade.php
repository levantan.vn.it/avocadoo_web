@extends('backend.layouts.app')
@section('title')
@lang('settings.delivery_title')
@endsection
@section('content')
<div class="backpage mb-5">
    <a href="{{route('settings.index')}}" class="back btn"><i class="fas fa-chevron-left"></i></a>
</div>
<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="align-items-center">
		<h1 class="h3 setting-title"><strong>@lang('settings.delivery_title')</strong></h1>
	</div>
</div>
<div class="company-information setting-page delivery-page">
    {{-- <div class="deli-formula">
        <div class="wrapper">
            <label class="box">@lang('settings.deli_fee')</label>
            <span>=</span>
            <label class="box">@lang('settings.weight')</label>
            <span>+</span>
            <label class="box">@lang('settings.distance')</label>
            <span>|</span>
            <label class="box">@lang('settings.interpro')</label>

        </div>
    </div> --}}
    <div class="setting-list row">
        <div class="col-md-3 left">
            <div class="card p-3">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link {{Request::is('admin/settings/weight-formula') ? 'active' : ''}}" id="weight-tab" href="{{route('settings.weight')}}" >@lang('settings.deli_fee')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{Request::is('admin/settings/delivery-condition') ? 'active' : ''}}" id="weight-tab" href="{{route('settings.delivery-condition')}}" >@lang('settings.special_delivery')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{Request::is('admin/settings/delivery-company') ? 'active' : ''}}" id="weight-tab" href="{{route('settings.delivery-company')}}" >@lang('settings.delivery_company')</a>
                    </li>
                      {{-- <li class="nav-item">
                        <a class="nav-link {{Request::is('admin/settings/distance-formula') ? 'active' : ''}}" id="distance-tab" href="{{route('settings.distance')}}">@lang('settings.distance_formu')</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link {{Request::is('admin/settings/interprovincial-delivery') ? 'active' : ''}}" id="interprovincial-tab" href="{{route('settings.interprovincial')}}">@lang('settings.inter_deli')</a>
                      </li> --}}
                  </ul>
            </div>
        </div>
        <div class="col-md-9 right">
            <div class="card" id="delivery">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active weight_tabs" id="weight" role="tabpanel" aria-labelledby="weight-tab">
                        <p class="deli_title">@lang('settings.deli_fee')</p>
                        <form class="form" action="{{route('settings.updateOrInsert')}}" method="post" id="add">
                            @csrf
                            <div class="item mb-3">
                                <label for="">@lang('settings.fixed_price') <span class="text-danger">*</span></label>
                                <div class="row">
                                    <div class="col-md-12 field">
                                        <input type="number" name="fixed_price" value="{{get_setting('fixed_price')?? old('fixed_price')}}" id="" placeholder="@lang('settings.fixed_price')" class="form-control">
                                        <span class="unit">@lang('settings.price')</span>
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="list-weight-range">
                                @if(count($weights) && !empty($weights))
                                @foreach($weights as $index => $weight)
                                <div class="weight-range update">
                                    <input type="hidden" name="delivery[{{$index}}][id]" value="{{$weight->id}}">
                                    <label class="font-600">{{$index == 0 ? __('settings.first')." (최소 무게를 입력해야 반드시 입력해 주세요: 0Kg - .....)" : __('settings.next')}} </label>
                                    <div class="row weight_row">
                                        <div class="col-md-6 field">
                                            @if(optional($weight)->delivery_from == 0)
                                            <input type="hidden" name="delivery[{{$index}}][delivery_from]" value="{{optional($weight)->delivery_from}}" id="">
                                            <input type="number" name="delivery[{{$index}}][delivery_to]" value="{{optional($weight)->delivery_to}}" id="">
                                            <span class="unit">@lang('settings.kg')</span>
                                            @else
                                            <div class="to-from row">
                                                <div class="col-md-6 field">
                                                    <input type="number"  name="delivery[{{$index}}][delivery_from]" value="{{optional($weight)->delivery_from}}" id="">
                                                    <span class="unit">@lang('settings.kg')</span>
                                                </div>
                                                <div class="field col-md-6">
                                                    <input type="number"  name="delivery[{{$index}}][delivery_to]" value="{{optional($weight)->delivery_to}}" id="">
                                                    <span class="unit">@lang('settings.kg')</span>
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                        <div class="col-md-6 field">
                                            <input type="number" name="delivery[{{$index}}][price]" id="" value="{{optional($weight)->price}}">
                                            <span class="unit">@lang('settings.price')</span>
                                        </div>
                                        @if($index)
                                        <a href="javascript:void(0)" data-id="{{optional($weight)->id}}" class="btn-del btn-del-now"><i class="fas fa-trash"></i></a>
                                        @endif
                                    </div>
                                </div>
                                @endforeach
                                @else
                                <div class="weight-range update">
                                    <label class="font-600">{{__('settings.first')}} (최소 무게를 입력해야 반드시 입력해 주세요: 0Kg - .....)</label>
                                    <div class="row weight_row">
                                        <div class="col-md-6 field">
                                            <input type="hidden"  name="delivery[0][type_delivery]" value="1">
                                            <input type="hidden" name="delivery[0][delivery_from]" value="0">
                                            <input type="number" name="delivery[0][delivery_to]" value="">
                                            <span class="unit">@lang('settings.kg')</span>

                                        </div>
                                        <div class="col-md-6 field">
                                            <input type="number" name="delivery[0][price]" value="">
                                            <span class="unit">@lang('settings.price')</span>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="addnew-range">
                                <a class="btn-add-range" href="javascript:void(0)"><i class="fas fa-plus-circle"></i> @lang('settings.add_weight')</a>
                            </div> --}}
                            <button type="submit" class="btn-info"> @lang('settings.btn_save')</button>
                            {{-- <button class="btn-submit" data-action="{{route('settings.updateOrInsert')}}">SAVE</button> --}}
                        </form>
                    </div>
                  </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('script')
<script src="{{static_asset('plugins/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(function () {
        $('.tags').tagsinput();

    });
    })
    let i = $('.update').length;
    $(document).on('click','.weight_tabs .btn-add-range',function(){
        $('.weight_tabs .list-weight-range ').append(`
        <div class="weight-range"><label class="font-600">{{__('settings.next')}}</label>
            <div class="row weight_row">
                <div class="col-md-6 field">
                    <div class="to-from row">
                        <div class="col-md-6 field">
                            <input type="number"  name="delivery[${i}][delivery_from]" value="">
                             <span class="unit">{{__('settings.kg')}}</span>
                             @error('delivery_from')
                             <p class="error mt-1">{{$message}}</p>
                             @enderror
                             </div> <div class="field col-md-6">
                                <input type="hidden" name="delivery[${i}][type_delivery]" value="1" >
                                <input type="number"  name="delivery[${i}][delivery_to]" value="">
                                 <span class="unit">{{__('settings.kg')}}</span></div> </div> </div> <div class="col-md-6 field">
                                    @error('delivery_to')
                                    <p class="error mt-1">{{$message}}</p>
                                    @enderror
                                    <input type="number" name="delivery[${i}][price]" value="">
                                     <span class="unit">{{__('settings.price')}}</span>
                                     @error('price')
                                    <p class="error mt-1">{{$message}}</p>
                                    @enderror
                                     </div> <a href="javascript:void(0)" class="btn-del btn-remove">
                                        <i class="fas fa-trash"></i></a> </div> </div>`)

        i++;
    })
    $(document).on('click','.weight_tabs .btn-remove',function(){
        $(this).parents('.weight_tabs .weight-range').remove();
    })
    $(document).on('click', '.btn-del-now', function() {
        let delete_id= $(this).attr('data-id');
        Swal.fire({
            title: "{{__('settings.popup_delete')}}",
            text: "{{__('settings.confirm_popup')}}",
            // icon: 'error',
            confirmButtonText: "{{__('settings.yes')}}",
            cancelButtonText: "{{__('settings.no')}}",
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": delete_id,
                };
                $.ajax({
                    type: "DELETE",
                    url: '/admin/settings/delivery-management/destroy/'+delete_id,
                    data: data,
                    success: function (response){
                        /* Read more about isConfirmed, isDenied below */
                        location.reload();

                    },
                    error : function(err) {
                        Swal.fire('Changes are not saved', '', 'info');
                    }
                });
            }
        });
    });
    $(document).on('submit', '#add', function(e){
        e.preventDefault();
        const form = e.target;
        $.ajax({
            url: form.action,
            method: form.method,
            data: new FormData(form),
            contentType: false,
            processData: false,
            statusCode: {
                422: (err)=>{
                    // console.log(err);
                    const errors = Object.entries(err.responseJSON.errors);
                    // console.log(errors,'errors');
                    for (let [name, message] of errors) {
                        name = name.split('.');
                        // console.log(name,'name');
                        name = name[0];
                        // console.log(name,'name');
                        $(`input[name="${name}"]`).addClass('is-invalid').after(`
                            <p class="error">${message}</p>
                        `);
                    }

                }
            },
            success:(data)=>{
                AIZ.plugins.notify('success', "{{__('settings.msg_weight_sucess')}}");
                setTimeout(
                function() {
                    window.location.reload(true);
                }, 2000);
                $('.error').hide();
            }
        })
    })
</script>
@endsection
