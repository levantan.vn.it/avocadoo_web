@extends('backend.layouts.app')
@section('title')
@lang('settings.title_company')
@endsection
@section('content')
<div class="backpage mb-5">
    <a href="{{route('settings.index')}}" class="back btn"><i class="fas fa-chevron-left"></i></a>
</div>
<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="align-items-center">
		<h1 class="h3 setting-title"><strong>@lang('settings.title_company')</strong></h1>
	</div>
</div>
<div class="company-information setting-page">
    <div class="setting-list row">
        <div class="col-md-4 left">
            <div class="card p-3">
                <p class="title mb-0 p-2"><strong>@lang('settings.title_company')</strong></p>
            </div>
        </div>
        <div class="col-md-8 right">
            <div class="card">
                <p class="form-title"><strong>@lang('settings.title_infor')</strong></p>
                <form action="{{route('settings.company.update')}}" method="POST">
                    @csrf
                    <div class="item mb-3">
                        <label for="">@lang('settings.company_name') <span class="text-danger">*</span></label>
                        <input type="text" name="company_name" value="{{$setting->company_name?? old('company_name')}}" id="" placeholder="@lang('settings.company_name')" class="form-control">
                        @if($errors->has('company_name'))
                            <p class="error mt-1">{{$errors->first('company_name')}}</p>
                        @endif
                    </div>
                    <div class="item mb-3">
                        <label for="">@lang('settings.represent_name') <span class="text-danger">*</span></label>
                        <input type="text" name="represent_name" value="{{$setting->represent_name?? old('represent_name')}}" id="" placeholder="@lang('settings.represent_name')" class="form-control">
                        @if($errors->has('represent_name'))
                            <p class="error mt-1">{{$errors->first('represent_name')}}</p>
                        @endif
                    </div>

                    <div class="item mb-3">
                        <label for="">@lang('settings.address') <span class="text-danger">*</span></label>
                        <input type="text" name="address" value="{{$setting->address?? old('address')}}" id="" placeholder="@lang('settings.address')" class="form-control">
                        @if($errors->has('address'))
                            <p class="error mt-1">{{$errors->first('address')}}</p>
                        @endif
                    </div>
                    <div class="item mb-3">
                        <label for="">@lang('settings.register_number') <span class="text-danger">*</span></label>
                        <input type="text" name="business_number" value="{{$setting->business_number?? old('business_number')}}" id="" placeholder="@lang('settings.register_number')" class="form-control">
                        @if($errors->has('business_number'))
                            <p class="error mt-1">{{$errors->first('business_number')}}</p>
                        @endif
                    </div>
                    <div class="item mb-3">
                        <label for="">@lang('settings.email') <span class="text-danger">*</span></label>
                        <input type="text" name="email" id="" value="{{$setting->email?? old('email')}}" placeholder="@lang('settings.email')" class="form-control">
                        @if($errors->has('email'))
                            <p class="error mt-1">{{$errors->first('email')}}</p>
                        @endif
                    </div>
                    <div class="item mb-3">
                        <label for="">@lang('settings.hotline') <span class="text-danger">*</span></label>
                        <input type="text" name="hotline" id="" value="{{$setting->hotline?? old('hotline')}}" placeholder="@lang('settings.hotline')" class="form-control">

                        @if($errors->has('hotline'))
                            <p class="error mt-1">{{$errors->first('hotline')}}</p>
                        @endif
                    </div>
                    <div class="item mb-3">
                        <label for="">@lang('settings.tax_code') <span class="text-danger">*</span></label>
                        <input type="text" name="tax_code" value="{{$setting->tax_code?? old('tax_code')}}" id="" placeholder="@lang('settings.tax_code')" class="form-control">
                        @if($errors->has('tax_code'))
                            <p class="error mt-1">{{$errors->first('tax_code')}}</p>
                        @endif
                    </div>
                    <div class="item mb-3">
                        <label for="">@lang('settings.description') <span class="text-danger">*</span></label>
                        {{-- <input type="text" name="description" id="" value="{{$setting->description?? null}}" placeholder="Description" class="form-control"> --}}
                        <textarea name="description" id="" rows="4" placeholder=@lang('settings.description') class="form-control">{{$setting->description?? old('description')}}</textarea>
                        @if($errors->has('description'))
                            <p class="error mt-1">{{$errors->first('description')}}</p>
                        @endif
                    </div>
                    <button type="submit" class="btn-info">@lang('settings.btn_save')</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection


@section('script')
<script type="text/javascript">
    // $(document).ready(function(){
    //     setTimeout(function(){
    //         $('.noti').fadeOut(1000);
    //     },2000)
    // })
    /**
     * Danh sách quận huyện theo tỉnh thành
     */
    function get_districts(event){
        var control = $(event.target);
        var city_id = control.val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url : base_url + '/ajax/get-districts',
            data : {
                'city_id' : city_id,
            },
            type : 'POST',
            success : function (data) {
                control.parents("form").find('select[name=district_id]').html(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    };

    /**
     * Danh sách Phường xã theo Quận huyện
     */
    function get_wards(event){
        var control = $(event.target);
        var district_id = control.val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url : base_url + '/ajax/get-wards',
            data : {
                'district_id' : district_id,
            },
            type : 'POST',
            success : function (data) {
                control.parents("form").find('select[name=ward_id]').html(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    };
</script>
@endsection
