@extends('backend.layouts.app')
@section('title')
@lang('settings.settings')
@endsection
@section('content')

<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="align-items-center">
		<h1 class="h3"><strong>@lang('settings.settings')</strong></h1>
	</div>
</div>


<div class="settings">
    <div class="setting-list row">
        <div class="col-md-2">
            <a href="{{route('settings.weight')}}" class="card setting-item">
                <i class="fas fa-truck mb-3"></i>
                <p class="title mb-0">@lang('settings.delivery_title')</p>
            </a>
        </div>
        <div class="col-md-2">
            <a href="{{route('settings.role')}}" class="card setting-item">
                <i class="fas fa-user-lock mb-3"></i>
                <p class="title mb-0">@lang('settings.role_title')</p>
            </a>
        </div>
        <div class="col-md-2">
            <a href="{{route('settings.company')}}" class="card setting-item">
                <i class="fas fa-building mb-3"></i>
                <p class="title mb-0">@lang('settings.title_company')</p>
            </a>
        </div>
        <div class=" col-md-2">
            <a href="{{route('settings.faq')}}" class="card setting-item">
                <i class="fas fa-question mb-3"></i>
                <p class="title mb-0">@lang('settings.faq')</p>
            </a>
        </div>
        <div class="col-md-2">
            <a href="{{route('settings.policies')}}" class="card setting-item">
                <i class="fas fa-lock mb-3"></i>
                <p class="title mb-0">@lang('settings.privacy_title')</p>
            </a>
        </div>
        <div class="col-md-2">
            <a href="{{route('settings.service')}}" class="card setting-item">
                <i class="fas fa-copy mb-3"></i>
                <p class="title mb-0">@lang('settings.terms_title')</p>
            </a>
        </div>
    </div>
</div>

@endsection


@section('script')
    <script type="text/javascript">
    
    </script>
@endsection
