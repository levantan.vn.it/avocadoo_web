@extends('backend.layouts.app')
@section('title')
@lang("settings.faq")
@endsection
@section('content')
<div class="backpage mb-5">
    <a href="{{route('settings.index')}}" class="back btn"><i class="fas fa-chevron-left"></i></a>
</div>

<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="align-items-center">
		<h1 class="h3 setting-title"><strong>@lang("settings.faq")</strong></h1>
	</div>
</div>
@php
$active = request()->active;
@endphp
<div class="company-information faq setting-page">
    <div class="setting-list row">
        <div class="col-md-4 left">
            <div class="card p-3">
                <div class="title-field">
                      <ul class="nav nav-tabs" id="myTab" role="tablist">
                            @if(count($faqs)?? [])
                            @foreach($faqs as $faq)
                                {{-- <a class="nav-link {{$loop->first ? 'active': ''}}" id="faq{{$faq->id}}-tab" data-toggle="tab" href="#faq{{$faq->id}}" role="tab" aria-controls="faq{{$faq->id}}" aria-selected="true">{{$faq->title}}</a> --}}
                            <li class="nav-item mb-1">
                                <a class="nav-link edit {{ $loop->first ?  'active' : '' }}" id="faq{{$faq->id}}-tab" data-toggle="tab" href="#faq{{$faq->id}}"
                                role="tab" aria-controls="faq{{$faq->id}}" aria-selected="true">
                                <span class="text">{{$faq->title}}</span><button data-id="{{$faq->id}}" class="btn_edit btn_option"><i class="far fa-edit"></i></button>
                                </a>
                                <a class="nav-link save" id="faq{{$faq->id}}-tab" data-toggle="tab" href="#faq{{$faq->id}}"
                                   role="tab" aria-controls="faq{{$faq->id}}" aria-selected="true">
                                   <form method="post" class="form-role-setting">
                                       @csrf
                                       <input type="text" name="title" class="role_name" value="{{$faq->title}}" id="">
                                   </form>
                                   <button data-id="{{$faq->id}}" data-action="{{route('settings.faq.updateFaqTitle',$faq->id)}}" class="btn_save btn_option"><i class="far fa-edit"></i></button>
                                </a>
                            <p class="error mt-1"></p>
                            </li>
                            @endforeach
                            @endif
                      </ul>

                      <div class="create-title mt-2">
                          <form action="{{route('settings.faq.store')}}" method="post">
                              @csrf
                              <input type="text" name="title" class="form-control mb-1" id="" placeholder="@lang("settings.placeholder_question")">
                              @if($errors->has('title'))
                              <p class="error mt-1">{{$errors->first('title')}}</p>
                              @endif
                              <button type="submit"><i class="fas fa-plus-circle"></i> @lang("settings.btn_add_faq")</button>
                          </form>
                      </div>
                      
                </div>
            </div>
        </div>
        <div class="col-md-8 right">
            <div class="card">
                <div class="tab-content" id="myTabContent">
                    @php
                    @endphp
                    @if(count($faqs)?? [])
                    @foreach($faqs as $i => $faq)
                   
                    <div class="tab-pane fade show {{$loop->first ? 'active': ''}}" id="faq{{$faq->id}}" role="tabpanel" aria-labelledby="faq{{$faq->id}}-tab">
                        <form  method="POST" id="faq-form">
                            @csrf
                            <div class="item mb-3">
                                <label for="">@lang("settings.answer") <span class="required-icon"> *</span></label>
                                <textarea name="content" id="" rows="6" placeholder=@lang("settings.answer") class="form-control faq-text aiz-text-editor">{{$faq->content}}</textarea>
                                    <p class="error mt-1"></p>
                            </div>
                            <div class="btn-option-faq">
                                <a href="javascript:void(0)" data-id="{{optional($faq)->id}}" class="btn-destroy btn" >{{__('settings.btn_delete')}}</a>
                                <a href="javascript:void(0)" data-id="{{$faq->id}}" class="btn-role btn btn-info" id="btn-faq" data-action="{{route('settings.faq.update',$faq->id)}}">{{__('settings.btn_save')}}</a>
                            </div>
                        </form>
                    </div>
                    @endforeach
                    @endif
                  </div>
                
            </div>
        </div>
    </div>
</div>

@endsection


@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            $(document).on('click','.btn_option',function(){
                $('.title-field .nav-item').removeClass('show');
                $(this).parents('.nav-item').toggleClass('show');
            })
            $('.nav-item .error').hide();
            $(document).on('click','.btn_save',function(){
                var id = $(this).data('id');
                url = $(this).data('action');
                var name = $(this).parents('.save').find('.role_name').val();
                var err_msg = $(this).parents('.nav-item').find('.error');
                $.ajax({
                    url: url,
                    type: "POST",
                    data : {
                        "_token" : "{{csrf_token()}}",
                        "title" : name
                    },
                    statusCode : {
                        200 : (res) => {
                            $(this).parents('.nav-item').find('.nav-link .text').html(name);
                            AIZ.plugins.notify('success', "{{__('settings.msg_question_success')}}");
                            err_msg.hide();
                            $(this).parents('.nav-item').find('.edit').click();
                        },
                        422 : (err) => {
                            err_msg.show()
                            var errx = JSON.parse(err.responseText);
                            console.log(errx);
                            var errrs = errx.errors.title[0];
                            err_msg.html(errrs);
                        }
                    },
                    success : function(res) {
                        console.log(res);
                    },
                    error : function(err) {
                    }
                }) 
            })

            $(document).on('click','#btn-faq',function(){
                var value = $(this).parents('form').find('.faq-text');
                var err_msg = $(this).parents('form').find('.error');
                var faq_val = value.val();
                var url = $(this).data('action');
                var id_faq = $(this).data("id");
                data = {
                        "_token" : "{{csrf_token()}}",
                        "content" : faq_val
                    }
                $.ajax({
                    url : url,
                    type : "POST",
                    data : data,
                    success : function(res) {
                        console.log(res);
                            AIZ.plugins.notify('success', "{{__('settings.msg_answer_success')}}");
                            err_msg.html('');
                        
                    },
                    error : function(err) {
                        if(err) {
                            var errx = JSON.parse(err.responseText);
                            var errrs = errx.errors.content[0];
                            console.log(errx);
                            err_msg.html(errrs);
                        }
                    }
                })
            })
            $(document).on('click', '.btn-destroy', function() {
                let delete_id= $(this).attr('data-id');
                Swal.fire({
                    title: "{{__('settings.popup_delete')}}",
                    text: "{{__('settings.confirm_popup')}}",
                    // icon: 'error',
                    confirmButtonText: "{{__('settings.yes')}}",
                    cancelButtonText: "{{__('settings.no')}}",
                    showCancelButton: true,
                    showCloseButton: true,

                }).then((result) => {
                    if (result.isConfirmed) {
                        var data = {
                            "_token": "{{ csrf_token() }}",
                            "id": delete_id,
                        };
                        $.ajax({
                            type: "DELETE",
                            url: '/admin/settings/faq/destroy/'+delete_id,
                            data: data,
                            success: function (response){
                                /* Read more about isConfirmed, isDenied below */
                                location.reload();
                                
                            },
                            error : function(err) {
                                Swal.fire('Changes are not saved', '', 'info')
                            }
                        });
                    }
                });
            });
        })
    </script>
@endsection
