@extends('backend.layouts.app')
@section('title')
@lang('members.members')
@endsection
@section('content')
<div class="aiz-titlebar text-left mt-2 mb-3">
    <form action="" method="GET">
        <div class="align-items-center">
            <h1 class="h3"><strong>@lang('members.members')</strong></h1>
        <div class="row mb-2">
            <div class="col-md-9 d-flex">
                <img src="{{ static_asset('assets/img/icons/search.svg') }}" class="img-search3 mr-2" alt="Search">
                <input type="text" class="form-control res-placeholder res-FormControl" id="search" name="search" value="{{request('search')}}" placeholder="@lang('members.search_place')">
            </div>
            <div class="col-md-3 text-md-right res-FormControl notifi-padding">
                <a href="{{ route('members.create') }}" class="btn btn-circle btn-info btn-add-product d-flex">
                    <i class="fas fa-plus-circle img-redo mr-2"></i>
                    <span class="custom-FontSize">@lang('members.add')</span>
                </a>
            </div>
        </div>
        <div class="row gutters-5 mb-5">
            <div class="col-md-4 ">
                <!-- <input type="date" value="{{translate('Add New Product')}}"  class="form-control form-control-sm aiz-selectpicker mb-2 mb-md-0" id="status6" name="status6"> --> 
                <!-- <input type="text" onfocus="(this.type='date')" class="form-control custom-placeholder" id="joined_date35" name="joined_date"  value="{{ request('joined_date') }}" placeholder="@lang('members.joined')"> -->
                <input type="text" onkeypress="return event.charCode>=48 && event.charCode<=57" autocomplete="off"
                class="form-control custom-placeholder res-FormControl"
                  id="joined_date" name="joined_date" value="{{request()->get('joined_date')}}" placeholder="@lang('members.joined')">
                <div class="custom-down"><i class="fas fa-chevron-down"></i></div>

            </div>
            <div class="col-md-4 res-status">
                <select class="form-control form-control-sm aiz-selectpicker res-FormControl mb-md-0 font-weight-800" id="" name="status">
                    <option value="">@lang('members.status')</option>
                    <option value="1" {{request()->status == 1 ? 'selected' : ''}}>@lang('members.active')</option>
                    <option value="2" {{request()->status == 2 ? 'selected' : ''}}>@lang('members.inactive')</option>
                    <option value="3" {{request()->status == 3 ? 'selected' : ''}}>@lang('members.blocked')</option>
                </select>
                
            </div>
            <div class="col-md all-btn-search">
                <button type="submit" class="pl-0 pr-0 btn btn-info w-32 mr-2 d-flex btn-responsive justify-content-center">
                    <i class="fas fa-search img-redo mr-2"></i>
                    <span class="custom-FontSize">@lang('members.btn_search')</span>
                </button>
                <a href="{{ route('members.index') }}" class="pl-0 pr-0 w-32 btn btn-info ml-2 mr-2 d-flex btn-responsive justify-content-center">
                     <i class="fas fa-redo-alt img-redo mr-2"></i>
                    <span class="custom-FontSize">@lang('members.btn_reset')</span>
                </a>
                <?php 
                $request = request()->all();
                $newRequest = http_build_query($request);
                ?>
                <a href="{{ route('members.export') . '?' . $newRequest }}" class="pl-0 pr-0 btn btn-info w-34 ml-2 d-flex btn-responsive justify-content-center">
                    <i class="fas fa-cloud-download-alt img-redo mr-2"></i>
                    <span class="custom-FontSize">@lang('members.excel')</span>
                </a>
            </div>
        </div>
    </form>
</div>
<div class="card">
    <div class="custom-overflow">
                <table class="table aiz-table mb-0">
                    <thead>
                        <tr>
                            <th class="">@lang('members.id')</th>
                            <th class="">@lang('members.name')</th>
                            <th class="">@lang('members.email')</th>
                            <th class="">@lang('members.phone')</th>
                            <th class="">@lang('members.role')</th>
                            <th class="">@lang('members.joined')</th>
                            <th class="">@lang('members.status')</th>
                            <th class="text-right">{{translate('')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($members) && !empty($members))
                        @foreach($members as $member)
                            <tr>
                                    <th>{{optional($member)->id}}</th>
                                    <th class="font-weight-500 d-flex align-items-center">
                                    @if(optional($member)->avatar)
                                    <img src='{{static_asset(optional($member)->avatar)}}' class="img-ava mr-2" alt="Search">
                                    <span class="name">{{optional($member)->name}}</span>
                                    @else 
                                    <span class="empty_image mr-2">
                                        {{optional($member)->name[0]}}
                                    </span>
                                    <span class="name">{{optional($member)->name}}</span>
                                    @endif
                                    </th>
                                    <th class="font-weight-500">{{optional($member)->email}}</th>
                                    <th class="font-weight-500">
                                        @php
                                        echo (substr(optional($member)->phone, 0, 3) . "-" ) . (substr(optional($member)->phone, 3, 4) . "-" ) . (substr(optional($member)->phone, 7, 4));
                                        @endphp
                                    </th>
                                    <th class="font-weight-500">{{optional($member)->role->name ?? 'Not Role'}}</th>
                                    <th class="font-weight-500">{{date('Y/m/d', strtotime(optional($member)->created_at))}}</th>
                                    <th class="font-weight-500">{{optional($member)->statusText()}}</th>
                                    <th class="text-right p-0">
                                        <div class="dropdown">
                                            <button class="btn btn-drop mt-1" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-h"></i>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="{{route('members.edit',optional($member)->id)}}">@lang('members.view')</a>
                                                {{-- <a class="dropdown-item" href="{{route('members.status',$member->id)}}">{{$member->checkStatus()}}</a> --}}
                                                <div class="changeStatus">
                                                    <form class="mr-2">
                                                        @csrf
                                                        <input type="hidden" id="status" name="status" value="{{ optional($member)->status == 1? '2':'1'}}">
                                                        <a class="dropdown-item toggle-active change-status" href="javascript:void(0)" data-id="{{optional($member)->id}}" type="sum" >
                                                            @if (optional($member)->status==2)
                                                                @lang('members.active')
                                                            @else
                                                                @lang('members.inactive')
                                                            @endif
                                                        </a>
                                                    </form>
                                                </div>
                                                <a class="dropdown-item delete-member" data-id ="{{optional($member)->id}}" href="javascript:void(0)">@lang('members.delete')</a>
                                            </div>
                                        </div>
                                    </th>
                                </a>
                            </tr> 
                        @endforeach
                        @endif
                    </tbody>
                </table>
    </div>
</div>
</div>
<div class="aiz-pagination paginationOrder">
    {{ optional($members)->appends(request()->input())->links() }}
</div> 
@endsection

@section('script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>  
<script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script type="text/javascript">
    $('.custom-drop').click(function(){
        // alert();
        $(this).find('.custom-ul').toggle('d-none');
    })

    // Delete member
    $(document).on('click', '.delete-member', function() {
        var delete_id= $(this).attr('data-id');
        Swal.fire({
            title: "{{__('members.delete_member')}}",
            text: "{{__('members.confirm_popup')}}",
            // icon: 'error',
            confirmButtonText: "{{__('members.yes')}}",
            cancelButtonText: "{{__('members.no')}}",
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": delete_id,
                };
                $.ajax({
                    type: "DELETE",
                    url: '/admin/members/destroy/'+delete_id,
                    data: data,
                    success: function (response){
                        /* Read more about isConfirmed, isDenied below */
                        location.reload();
                            
                    },
                    error : function(err) {
                        // Swal.fire('Changes are not saved', '', 'info');
                    }
                });
            }
        });
    });
    $(document).on('click', '.change-status', function() {
        var status_id = $(this).attr('data-id');
        var status = $('#status').val();
        Swal.fire({
            title: "{{__('members.change_status')}}",
            text: "{{__('members.confirm_popup')}}",
            // icon: 'error',
            confirmButtonText: "{{__('members.yes')}}",
            cancelButtonText: "{{__('members.no')}}",
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": status_id,
                    "status" : status
                };
                console.log(data);
                $.ajax({
                    type: "POST",
                    url: '/admin/members/status/'+status_id,
                    data: data,
                    success: function (response){
                        /* Read more about isConfirmed, isDenied below */
                        location.reload();
                    },
                    error : function(err) {
                        // Swal.fire('Changes are not saved', '', 'info');
                    }
                });
            }
        });
    });
    
    $('#joined_date').daterangepicker({
        singleDatePicker: true,
        autoUpdateInput: false,
        minDate: '1921/01/01',
        showDropdowns: true,
        locale: {
            format: 'YYYY/MM/DD',
            applyLabel: "대다",
            cancelLabel: "취소",
                "monthNames":[
                    "일월" , // Tháng 1
                    "이월" , // Tháng 2
                    "삼월" , // Tháng 3
                    "사월" , // Tháng 4
                    "오월" , // Tháng 5
                    "유월" , // Tháng 6
                    "칠월" , // Tháng 7
                    "팔월" , // Tháng 8
                    "구월" , // Tháng 9
                    "시월" , // Tháng 10
                    "십일월" , // Tháng 11
                    "십이월" , // Tháng 12
                ],
                daysOfWeek:[
                    "월요일" ,// Thứ 2
                    "화요일" ,// Thứ 3
                    "수요일" ,// Thứ 4
                    "목요일" ,// Thứ 5
                    "금요일" ,// Thứ 6
                    "토요일" ,// Thứ 7
                    "일요일" ,//  Chủ nhật.
                ]
        }
    });
    $('#joined_date').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('YYYY/MM/DD'));
    });

    $('#joined_date').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });

    $('input[name="joined_date"]').val('{{ request()->get('joined_date') }}');

</script>
@endsection
