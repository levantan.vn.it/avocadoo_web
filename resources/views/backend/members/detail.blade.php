@extends('backend.layouts.app')
@section('title')
@lang('members.member_detail')
@endsection
@section('content')
@php
    $trans = ["Dashboard" => __('members.dashboard'),
    "Users" => __('members.users'),
    "Orders" => __('members.orders'),
    "Suppliers" => __('members.suppliers'),
    "Products" => __('members.products'),
    "Categories" => __('members.categories'),
    "Collections" => __('members.collections'),
    "Banners" => __('members.banners'),
    "Notifications" => __('members.notifications'),
    "Messages" => __('members.messages'),
    "Members" => __('members.members'),
    "Settings" => __('members.settings')
    ]
@endphp
<div class="mb-4">
{{-- @dump($errors) --}}
    <div class="backpage mb-5">
        <a href="{{url()->previous()}}" class="back btn"><i class="fas fa-chevron-left"></i></a>
    </div>
</div>
<div class="row  m-0 justify-content-space-between">
    <div class="row m-0">
        {{-- <div>
            <img src='{{asset("$member->avatar")}}' class="img-ava2 mr-2" alt="Search">
        </div> --}}
        <div class="image-avt">
            <input type="file" name="avatar" id="avatar" class="d-none" accept="image/*">
            
            @if(optional($member)->avatar)
            <img src='{{static_asset(optional($member)->avatar)}}' id="img_avt" class="img-ava2 mr-2" alt="Avatar">
            @else 
            <span class="empty_image2 mr-2">
                {{optional($member)->name[0]}}
            </span>
            @endif
            <div class="overlay-avt"><span>{{__('members.upload_image')}}</span></div>
        </div>
        <div>
            <p class="mb-0 font-weight-800 font-size-18"><strong>#{{optional($member)->id}}</strong> </p>
            <p class="mb-1 font-weight-800 font-size-18"><strong>{{optional($member)->name}}</strong></p>
            @if(optional($member)->status == 1)
            <label class="label-status active">{{$member->statusText()}}</label>
            @elseif(optional($member)->status == 2)
            <label class="label-status inactive">{{$member->statusText()}}</label>
            @else 
            <label class="label-status blocked">{{$member->statusText()}}</label>
            @endif
        </div>
    </div>
    <div class="all-BtnReset d-flex">
        @if(optional($member)->id == Auth::user()->id)
        <button type="submit" class="btn btn-info mr-2 btn-resetpw"><i class="fas fa-redo-alt img-redo "></i></button>
        @endif
        <form action="{{ route('members.status',$member->id)}}" method="POST" class="mr-2">
            @csrf
            {{-- <input type="hidden" id="status" name="status" value="{{ optional($member)->checkStatus()}}"> --}}
            @if (optional($member)->status == 3)
            <button type="button" title="@lang('members.unlock')" class="btn btn-info btn-unlock" data-id="{{optional($member)->id}}"><i class="fas fa-unlock-alt"></i></i></i></button>
            @else
            <button type="button" title="@lang('members.lock')" class="btn btn-info btn-lock" data-id="{{optional($member)->id}}"><i class="fas fa-lock img-redo "></i></button>
            @endif
        </form>
        <a href="javascript:void(0)"  class="btn btn-info delete-member" data-id="{{optional($member)->id}}"><i class="fas fa-trash-alt"></i></a>
    </div>
</div>
<div class="mt-3 row ml-0 mr-0">
    <div class="col-md-4 detail-content1">
        <a href="javascript:void(0)" class="content1-img members-information">
            <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail" alt="Search">
        </a>
        <div class="all-content1">
            <div>
                <p class="font-weight-800">@lang('members.member_id')</p>
                <p>#{{optional($member)->id}}</p>
            </div>
            <div>
                <p class="font-weight-800">@lang('members.noti-name')</p>
                <p>{{optional($member)->name}}</p>
            </div>
            <div>
                <p class="font-weight-800">@lang('members.email')</p>
                <p>{{optional($member)->email}}</p>
            </div>
            <div>
                <p class="font-weight-800">@lang('members.noti-date')</p>
                <p>{{date('Y/m/d',strtotime(optional($member)->birthday))}}</p>
            </div>
            
            <div>
                <p class="font-weight-800">@lang('members.phone')</p>
                <p>
                @php
                echo (substr(optional($member)->phone, 0, 3) . "-" ) . (substr(optional($member)->phone, 3, 4) . "-" ) . (substr(optional($member)->phone, 7, 4));
                @endphp
                </p>
            </div>
            <div>
                <p class="font-weight-800">@lang('members.noti-address')</p>
                <p>{{optional($member)->address}}</p>
            </div>
            <div>
                <p class="font-weight-800">@lang('members.noti-id')</p>
                <p>{{optional($member)->id_number}}</p>
            </div>
            <div>
                <p class="font-weight-800">@lang('members.noti-date2')</p>
                <p>{{date('Y/m/d', strtotime($member->date_of_issue))}}</p>
            </div>
        </div>
    </div>
    <div class="col-md-4 detail-content1">
        <a href="#" class="content1-img members-role">
            <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail" alt="Search">
        </a>
        <div class="all-content1">
            <div>
                <p class="font-weight-800">@lang('members.role')</p>
                <p>{{optional($member->role)->name}}</p>
            </div>
            <div>
                <p class="font-weight-800">@lang('members.noti-access')</p>
                @php
                    $pers = json_decode(optional($member->role)->permissions) ?? [];
                    $arr = [];
                    foreach($pers as $per) {
                        $arr[] = $trans[$per];
                    }
                    if(count($arr) == 1)
                    $access = implode("",$arr);
                    elseif(count($arr) >= 2)
                    $access = implode(", ",$arr);
                    else
                    $access = 'Not have access';                    
                @endphp
                {{-- @dump($arr) --}}
                <p>{{$access}}</p>
                
            </div>
            <div>
                <p class="font-weight-800">@lang('members.noti-joined')</p>
                <p>{{date('Y/m/d', strtotime($member->created_at))}}</p>
            </div>
        </div>

    </div>
    <div class="col-md detail-content2">
    </div>
</div> 

@endsection

@section('modal')
    @include('templates.popup.members-role',['edit'=> true])
    @include('templates.popup.members-information',['edit' => true])
    @include('templates.popup.members-changepw')
    @include('templates.popup.upload-img-member',['edit'=> true])
@endsection

@section('script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>  
<script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    @error('ChangePasswordRequest')
            document.querySelector('.members-changepw').classList.add('show');
    @enderror
    $('.select_role').on('change',function(e){
                var role_id  = e.target.value ;
                console.log(role_id);
                $('.access_edit').hide();
                $('.access').removeClass('active');
                $('.access').removeClass('show')
                $('#'+ role_id).addClass('active');
                
            })
            $('.btn-resetpw').click(function(){
                $('.members-changepw').addClass('show');
            })
})
        $('.members-information').click(function() {
            let url = $(this).attr('href');
            $('.members-informations').addClass('show');
        });
        $('.members-role').click(function() {
            let url = $(this).attr('href');
            $('.members-roles').addClass('show');
        });
        //
        function myFunction() {
            document.getElementById("status6");
        }
        function myFunction() {
            document.getElementById("status7");
        }
        function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
            $('#img_avt').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
        }

        $("#avatar").change(function() {
            readURL(this);
        });
        $('.overlay-avt').click(function(){
            $('.mark-member-img').addClass('show');
        })
        
        // Delete member
    $(document).on('click', '.delete-member', function() {
        var delete_id= $(this).attr('data-id');
        Swal.fire({
            title: "{{__('members.delete_member')}}",
            text: "{{__('members.confirm_popup')}}",
            // icon: 'error',
            confirmButtonText: "{{__('members.yes')}}",
            cancelButtonText: "{{__('members.no')}}",
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": delete_id,
                };
                $.ajax({
                    type: "DELETE",
                    url: '/admin/members/destroy/'+delete_id,
                    data: data,
                    success: function (response){
                        /* Read more about isConfirmed, isDenied below */
                        location.href = '/admin/members';
                    },
                    error : function(err) {
                        Swal.fire('Changes are not saved', '', 'info');
                    }
                });
            }
        });
    });

    $('#created_at').val('{{date('Y/m/d',strtotime($member->created_at)) }}');

    $('#member-issue').daterangepicker({
            singleDatePicker: true,
            minDate: '1921/01/01',
            autoUpdateInput: false,
            drops:'up',
            showDropdowns: true,
            locale: {
                format: 'YYYY/MM/DD',
            applyLabel: "대다",
            cancelLabel: "취소",
                "monthNames":[
                    "일월" , // Tháng 1
                    "이월" , // Tháng 2
                    "삼월" , // Tháng 3
                    "사월" , // Tháng 4
                    "오월" , // Tháng 5
                    "유월" , // Tháng 6
                    "칠월" , // Tháng 7
                    "팔월" , // Tháng 8
                    "구월" , // Tháng 9
                    "시월" , // Tháng 10
                    "십일월" , // Tháng 11
                    "십이월" , // Tháng 12
                ],
                daysOfWeek:[
                    "월요일" ,// Thứ 2
                    "화요일" ,// Thứ 3
                    "수요일" ,// Thứ 4
                    "목요일" ,// Thứ 5
                    "금요일" ,// Thứ 6
                    "토요일" ,// Thứ 7
                    "일요일" ,//  Chủ nhật.
                ]
            }
        });

        $('#member-issue').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('YYYY/MM/DD'));
        });

        $('#member-issue').val('{{ date('Y/m/d', strtotime(optional($member)->date_of_issue)) }}');

        $('#member-issue').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });

    $('#birthday').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            minDate: '1921/01/01',
            drops:'up',
            locale: {
                format: 'YYYY/MM/DD',
            applyLabel: "대다",
            cancelLabel: "취소",
                "monthNames":[
                    "일월" , // Tháng 1
                    "이월" , // Tháng 2
                    "삼월" , // Tháng 3
                    "사월" , // Tháng 4
                    "오월" , // Tháng 5
                    "유월" , // Tháng 6
                    "칠월" , // Tháng 7
                    "팔월" , // Tháng 8
                    "구월" , // Tháng 9
                    "시월" , // Tháng 10
                    "십일월" , // Tháng 11
                    "십이월" , // Tháng 12
                ],
                daysOfWeek:[
                    "월요일" ,// Thứ 2
                    "화요일" ,// Thứ 3
                    "수요일" ,// Thứ 4
                    "목요일" ,// Thứ 5
                    "금요일" ,// Thứ 6
                    "토요일" ,// Thứ 7
                    "일요일" ,//  Chủ nhật.
                ]
            }
        });
        $('#birthday').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('YYYY/MM/DD'));
        });
        $('#birthday').val('{{ date('Y/m/d', strtotime(optional($member)->birthday)) }}');


        $('#birthday').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });
        $(document).on('click', '.btn-lock', function() {
        var status_id = $(this).attr('data-id');
        var status = $('#status').val();
        Swal.fire({
            title: "{{__('members.block_member')}}",
            text: "{{__('members.confirm_popup')}}",
            // icon: 'error',
            confirmButtonText: "{{__('members.yes')}}",
            cancelButtonText: "{{__('members.no')}}",
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": status_id
                };
                $.ajax({
                    type: "POST",
                    url: '/admin/members/block/'+status_id,
                    data: data,
                    success: function (response){
                        /* Read more about isConfirmed, isDenied below */
                            location.reload();
                    },
                    error : function(err) {
                        Swal.fire('Changes are not saved', '', 'info')
                    } 
                });
            }
        });
    });
    $(document).on('click', '.btn-unlock', function() {
        var status_id = $(this).attr('data-id');
        var status = $('#status').val();
        Swal.fire({
            title: "{{__('members.unlock_member')}}",
            text: "{{__('members.confirm_popup')}}",
            // icon: 'error',
            confirmButtonText: "{{__('members.yes')}}",
            cancelButtonText: "{{__('members.no')}}",
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": status_id
                };
                $.ajax({
                    type: "POST",
                    url: '/admin/members/block/'+status_id,
                    data: data,
                    success: function (response){
                        /* Read more about isConfirmed, isDenied below */
                        location.reload();
                         },
                    error : function(err) {
                        console.log(err);
                        Swal.fire('Changes are not saved', '', 'info');
                    }
                });
            }
        });
    });

    $(document).on('click','.btn-update-infor',function(){
            var url = $(this).data('action');
            console.log(url)
            var name = $('#name').val();
            var email = $('#email').val();
            var phone = $('#phone').val();
            var birthday = $('#birthday').val();
            var address = $('#address').val();
            var id_number = $('#id_number').val();
            var member_issue = $('#member-issue').val();
            var data = {
                "token" : "{{csrf_token()}}",
                "name" : name,
                "email": email,
                "phone": phone,
                "birthday": birthday,
                "address": address,
                "id_number": id_number,
                "date_of_issue" : member_issue
            }
            $.ajax({
                url : url,
                data: data,
                type : "POST",
                success : function(res) {
                    $('.members-informations').removeClass('show');
                    $('.error').hide();
                    setTimeout( 
                    function() {
                        window.location.reload(true);
                    }, 2000);
                    AIZ.plugins.notify('success', "{{__('members.msg_infor_success')}}");
                },
                error : function(err) {
                    var response = JSON.parse(err.responseText);
                    console.log(response.errors)
                    printErrorMsg(response.errors)
                }
            })

            function printErrorMsg (msg) {
            $.each( msg, function( key, value ) {
            console.log(key);
              $('.'+key+'_err').text(value);
                });
            }
        })


        $(document).on('click','.btn-update-roles',function(){
            var url = $(this).data('action');
            var role_id = $('#role_id').val();
            var date = $('#created_at').val();
            var created_at = date.replace(/\//g,'-')
            var data = {
                "token" : "{{csrf_token()}}",
                "role_id" : role_id,
                "created_at": created_at,
            }
            $.ajax({
                url : url,
                data: data,
                type : "POST",
                success : function(res) {
                    $('.members-roles').removeClass('show');
                    $('.error').hide();
                    AIZ.plugins.notify('success', "{{__('members.msg_roles_success')}}");
                    setTimeout( 
                    function() {
                        window.location.reload(true);
                    }, 2000);
                },
                error : function(err) {
                    var response = JSON.parse(err.responseText);
                    console.log(err)
                    printErrorMsg(response.errors)
                }
            })

            function printErrorMsg (msg) {
            $.each( msg, function( key, value ) {
            console.log(key);
              $('.'+key+'_err').text(value);
                });
            }
        })

        $(document).on('click','.btn-saveimg',function(){
                var url = $(this).data('action');
                const parent = $(this).parents('form');
                $.ajax({
                    url : url,
                    data: parent.serialize(),
                    type : "POST",
                    statusCode : {
                        200 : () => {
                            parent.find('.error').remove();
                        },
                        422 : ({responseJSON}) => {
                            parent.find('.error').remove();
                            const e = Object.entries(responseJSON.errors);
                            // console.log(e);
                            for (const [key,val] of e) {
                                parent.find(`[name="${key}"]`).parents('.row').after(`
                                    <div class="error">${val}</div>
                                `)
                            }
                        }
                    },
                    success : function(res) {
                        parent.submit();
                        $('.mark-member-img').removeClass('show');
                        
                    },
                    error : function(err) {
                        console.log(err)
                    }
                })
            })
  
</script>

@endsection