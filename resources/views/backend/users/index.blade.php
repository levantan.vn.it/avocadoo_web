@extends('backend.layouts.app')
@section('title')
@lang('users.users')
@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection

@section('content')
<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="align-items-center">
		<h1 class="h3"><strong>@lang('users.users')</strong></h1>
	</div>
</div>
<form class="" id="sort_customers" action="" method="GET">
    <div class="row mb-2">
        <div class="col-md-12">
            <img src="{{ static_asset('assets/img/icons/search.svg') }}" class="img-search4 mr-2" alt="Search">
            <input type="text" class="form-control res-placeholder2" id="search" name="search" value="{{ request('search') }}" placeholder="@lang('users.search_id')">
        </div>
    </div>
    <div class="row gutters-5 mb-5">
        <div class="col-md-4">
            <input type="text" autocomplete="off" onkeypress='return event.charCode >= 48 && event.charCode <= 57'
            class="form-control custom-placeholder" id="joined_date" name="joined_date"
            placeholder="{{__('users.joined_date')}}" value="{{ request('joined_date') }}"/>
            <div class="custom-down"><i class="fas fa-chevron-down"></i></div>
        </div>
        <div class="col-md-4 res-status">
            <select class="form-control form-control-sm aiz-selectpicker mb-2 mb-md-0 font-weight-800" id="status" name="status">
                <option value="">@lang('users.status')</option>
                <option value="1" {{ request('status') == "1" ? 'selected' : '' }}>@lang('users.active')</option>
                <option value="2" {{ request('status') == "2" ? 'selected' : '' }}>@lang('users.deactive')</option>
                <option value="3" {{ request('status') == "3" ? 'selected' : '' }}>@lang('users.block')</option>
            </select>
        </div>
        <div class="col-md all-btn-search">
            <button type="submit" class="pl-0 pr-0 btn btn-info w-32 mr-2 d-flex  btn-responsive justify-content-center">
                <i class="fas fa-search img-redo mr-2"></i>
                <span class="custom-FontSize">@lang('users.search')</span>
            </button>
            <a href="{{ route('users.index') }}" class="pl-0 pr-0 btn btn-info w-32 mr-2 d-flex  btn-responsive justify-content-center">
                <i class="fas fa-redo-alt img-redo mr-2"></i>
                <span class="custom-FontSize">@lang('users.reset')</span>
            </a>
            <?php
                $request = request()->all();
                $newRequest = http_build_query($request);
            ?>
            <a href="{{ route('users.export') . '?' . $newRequest }}" class="pl-0 pr-0 btn btn-info w-32 mr-2 d-flex  btn-responsive justify-content-center">
                <i class="fas fa-cloud-download-alt img-redo mr-2"></i>
                <span class="custom-FontSize">@lang('users.excel')</span>
            </a>
        </div>
    </div>
</form>

<div class="card">
    <div class="card-body-table ">
        <table class="table aiz-table mb-0 table-hover ">
            <thead>
                <tr  >
                    <th>@lang('users.id')</th>
                    <th>@lang('users.display')</th>
                    <th>@lang('users.email')</th>
                    <th class="w-200">@lang('users.phone')</th>
                    <th>@lang('users.business_name')</th>
                    <th>@lang('users.joined_date')</th>
                    <th class="w-98">@lang('users.status')</th>
                    <th class="text-right w-60">@lang('')</th>
                </tr>
            </thead>
            <tbody>
                @if (!empty($customers) && count($customers))
                    @foreach ($customers as $item)
                        <tr>
                            <th class="font-weight-500 align-middle" font-weight-800><strong>#{{$item->id}}</strong></th>
                            <th class="font-weight-500 d-flex align-items-center br-top">
                                @if(optional($item)->avatar)
                                    <img src='{{ my_asset($item->avatar)}}' class="img-ava mr-2" alt="">
                                    <span class="name text-over-avt">
                                        {{optional($item)->displayname}}
                                    </span>
                                @else
                                    <span class="empty_image mr-2">
                                        {{$item->displayname[0]??'A'}}
                                    </span>
                                    <span class="name text-over-avt" >
                                        {{optional($item)->displayname}}
                                    </span>
                                @endif
                            </th>
                            <td class="font-weight-500 align-middle">
                                {{$item->email}}
                            </td>
                            <td class="font-weight-500 align-middle">
                                {{-- {{$item->phone}} --}}
                                <?php
                                $number = $item->phone;
                                echo (substr($number, 0, 3) . "-" ) . (substr($number, 3, 4) . "-" ) . (substr($number, 7, 4));
                                ?>
                            </td>
                            <td class="font-weight-500 align-middle text-over">
                                <span class="text-over2">
                                    {{$item->business_name}}
                                </span>
                            </td>
                            <td class="font-weight-500 align-middle">
                                {{$item->created_at->format('Y/m/d')}}
                            </td>
                            <td class="font-weight-500 align-middle">
                                {{$item->statusText()}}
                            </td>
                            <input type="hidden" name="" class="serdelete_val" value="{{$item->id}}">
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-drop" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-h"></i>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="{{ route('users.detail', ['id'=>$item->id]) }}">@lang('users.view')</a>
                                    <div>
                                        <form action="{{ route('users.updatestatus',$item->id)}}" method="POST" class="mr-2" id="form-active-user">
                                            <input type="hidden" name="status" value="{{ $item->status == 1? '2':'1'}}">
                                                @csrf
                                                @if ($item->status==2)
                                                    <a class="dropdown-item" href="#" id="active-popup"> @lang('users.active')</a>
                                                @else
                                                    <a class="dropdown-item" href="#" id="inactive-popup"> @lang('users.deactive')</a>
                                                @endif
                                            </a>
                                        </form>
                                    </div>
                                        <a class="dropdown-item delete-user" href="javascript:void(0)" data-delete="{{$item->id}}">@lang('users.del')</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>
<div class="pagination-us">
    <div class="aiz-pagination paginationOrder">
        {{ $customers->appends(request()->input())->links() }}
    </div>
</div>

<div class="modal fade " id="confirm-ban">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title h6">{{translate('Confirmation')}}</h5>
                <button type="button" class="close" data-dismiss="modal"></button>
            </div>
            <div class="modal-body">
                <p>{{translate('Do you really want to ban this Customer?')}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">{{translate('Cancel')}}</button>
                <a type="button" id="confirmation" class="btn btn-primary">{{translate('Proceed!')}}</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="confirm-unban">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title h6">{{translate('Confirmation')}}</h5>
                <button type="button" class="close" data-dismiss="modal"></button>
            </div>
            <div class="modal-body">
                <p>{{translate('Do you really want to unban this Customer?')}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">{{translate('Cancel')}}</button>
                <a type="button" id="confirmationunban" class="btn btn-primary">{{translate('Proceed!')}}</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
    @include('modals.delete_modal')
@endsection

@section('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript">

    $('.mightOverflow').each(function() {
        var $ele = $(this);
        if (this.offsetWidth < this.scrollWidth)
            $ele.attr('title', $ele.text());
    });

    function sort_customers(el){
        $('#sort_customers').submit();
    }
    function confirm_ban(url)
    {
        $('#confirm-ban').modal('show', {backdrop: 'static'});
        document.getElementById('confirmation').setAttribute('href' , url);
    }

    function confirm_unban(url)
    {
        $('#confirm-unban').modal('show', {backdrop: 'static'});
        document.getElementById('confirmationunban').setAttribute('href' , url);
    }
    // delete
    $(document).on('click', '.delete-user', function() {
        var delete_id= $(this).closest('tr').find('.serdelete_val').val();
        // alert(delete_id);
        Swal.fire({
            title: '@lang('users.del_user')',
            text: '@lang('users.continue')',
            // icon: 'error',
            confirmButtonText: '@lang('users.yes')',
            cancelButtonText: '@lang('users.no')',
            showCancelButton: true,
            showCloseButton: true,
        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": delete_id,
                };
                $.ajax({
                    type: "DELETE",
                    url: '/admin/users/destroy/'+ delete_id,
                    data: data,
                    success: function(response) {
                        location.reload()
                        // Swal.fire('@lang('users.deleted')', '', 'success').then(() => {
                        // });
                    },
                    error: function(err) {
                        // Swal.fire('@lang('users.deleted_er')', '', 'info');
                    }
                });
            }
        });
    });
// active popup
    $(document).on('click', '#active-popup', function() {
        Swal.fire({
            title: '@lang('users.active_user')',
            text: '@lang('users.continue')',
            // icon: 'error',
            confirmButtonText: '@lang('users.yes')',
            cancelButtonText: '@lang('users.no')',
            showCancelButton: true,
            showCloseButton: true,
        }).then((result) => {
            if (result.isConfirmed) {
                $(this).parents('form').submit();
            }
        });
    });
    $(document).on('click', '#inactive-popup', function() {
        Swal.fire({
            title: '@lang('users.inactive_user')',
            text: '@lang('users.continue')',
            // icon: 'error',
            confirmButtonText: '@lang('users.yes')',
            cancelButtonText: '@lang('users.no')',
            showCancelButton: true,
            showCloseButton: true,
        }).then((result) => {
            if (result.isConfirmed) {
                $(this).parents('form').submit();
            }
        });
    });
    //yyyy/mm/dd
    $('#joined_date').daterangepicker({
        autoUpdateInput: false,
        minDate: '1921/01/01',
        singleDatePicker: true,
        showDropdowns: true,

        locale: {
            format: 'YYYY/MM/DD',
            applyLabel: "대다",
            cancelLabel: "취소",
                "monthNames":[
                    "일월" , // Tháng 1
                    "이월" , // Tháng 2
                    "삼월" , // Tháng 3
                    "사월" , // Tháng 4
                    "오월" , // Tháng 5
                    "유월" , // Tháng 6
                    "칠월" , // Tháng 7
                    "팔월" , // Tháng 8
                    "구월" , // Tháng 9
                    "시월" , // Tháng 10
                    "십일월" , // Tháng 11
                    "십이월" , // Tháng 12
                ],
                daysOfWeek:[
                    "월요일" ,// Thứ 2
                    "화요일" ,// Thứ 3
                    "수요일" ,// Thứ 4
                    "목요일" ,// Thứ 5
                    "금요일" ,// Thứ 6
                    "토요일" ,// Thứ 7
                    "일요일" ,//  Chủ nhật.
                ]
        }
    });
    $('#joined_date').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('YYYY/MM/DD'));
    });

    $('#joined_date').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
    @if(request('joined_date'))
    $('#joined_date').value("{{request('joined_date')}}");
    @endif
    $('input[name="joined_date"]').val('');
    // $(document).on('click', ".toggle-active", function(e) {
    //     if ($item->status==2){
    //         Swal.fire({
    //             title: 'Active User',
    //             text: 'Do you want to continue',
    //             confirmButtonText: 'Yes',
    //             cancelButtonText: 'No',
    //             showCancelButton: true,
    //             showCloseButton: true,
    //         }).then((result) => {
    //             if (result.isConfirmed)
    //                 $('#form-active-user').submit();
    //         });
    //     } else {
    //         Swal.fire({
    //             title: 'Active User',
    //             text: 'Do you want to continue',
    //             confirmButtonText: 'Yes',
    //             cancelButtonText: 'No',
    //             showCancelButton: true,
    //             showCloseButton: true,
    //         }).then((result) => {
    //             if (result.isConfirmed)
    //                 $('#form-active-user').submit();
    //         });
    //     }
    // });

    // $(document).on('click', '.toggle-status', function(e) {
    //     e.preventDefault();
    //     const active = this.dataset.active;
    //     Swal.fire({
    //         title:  active? 'In Active User?' :'Active User?' ,
    //         text: "Are you sure?",
    //         icon: 'warning',
    //         showCancelButton: true,
    //         confirmButtonColor: '#3085d6',
    //         cancelButtonColor: '#d33',
    //         confirmButtonText: 'Yes, do it'
    //         }).then((result) => {
    //             if (result.isConfirmed)
    //             $.post(this.href).then(()=>{
    //                 location.reload();
    //             }).catch((e)=>{
    //             console.log('eerror ', e)
    //             })
    //         })
    // });


</script>
@endsection
