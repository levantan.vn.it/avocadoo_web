@extends('backend.layouts.app')
@section('title')
@lang('users.users_detail')
@endsection
@section('content')
<div class="backpage mb-5">
    <a href="{{url()->previous()}}" class="back btn"><i class="fas fa-chevron-left"></i></a>
</div>
<div class="row m-0 justify-content-space-between">
    <div class="row m-0 ">
        <div class="hihi img-ava2">
            @if($customers->avatar)
                <img class="img-ava3 align-middle mr-1" src="{{ my_asset($customers->avatar)}}" alt="">
            @else
                {{-- <button class="button img-ava3">{{$customers->displayname[0]}}</button> --}}
                <div class=" img-ava3">
                    <h1 class="hhhhhhhh vertical-align">
                        {{$customers->displayname[0]}}
                    </h1>
                </div>
            @endif
            {{-- <img src="{{ my_asset($customers->avatar) }}" class="img-ava2 mr-2" alt=""> --}}
        </div>
        <div class="avt-detail ml-1">
            <h6 class="mb-2 font-weight-bold">#{{$customers->id}}</h6>
            <h5 class="mb-2 font-weight-bold">{{$customers->displayname}} </h5>
            @if ($customers->status==1)
                <label class=" bg-success text-white lb-status-user" >@lang('users.active')</label>
            @else
                <label class=" bg-danger text-white lb-status-user" >@lang('users.deactive')</label>
            @endif
        </div>
    </div>
    <div class="all-BtnReset row ml-0 mr-0 ">
        <form action="{{ route('users.updatestatusblock',$customers->id)}}" method="POST" class="mr-2" id="form-block">
            @csrf
                <input type="hidden" name="status" value="{{ $customers->status == 1? '3':'1'}}">
            @if ($customers->status==1)
                <button type="button" class="btn btn-info br-10"  id="block" ><i class="fas fa-lock btn-img-redo "></i></button>
            @else
                <button type="button" class="btn btn-info br-10" id="unblock" ><i class="fas fa-unlock-alt btn-img-redo"></i></button>
            @endif
        </form>
    </div>
</div>
<div class="mt-1 row ml-0 mr-0">
    <div class="col-md-3 detail-content1 br-10">
        <a href="javascript:void(0)" class="content1-img mark-deceasedd">
            <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail" alt="">
        </a>
        <div class="all-content1">
            <div>
                <label class="font-weight-800">@lang('users.us_id')</label>
                <p>#{{$customers->id}}</p>
            </div>
            <div>
                <label class="font-weight-800">@lang('users.display')</label>
                <p>{{$customers->displayname}} </p>
            </div>
            <div>
                <label class="font-weight-800">@lang('users.us_name')</label>
                <p>{{$customers->fullname}}</p>
            </div>
            <div>
                <label class="font-weight-800">@lang('users.phone') </label>
                <p>
                    <?php
                        $number = $customers->phone;
                        echo (substr($number, 0, 3) . "-" ) . (substr($number, 3, 4) . "-" ) . (substr($number, 7, 4));
                    ?>
                    {{-- {{$customers->phone}} --}}
                </p>
            </div>
            <div>
                <label class="font-weight-800">@lang('users.email')</label>
                <p>{{$customers->email}}</p>
            </div>
            <div>
                <label class="font-weight-800">@lang('users.joined_date') </label>
                <p>{{$customers->created_at->format('Y/m/d')}}</p>
            </div>
        </div>
    </div>
    <div class="col-md-3 detail-content1 br-10">
        <a href="javascript:void(0)" class="content1-img mark-deceaseddd">
            <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail" alt="edit">
        </a>
        <div class="all-content1">
            <div>
                <label class="font-weight-800"> @lang('users.business_name') </label>
                <p> {{$customers->business_name}}</p>
            </div>
            <div>
                <label class="font-weight-800"> @lang('users.business_registration_number')  </label>
                <p>
                    <?php
                        $number = $customers->tax_invoice;
                        echo (substr($number, 0,3) . '-') . (substr($number, 3,2) . '-') . (substr($number, 5,5));
                    ?>
                    {{-- {{$customers->tax_invoice}} --}}
                </p>
                </p>
            </div>
            <div>
                <label class="font-weight-800"> @lang('users.representative')</label>
                <p> {{$customers->representative}}</p>
            </div>
            <div class="all-ImgContent10 mt-3 detai-user-img">
                <p class="font-weight-800 mb-1 "> @lang('users.business_registration_document') </p>
                    @if(!empty($documents) && count($documents))
                    @foreach ($documents as $item)
                        <img src="{{ my_asset($item) }}" class=" ml-1 br-10 img-user-business" alt="images">
                    @endforeach
                @else
                    {{-- <img src="{{asset('/public/assets/img/icons/no-img.png')}}" class=" ml-1 br-10 img-user-business" alt="images"> --}}
                @endif
            </div>
            <div>
                <label class="font-weight-800">@lang('users.store_name')  </label>
                <p>
                    {{$customers->store_name}}
                </p>
            </div>
            <div>
                <label class="font-weight-800">@lang('users.store_address')  </label>
                <p> {{$customers->store_address}}</p>
            </div>
        </div>
    </div>
    <div class="col-md detail-content2 br-10">
        <div class="row btn-test">
            <div class="col-lg-4 col-sm-4 my-4  ">
            <a href="{{route('admin.dashboard')}}">
                <button class="bg-secondary btn-user br-10">
                    <div class="icon-hi text-white">
                        <i class="fas fa-chart-line"></i>
                    </div>
                    <h5 class="text-light font-weight-bold">@lang('users.analytics')</h5>
                </button>
            </a>
            </div>
            <div class="col-lg-4 col-sm-4 my-4  ">
                <button class="bg-secondary btn-user br-10 mark-transaction">
                    <div class="icon-hi text-white">
                        <i class="fas fa-dollar-sign"></i>
                    </div>
                    <h5 class="text-light font-weight-bold">@lang('users.transaction')</h5>
                </button>
            </div>
            <div class="col-lg-4 col-sm-4 my-4 ">
                <button class="bg-secondary btn-user br-10 mark-delivery">
                    <div class="icon-hi text-white">
                        <i class="fas fa-address-card"></i>
                    </div>
                    <h5 class="text-light font-weight-bold">@lang('users.delivery_book')</h5>
                </button>
            </div>
            <div class="col-lg-4 col-sm-4 my-4  ">
                <button class="bg-secondary btn-user br-10 mark-payment">
                    <div class="icon-hi text-white">
                        <i class="fas fa-credit-card"></i>
                    </div>
                    <h5 class="text-light font-weight-bold">@lang('users.payment_method')</h5>
                </button>
            </div>
            <div class="col-lg-4 col-sm-4 my-4  ">
                <button class="bg-secondary btn-user br-10 mark-liked">
                    <div class="icon-hi text-white">
                        <i class="fas fa-heart"></i>
                    </div>
                    <h5 class="text-light font-weight-bold">@lang('users.like_product')</h5>
                </button>
            </div>
            <div class="col-lg-4 col-sm-4 my-4  ">
                <button class="bg-secondary btn-user br-10 mark-bought">
                    <div class="icon-hi text-white">
                        <i class="fas fa-box"></i>
                    </div>
                    <h5 class="text-light font-weight-bold">@lang('users.bought_product')</h5>
                </button>
            </div>
        </div>
    </div>
</div>

@endsection
@section('modal')
    @include('templates.popup.user-edit')
    @include('templates.popup.user-editbusiness')
    @include('templates.popup.user-transaction')
    @include('templates.popup.user-delivery')
    @include('templates.popup.user-payment')
    @include('templates.popup.user-liked')
    @include('templates.popup.user-bought')
@endsection
@section('script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
<script type="text/javascript">

    $("#maskpopupbusiness").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
        var curchr = this.value.length;
        var curval = $(this).val();
            if (curchr == 10 ) {
                $(this).val(curval);
            } 
        $(this).attr('maxlength', '10');
    });
    $("#maskpopupinfor").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
        var curchr = this.value.length;
        var curval = $(this).val();
            if (curchr == 11 ) {
                $(this).val(curval);
            } 
        $(this).attr('maxlength', '11');
    });
    $(document).keydown(function(e) {
    if (e.keyCode == 27) {
        $('.mark-transactions').removeClass('show');
        $('.mark-deceasedds').removeClass('show');
        $('.mark-deceaseddds').removeClass('show');
        $('.mark-deliverys').removeClass('show');
        $('.mark-payments').removeClass('show');
        $('.mark-likeds').removeClass('show');
        $('.mark-boughts').removeClass('show');
        }
    });
    $('.img-ava2').click(function(){
        $('div .img-ava2').addClass('test')
        console.log('fa')
    })
    $('.mark-deceasedd').click(function() {
        $('.mark-deceasedds').addClass('show');
    });
    $('.mark-deceaseddd').click(function() {
        $('.mark-deceaseddds').addClass('show');
    });
    $('.mark-transaction').click(function() {
        $('.mark-transactions').addClass('show');
    });
    $('.mark-delivery').click(function() {
        $('.mark-deliverys').addClass('show');
    });
    $('.mark-payment').click(function() {
        $('.mark-payments').addClass('show');
    });
    $('.mark-liked').click(function() {
        $('.mark-likeds').addClass('show');
    });
    $('.mark-bought').click(function() {
        $('.mark-boughts').addClass('show');
    });
    //request error
    $(document).ready(function(){
        @error('UserRequest')
            document.querySelector('.mark-deceasedds').classList.add('show');
        @enderror
    });
    $(document).ready(function(){
        @error('UserRequestBusiness')
            document.querySelector('.mark-deceaseddds').classList.add('show');
        @enderror
    });
    //popup block
    $(document).on('click', "#block", function(e) {
        Swal.fire({
            title: '@lang('users.block_user')',
            text: '@lang('users.continue')',
            confirmButtonText: '@lang('users.yes')',
            cancelButtonText: '@lang('users.no')',
            showCancelButton: true,
            showCloseButton: true,
        }).then((result) => {
            if (result.isConfirmed)
                $('#form-block').submit();
        });
    });
    $(document).on('click', "#unblock", function(e) {
        Swal.fire({
            title: '@lang('users.unblock_user')',
            text: '@lang('users.continue')',
            confirmButtonText: '@lang('users.yes')',
            cancelButtonText: '@lang('users.no')',
            showCancelButton: true,
            showCloseButton: true,
        }).then((result) => {
            if (result.isConfirmed)
                $('#form-block').submit();
        });
    });

</script>

@endsection
