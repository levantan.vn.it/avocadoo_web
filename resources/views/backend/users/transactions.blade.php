@extends('backend.layouts.app')
@section('title')
    @lang('transaction.transaction')
@endsection
@section('content')
<div class="aiz-titlebar text-left mt-2 mb-3">
	<div class="align-items-center">
		<h1 class="h3"><strong>@lang('transaction.transaction')</strong></h1>
	</div>
</div>
<form class="" action="" method="GET">
    <div class="row mb-2">
        <div class="col-md-12">
          <img src="{{ static_asset('assets/img/icons/search.svg') }}" class="img-search3 mr-2" alt="Search"/>
            <input  type="text" class="form-control res-placeholder" id="search" name="search" value="{{ request('search') }}"  placeholder=" @lang('transaction.tran_search')" >
        </div>
    </div>
</form>
<div class="card">
  <div class="card-body-table">
      <table class="table-collection aiz-table mb-0 table-hover">
          <thead>
              <tr>
                  <th>@lang('transaction.id')</th>
                  <th>@lang('transaction.date')</th>
                  <th>@lang('transaction.us_id')</th>
                  <th>@lang('transaction.od_id')</th>
                  <th>@lang('transaction.money')</th>
                  <th>@lang('transaction.method')</th>
                  <th>@lang('transaction.status')</th>
                  {{-- <th class="text-right w-40">@lang('')</th> --}}
              </tr>
          </thead>
          <tbody>
                  
            @if(count($data ?? []))
                @foreach($data as $item)
                <tr>
                    <th><strong>#{{$item->id}}</strong></th>
                    <td>{{$item->created_at->format('Y/m/d')}}</td>
                    <td>#{{$item->user_id}}</td>
                    <td>#{{$item->order_id}} </td>
                    <td>
                        <?php
                            $number = $item->money;
                            echo( format_price( $number).' 원');
                        ?>
                    </td>
                    <td > {{$item->getCardBrand()}} - **** 
                        <?php 
                         echo (substr($item->method, -4, 4));
                         ?>
                      </td>
                    <td>{{$item->statusTran()}}</td>
                    {{-- <td>
                        <div class="dropdown">
                            <button class="btn btn-drop" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-h"></i>
                            </button>
                        </div>
                    </td> --}}
                </tr>
                @endforeach
            @endif
          </tbody>
      </table>
  </div>
</div>

<div class="aiz-pagination paginationOrder">
    {{ $data->appends(request()->input())->links() }}
</div> 

<div class="modal fade" id="confirm-ban">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title h6">{{translate('Confirmation')}}</h5>
                <button type="button" class="close" data-dismiss="modal"></button>
            </div>
            <div class="modal-body">
                <p>{{translate('Do you really want to ban this Customer?')}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">{{translate('Cancel')}}</button>
                <a type="button" id="confirmation" class="btn btn-primary">{{translate('Proceed!')}}</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="confirm-unban">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title h6">{{translate('Confirmation')}}</h5>
                <button type="button" class="close" data-dismiss="modal"></button>
            </div>
            <div class="modal-body">
                <p>{{translate('Do you really want to unban this Customer?')}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">{{translate('Cancel')}}</button>
                <a type="button" id="confirmationunban" class="btn btn-primary">{{translate('Proceed!')}}</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
    @include('modals.delete_modal')
@endsection

@section('script')
    <script type="text/javascript">
        function sort_customers(el){
            $('#sort_customers').submit();
        }
        function confirm_ban(url)
        {
            $('#confirm-ban').modal('show', {backdrop: 'static'});
            document.getElementById('confirmation').setAttribute('href' , url);
        }

        function confirm_unban(url)
        {
            $('#confirm-unban').modal('show', {backdrop: 'static'});
            document.getElementById('confirmationunban').setAttribute('href' , url);
        }
        $('.custom-drop').click(function(){
        // alert();
        $(this).find('.custom-ul').toggle('d-none');
    })
    </script>
@endsection
