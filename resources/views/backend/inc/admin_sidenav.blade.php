<div class="aiz-sidebar-wrap">
    <div class="aiz-sidebar left c-scrollbar">
        <div class="aiz-side-nav-logo-wrap">
            <a href="{{ route('admin.dashboard') }}" class="d-block text-left">
                @if(get_setting('system_logo_white') != null)
                    <img class="mw-100" src="{{ uploaded_asset(get_setting('system_logo_white')) }}" class="brand-icon" alt="{{ get_setting('site_name') }}">
                @else
                    <img class="mw-100" src="{{ static_asset('assets/img/logo.png') }}" class="brand-icon" alt="{{ get_setting('site_name') }}">
                @endif
            </a>
        </div>
        @php
            $roles = Auth::user()->role;
            $roles = json_decode($roles);
            $permissions = json_decode($roles->permissions) ?? [];

        @endphp
        {{-- @dump($permissions) --}}
        <div class="aiz-side-nav-wrap">
            {{-- <div class="px-20px mb-3 hide-text">
                <input class="form-control bg-soft-secondary border-0 form-control-sm text-white" type="text" name="" placeholder="@lang('sidebar.search-menu')" id="menu-search" onkeyup="menuSearch()">
            </div> --}}
            <ul class="aiz-side-nav-list" id="search-menu">
            </ul>
            <ul class="aiz-side-nav-list" id="main-menu" data-toggle="aiz-side-menu">
                <li class="aiz-side-nav-item {{in_array('Dashboard',$permissions) ? 'show' : ''}}">
                    <a href="{{route('admin.dashboard')}}" class="aiz-side-nav-link  {{ areActiveRoutes(['users.transactions'])}}">
                        <i class="las la-home aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text hide-text">@lang('sidebar.Dashboard')</span>
                    </a>
                </li>

                <!-- Customers -->
                <li class="aiz-side-nav-item {{in_array('Users',$permissions) ? 'show' : ''}}" >
                <a href="{{route('users.index')}}" class="aiz-side-nav-link  {{ areActiveRoutes(['users.detail'])}} ">
                        <i class="las la-user-friends aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text hide-text">@lang('sidebar.Users')</span>
                    </a>
                </li>

                <!-- Sale -->
                <li class="aiz-side-nav-item {{in_array('Orders',$permissions) ? 'show' : ''}}">
                    <a href="{{route('orders.index')}}" class="aiz-side-nav-link  {{ areActiveRoutes(['orders.detail'])}} {{ areActiveRoutes(['orders.detail2'])}} {{ areActiveRoutes(['orders.create'])}} {{ areActiveRoutes(['orders.changestatus'])}}">
                        <i class="las la-money-bill aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text hide-text">@lang('sidebar.Orders')</span>
                    </a>
                </li>
                <!-- Suppliers -->
                <li class="aiz-side-nav-item {{in_array('Suppliers',$permissions) ? 'show' : ''}}">
                    <a href="{{ route('suppliers.index') }}" class="aiz-side-nav-link  {{ areActiveRoutes(['suppliers.detail'])}} {{ areActiveRoutes(['suppliers.create'])}}">
                        <i class="las la-campground aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text hide-text">@lang('sidebar.Suppliers')</span>
                    </a>
                </li>

                <!-- Product -->
                <li class="aiz-side-nav-item {{in_array('Products',$permissions) ? 'show' : ''}}">
                    <a href="{{ route('products.index') }}" class="aiz-side-nav-link  {{ areActiveRoutes(['products.detail'])}} {{ areActiveRoutes(['products.create'])}}">
                        <i class="las la-apple-alt aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text hide-text">@lang('sidebar.Products')</span>
                    </a>
                </li>

                <li class="aiz-side-nav-item {{in_array('Products',$permissions) ? 'show' : ''}}">
                    <a href="{{route('uploaded-files.index')}}" class="aiz-side-nav-link  {{Request::is('admin/uploaded-files/*') ? 'active' : ''}}">
                        <i class="las la-folder-open aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text hide-text">@lang('sidebar.uploaded-files')</span>
                    </a>
                </li>

                <!-- Category -->
                <li class="aiz-side-nav-item {{in_array('Categories',$permissions) ? 'show' : ''}}">
                    <a href="{{ route('categories.index') }}" class="aiz-side-nav-link ">
                        <i class="las la-radiation-alt aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text hide-text">@lang('sidebar.Categories')</span>
                    </a>
                </li>

                <!-- Collection -->
                <li class="aiz-side-nav-item {{in_array('Collections',$permissions) ? 'show' : ''}}">
                    <a href="{{ route('collections.index') }}" class="aiz-side-nav-link  {{ areActiveRoutes(['collections.information'])}} {{ areActiveRoutes(['collections.create'])}}">
                        <i class="las la-chart-area aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text hide-text">@lang('sidebar.Collections')</span>
                    </a>
                </li>

                <!-- Banner -->
                <li class="aiz-side-nav-item {{in_array('Banners',$permissions) ? 'show' : ''}}">
                    <a href="{{route ('banners.index')}}" class="aiz-side-nav-link  {{ areActiveRoutes(['banners.detail'])}} {{ areActiveRoutes(['banners.create'])}}">
                        <i class="las la-columns aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text hide-text">@lang('sidebar.Banners')</span>
                    </a>
                </li>

                <!-- Notification -->
                <li class="aiz-side-nav-item {{in_array('Notifications',$permissions) ? 'show' : ''}}">
                    <a href="{{route ('notifications.index')}}" class="aiz-side-nav-link  {{ areActiveRoutes(['notifications.detail'])}} {{ areActiveRoutes(['notifications.create'])}}">
                        <i class="las la-bell aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text hide-text">@lang('sidebar.Notifications')</span>
                    </a>
                </li>

                <!-- Message -->
                <li class="aiz-side-nav-item {{in_array('Messages',$permissions) ? 'show' : ''}}">
                    <a href="{{route ('messages.index')}}" class="aiz-side-nav-link ">
                        <i class="las la-comment-alt aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text hide-text">@lang('sidebar.Messages')</span>
                    </a>
                </li>

                <!-- Team Member -->
                <li class="aiz-side-nav-item {{in_array('Members',$permissions) ? 'show' : ''}}">
                    <a href="{{route ('members.index')}}" class="aiz-side-nav-link  {{ areActiveRoutes(['members.edit'])}} {{ areActiveRoutes(['members.create'])}}">
                        <i class="las la-user-friends aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text hide-text">@lang('sidebar.Team-Members')</span>
                    </a>
                </li>

                <li class="aiz-side-nav-item {{in_array('Settings',$permissions) ? 'show' : ''}}">
                    <a href="{{route('settings.index')}}" class="aiz-side-nav-link  {{Request::is('admin/settings/*') ? 'active' : ''}}">
                        <i class="las la-dharmachakra aiz-side-nav-icon"></i>
                        <span class="aiz-side-nav-text hide-text">@lang('sidebar.Settings')</span>
                    </a>
                </li>
            </ul><!-- .aiz-side-nav -->
            <a href="javascrip:void(0);" class="toogle-menu">
                <i class="las la-arrow-circle-left"></i>
            </a>
        </div><!-- .aiz-side-nav-wrap -->
    </div><!-- .aiz-sidebar -->

    <div class="aiz-sidebar-overlay"></div>
</div><!-- .aiz-sidebar -->
