@extends('backend.layouts.app')
@section('title')
@lang('products.add')
@endsection
@section('css')
<link href="{{ static_asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
@endsection
@section('content')
<div class="mb-4">
    {{--@dump($errors)--}}
    <a href="{{route('products.index')}}">
        <button type="submit" class="btn-back">
            <img src="{{ static_asset('assets/img/icons/back-admin.svg') }}" class="img-search mr-2" alt="Search">
        </button>
    </a>
</div>
<div class="row m-0 ">
   <div class="icon-lock product-iconCre col-md">
    <!-- <button type="submit" form="create" class="btn-info btn click-ProCreate ">@lang('products.create-product')</button> -->
    <a href="javascript:void(0)" data-action="{{route('products.validatedProduct')}}" class="btn-info btn btn-create-product-now">@lang('products.create-product')</a>
   </div>
</div>
<div class="create-collection row mt-3">
    <div class="col-md-3 clickAddValidateDetail">
        <button class="btn-create-collection product-addInfo">
            <i class="fas fas-create-collection fa-plus-circle img-redo mr-2"></i>
            <p class="mt-3">@lang('products.add-info')</p>
        </button>
    </div>
    <div class="col-md-3 d-none clickDoneValidateDetail">
        <a href="javascript:void(0)" class="content1-img clickEditProductDetail">
            <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail2 alt="Search">
        </a>
        <button class="btn-create-collection ">
            <i class="text-success fas fas-create-collection fa-check img-redo mr-2"></i>
            <p class="mt-3">@lang('products.add-info')</p>
        </button>
    </div>
    <div class="col-md-3">
        <div class='clickUpdateImg'>
            <button class="btn-create-collection product-addImg">
                <i class="fas fas-create-collection fa-plus-circle img-redo mr-2"></i>
                <p class="mt-3">@lang('products.add-img')</p>
            </button>
        </div>
        <div class="d-none clickDoneImg">
            <a href="javascript:void(0)" class="content1-img clickEditProductImg">
                <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail2 alt="Search">
            </a>
            <button class="btn-create-collection product-addImg">
                <i class="text-success fas fas-create-collection fa-check img-redo mr-2"></i>
                <p class="mt-3">@lang('products.add-img')</p>
            </button>
        </div>
        <div class="clickAddDescription ">
            <button class="btn-create-collection product-addDes">
                <i class="fas fas-create-collection fa-plus-circle img-redo mr-2"></i>
                <p class="mt-3">@lang('products.pr-des')</p>
            </button>
        </div>
        <div class="d-none clickDoneDescription">
            <a href="javascript:void(0)" class="content1-img clickEditProductDescription">
                <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail2 alt="Search">
            </a>
            <button class="btn-create-collection ">
                <i class="text-success fas fas-create-collection fa-check img-redo mr-2"></i>
                <p class="mt-3">@lang('products.add-img')</p>
            </button>
        </div>
    </div>
    <div class="col-md">
        <button class="btn-create-collection edit-product-collect" >
            <i class="fas fas-create-collection fa-plus-circle img-redo mr-2"></i>
            <p class="mt-3">@lang('products.add-skus')</p>
        </button>
    </div>
</div>

@endsection
@section('modal')
<form method="post" action="{{route('products.store')}}" name="create" class="FormAddProduct">
    @csrf
    @include('templates.popup.product-create-description')
    @include('templates.popup.products-create-img')
    @include('templates.popup.products-edit',['edit'=>false])
    @include('templates.popup.product-create-sku')
</form>
@endsection
@section('script')
<script src="{{ static_asset('plugins/select2/js/select2.min.js') }}" ></script>
<script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script type="text/javascript">
        // $(document).on('click','.add-btnVariant15',function(){
        //     alert();
        // })

                $(document).on('click','.btn-create-product-now',function(){
                    var url = $(this).data('action');
                    const parent = $(this).parents('body').find('.FormAddProduct');
                    $.ajax({
                        url : url,
                        data: parent.serialize(),
                        type : "POST",
                        statusCode : {
             
                            422 : ({responseJSON}) => {
                                parent.find('.error').remove();
                                
                                const e = Object.entries(responseJSON.errors);
                                console.log(e);
                                const arr_info = ['name','category_id','supplier_id','variant.title.*','variant.value.*.*'];
                                const arr_image = ['id_files'];
                                const arr_des = ['description'];
                                for (const [key,val] of e) {
                                    if(arr_info.includes(key)) {
                                        $('.mark-deceaseds').addClass('show');
                                    }
                                    if(arr_image.includes(key)) {
                                        $('.mark-create-image').addClass('show');
                                    }
                                    if(arr_des.includes(key)) {
                                        $('.product-addDescription').addClass('show');
                                    }
                                    if(key == 'id_files') {
                                        parent.find(`[name="${key}"]`).parents('.row').after(`
                                        <div class="error">${val}</div>
                                    `)
                                    }else if(key == 'category_id'){
                                        parent.find(`[name="${key}"]`).parents('.dropdown').after(`
                                        <div class="error">${val}</div>
                                    `)
                                    }else if(key == 'supplier_id'){
                                        parent.find(`[name="${key}"]`).parents('.dropdown').after(`
                                        <div class="error">${val}</div>
                                    `)
                                    }else if(key.includes('.')){
                                        let newArr = key.split('.');
                                        newArr = newArr.map( (key,index) => index ? `[${key}]`:key);
                                        console.log(newArr);
                                        test= newArr.join('');
                                        $(`[name="${test}"]`).parent().parent().append(`
                                        <div class="error">${val}</div>
                                    `)
     
                                    }else {
                                        parent.find(`[name="${key}"]`).after(`
                                        <div class="error">${val}</div>
                                    `)
                                    }
                                    
                                }
                            }
                        },
                        success : function(res) {
                            $('.FormAddProduct').submit();
                        },
                        error : function(err) {
                            console.log(err)
                        }
                    })
                })

        //ajax validate product popup detail

        // validate image product

        $(document).on('click','.btn-delete-now',function(){
            $(this).parents().parents().parents().parents().parents('.row-my-product').remove();
        });

        $(document).on('click','.btn-del-now',function(){
            let countParent = $(this).parents().parents('.variant-parent').children('.click-totalKey').length;
            if(countParent <2)
            {
                $(this).addClass('d-none');
            }
            if(countParent >1)
            {
                $(this).parent().remove();
            }
        });
        $(document).on('click','.btn-del-now',function(){
            let countParent = $(this).parents().parents('.variant-parent156').children('.click-totalKey').length;
            if(countParent <2)
            {
                $(this).addClass('d-none');
            }
            if(countParent >1)
            {
                $(this).parent().remove();
            }
        });

        $(document).on('click','.form-control-vari',function(){
            $(this).parents().find('.btn-del-now').removeClass('d-none');
            console.log('test1');
        })

        $(document).on('click','.clickvalidateProductInfor',function(){
            var url = $(this).data('action');
            console.log(url);
            var name = $('#joined_date2').val();
            var category_id = $('#status3').val();
            var supplier_id = $('#supplier_id').val();
            var updated_at=$('#click_dateProductInfomation').val();
            var created_at=$('#click_dateProInfomation').val();
            var value=$('.value-variants').val();
            // data = {
            //     "token" : "{{csrf_token()}}",
            //     "name": name,
            //     "category_id": category_id,
            //     "supplier_id" : supplier_id,
            //     "variant.value.*.*": value,
            // };
            // console.log(data);
            // return;
            const data = $('.popup-products-edit.popup-products-transform input, .popup-products-edit.popup-products-transform select').serialize();
            $.ajax({
                url: url,
                data: data,
                type: "POST",
                beforeSend: function(){
                    $('.mark-deceaseds .error').remove();
                },
                success: function(res){
                    $('.mark-deceaseds').removeClass('show');
                    $('.mark-deceaseds .error').hide();
                    document.querySelector('.mark-deceaseds').classList.remove('show');
                    $('.clickAddValidateDetail').addClass('d-none');
                    $('.clickDoneValidateDetail').removeClass('d-none');
                },
                error : function(err) {
                    // var response= JSON.parse(err.responseText);
                    // showErrorMsg(response.errors);
                    const {errors} = err.responseJSON;

                    Object.keys(errors).forEach(item=>{
                        let errorElement = $(` <p class="error mt-1 ml-2"> ${errors[item]}</p>`);
                        if(item.includes('.')){
                            let newArr = item.split('.');
                            newArr = newArr.map( (item,index) => index ? `[${item}]`: item);
                            item = newArr.join('');
                        }
                        $(`[name="${item}"]`).parent().parent().append(errorElement);
                    });
                }
            })

        });

        $(document).on('click','.clickEditProductDetail',function(){
            $('.mark-deceaseds').addClass('show');
        });

        //click change form image and validate
        $(document).on('click','.clickImgCreateProduct',function(){
            $('.mark-create-image .error').remove();
            $('.text-danger-image').remove();

            let count=$(this).parent().parent('.parent-custom-image').children('.children-count-img').find('.d-flex.justify-content-between.align-items-center.file-preview-item').length;
            if(count<1){
                $('.children-count-img').append(`<div class='error mt-3 text-danger-image'>@lang('products.required-image')</div>`);
            };
            console.log(count);
            if(count>=1){
                $('.clickUpdateImg').addClass('d-none');
                $('.clickDoneImg').removeClass('d-none');
                $('.mark-create-image').removeClass('show');
                $('.text-danger-image').remove();

            }
        });
        $(document).on('click','.clickEditProductImg',function(){
            $('.mark-create-image').addClass('show');
        });

        //click change form description
        $('.mark-edit-description').click(function() {
            $('.mark-edit-descriptions').addClass('show');
        });

        $(document).on('click','.clickEditDescription',function(){
            var url=$(this).data('action');
            // console.log(url);
            var description = $("#description").val();
            data = {
                "token" : "{{csrf_token()}}",
                "description": description,
            };

            $.ajax({
                url : url,
                data:data,
                type: "post",
                beforeSend: function(){
                    $('.product-addDescription .errors').remove();
                },
                success: function(res){
                    $('.product-addDescription').removeClass('show');
                    $('.error').hide();
                    document.querySelector('.product-addDescription').classList.remove('show');
                    $('.product-addDescription').removeClass('show');
                    $('.clickDoneDescription').removeClass('d-none');
                    $('.clickAddDescription').addClass('d-none');
                },
                error : function(err) {
                    $('.bg-popup-description .error').remove();
                    // var response= JSON.parse(err.responseText);
                    // showErrorMsg(response.errors);
                    const {errors} = err.responseJSON;
                    console.log(err);
                    Object.keys(errors).forEach(item=>{
                        let errorElement = $(` <p class="error mt-1"> ${errors[item]}</p>`);
                        $(`[name="${item}"]`).parent().append(errorElement);
                    });
                }
            })
        });

        //click edit description
        $(document).on('click','.clickEditProductDescription',function(){
            $('.product-addDescription').addClass('show');
        })

        $(document).ready(function() {
            var max_fields      = 10; //maximum input boxes allowed
            var add_button      = $(".add-btnVariant156"); //Add button ID
            var x = 1; //initlal text box count
            // var yum = $('#variant-late').data('index');
            // alert(yum);


            $(document).on('click',".add-my-input",function(e){ //on add input button click
                e.preventDefault();
                var wrapper = $(this); //Fields wrapper
                let countParent = $(this).parents('.variant-parent156').children().length;
                // console.log(countParent);
                countParent = countParent > 0 ? countParent + 10 : 0;
                const indexParent = $(this).parents('.row-my-product').index() + 1;
                // yum++;
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment

                    $(wrapper).before(`
                        <div class="col-md-6 pr-0  mb-3">
                            <div class="click-totalKey">
                                <input type="text" class="fs-16 form-control value-variants"  name="variant[value][${indexParent}][${countParent}]" placeholder="{{ translate('값') }}">
                                <a href="javascript:void(0)"  class="btn-del btn-del-now"><i class="fas fa-trash"></i></a>
                            </div>
                        </div>`); //add input box
                }
            });
        });

        //add variant[value]
        $(document).ready(function() {
            var max_fields      = 10; //maximum input boxes allowed
            var add_button      = $(".add-btnVariant"); //Add button ID
            var x = 1; //initlal text box count
            // var yum = $('#variant-late').data('index');
            // alert(yum);

            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                var wrapper        = $(".add-Column"); //Fields wrapper
                let countParent = $(this).parents('.variant-parent').children().length;
                // console.log(countParent);
                countParent = countParent > 0 ? countParent + 10 : 0;

                // yum++;
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).before(`
                        <div class="col-md-6 pr-0 mb-3 ">
                            <div class="click-totalKey">
                                <input type="text" class="fs-16 form-control value-variants"  name="variant[value][0][${countParent}]" placeholder="{{ translate('값') }}">
                                <a href="javascript:void(0)"  class="btn-del btn-del-now"><i class="fas fa-trash"></i></a>
                            </div>
                        </div>`
                    ); //add input box
                }
            });
        });

        $(document).ready(function() {
            var max_fields      = 10; //maximum input boxes allowed
            var add_button      = $(".add-btnVariant37"); //Add button ID
            var x = 1; //initlal text box count

            // alert(host);
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                var index = $('.row-my-product').length;
                var wrapper  = $(".add-Column39"); //Fields wrapper
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).before(`
                    <div class="m-0 all-form-productid row-my-product mb-3 ">
                    <div class="row m-0 all-form-productid mb-3 ">
                        <div class=" p-0 mb-3 " id="title-last-index-product23" >
                            <div class="row m-0">
                                <div class="col-md-4 widthVariant p-0 ">
                                    <div class="width-Variant">
                                        <a href="javascript:void(0)" class="btn-del btn-delete-now"><i class="fas fa-trash"></i></a>
                                        <input placeholder="@lang('products.value')" type="text" class="fs-16 form-control" name="variant[title][${index}]" value="">
                                    </div>
                                </div>
                                <div class="row m-0 col-md-8 p-0 variant-parent156" >
                                        <div class="col-md-6 pr-0 mb-3 " >
                                            <div class="click-totalKey">
                                            <input placeholder="@lang('products.value')" type="text" class="fs-16 form-control value-variants" name="variant[value][${index}][0]" value="">
                                            <a href="javascript:void(0)"  class="btn-del btn-del-now"><i class="fas fa-trash"></i></a>
                                            </div>
                                        </div>
                                                    <a href="javascript:void(0);" class="d-flex add-my-input mt-2 add-Column156 form-control-vari" >
                                                        <img src="{{ static_asset('assets/img/icons/plus1.png') }}" class="img-plus mr-2" alt="Search">
                                                        <span class="fs-16">@lang('products.add-value')</span>
                                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`); //add input box
                    // $(wrapper).before('<div class="col-md-3 pr-0 mb-3 pl-0"><input type="text" class="form-control" id="" name="title['+$index+']"></div>'); //add input box
                }
            });
        });



        //click submit
        // $(document).on('click','.click-ProCreate',function(){
        //     // let test = $('.text-danger-image');
        //     // if(!test){
        //     //     $('.clickUpdateImg').click()
        //     // }
        //     $('.FormAddProduct').submit();
        // });



        $('#click_dateProductInfomation').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                minDate: '1921/01/01',
                drops: 'up',
                locale: {
                    format: 'YYYY/MM/DD',
                    applyLabel: "대다",
                    cancelLabel: "취소",
                    "monthNames":[
                        "일월" , // Tháng 1
                        "이월" , // Tháng 2
                        "삼월" , // Tháng 3
                        "사월" , // Tháng 4
                        "오월" , // Tháng 5
                        "유월" , // Tháng 6
                        "칠월" , // Tháng 7
                        "팔월" , // Tháng 8
                        "구월" , // Tháng 9
                        "시월" , // Tháng 10
                        "십일월" , // Tháng 11
                        "십이월" , // Tháng 12
                    ],
                    daysOfWeek:[
                        "월요일" ,// Thứ 2
                        "화요일" ,// Thứ 3
                        "수요일" ,// Thứ 4
                        "목요일" ,// Thứ 5
                        "금요일" ,// Thứ 6
                        "토요일" ,// Thứ 7
                        "일요일" ,//  Chủ nhật.
                    ]
                }
        });

        $('#click_dateProInfomation').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                minDate: '1921/01/01',
                drops:'up',
                locale: {
                    format: 'YYYY/MM/DD',
                    applyLabel: "대다",
                    cancelLabel: "취소",
                    "monthNames":[
                        "일월" , // Tháng 1
                        "이월" , // Tháng 2
                        "삼월" , // Tháng 3
                        "사월" , // Tháng 4
                        "오월" , // Tháng 5
                        "유월" , // Tháng 6
                        "칠월" , // Tháng 7
                        "팔월" , // Tháng 8
                        "구월" , // Tháng 9
                        "시월" , // Tháng 10
                        "십일월" , // Tháng 11
                        "십이월" , // Tháng 12
                    ],
                    daysOfWeek:[
                        "월요일" ,// Thứ 2
                        "화요일" ,// Thứ 3
                        "수요일" ,// Thứ 4
                        "목요일" ,// Thứ 5
                        "금요일" ,// Thứ 6
                        "토요일" ,// Thứ 7
                        "일요일" ,//  Chủ nhật.
                    ]
                }
        });

        $(document).on('click','.click-validate',function(){
            var url = $(this).data('action');
            console.log(url);
            var user_id = $('#order_userId3').val();
            var delivery_address = $('#delivery_address').val();
            var displayname = $('#order-userName').val();
            var receiver=$('#receiver').val();
            var phone=$('#phone').val();
            var value=$('.value-variants').val();
            data = {
                "token" : "{{csrf_token()}}",
                "user_id": user_id,
                "delivery_address": delivery_address,
                "displayname" : displayname,
                "receiver" : receiver,
                "phone": phone,
                "variant.value.*.*": value,
            };
            console.log(data);
            return;
            $.ajax({
                url: url,
                data: data,
                type: "POST",
                beforeSend: function(){
                    $('.mark-add-orders .error').remove();
                },
                success: function(res){
                    $('.mark-add-orders').removeClass('show');
                    $('.error').hide();
                    document.querySelector('.mark-add-orders').classList.remove('show');
                },
                error : function(err) {
                    // var response= JSON.parse(err.responseText);
                    // showErrorMsg(response.errors);
                    const {error} = err.responseJSON;
                    console.log(err);
                    Object.keys(error).forEach(item=>{
                        let errorElement = $(` <p class="error mt-1"> ${errors[item]}</p>`);
                        $(`[name="${item}"]`).parent().append(errorElement);
                    });
                }
            })

        })

        $(document).on('click','.click-orderCre',function(){

            $('.click-createOrder').submit();
        })

        $('#delivery_method').change(function(){
            $('#order_delivery_fee').val($(this).find('option:selected').data('title'))
        })

        $('.product-addImg').click(function() {
            $('.mark-create-image').addClass('show');
        });

		$('.product-addDes').click(function() {
			$('.product-addDescription').addClass('show');
    	});

        $('.product-addInfo').click(function() {
			let url = $(this).attr('href');
			$('.mark-deceaseds').addClass('show');
    	});

        $('.mark-add-delivery').click(function() {
            let url = $(this).attr('href');
            $('.mark-add-deliveries').addClass('show');
        });

        // $('.edit-product-collect').click(function() {
        //     let url = $(this).attr('href');
        //     $('.order-add-products').addClass('show');
        // });

        $(document).on('click','.edit-product-collect',function(){
            $('.form-create-skus').addClass('show');
        })

        $(document).on('click','.btn-search',function(){
            // $(this).parents('.modal-product').find('.all-product').removeClass('d-none');
            const url = $(this).data('action');
            const val = $('#search-product').val();
            $.get(url+'?search='+val,function(data){
                $('#product-list').html(data)
            })
        })

        // //select2
        $(function(){
            $('#order_userId3').select2({
                ajax: {
                    url: '',
                    dataType: 'json',
                    processResults: function (data) {
                        let data1 = {
                            results: data.data.map((item)=>{
                                return {
                                id: item.id,
                                text: '#'+item.id
                                }
                            })
                        };
                        return data1;
                    }
                }
                });
                let deliveryBooks = [];
                $(document).on('change','#order_userId3', function(){
                    const id = this.value;
                    if(id){
                        $.get("{{ route('orders.create',['user_id'=>0])}}" + id).then(function(data){
                            $('#order-userName').val(data.displayname)
                            deliveryBooks = data.delivery_books ?? [];
                            let options = (deliveryBooks ?? []).map(({id, address})=>(
                                `<option value="${id}">${address}</option>`
                            ))
                            $('#delivery_address').html(options);
                            deliveryBookPrimary = deliveryBooks.find((data)=>data.type ==1) ?? deliveryBooks[0] ?? {};
                            $('#delivery_address').val(deliveryBookPrimary.id).change();
                        })
                        $.get("{{route('orders.create',['user_id'=>0])}}"+id).then(function(data){
                            $('#order-phoneNumber').val(data.phone)
                        })
                        $.get("{{route('orders.create',['user_id'=>0])}}"+id).then(function(data){
                            $('#delivery-bookRec').val(data.receiver)
                        })
                    }
                });
                $('#delivery_address').change(function(){
                    delivery = deliveryBooks.find((data)=>data.id == this.value) ?? {};
                    $('#receiver').val(delivery.receiver);
                    $('#phone').val(delivery.phone);
                })
                // $(document).on('change','#',function(){
                //     const id = this.value;
                //     if(id){
                //         $.get("{{route('orders.create',['user_id'=>0])}}"+id).then(function(data){
                //             $('#order-phoneNumber').val(data.phone)
                //         })
                //     }
                // })

        })

        //Show image
        $(".imgAdd").click(function(){
            $(this).closest(".row").find('.imgAdd').before('<div class="col-sm-2 imgUp"><div class="imagePreviews"></div><label class="btn btn-add-image-supplier">Upload<input type="file" class="uploadFile img" value="Upload Photo" style="width:0px;height:0px;overflow:hidden;"></label><i class="fa fa-times del"></i></div>');
            });
            $(document).on("click", "i.del" , function() {
                $(this).parent().remove();
        });

        $(function() {
            $(document).on("change",".uploadFile", function()
            {
                var uploadFile = $(this);
                var files = !!this.files ? this.files : [];
                if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

                if (/^image/.test( files[0].type)){ // only image file
                    var reader = new FileReader(); // instance of the FileReader
                        reader.readAsDataURL(files[0]); // read the local file

                        reader.onloadend = function(){ // set image data as background of div
                            //alert(uploadFile.closest(".upimage").find('.imagePreview').length);
            uploadFile.closest(".imgUp").find('.imagePreviews').css("background-image", "url("+this.result+")");
                }
            }
        });
        });

        @if($errors->has('name')||$errors->has('category_id')||$errors->has('supplier_id')){
            $('button.btn-create-collection.product-addInfo').click();
        }
        @endif
        @if($errors->has('description')){
            $('button.btn-create-collection.product-addDes').click();
        }
        @endif
        @if($errors->has('id_files')){
            $('button.btn-create-collection.product-addImg').click();
        }
        @endif




</script>

@endsection


