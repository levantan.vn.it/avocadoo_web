@extends('backend.layouts.app')
@section('title')
@lang('products.product-detail')
@endsection
@section('content')

<div class="mb-4">
{{--@dump($errors)--}}
    <a href="{{url()->previous()}}">
        <button type="submit" class="btn-back">
            <img src="{{ static_asset('assets/img/icons/back-admin.svg') }}" class="img-search mr-2" alt="Search">
        </button>
    </a>

</div>
<div class="row  m-0 justify-content-space-between">
    <div class="row m-0 ">
        <div>
            @if(!empty($images) && !empty(my_asset($images[0])))
            <img src="{{ my_asset($images[0]) }}" class="img-ava2 mr-2" alt="{{$products->name}}">
            @else
            <img class="img-ava2 mr-2" src="/public/assets/img/icons/no-img.png" alt="{{ $products->name }}">
            @endif
        </div>
        <div>
            <p class="mb-0 font-weight-800 font-size-18">#{{$products->id}}</p>
            <p class="mb-1 font-weight-800 font-size-18">{{$products->name}}</p>
            <label class="{{$products->status == 1 ?'activeBtn' : 'inactiveBtn'}}">{{optional($products)->checkStatus()}}</label>
            <!-- <button type="submit" class="btn btn-info">@lang('products.in')</button> -->
        </div>
    </div>
    <div class="all-BtnReset row ml-0 mr-0 ">
        <!-- <button type="submit" class="btn btn-info "><i class="fas fa-lock"></i></button> -->
        <button type="submit" class=" @if ($products->status ==1 ) btn btn-secondary mr-2 click-changeStatus @else btn btn-info click-changeStatus  mr-2 @endif ">@if ($products->status ==1) <i class="fas fa-lock"></i> @else <i class="fas fa-unlock-alt"></i> @endif </button>
        <!-- <button type="submit" class="btn btn-info "><i class="fas fa-lock"></i></button> -->
        <button type="submit" class="btn btn-info click-delete"><i class="fas fa-trash-alt"></i></button>
    </div>
</div>
<!-- <div class="row  m-0">
        <div>
        </div>
        <div>
            <p class="mb-0 font-weight-800 font-size-18">#PROD{{$products->id}}</p>
            <p class="mb-1 font-weight-800 font-size-18">{{$products->name}}</p>
            <button type="submit" class="btn btn-info">@lang('products.in')</button>
        </div>
    </div> -->
<div class="mt-3 row ml-0 mr-0">
    <div class="col-md-3 detail-content1">
        <a href="javascript:void(0)" id="form-editProducts" class="content1-img mark-deceased">
            <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail alt=" Search">
        </a>
        <div class="all-content1">
            <div>
                <p class="font-weight-800">@lang('products.id')</p>
                <p>#{{$products->id}}</p>
            </div>
            <div>
                <p class="font-weight-800">@lang('products.title')</p>
                <p>{{$products->name}}</p>
            </div>
            <div>
                <p class="font-weight-800">@lang('products.cate')</p>
                <p>{{optional($products->category)->title}}</p>
            </div>
            <div>
                <p class="font-weight-800">@lang('products.sup')</p>
                <p>{{ optional($products->supplier)->name }}</p>
            </div>
            <div>
                <p class="font-weight-800">@lang('orders.quantity')</p>
                <p>{{$products->skus->sum('qty')}}</p>
            </div>
            <div>
                <p class="font-weight-800">@lang('products.sku')</p>
                <p>{{$products->skus->count('sku')}} @lang('products.sku')</p>
            </div>
            <div>
                <?php $variants=$products->parseVariant; ?>
                <p class="font-weight-800">@lang('products.var')</p>
                <p>{{count($variants['title'])}} @lang('products.var')</p>

            </div>
            <div>
                <p class="font-weight-800">@lang('products.lasted')</p>
                <p>{{$products->updated_at->format('Y/m/d')}}</p>
            </div>
            <div>
                <p class="font-weight-800">@lang('products.created')</p>
                <p>{{$products->created_at->format('Y/m/d')}}</p>
            </div>
        </div>
    </div>
    <div class="col-md-3 detail-content12">
        <a href="javascript:void(0)" class="content1-img mark-edit-img">
            <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail" alt="Search">
        </a>
        <div class="all-content12">
            <div>
                <p class="font-weight-800">@lang('products.pr-img')</p>
            </div>
            <div class="all-ImgContent mt-3 row custom-imgPro ml-0">
                @if(!empty($images) && count($images))
                @foreach($images as $item)
                @if ($loop->first)
                    <div class="w-100">
                        <img src="{{ my_asset($item) }}" class="img-product-detail-1 mb-2" alt="{{$products->name}}">
                    </div>
                @else
                    <img src="{{ my_asset($item) }}" class="img-item2 mb-2 " alt="{{$products->name}}">
                @endif
                @endforeach
                @endif

            </div>
        </div>
        <div class="rounded mt-3">
            <a href="javascript:void(0)" class="content1-img mark-edit-description">
                <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail" alt="Search">
            </a>
            <div class="all-content12-desc">
                <p class="font-weight-800">@lang('products.pr-des')</p>
                <p>{!!$products->description!!}</p>
            </div>
        </div>
    </div>
    <div class="col-md detail-content2">
        <a href="#" class="content1-img form-sku">
            <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail alt=" Search">
        </a>
        <div class="all-content1">
            <div>
                <p class="font-weight-800">@lang('products.pr-var')</p>
            </div>
            <div class="overflow-auto">
                <table style="width:100%">
                    <thead>
                        <tr>
                            <th class="font-weight-300">@lang('products.sku')</th>
                            <th class="font-weight-300">@lang('products.price')</th>
                            <th class="font-weight-300">@lang('products.quantity')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($products->skus ?? [])
                        @foreach($products->skus as $items)
                        <tr>
                            <th class="font-weight-800">{{optional($items)->sku}}</th>
                            <th class="font-weight-500 w-20">{{format_price(optional($items)->price)}} @lang('orders.pri')</th>
                            <th class="font-weight-500">{{optional($items)->qty}}</th>
                        </tr>
                        @endforeach
                        @endif
                    <tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
@section('modal')
    @include('templates.popup.products-edit',['edit'=>true])
    @include('templates.popup.products-sku')
    @include('templates.popup.products-editimage')
    @include('templates.popup.products-edit-description')
@endsection

@section('script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript">

    $(document).on('click','.btn-del-now',function(){
        let countParent = $(this).parents().parents('.variant-parent156').children('.click-totalKey').length;
        if(countParent <2)
        {
            $(this).addClass('d-none');
        }
        if(countParent >1)
        {
            $(this).parent().remove();
        }
    });
    $(document).on('click','.btn-del-now',function(){
        let countParent = $(this).parents().parents('.variant-parent').children('.click-totalKey').length;
        if(countParent <2)
        {
            $(this).addClass('d-none');
        }
        if(countParent >1)
        {
            $(this).parent().remove();
        }
    });
    $('.count-row-variant').first().children().children('.btn-delete-now').addClass('d-none');
    $('.count-row-variant').first().children().children('input.form-control-value.form-control').removeClass('form-control-value');
    

    $(document).on('click','.btn-del-now-show',function(){
        $(this).parents().parents('.count-row-variant').remove();
    });
    $(document).on('click','.btn-delete-now',function(){
        $(this).parents().parents('.count-row-variant').remove();
    });
    $(document).on('click','.form-control-vari',function(){
        $(this).parents().find('.btn-del-now').removeClass('d-none');
        console.log('test1');
    })

    $(document).ready(function(){
        @error('ProductDescriptionRequest')
            document.querySelector('.mark-edit-descriptions').classList.add('show');
        @enderror
    });
    // $('.test4321').each(function(){ 
    //     if(!$(this).val()){ 
    //         $(this).removeAttr('name');
    //         $(this).remove();
    //         } 
    // })

    // $(document).on('input', '.test4321', function(e){
    //     alert('.test4321');
    //     if(this.value && this.value.trim().length==0){
    //         $(this).data('name', this.name).removeAttr('name');
    //     }
    //      else {
    //         $(this).attr('name', $(this).data('name'));
    //     }
    // })
    // $(document).on('click', '.btn-del-now', function() {
    //     var delete_id = $(this).attr('data-id');
    //     Swal.fire({
    //         title: '배너 삭제',
    //         text: '@lang('banners.do-you-want')',
    //         // icon: 'error',
    //         confirmButtonText: '@lang('banners.yes')',
    //         cancelButtonText: '@lang('banners.no')',
    //         showCancelButton: true,
    //         showCloseButton: true,

    //     }).then((result) => {
    //         if (result.isConfirmed) {
    //             var data = {
    //                 "_token": "{{ csrf_token() }}",
    //                 "id": delete_id,
    //             };
    //             $.ajax({
    //                 method: "DELETE",
    //                 url: '{{route("products.deleteTitle", 0)}}' + delete_id,
    //                 // data: data,
    //                 success: function(response) {
    //                     /* Read more about isConfirmed, isDenied below */
    //                     // Swal.fire('@lang('banners.saved')', '', 'ra').then(() => {
    //                         location.reload()
    //                     // });
    //                 },
    //                 error: function(err) {
    //                     // Swal.fire('@lang('banners.not-saved')', '', '정보');
    //                 }
    //             });
    //         }
    //     });
    // });
    // $(document).on('click','.click-validateSku',function(){
    //     var url = $(this).data('action');
    //     var weight=$('#weight').val();
    //     var price=$('#price').val();
    //     var qty=$('#qty').val();
        
    //     data= {
    //         "token":"{{csrf_token()}}",
    //         "weight": weight,
    //         "price": price,
    //         "qty": qty,
    //     };
    //     $.ajax({
    //         url:url,
    //         data:data,
    //         type: "POST",
    //         success: function(res){
    //                 $('.form-skus').removeClass('show');
    //                 $('.error').hide();
    //                 document.querySelector('.form-skus').classList.remove('show');
    //             },
    //             error : function(err) {
    //                 // var response= JSON.parse(err.responseText);
    //                 // showErrorMsg(response.errors);
    //                 const {errors} = err.responseJSON;
    //                 console.log(err);
    //                 Object.keys(errors).forEach(item=>{
    //                     let errorElement = $(` <p class="error mt-1"> ${errors[item]}</p>`);
    //                     $(`[name="${item}"]`).parent().append(errorElement);
                        
    //                 });
    //             }
    //     })
    // })
	$(function(){
        if($('.form-skus .error').length){
            $('.form-skus').addClass('show');
        }
	})
    $(function(){
        if($('.mark-deceaseds .error').length){
            $('.mark-deceaseds').addClass('show');
        }
    })

    $('.img-ava2').click(function() {
        $('div .img-ava2').addClass('Custom-AddColumn')
        console.log('fa')
    })
    $('.mark-deceased').click(function() {
        let url = $(this).attr('href');
        $('.mark-deceaseds').addClass('show');
    });
    $('.mark-edit-img').click(function() {
        let url = $(this).attr('href');
        $('.mark-edit-imgs').addClass('show');
    });
    $('.mark-edit-description').click(function() {
        let url = $(this).attr('href');
        $('.mark-edit-descriptions').addClass('show');
    });
    $('.form-sku').click(function() {
        let url = $(this).attr('href');
        $('.form-skus').addClass('show');
    });
    // adds input

    $(document).ready(function() {
        var max_fields      = 10; //maximum input boxes allowed
        var add_button      = $(".add-btnVariant"); //Add button ID
        var x = 1; //initlal text box count
        // var yum = $('#variant-late').data('index');
        // alert(yum);

        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            var index = $(this).data('index');
            var wrapper        = $(".add-Column"+index); //Fields wrapper
            let countParent = $(this).parents('.variant-parent').children().length;
            // console.log(countParent);
            countParent = countParent > 0 ? countParent + 100 : 0;

            // yum++;
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).before(`
                    <div class="col-md-6 pr-0 mb-3 click-totalKey">
                        <input type="text" class="fs-16 form-control"  name="variant[value][${index}][${countParent}]" placeholder="{{ translate('값') }}">
                        <a href="javascript:void(0)"  class="btn-del btn-del-now"><i class="fas fa-trash"></i></a>
                    </div>`
                ); //add input box
            }
        });
    });
    $(document).ready(function() {
        var max_fields      = 10; //maximum input boxes allowed
        var add_button      = $(".add-btnVariant3"); //Add button ID
        var x = 1; //initlal text box count
        var host = $('#title-last-index').data('index');
        
        // alert(host);
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            var index = $(this).data('index');
            var wrapper  = $(".add-Column3"+index); //Fields wrapper
            var number = $('.count-row-variant').length;
            if(x < max_fields){ //max input box allowed
                x++; //text box increment

                $(wrapper).before(`

                            <div class="row m-0 count-row-variant">
                                <div class="col-md-4 widthVariant p-0 width-Variant">
                                    <a href="javascript:void(0)"  class="btn-del btn-delete-now"><i class="fas fa-trash"></i></a>
                                    <input placeholder="@lang('products.value')" type="text" class="fs-16 form-control" name="variant[title][${number}]" value="">
                                </div>
                                <div class="row m-0 col-md-8 p-0 variant-parent156 " >
                                        <div class="col-md-6 pr-0 mb-3 click-totalKey count-add-value click-totalKey" >
                                            <input placeholder="@lang('products.value')" type="text" class="fs-16 form-control value-variants" name="variant[value][${number}][0]" value="">
                                            <a href="javascript:void(0)"  class="btn-del btn-del-now"><i class="fas fa-trash"></i></a>
                                        </div>
                                                    <a href="javascript:void(0);" class="d-flex form-control-vari add-my-input mt-2 add-Column156" >
                                                        <img src="{{ static_asset('assets/img/icons/plus1.png') }}" class="img-plus mr-2" alt="Search">
                                                        <span class="fs-16">@lang('products.add-value')</span>
                                                    </a>
                                </div>
                </div>`);
            }
        });
    });

    $(document).ready(function() {
            var max_fields      = 10; //maximum input boxes allowed
            var add_button      = $(".add-btnVariant156"); //Add button ID
            var x = 1; //initlal text box count
            // var yum = $('#variant-late').data('index');
            // alert(yum);
            $(document).on('click',".add-my-input",function(e){ //on add input button click
                e.preventDefault();
                var wrapper = $(this); //Fields wrapper
                var number = $('.count-row-variant').length;
                number2 = number > 0 ? number - 1 : 0;
                let numberValue = $('.count-add-value').length;
                let countParent = $(this).parents('.variant-parent156').children().length;
                console.log(countParent);
                countParent2 = numberValue > 0 ? numberValue +10: 0;
                // const indexParent = $(this).parents('.count-row-variant').index() + 1;
                // yum++;
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).before(`
                        <div class="col-md-6 pr-0 mb-3 count-add-value click-totalKey">
                            <input type="text" class="fs-16 form-control value-variants" name="variant[value][${number2}][${countParent2}]" placeholder="{{ translate('값') }}">
                            <a href="javascript:void(0)"  class="btn-del btn-del-now"><i class="fas fa-trash"></i></a>
                        </div>`); //add input box
                }
            });
        });


    $(function() {
        $('.mb-3 .all-form-productid .all-push').click(function(e) {
            e.preventDefault();
            $(this).parent().find('.remove-readonly').removeAttr('readonly');
            $(this).parent().find('.all-push2.d-none').removeClass('d-none');
            $(this).parent().parent().find('.opacity-5').removeClass('opacity-5');
            $(this).addClass('d-none');
            $(this).parent().find('.status').val(1);
            // $('.all-push').removeClass('d-none')
        })
    })
    $(function() {
        $('.mb-3 .all-form-productid .all-push2').click(function(e) {
            e.preventDefault();
            $(this).parent().find('.remove-readonly').attr('readonly', true);
            $(this).parent().find('.all-push.d-none').removeClass('d-none');
            $(this).parent().parent().find('.d-flex').addClass('opacity-5');
            $(this).addClass('d-none');
            $(this).parent().find('.status').val(2);
            // $('.all-push').removeClass('d-none')
        })
    })

    //Show image
    $('#input-file').change(function() {
        $('.label-image-add').before(`<img id="output" src="${window.URL.createObjectURL(this.files[0])}" width="120" height="120">'`);
    })


    //delete product use sweetalert2
    $(document).on('click', '.click-delete', function() {
        var delete_id = {{$products -> id}};
        Swal.fire({
            title: '@lang('products.delete-pro')',
            text: "{{__('notifications.confirm_popup')}}",
            // icon: 'error',
            confirmButtonText: "{{__('notifications.yes')}}",
            cancelButtonText: "{{__('notifications.no')}}",
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": delete_id,
                };
                $.ajax({
                    method: "DELETE",
                    url: '{{route ("products.delete",0)}}' + delete_id,
                    data: data,
                    success: function(response) {
                        /* Read more about isConfirmed, isDenied below */

                        location.href="{{route('products.index')}}";
                        // Swal.fire('@lang('banners.saved')', '', 'success').then(() => {
                        // });
                    },
                    error: function(err) {
                        // Swal.fire('@lang('banners.not-saved')', '', 'info');
                    }
                });
            }
        });
    });

    // change status use sweetalert2

    $(document).on('click', '.click-changeStatus', function() {
        status_id={{$products->id}};
        Swal.fire({
            title: '@lang('banners.change-status')',
            text: "{{__('notifications.confirm_popup')}}",
            // icon: 'error',
            confirmButtonText: "{{__('notifications.yes')}}",
            cancelButtonText: "{{__('notifications.no')}}",
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if(result.isConfirmed){
                var data = {
                    "_token":"{{csrf_token() }}",
                    "id":status_id,
                };
                $.ajax({
                    method: "post",
                    url: '{{route ("products.toggle-status",0)}}' + status_id,
                    data:data,
                    success: function(response) {
                        /* Read more about isConfirmed, isDenied below */
                        // Swal.fire('@lang('banners.saved')', '', 'success').then(() => {
                            location.reload()
                        // });
                    },
                    error: function(err) {
                        // Swal.fire('@lang('banners.not-saved')', '', 'info');
                    }
                })
            }


        });
    });
    // @if($errors->any()){
    //         $('.form-sku').click();
    //     }
    // @endif

    @if($errors->has('name')||$errors->has('category_id')||$errors->has('supplier_id')||$errors->has('created_at')||$errors->has('updated_at')||$errors->has('variant[title][]')){
        $('#form-editProducts').click();
    }
    @endif

    @if($errors->has('id_files')){
        $('.mark-edit-img').click();
    }
    @endif

        // Date picker popup infomation

    // $('#click_dateProInfomation').daterangepicker({
    //         singleDatePicker: true,
    //         drops:'up',
    //         minDate: '2000/01/01',
    //         showDropdowns: true,
    //         locale: {
    //             format: 'YYYY/MM/DD',
    //             applyLabel: "대다",
    //             cancelLabel: "취소",
    //             "monthNames":[
    //                 "일월" , // Tháng 1
    //                 "이월" , // Tháng 2
    //                 "삼월" , // Tháng 3
    //                 "사월" , // Tháng 4
    //                 "오월" , // Tháng 5
    //                 "유월" , // Tháng 6
    //                 "칠월" , // Tháng 7
    //                 "팔월" , // Tháng 8
    //                 "구월" , // Tháng 9
    //                 "시월" , // Tháng 10
    //                 "십일월" , // Tháng 11
    //                 "십이월" , // Tháng 12
    //             ],
    //             daysOfWeek:[
    //                 "월요일" ,// Thứ 2
    //                 "화요일" ,// Thứ 3
    //                 "수요일" ,// Thứ 4
    //                 "목요일" ,// Thứ 5
    //                 "금요일" ,// Thứ 6
    //                 "토요일" ,// Thứ 7
    //                 "일요일" ,//  Chủ nhật.
    //             ]
            
    //         }
    // });
    // $('#click_dateProInfomation').val('{{date('Y/m/d',strtotime($products->created_at)) }}');

    // $('#click_dateProductInfomation').daterangepicker({
    //     singleDatePicker: true,
    //     showDropdowns: true,
    //     minDate: '2000/01/01',
    //     drops:'up',
    //     locale: {
    //         format: 'YYYY/MM/DD',
    //         applyLabel: "대다",
    //             cancelLabel: "취소",
    //             "monthNames":[
    //                 "일월" , // Tháng 1
    //                 "이월" , // Tháng 2
    //                 "삼월" , // Tháng 3
    //                 "사월" , // Tháng 4
    //                 "오월" , // Tháng 5
    //                 "유월" , // Tháng 6
    //                 "칠월" , // Tháng 7
    //                 "팔월" , // Tháng 8
    //                 "구월" , // Tháng 9
    //                 "시월" , // Tháng 10
    //                 "십일월" , // Tháng 11
    //                 "십이월" , // Tháng 12
    //             ],
    //             daysOfWeek:[
    //                 "월요일" ,// Thứ 2
    //                 "화요일" ,// Thứ 3
    //                 "수요일" ,// Thứ 4
    //                 "목요일" ,// Thứ 5
    //                 "금요일" ,// Thứ 6
    //                 "토요일" ,// Thứ 7
    //                 "일요일" ,//  Chủ nhật.
    //             ]
    //         }
            
    //     });
    // $('#click_dateProductInfomation').val('{{ Date('Y/m/d', strtotime($products->updated_at)) }}');
</script>

@endsection
