@extends('backend.layouts.app')
@section('title')
@lang('products.products')
@endsection
@section('content')
<div class="aiz-titlebar text-left mt-2 mb-3">

    <div class="align-items-center">
        <h1 class="h3"><strong>@lang('products.products')</strong> </h1>
    </div>
</div>
<form class="" id="sort_customers" action="" method="GET">
    <div class="row mb-2">
        <div class="col-md-9 d-flex">
            <img src="{{ static_asset('assets/img/icons/search.svg') }}" class="img-search3 mr-2" alt="Search">
            <input type="text" class="form-control res-placeholder res-FormControl" id="search" name="search" value="{{ request('search')}}" @isset($sort_search) @endisset placeholder="@lang('products.place-search')">
        </div>
        @if($type != 'Seller')
        <div class="col-md-3 text-md-right res-FormControl">
            <a href="{{ route('products.create') }}" class="btn btn-circle btn-info btn-add-product d-flex">
                <!-- <img src="{{ static_asset('assets/img/icons/add.png') }}" class="img-search2 mr-2" alt="Search"> -->
                <i class="fas fa-plus-circle img-redo mr-2"></i>
                <span class="custom-FontSize">@lang('products.add')</span>
            </a>
        </div>
        @endif
    </div>
    <div class="row gutters-5 mb-5 custom-change">
        <div class="col-md-4 ">
            <input type="text" onkeypress='return event.charCode >=48 && event.charCode<=57' autocomplete="off" class="form-control custom-placeholder"  name="date" id="joined_date38" placeholder="@lang('products.add-date')" value="{{ request()->get('date') }}">
            <div class="custom-down"><i class="fas fa-chevron-down"></i></div>
        </div>
        <div class="col-md-4 res-status">
            <select class="form-control form-control-sm aiz-selectpicker mb-2 mb-md-0 font-weight-800" id="status" name="status">
                <option value="">@lang('banners.state')</option>
                <option @if(request('status') == (1)) selected @endif value="1">@lang('products.in')</option>
                <option @if(request('status') == (2)) selected @endif value="2">@lang('products.out')</option>


            </select>
        </div>
        <div class="col-md all-btn-search">
            <button type="submit" class=" pl-0 pr-0 btn btn-info w-32 mr-2 d-flex  btn-responsive justify-content-center">
                <i class=" fas fa-search img-redo mr-2"></i>
                <span class="custom-FontSize">@lang('banners.search')</span>
            </button>
            <a href="{{route ('products.index')}}" class="pl-0 pr-0 w-32 btn btn-info ml-2 mr-2 d-flex btn-responsive justify-content-center">
                <i class="fas fa-redo-alt img-redo mr-2"></i>
                <span class="custom-FontSize">@lang('banners.reset')</span>
            </a>
            <?php
                $request = request()->all();
                $newRequest = http_build_query($request);
            ?>
            <a href="{{ route('products.export') . '?' . $newRequest }}" class=" btn btn-info w-34 ml-2 d-flex  btn-responsive justify-content-center">
                <i class="fas fa-cloud-download-alt img-redo mr-2"></i>
                <span class="custom-FontSize">@lang('banners.excel')</span>
            </a>
        </div>
    </div>
</form>


<!-- <div class="aiz-titlebar text-left mt-2 mb-3">
    <div class="row align-items-center">
        <div class="col-md-6">
            <h1 class="h3">{{translate('All products')}}</h1>
        </div>
        @if($type != 'Seller')
        <div class="col-md-6 text-md-right">
            <a href="{{ route('products.create') }}" class="btn btn-circle btn-info">
                <span>{{translate('Add New Product')}}</span>
            </a>
        </div>
        @endif
    </div>
</div> -->
<div class="card">
    <form class="" id="sort_products" action="" method="GET">
        <!-- <div class="card-header row gutters-5">
            <div class="col text-center text-md-left">
                <h5 class="mb-md-0 h6">{{ translate('All Product') }}</h5>
            </div>
            @if($type == 'Seller')
            <div class="col-md-2 ml-auto">
                <select class="form-control form-control-sm aiz-selectpicker mb-2 mb-md-0" id="user_id" name="user_id" onchange="sort_products()">
                    <option value="">{{ translate('All Sellers') }}</option>
                    @foreach (App\Seller::all() as $key => $seller)
                    @if ($seller->user != null && $seller->user->shop != null)
                    <option value="{{ $seller->user->id }}" @if ($seller->user->id == $seller_id) selected @endif>{{ $seller->user->shop->name }} ({{ $seller->user->name }})</option>
                    @endif
                    @endforeach
                </select>
            </div>
            @endif
            @if($type == 'All')
            <div class="col-md-2 ml-auto">
                <select class="form-control form-control-sm aiz-selectpicker mb-2 mb-md-0" id="user_id" name="user_id" onchange="sort_products()">
                    <option value="">{{ translate('All Sellers') }}</option>
                    @foreach (App\User::where('user_type', '=', 'admin')->orWhere('user_type', '=', 'seller')->get() as $key => $seller)
                    <option value="{{ $seller->id }}" @if ($seller->id == $seller_id) selected @endif>{{ $seller->name }}</option>
                    @endforeach
                </select>
            </div>
            @endif
            <div class="col-md-2 ml-auto">
                <select class="form-control form-control-sm aiz-selectpicker mb-2 mb-md-0" name="type" id="type" onchange="sort_products()">
                    <option value="">{{ translate('Sort By') }}</option>
                    <option value="rating,desc" @isset($col_name , $query) @if($col_name=='rating' && $query=='desc' ) selected @endif @endisset>{{translate('Rating (High > Low)')}}</option>
                    <option value="rating,asc" @isset($col_name , $query) @if($col_name=='rating' && $query=='asc' ) selected @endif @endisset>{{translate('Rating (Low > High)')}}</option>
                    <option value="num_of_sale,desc" @isset($col_name , $query) @if($col_name=='num_of_sale' && $query=='desc' ) selected @endif @endisset>{{translate('Num of Sale (High > Low)')}}</option>
                    <option value="num_of_sale,asc" @isset($col_name , $query) @if($col_name=='num_of_sale' && $query=='asc' ) selected @endif @endisset>{{translate('Num of Sale (Low > High)')}}</option>
                    <option value="unit_price,desc" @isset($col_name , $query) @if($col_name=='unit_price' && $query=='desc' ) selected @endif @endisset>{{translate('Base Price (High > Low)')}}</option>
                    <option value="unit_price,asc" @isset($col_name , $query) @if($col_name=='unit_price' && $query=='asc' ) selected @endif @endisset>{{translate('Base Price (Low > High)')}}</option>
                </select>
            </div>
            <div class="col-md-2">
                <div class="form-group mb-0">
                    <input type="text" class="form-control form-control-sm" id="search" name="search" @isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="{{ translate('Type & Enter') }}">
                </div>
            </div>
        </div> -->
        </from>
        <div class="custom-overflow">
            <table class="table aiz-table mb-0">
                <thead>
                    <tr>
                        <th class="w-10">@lang('products.id')</th>
                        <th class="w-12">@lang('products.title')</th>
                        <th class="w-12">@lang('products.sku')</th>
                        <th class="w-12">@lang('products.var')</th>
                        <th class="w-12">@lang('products.price')</th>
                        <th class="w-12">@lang('products.lasted')</th>
                        <th class="w-12">@lang('orders.quantity')</th>
                        <th class="w-12">@lang('products.status')</th>
                        <th class="text-right">{{translate('')}}</th>
                    </tr>
                </thead>
                <tbody>

                    @if(count($products ?? []))
                    @foreach($products as $item)

                    <tr>
                        <th class="font-weight-800">#{{$item->id}}</th>
                        @php
                            $images = explode(",",$item->images);
                        @endphp
                        @if($images && $images[0])
                            <th class="d-flex font-weight-500">
                                <img src="{{static_asset($images[0]) }}" class="img-ava mr-2" alt="{{$item->name}}">
                                {{$item->name}}</th>

                        @else
                        <th class="d-flex font-weight-500">
                            <img class="img-ava mr-2" src="/public/assets/img/icons/no-img.png" alt="">{{ $item->name }}

                        @endif
                        <!-- @if(!empty($images) && !empty(my_asset($images[0])))
                        <img src="{{ my_asset($images[0]) }}" class="img-ava2 mr-2" alt="{{$item->name}}">
                        @else
                        <img class="img-ava2 mr-2" src="/public/assets/img/icons/no-img.png" alt="{{ $item->name }}">
                        @endif -->

                        <th class="font-weight-500">{{optional($item->skus)->count('sku')}} @lang('products.skus')</th>
                        <?php $variants = $item->parseVariant;  ?>
                        <th class="font-weight-500">{{count( $variants['title'] ?? [] )}} @lang('products.var')</th>
                        <!-- <th class="font-weight-500"> <a class="dropdown-item btn-delete" href="#">@lang('banners.del')</a></th> -->
                        <th class="font-weight-500">
                            @if($item->skus->min('price'))
                            {{format_price(optional($item)->skus->min('price'))}} @lang('orders.pri')
                            @else 
                            @endif

                        </th>
                        <th class="font-weight-500">{{optional($item)->updated_at->format('Y/m/d')}}</th>
                        <th class="font-weight-500">{{optional($item->skus)->sum('qty')}}</th>
                        <th class="font-weight-500">{{optional($item)->checkStatus() }}</th>
                        <th class="text-right p-0">
                            <div class="dropdown">
                                <button class="btn btn-drop mt-1" type="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-h"></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="{{ route('products.detail', ['id'=>$item->id]) }}">@lang('orders.view')</a>
                                    <a class="dropdown-item click-productChange" status_id="{{$item->id}}" href="#">{{$item->changeStatus()}}</a>
                                    <a class="dropdown-item btn-delete" href="javascript:void(0)" data-id="{{$item->id}}">@lang('banners.del')</a>
                                </div>
                            </div>
                        </th>
                    </tr>
                    @endforeach
                    @endif

                </tbody>
            </table>
        </div>

    </div>
<div class="aiz-pagination paginationOrder">
    {{ $products->appends(request()->input())->links()}}
</div>

@endsection

@section('modal')
@include('modals.delete_modal')
@endsection


@section('script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" >
    $('.custom-drop').click(function() {
        // alert();
        $(this).find('.custom-ul').toggle('d-none');
    })
    $(document).ready(function() {
        //$('#container').removeClass('mainnav-lg').addClass('mainnav-sm');
    });

    // function update_todays_deal(el) {
    //     if (el.checked) {
    //         var status = 1;
    //     } else {
    //         var status = 0;
    //     }
    //     $.post('{{ route('products.todays_deal')}}', { _token: '{{ csrf_token() }}',
    //             id: el.value,
    //             status: status
    //         },
    //         function(data) {
    //             if (data == 1) {
    //                 AIZ.plugins.notify('success', '{{ translate('
    //                     Todays Deal updated successfully ') }}');
    //             } else {
    //                 AIZ.plugins.notify('danger', '{{ translate('
    //                     Something went wrong ') }}');
    //             }
    //         });
    // }

    // function update_published(el) {
    //     if (el.checked) {
    //         var status = 1;
    //     } else {
    //         var status = 0;
    //     }
    //     $.post('{{ route('products.published') }}', {
    //             _token: '{{ csrf_token() }}',
    //             id: el.value,
    //             status: status
    //         },
    //         function(data) {
    //             if (data == 1) {
    //                 AIZ.plugins.notify('success', '{{ translate('
    //                     Published products updated successfully ') }}');
    //             } else {
    //                 AIZ.plugins.notify('danger', '{{ translate('
    //                     Something went wrong ') }}');
    //             }
    //         });
    // }

    // function update_featured(el) {
    //     if (el.checked) {
    //         var status = 1;
    //     } else {
    //         var status = 0;
    //     }
    //     $.post('{{ route('products.featured') }}', {
    //             _token: '{{ csrf_token() }}',
    //             id: el.value,
    //             status: status
    //         },
    //         function(data) {
    //             if (data == 1) {
    //                 AIZ.plugins.notify('success', '{{ translate('
    //                     Featured products updated successfully ') }}');
    //             } else {
    //                 AIZ.plugins.notify('danger', '{{ translate('
    //                     Something went wrong ') }}');
    //             }
    //         });
    // }

    function sort_products(el) {
        $('#sort_products').submit();
    }
    // $(document).on('dblclick', '[data-redirect]', function(){
    //     alert()
    //         location.href = $(this).data('redirect');
    //     })


    // popup change status
    $(document).on('click', '.click-productChange', function() {
        var status_id = $(this).attr('status_id');
        // alert(test);
        Swal.fire({
            title: '@lang('banners.change-status')',
            text: "{{__('notifications.confirm_popup')}}",
            // icon: 'error',
            confirmButtonText: "{{__('notifications.yes')}}",
            cancelButtonText: "{{__('notifications.no')}}",
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": status_id,
                };
                $.ajax({
                    method: "POST",
                    url: '{{route ("products.toggle-status",0)}}' + status_id,
                    data: data,
                    success: function(response) {
                        /* Read more about isConfirmed, isDenied below */
                        location.reload()
                        // Swal.fire('@lang('banners.saved')', '', 'success').then(() => {
                        // });
                    },
                    error: function(err) {
                        // Swal.fire('@lang('banners.not-saved')', '', 'info');
                    }
                });
            }
        });
    });

    $(document).on('click', '.btn-delete', function() {
        var delete_id = $(this).attr('data-id');
        // alert(test);
        Swal.fire({
            title: '@lang('products.delete-pro')',
            text: "{{__('notifications.confirm_popup')}}",
            // icon: 'error',
            confirmButtonText: "{{__('notifications.yes')}}",
            cancelButtonText: "{{__('notifications.no')}}",
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": delete_id,
                };
                $.ajax({
                    method: "DELETE",
                    url: '{{route ("products.delete",0)}}' + delete_id,
                    data: data,
                    success: function(response) {
                        /* Read more about isConfirmed, isDenied below */

                        location.reload()
                        // Swal.fire('@lang('banners.saved')', '', 'success').then(() => {
                        // });
                    },
                    error: function(err) {
                        // Swal.fire('@lang('banners.not-saved')', '', 'info');
                    }
                });
            }
        });
    });

    function confirm_ban(url) {
        $('#confirm-ban').modal('show', {
            backdrop: 'static'
        });
        document.getElementById('confirmation').setAttribute('href', url);
    }

    function confirm_unban(url) {
        $('#confirm-unban').modal('show', {
            backdrop: 'static'
        });
        document.getElementById('confirmationunban').setAttribute('href', url);
    }
    $(document).on('click','.click-addAttr',function(){
        $(this).attr("selected");
    })

    //date picker products
    $('#joined_date38').daterangepicker({
        autoUpdateInput: false,
        singleDatePicker: true,
        showDropdowns: true,
        minDate: '1921/01/01',
        locale: {
            format: 'YYYY/MM/DD',
            applyLabel: "대다",
            cancelLabel: "취소",
                "monthNames":[
                    "일월" , // Tháng 1
                    "이월" , // Tháng 2
                    "삼월" , // Tháng 3
                    "사월" , // Tháng 4
                    "오월" , // Tháng 5
                    "유월" , // Tháng 6
                    "칠월" , // Tháng 7
                    "팔월" , // Tháng 8
                    "구월" , // Tháng 9
                    "시월" , // Tháng 10
                    "십일월" , // Tháng 11
                    "십이월" , // Tháng 12
                ],
                daysOfWeek:[
                    "월요일" ,// Thứ 2
                    "화요일" ,// Thứ 3
                    "수요일" ,// Thứ 4
                    "목요일" ,// Thứ 5
                    "금요일" ,// Thứ 6
                    "토요일" ,// Thứ 7
                    "일요일" ,//  Chủ nhật.
                ]
        }
    });
    $('#joined_date38').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('YYYY/MM/DD'));
    });

    $('#joined_date38').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
    $('input[name="date"]').val('{{ request()->get('date') }}');
</script>
@endsection
