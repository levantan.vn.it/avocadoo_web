@extends('backend.layouts.app')
@section('title')
@lang('banners.banner-detail')
@endsection
@section('content')
<div class="mb-4">
{{--@dump($errors) --}}
    <a href="{{url()->previous()}}">
        <button type="submit" class="btn-back">
            <img src="{{ static_asset('assets/img/icons/back-admin.svg') }}" class="img-search mr-2" alt="Search">
        </button>
    </a>
</div>
<div class=" row  m-0">
    <div>
        @if (!empty($banners->images))
        <img src="{{ my_asset($banners->images) }}" class="img-ava2 mr-2" alt="{{$banners->title}}">
        @else
        <img class="img-ava2 mr-2" src="/public/assets/img/icons/no-img.png" alt="{{ $banners->title }}">
        @endif
    </div>
    <div>
        <p class="mb-0 font-weight-800 font-size-18">#{{$banners->id}}</p>
        <p class="mb-1 font-weight-800 font-size-18">
            @lang('banners.link') 
            @if($banners->collection_id)
                 #{{$banners->collection_id}}
            @else
                <p></p>
            @endif
        </p>
        <label  class=" {{$banners->status==1 ? 'activeBtn' : 'inactiveBtn'}}">{{$banners->status==1?'활동적인':'활성 상태'}}</label>
    </div>
</div>
<div class=" mt-3 row ml-0 mr-0">
    <div class="col-md-3 detail-content1">
        <a href="javascript:void(0)" class="content1-img banner-info">
            <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail alt="Search">
        </a>
        <div class="all-content1">
            <div>
                <p class="font-weight-800">@lang('banners.id')</p>
                <p>#{{$banners->id}}</p>
            </div>
            <div>
                <p class="font-weight-800">@lang('banners.pos')</p>
                <p>{{$banners->checkPosition()}}</p>
            </div>
            <div>
                <p class="font-weight-800">@lang('banners.start')</p>
                <p>{{Date('Y/m/d', strtotime($banners->start_date))}}</p>
            </div>
            <div>
                <p class="font-weight-800">@lang('banners.end')</p>
                <p>{{Date('Y/m/d', strtotime($banners->end_date))}}</p>
            </div>

        </div>
    </div>
    <div class="col-md-3 detail-content1">
        <a href="javascript:void(0)" class="content1-img mark-banner-img">
            <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail" alt="Edit">
        </a>
        <div class="all-content1">
            <div>
                <p class="font-weight-800">@lang('banners.bn_img')</p>
                @if ($banners->images)
                    <img src="{{ my_asset($banners->images) }}" class="img-itemBanner" alt="{{ $banners->title }}">
                @else
                    <img class="img-itemBanner" src="/public/assets/img/icons/no-img.png" alt="{{ $banners->title }}">

                @endif
            </div>
        </div>

    </div>
    <div class="col-md detail-content2">
        <a href="#" class="content1-img collectionEdit" id="collectionEdit2">
            <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail alt="Search">
        </a>
        <div class="all-content1">
            <div class="overflow-auto">
                <table style="width:100%">
                    <thead>
                        <tr>
                            <th class="font-weight-800">@lang('banners.link')</th>
                            <th class="font-weight-300">@lang('')</th>
                        </tr>
                    </thead>
                    <tbody>
                            <th class="font-weight-500 ">
                                @if($banners->collection_id)
                                #{{$banners->collection_id}}
                                @else
                                <p></p>
                                @endif
                            </th>
                            <th class="font-weight-800 res-idBanner">{{optional($banners)->title}}</th>
                           </tr>
                    <tbody>
                </table>

            </div>
        </div>
    </div>
</div>



@endsection

@section('modal')
    @include('templates.popup.banners-edit',['edit'=>true])
    @include('templates.popup.banner-img-popup')
    @include('templates.popup.banner-collection-edit')
@endsection

@section('script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script type="text/javascript">
$(function(){
        $('#collection_id option:first').html("선택 안됨");
        $('.banner-info').click(function() {
            let url = $(this).attr('href');
            $('.banner-infos').addClass('show');
        });
        $('.form-kus').click(function() {
            let url = $(this).attr('href');
            $('.form-skus').addClass('show');
        });
        $('.mark-banner-img').click(function() {
            let url = $(this).attr('href');
            $('.mark-banners-img').addClass('show');
        });
        $('.collectionEdit').click(function() {
            let url = $(this).attr('href');
            $('.collectionsEdit').addClass('show');
        });
         //Show image
         $('#input-file').change(function () {
            $('.label-image-add-banner').before(`<img id="output" src="${window.URL.createObjectURL(this.files[0])}" width="150" height="150">'`);
        });

        //
        // function myFunction() {
        //     document.getElementById("status6");
        // }
        // function myFunction() {
        //     document.getElementById("status7");
        // }

        $('#change-banner').submit(function(e){
            // e.preventDefault();
            // const formData = new FormData(document.forms.changebanner);
            // // $('#change-banner input, #change-banner select').each(function(){
            // //     formData.append($(this).attr('name'), $())
            // // });
            // // console.log(formData.get('start_date'));
            // const url = $(this).attr('action');
            // $.ajax({
            //     url,
            //     method: "POST",
            //     data: formData,
            //     processData: false,
            //     dataType: "JSON",
            //     success: function(res){
            //         console.log(res);
            //     },
            //     error: function(err){
            //         const errors = err.responseJSON.errors;
            //         // const newError = Object.values(errors).join('');
            //     }
            // });
        });

        @if($errors->first('collection_id') ||$errors->first('title')){
            $('#collectionEdit2').click();
        }
        @endif
        @if($errors->first('end_date')|| $errors->first('start_date')|| $errors->first('position') ){
            $('.banner-info').click();
        }
        @endif
        @if($errors->has('id_files')){
            $('a.content1-img.mark-banner-img').click();
        }
        @endif


        $('#status51').change(function(){
            $('#joined_date131').val($(this).find('option:selected').data('title'))
        })

    });
        // date picker banners
        $('#status6').daterangepicker({
            singleDatePicker: true,
            autoUpdateInput: false,
            drops:'up',
            showDropdowns: true,
            minDate: '1921/01/01',
            locale: {
                format: 'YYYY/MM/DD',
                applyLabel: "대다",
                cancelLabel: "취소",
                "monthNames":[
                    "일월" , // Tháng 1
                    "이월" , // Tháng 2
                    "삼월" , // Tháng 3
                    "사월" , // Tháng 4
                    "오월" , // Tháng 5
                    "유월" , // Tháng 6
                    "칠월" , // Tháng 7
                    "팔월" , // Tháng 8
                    "구월" , // Tháng 9
                    "시월" , // Tháng 10
                    "십일월" , // Tháng 11
                    "십이월" , // Tháng 12
                ],
                daysOfWeek:[
                    "월요일" ,// Thứ 2
                    "화요일" ,// Thứ 3
                    "수요일" ,// Thứ 4
                    "목요일" ,// Thứ 5
                    "금요일" ,// Thứ 6
                    "토요일" ,// Thứ 7
                    "일요일" ,//  Chủ nhật.
                ]
            }
        });
        // $('#status6').on('apply.daterangepicker', function (ev, picker) {
        //     $(this).val(picker.startDate.format('YYYY/MM/DD'));
        // });

        // $('#status6').on('cancel.daterangepicker', function (ev, picker) {
        //     $(this).val('');
        // });
        $('#status6').val('{{ Date('Y/m/d',strtotime($banners->start_date)) }}');
        $('#status7').daterangepicker({
            singleDatePicker: true,
            autoUpdateInput: false,
            drops:'up',
            showDropdowns: true,
            minDate: '1921/01/01',
            locale: {
                format: 'YYYY/MM/DD',
                applyLabel: "대다",
                cancelLabel: "취소",
                "monthNames":[
                    "일월" , // Tháng 1
                    "이월" , // Tháng 2
                    "삼월" , // Tháng 3
                    "사월" , // Tháng 4
                    "오월" , // Tháng 5
                    "유월" , // Tháng 6
                    "칠월" , // Tháng 7
                    "팔월" , // Tháng 8
                    "구월" , // Tháng 9
                    "시월" , // Tháng 10
                    "십일월" , // Tháng 11
                    "십이월" , // Tháng 12
                ],
                daysOfWeek:[
                    "월요일" ,// Thứ 2
                    "화요일" ,// Thứ 3
                    "수요일" ,// Thứ 4
                    "목요일" ,// Thứ 5
                    "금요일" ,// Thứ 6
                    "토요일" ,// Thứ 7
                    "일요일" ,//  Chủ nhật.
                ]
            }
        });
        $('#status7').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('YYYY/MM/DD'));
        });

        $('#status7').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });
        $('#status7').val('{{ Date('Y/m/d',strtotime($banners->end_date)) }}');
</script>

@endsection
