@extends('backend.layouts.app')
@section('title')
    @lang('banners.banners')
@endsection
@section('content')
<div class="aiz-titlebar text-left mt-2 mb-3">
    <div class="align-items-center">
        <h1 class="h3"><strong>@lang('banners.banners')</strong> </h1>
    </div>
</div>
<form class="" id="sort_customers" action="" method="GET">
    <div class="row mb-2">
        <div class="col-md-9 d-flex">
            <img src="{{ static_asset('assets/img/icons/search.svg') }}"  class="img-search3 mr-2" alt="Search" >
            <input type="text" class="form-control res-placeholder res-FormControl"  id="search" name="search" value="{{request()->get('search')}}" placeholder="@lang('banners.search_id')">
        </div>
        <div class="col-md-3 text-md-right res-FormControl">
            <a href="{{route('banners.create')}}" class="btn btn-circle btn-info btn-add-product d-flex">
                <i class="fas fa-plus-circle img-redo mr-2"></i>
                <span class="custom-FontSize">@lang('banners.add')</span>
            </a>
        </div>
    </div>
    <div class="row gutters-5 mb-5 custom-change">
        <div class="col-md-4 ">
            <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' autocomplete="off" class="form-control custom-placeholder"  name="date" id="click-dateBanners" placeholder="@lang('banners.start')" value="{{ request()->get('date') }}">
            <div class="custom-down"><i class="fas fa-chevron-down"></i></div>
        </div>
        <div class="col-md-4 res-status">
            <select class="form-control form-control-sm aiz-selectpicker mb-2 mb-md-0 font-weight-800" id="status" name="status">
                <option value="">@lang('banners.state')</option>
                <option value="1" @if(request('status') == (1)) selected @endif>@lang('banners.stand_by')</option>
                <option value="2" @if(request('status') == (2)) selected @endif>@lang('banners.on_air')</option>
                <option value="3" @if(request('status') == (3)) selected @endif>@lang('banners.complete')</option>
                <option value="4" @if(request('status') == (4)) selected @endif>@lang('banners.cancel')</option>
            </select>
        </div>
        <div class="col-md all-btn-search">
            <button type="submit" class="btn btn-info w-32 mr-2 d-flex btn-responsive justify-content-center pl-0 pr-0">
                <i class="fas fa-search img-redo mr-2"></i>
                <span class="custom-FontSize">@lang('banners.search')</span>
            </button>
            <a href="{{ route('banners.index') }}" class="pl-0 pr-0 w-32 btn btn-info ml-2 mr-2 d-flex btn-responsive justify-content-center">
                 <i class="fas fa-redo-alt img-redo mr-2"></i>
                <span class="custom-FontSize">@lang('banners.reset')</span>
            </a>

            <?php
            $newRequest = request()->all();
            $newRequest = http_build_query($newRequest);
            ?>
            <a href="{{route ('banners.export') . '?' . $newRequest}}" class="pl-0 pr-0 btn btn-info w-34 ml-2 d-flex btn-responsive justify-content-center">
                <i class="fas fa-cloud-download-alt img-redo mr-2"></i>
                <span class="custom-FontSize">@lang('banners.excel')</span>
            </a>
        </div>
    </div>
</form>
<div class="card">
        <div class="custom-overflow">
            <table class="table aiz-table mb-0">
                <thead>
                    <tr>
                        <th class="w-15 ">@lang('banners.id')</th>
                        <th class="w-10">@lang('banners.img')</th>
                        <th class="w-15">@lang('banners.pos')</th>
                        <th class="w-20 custom-link-banner">@lang('banners.link')</th>
                        <th class="w-15">@lang('banners.start')</th>
                        <th class="w-15">@lang('banners.end')</th>
                        <th class="w-15">@lang('banners.status')</th>
                        <th class="text-right">@lang('')</th>
                    </tr>
                </thead>
                <tbody>
                @if(count($banners ?? []))

                    @foreach($banners as $item)
                        <tr  >
                                <th class="font-weight-800">#{{$item->id}}</th>
                                <th class="">
                                @if(!empty($item->images))
                                <img src="{{ my_asset($item->images)}}" class="img-ava22 mr-2" alt="{{$item->title}}">
                                @else
                                <img class="img-ava22 mr-2" src="/public/assets/img/icons/no-img.png" alt="{{ $item->title }}">
                                @endif
                                </th>
                                <th class="font-weight-500">{{optional($item)->checkPosition()}}</th>
                                <th class="font-weight-500">
                                    @if(optional($item)->collection_id)
                                    #{{optional($item)->collection_id}}
                                    @else
                                    <p></p>
                                    @endif
                                </th>
                                <th class="font-weight-500">{{Date('Y/m/d', strtotime($item->start_date))}}</th>
                                <th class="font-weight-500">{{Date('Y/m/d', strtotime($item->end_date))}}</th>
                                <th class="font-weight-500">{{optional($item)->checkStatus()}}</th>
                                <th class="text-right p-0">
                                    <div class="dropdown">
                                        <button class="btn btn-drop mt-1" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-h"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="{{route('banners.detail', ['id'=>$item->id])}}">@lang('orders.view')</a>
                                            @if($item->status==1)
                                                <a class="dropdown-item click-change-status-2" status-id="{{$item->id}}"  href="javascript:void(0)">@lang('banners.on_air')</a>
                                                <a class="dropdown-item click-change-status-3" status-id="{{$item->id}}"  href="javascript:void(0)">@lang('banners.complete')</a>
                                                <a class="dropdown-item click-change-status-4" status-id="{{$item->id}}"  href="javascript:void(0)">@lang('banners.cancel')</a>
                                            @endif
                                            @if($item->status==2)
                                                <a class="dropdown-item click-change-status-1" status-id="{{$item->id}}"  href="javascript:void(0)">@lang('banners.stand_by')</a>
                                                <a class="dropdown-item click-change-status-3" status-id="{{$item->id}}"  href="javascript:void(0)">@lang('banners.complete')</a>
                                                <a class="dropdown-item click-change-status-4" status-id="{{$item->id}}"  href="javascript:void(0)">@lang('banners.cancel')</a>
                                            @endif
                                            @if($item->status==3)
                                                <a class="dropdown-item click-change-status-1" status-id="{{$item->id}}"  href="javascript:void(0)">@lang('banners.stand_by')</a>
                                                <a class="dropdown-item click-change-status-2" status-id="{{$item->id}}"  href="javascript:void(0)">@lang('banners.on_air')</a>
                                                <a class="dropdown-item click-change-status-4" status-id="{{$item->id}}"  href="javascript:void(0)">@lang('banners.cancel')</a>
                                            @endif
                                            @if($item->status==4)
                                                <a class="dropdown-item click-change-status-1" status-id="{{$item->id}}"  href="javascript:void(0)">@lang('banners.stand_by')</a>
                                                <a class="dropdown-item click-change-status-2" status-id="{{$item->id}}"  href="javascript:void(0)">@lang('banners.on_air')</a>
                                                <a class="dropdown-item click-change-status-3" status-id="{{$item->id}}"  href="javascript:void(0)">@lang('banners.complete')</a>
                                            @endif
                                            <a class="dropdown-item btn-delete" data-id="{{$item->id}}" href="javascript:void(0)">@lang('banners.del')</a>
                                        </div>
                                    </div>
                                </th>
                            </a>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
</div>
<div class="aiz-pagination paginationOrder">
    {{ optional($banners)->appends(request()->input())->links() }}
</div> 
  
@endsection

@section('script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript">
    $('.custom-drop').click(function(){
        // alert();
        $(this).find('.custom-ul').toggle('d-none');
    })


    $(document).on('click', '.click-changeStatusBanner', function() {
        var status_id = $(this).attr('status-id');
        // alert(test);
        Swal.fire({
            title: "{{__('banners.change-statuss')}}",
            text: '@lang('banners.do-you-want')',
            // icon: 'error',
            confirmButtonText: '@lang('banners.yes')',
            cancelButtonText: '@lang('banners.no')',
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": status_id,
                };
                $.ajax({
                    method: "POST",
                    url: '{{route("banners.status", 0)}}' + status_id,
                    // data: data,
                    success: function(response) {
                        /* Read more about isConfirmed, isDenied below */
                        // Swal.fire('@lang('banners.saved')', '', 'success').then(() => {
                            location.reload()
                        // });
                    },
                    error: function(err) {
                        // Swal.fire('@lang('banners.not-saved')', '', 'info');
                    }
                });
            }
        });
    });

    $(document).on('click', '.click-change-status-1', function() {
        var status_id = $(this).attr('status-id');
        // alert(test);
        Swal.fire({
            title: "{{__('banners.change-statuss')}}",
            text: '@lang('banners.do-you-want')',
            // icon: 'error',
            confirmButtonText: '@lang('banners.yes')',
            cancelButtonText: '@lang('banners.no')',
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": status_id,
                };
                $.ajax({
                    method: "POST",
                    url: '{{route("banners.changeStatus1", 0)}}' + status_id,
                    // data: data,
                    success: function(response) {
                        /* Read more about isConfirmed, isDenied below */
                        // Swal.fire('@lang('banners.saved')', '', 'success').then(() => {
                            location.reload()
                        // });
                    },
                    error: function(err) {
                        // Swal.fire('@lang('banners.not-saved')', '', 'info');
                    }
                });
            }
        });
    });

    $(document).on('click', '.click-change-status-2', function() {
        var status_id = $(this).attr('status-id');
        // alert(test);
        Swal.fire({
            title: "{{__('banners.change-statuss')}}",
            text: '@lang('banners.do-you-want')',
            // icon: 'error',
            confirmButtonText: '@lang('banners.yes')',
            cancelButtonText: '@lang('banners.no')',
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": status_id,
                };
                $.ajax({
                    method: "POST",
                    url: '{{route("banners.changeStatus2", 0)}}' + status_id,
                    // data: data,
                    success: function(response) {
                        /* Read more about isConfirmed, isDenied below */
                        // Swal.fire('@lang('banners.saved')', '', 'success').then(() => {
                            location.reload()
                        // });
                    },
                    error: function(err) {
                        // Swal.fire('@lang('banners.not-saved')', '', 'info');
                    }
                });
            }
        });
    });

    $(document).on('click', '.click-change-status-3', function() {
        var status_id = $(this).attr('status-id');
        // alert(test);
        Swal.fire({
            title: "{{__('banners.change-statuss')}}",
            text: '@lang('banners.do-you-want')',
            // icon: 'error',
            confirmButtonText: '@lang('banners.yes')',
            cancelButtonText: '@lang('banners.no')',
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": status_id,
                };
                $.ajax({
                    method: "POST",
                    url: '{{route("banners.changeStatus3", 0)}}' + status_id,
                    // data: data,
                    success: function(response) {
                        /* Read more about isConfirmed, isDenied below */
                        // Swal.fire('@lang('banners.saved')', '', 'success').then(() => {
                            location.reload()
                        // });
                    },
                    error: function(err) {
                        // Swal.fire('@lang('banners.not-saved')', '', 'info');
                    }
                });
            }
        });
    });

    $(document).on('click', '.click-change-status-4', function() {
        var status_id = $(this).attr('status-id');
        // alert(test);
        Swal.fire({
            title: "{{__('banners.change-statuss')}}",
            text: '@lang('banners.do-you-want')',
            // icon: 'error',
            confirmButtonText: '@lang('banners.yes')',
            cancelButtonText: '@lang('banners.no')',
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": status_id,
                };
                $.ajax({
                    method: "POST",
                    url: '{{route("banners.changeStatus4", 0)}}' + status_id,
                    // data: data,
                    success: function(response) {
                        /* Read more about isConfirmed, isDenied below */
                        // Swal.fire('@lang('banners.saved')', '', 'success').then(() => {
                            location.reload()
                        // });
                    },
                    error: function(err) {
                        // Swal.fire('@lang('banners.not-saved')', '', 'info');
                    }
                });
            }
        });
    });

    $(document).on('click', '.btn-delete', function() {
        var delete_id = $(this).attr('data-id');
        // alert(test);
        Swal.fire({
            title: "{{__('banners.delete-banner')}}",
            text: '@lang('banners.do-you-want')',
            // icon: 'error',
            confirmButtonText: '@lang('banners.yes')',
            cancelButtonText: '@lang('banners.no')',
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": delete_id,
                };
                $.ajax({
                    method: "DELETE",
                    url: '{{route("banners.bannersDelete", 0)}}' + delete_id,
                    // data: data,
                    success: function(response) {
                        /* Read more about isConfirmed, isDenied below */
                        // Swal.fire('@lang('banners.saved')', '', 'ra').then(() => {
                            location.reload()
                        // });
                    },
                    error: function(err) {
                        // Swal.fire('@lang('banners.not-saved')', '', '정보');
                    }
                });
            }
        });
    });

    // date picker banners
    $('#click-dateBanners').daterangepicker({
        autoUpdateInput: false,
        minDate: '1921/01/01',
        singleDatePicker: true,
        showDropdowns: true,
        minYear:2010,
        locale: {

            format: 'YYYY/MM/DD',
            applyLabel: "대다",
                cancelLabel: "취소",
                "monthNames":[
                    "일월" , // Tháng 1
                    "이월" , // Tháng 2
                    "삼월" , // Tháng 3
                    "사월" , // Tháng 4
                    "오월" , // Tháng 5
                    "유월" , // Tháng 6
                    "칠월" , // Tháng 7
                    "팔월" , // Tháng 8
                    "구월" , // Tháng 9
                    "시월" , // Tháng 10
                    "십일월" , // Tháng 11
                    "십이월" , // Tháng 12
                ],
                daysOfWeek:[
                    "월요일" ,// Thứ 2
                    "화요일" ,// Thứ 3
                    "수요일" ,// Thứ 4
                    "목요일" ,// Thứ 5
                    "금요일" ,// Thứ 6
                    "토요일" ,// Thứ 7
                    "일요일" ,//  Chủ nhật.
                ]
            }
    });
    $('#click-dateBanners').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('YYYY/MM/DD'));
    });

    $('#click-dateBanners').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
    $('input[name="date"]').val('{{ request()->get('date') }}');

</script>
@endsection
