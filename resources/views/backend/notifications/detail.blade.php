@extends('backend.layouts.app')
@section('title')
    @lang('notifications.noti_detail')
@endsection
@section('content')
<div class="mb-4">
    {{-- @dump($errors) --}}
        <div class="backpage mb-5">
            <a href="{{url()->previous()}}" class="back btn"><i class="fas fa-chevron-left"></i></a>
        </div>
</div>
<div class="row  m-0">
    <div>
        @if($notification->images)
        <img src="{{static_asset($notification->images)}}" class="img-ava2 mr-2" alt="Search">
        @else
        <img src="{{static_asset('assets/img/icons/placeholder.jpg')}}" class="img-ava2 mr-2" alt="Search">
        @endif
    </div>
    <div>
        <p class="mb-0 font-weight-800 font-size-18"><strong>#{{optional($notification)->id}}</strong> </p>
        <p class="mb-1 font-weight-800 font-size-18"><strong>{{optional($notification)->title}}</strong></p>
        <label class="label-status {{$notification->status == 1 ? 'active' : ''}}">{{$notification->statusText()}}</label>
    </div>
</div>
<div class="mt-3 row ml-0 mr-0">
    <div class="col-md-3 detail-content1">
        <a href="javascript:void(0)" class="content1-img noti-info">
            <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail" alt="Search">
        </a>
        <div class="all-content1">
            <div>
                <p class="font-weight-800">@lang('notifications.id')</p>
                <p>#{{optional($notification)->id}}</p>
            </div>
            <div>
                <p class="font-weight-800">@lang('notifications.title')</p>
                <p>{{optional($notification)->title}}</p>
            </div>
            <div>
                <p class="font-weight-800">@lang('notifications.type')</p>
                <p>{{optional($notification)->typeText()}}</p>
            </div>
            <div>
                <p class="font-weight-800">@lang('notifications.created')</p>
                <p>{{date('Y/m/d',strtotime(optional($notification)->created_at))}}</p>
            </div>
  
        </div>
    </div>
    <div class="col-md-3 detail-content1">
        <a href="#" class="content1-img mark-notification-img">
            <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail" alt="Search">
            
        </a>
        <div class="all-content1">
            <div>
                <p class="font-weight-800">@lang('notifications.noti-img')</p>
                @if($notification->images)
                <div class="image-resize">
                    <img src="{{static_asset($notification->images)}}" class="img-item1" alt="Search">
                </div>
                @else
                <div class="image-resize">
                    <img src="{{static_asset('assets/img/icons/placeholder.jpg')}}" class="img-item1" alt="Search">
                </div>
                @endif
            </div>
        </div>

    </div>
    <div class="col-md detail-content2">
        <a href="#" class="content1-img noti-Description">
            <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail" alt="Search">
        </a>
        <div class="all-content1">
            <div>
                <p class="font-weight-800">@lang('notifications.noti-des')</p>
                <p>{{optional($notification)->content}}</p>
            </div>
        </div>
    </div>
</div> 

@endsection

@section('modal')
    @include('templates.popup.notifications-info',['edit' => true])
    @include('templates.popup.notifications-description',['edit'=> true])
    @include('templates.popup.notification-img-popup')
@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script type="text/javascript">
        $(document).on('click','.btn_update_infor',function(){
            var url = $(this).data('action');
            console.log(url)
            var title = $('#title').val();
            var type = $('#type').val();
            var phone = $('#phone').val();
            var date = $('#created_at').val();
            var created_at = date.replace(/\//g,'-');
            var data = {
                "token" : "{{csrf_token()}}",
                "title" : title,
                "type": type,
                "created_at": created_at,
            }
            $.ajax({
                url : url,
                data: data,
                type : "POST",
                success : function(res) {
                    $('.noti-infos').removeClass('show');
                    $('.error').hide();
                    AIZ.plugins.notify('success', "{{__('notifications.msg_infor_success')}}");
                    setTimeout( 
                    function() {
                        window.location.reload(true);
                    }, 2000);
                    
                },
                error : function(err) {
                    var response = JSON.parse(err.responseText);
                    console.log(response.errors)
                    printErrorMsg(response.errors)
                }
            })

            function printErrorMsg (msg) {
            $.each( msg, function( key, value ) {
            console.log(key);
              $('.'+key+'_err').text(value);
                });
            }
        })

        $(document).on('click','.btn_update_descript',function(){
            var url = $(this).data('action');
            // console.log(url)
            var content = $('#content').val();
            var data = {
                "token" : "{{csrf_token()}}",
                "content" : content,
            }
            $.ajax({
                url : url,
                data: data,
                type : "POST",
                success : function(res) {
                    $('.noti-Descriptions ').removeClass('show');
                    $('.error').hide();
                    AIZ.plugins.notify('success', "{{__('notifications.msg_descript_success')}}");
                    setTimeout( 
                    function() {
                        window.location.reload(true);
                    }, 2000);
                },
                error : function(err) {
                    var response = JSON.parse(err.responseText);
                    console.log(response.errors)
                    printErrorMsg(response.errors)
                }
            })

            function printErrorMsg (msg) {
            $.each( msg, function( key, value ) {
            console.log(key);
              $('.'+key+'_err').text(value);
                });
            }
        })

        $('.noti-info').click(function() {
            let url = $(this).attr('href');
            $('.noti-infos').addClass('show');
        });
        $('.noti-Description').click(function() {
            let url = $(this).attr('href');
            $('.noti-Descriptions').addClass('show');
        });
        $('.mark-notification-img').click(function() {
            let url = $(this).attr('href');
            $('.mark-notifications-img').addClass('show');
        });

        //
        function myFunction() {
            document.getElementById("status6");
        }
        function myFunction() {
            document.getElementById("status7");
        } 
          //Show image
          $('#input-file').change(function () {
            $('.btn-add-image-notifications').before(`<img id="output" src="${window.URL.createObjectURL(this.files[0])}" width="150" height="150">'`);
        });
        $('#click-notiInfor').daterangepicker({
        singleDatePicker: true,
        drops:'up',
        showDropdowns: true,
        minDate: '1921/01/01',
        locale: {
            format: 'YYYY/MM/DD',
            applyLabel: "대다",
            cancelLabel: "취소",
            monthNames:[
                    "일월" , // Tháng 1
                    "이월" , // Tháng 2
                    "삼월" , // Tháng 3
                    "사월" , // Tháng 4
                    "오월" , // Tháng 5
                    "유월" , // Tháng 6
                    "칠월" , // Tháng 7
                    "팔월" , // Tháng 8
                    "구월" , // Tháng 9
                    "시월" , // Tháng 10
                    "십일월" , // Tháng 11
                    "십이월" , // Tháng 12
                ],
                daysOfWeek:[
                    "월요일" ,// Thứ 2
                    "화요일" ,// Thứ 3
                    "수요일" ,// Thứ 4
                    "목요일" ,// Thứ 5
                    "금요일" ,// Thứ 6
                    "토요일" ,// Thứ 7
                    "일요일" ,//  Chủ nhật.
                ]
        }
        });
        $('#click-notiInfor').val('{{ date('Y-m-d',strtotime(optional($notification)->created_at)) }}');
        
        $(document).on('click','.btn_addimg',function(){
            var url = $(this).data('action');
            const parent = $(this).parents('form');
            $.ajax({
                url : url,
                data: parent.serialize(),
                type : "POST",
                statusCode : {
                    200 : () => {
                        $('.'+$(this).data('text')).addClass('done');
                        parent.find('.error').remove();
                    },
                    422 : ({responseJSON}) => {
                        parent.find('.error').remove();
                        const e = Object.entries(responseJSON.errors);
                        // console.log(e);
                        for (const [key,val] of e) {
                            parent.find(`[name="${key}"]`).parents('.row').after(`
                                <div class="error">${val}</div>
                            `)
                        }
                    }
                },
                success : function(res) {
                    $('#form-upload-image').submit();
                },
                error : function(err) {
                    console.log(err)
                }
            })
        })

        $('#created_at').val('{{ date('Y/m/d',strtotime(optional($notification)->created_at)) }}');

</script>

@endsection