@extends('backend.layouts.app')
@section('title')
@lang('notifications.noti')
@endsection
@section('content')
<div class="aiz-titlebar text-left mt-2 mb-3">
    <div class="align-items-center">
        <h1 class="h3"><strong>@lang('notifications.noti')</strong> </h1>
    </div>
</div>
<form class="" id="sort_customers" action="" method="GET">
    <div class="row mb-2">
        <div class="col-md-9 d-flex">
            <img src="{{ static_asset('assets/img/icons/search.svg') }}" class="img-search3 mr-2" alt="Search">
            <input type="text" class="form-control res-placeholder res-FormControl" id="search" name="search" value="{{request('search')}}"  placeholder="@lang('notifications.search_place')">
        </div>
        <div class="col-md-3 text-md-right res-FormControl notifi-padding">
            <a href="{{ route('notifications.create') }}" class="btn btn-circle btn-info btn-add-product d-flex">
                <i class="fas fa-plus-circle img-redo mr-2"></i>
                <span class="custom-FontSize">@lang('notifications.add')</span>
            </a>
        </div>
    </div>
    <div class="row gutters-5 mb-5">
        <div class="col-md-4 ">
            <!-- <input type="date" class="form-control custom-placeholder"  name="joined_date" > -->
            <input type="text" onkeypress="return event.charCode>=48 && event.charCode<=57" class=" form-control custom-placeholder res-FormControl" autocomplete="off"
              id="search-notiForm" name="joined_date" value="{{request()->get('joined_date')}}" placeholder="@lang('notifications.created')">
            <div class="custom-down"><i class="fas fa-chevron-down"></i></div>
        </div>
        <div class="col-md-4 res-status">
            <select class="form-control form-control-sm aiz-selectpicker res-FormControl mb-md-0 font-weight-800"  name="status">
                <option value="">@lang('notifications.status')</option>
                <option value="1" {{request()->status == 1 ? 'selected' : ''}}>@lang('notifications.active')</option>
                <option value="2" {{request()->status == 2 ? 'selected' : ''}}>@lang('notifications.inactive')</option>
            </select>
        </div>
        <div class="col-md all-btn-search">
            <button type="submit" class=" pl-0 pr-0 btn btn-info w-32 mr-2 d-flex btn-responsive justify-content-center">
                <i class="fas fa-search img-redo mr-2 "></i>
                <span class="custom-FontSize">@lang('notifications.btn_search')</span>
            </button>
            <a href="{{ route('notifications.index') }}" class="pl-0 pr-0 w-32 btn btn-info ml-2 mr-2 d-flex btn-responsive justify-content-center">
                <i class="fas fa-redo-alt img-redo mr-2"></i>
                <span class="custom-FontSize">@lang('notifications.btn_reset')</span>
            </a>
            <?php
                $request = request()->all();
                $newRequest = http_build_query($request);
            ?>
            <a href="{{route('notifications.export') . '?' . $newRequest}}" class="pl-0 pr-0 btn btn-info w-34 ml-2 d-flex btn-responsive justify-content-center">
                <i class="fas fa-cloud-download-alt img-redo mr-2"></i>
                <span class="custom-FontSize">{{translate('EXCEL')}}</span>
            </a>
        </div>
    </div>
</form>
<br>
<div class="card">
    <div class="custom-overflow">
        <table class="table aiz-table mb-0">
            <thead>
                <tr>
                    <th class="w-15">@lang('notifications.id')</th>
                    <th class="w-15">@lang('notifications.img')</th>
                    <th class="w-32">@lang('notifications.title')</th>
                    <th class="w-15">@lang('notifications.type')</th>
                    <th class="w-15">@lang('notifications.created')</th>
                    <th class="w-15">@lang('notifications.status')</th>
                    <th class="text-right">@lang('')</th>
                </tr>
            </thead>
            <tbody>
                @if(count($notifications) && !empty($notifications))
                @foreach($notifications as $notification) 
                <tr>
                    <th class="font-weight-800">#{{optional($notification)->id}}</th>
                    <th class="">
                        @if($notification->images)
                        <img src="{{ static_asset(optional($notification)->images) }}" class="img-index mr-2" alt="Search">
                        @else
                          <img class="empty_image mr-2" src="{{no_asset()}}" alt="No image">
                        @endif
                    <th class="font-weight-500 noti-content">{{optional($notification)->title}}</th>
                    <th class="font-weight-500">{{optional($notification)->typeText()}}</th>
                    <th class="font-weight-500">{{date('Y/m/d',strtotime(optional($notification)->created_at))}}</th>
                    <th class="font-weight-500">{{optional($notification)->statusText()}}</th>
                    <th class="text-right p-0">
                        <div class="dropdown">
                            <button class="btn btn-drop mt-1" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-h"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="{{route('notifications.detail',optional($notification)->id)}}">@lang('notifications.view')</a>
                                <div class="changeStatus">
                                    <form  class="change_status_form" class="mr-2">
                                        @csrf
                                        <input type="hidden" id="status" name="status" value="{{ optional($notification)->status == 1? '2':'1'}}">
                                        <a class="dropdown-item toggle-active change-status" data-id="{{optional($notification)->id}}" href="javascript:void(0)" type="sum" >
                                            @if (optional($notification)->status==2)
                                                @lang('notifications.active')
                                            @else
                                                @lang('notifications.inactive')
                                            @endif
                                        </a>
                                    </form>
                                </div>
                                <a class="dropdown-item delete-noti" href="javascript:void(0)" data-id="{{optional($notification)->id}}">@lang('notifications.delete')</a>
                                <a class="dropdown-item send-noti" href="javascript:document.querySelector('#noti-{{optional($notification)->id}}').submit()" >@lang('notifications.noti-push')</a>
                                <form action="{{route('notifications.send',optional($notification)->id)}}" method="post" id="noti-{{optional($notification)->id}}">
                                    @csrf
                                </form>
                            </div>
                        </div>
                    </th>
                    </a>
                    </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>
<div class="aiz-pagination paginationOrder">
    {{ optional($notifications)->appends(request()->input())->links() }}
</div> 
@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript">


    $('.custom-drop').click(function() {
        // alert();
        $(this).find('.custom-ul').toggle('d-none');
    })

    $(document).on('click', '.delete-noti', function() {
        let delete_id= $(this).attr('data-id');

        Swal.fire({
            title: '@lang('notifications.delete-noti')',
            text: "{{__('notifications.confirm_popup')}}",
            // icon: 'error',
            confirmButtonText: "{{__('notifications.yes')}}",
            cancelButtonText: "{{__('notifications.no')}}",
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": delete_id,
                };
                $.ajax({
                    type: "DELETE",
                    url: '/admin/notifications/destroy/'+delete_id,
                    data: data,
                    success: function (response){
                        /* Read more about isConfirmed, isDenied below */
                        location.reload();
                            
                    },
                    error: function(err) {
                        Swal.fire('Changes are not saved', '', 'info');
                    }
                });
            }
        });
    });

    // Change status
    $(document).on('click', '.change-status', function() {
        var status_id = $(this).attr('data-id');
        var status = $('#status').val();
        Swal.fire({
            title: "{{__('notifications.popup_status')}}",
            text: "{{__('notifications.confirm_popup')}}",
            // icon: 'error',
            confirmButtonText: "{{__('notifications.yes')}}",
            cancelButtonText: "{{__('notifications.no')}}",
            showCancelButton: true,
            showCloseButton: true,

        }).then((result) => {
            if (result.isConfirmed) {
                var data = {
                    "_token": "{{ csrf_token() }}",
                    "id": status_id,
                    "status" : status
                };
                $.ajax({
                    type: "POST",
                    url: '/admin/notifications/status/'+status_id,
                    data: data,
                    success: function (response){
                        /* Read more about isConfirmed, isDenied below */
                        location.reload();
                        
                    },
                    error : function(err) {
                        Swal.fire('Changes are not saved', '', 'info');
                    }
                });
            }
        });
    });

    $('#search-notiForm').daterangepicker({
        autoUpdateInput: false,
        minDate: '1921/01/01',
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'YYYY/MM/DD',
            applyLabel: "대다",
            cancelLabel: "취소",
                "monthNames":[
                    "일월" , // Tháng 1
                    "이월" , // Tháng 2
                    "삼월" , // Tháng 3
                    "사월" , // Tháng 4
                    "오월" , // Tháng 5
                    "유월" , // Tháng 6
                    "칠월" , // Tháng 7
                    "팔월" , // Tháng 8
                    "구월" , // Tháng 9
                    "시월" , // Tháng 10
                    "십일월" , // Tháng 11
                    "십이월" , // Tháng 12
                ],
                daysOfWeek:[
                    "월요일" ,// Thứ 2
                    "화요일" ,// Thứ 3
                    "수요일" ,// Thứ 4
                    "목요일" ,// Thứ 5
                    "금요일" ,// Thứ 6
                    "토요일" ,// Thứ 7
                    "일요일" ,//  Chủ nhật.
                ]
        }
    });
    $('#search-notiForm').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('YYYY/MM/DD'));
    });

    $('#search-notiForm').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });

    $('#search-notiForm').val('{{ request()->get('joined_date') }}');


   
</script>
@endsection