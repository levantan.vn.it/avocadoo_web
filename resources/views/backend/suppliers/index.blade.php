@extends('backend.layouts.app')
@section('title')
@lang('suppliers.supplier')
@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection

@section('content')

<div class="aiz-titlebar text-left mt-2 mb-3">
    <div class="align-items-center">
        <h1 class="h3 font-weight-bold">@lang('suppliers.supplier')</h1>
    </div>
</div>
<form class="" id="sort_customers" action="" method="GET">
    <div class="row mb-2 md-3">
        <div class="col-md-9">
            <img src="{{ static_asset('assets/img/icons/search.svg') }}"
                class="img-search3 mr-2" alt="Search">
            <input type="text" class="form-control res-placeholder" id="search" name="search"
                value="{{ request('search') }}" @isset($sort_search) @endisset
                placeholder="@lang('suppliers.search-sup')">

        </div>
        <div class="col-md-3 text-md-right res-FormControl">
            <a href="{{ route('suppliers.create') }}"
                class="btn btn-circle btn-info btn-add-product d-flex">
                <i class="fas fa-plus-circle img-redo mr-2"></i>
                <span>@lang('suppliers.add')</span>
            </a>
        </div>
    </div>
    <div class="row gutters-5 mb-5">
        <div class="col-md-4">
            {{-- <input type="text" onfocus="(this.type='date')" class="form-control" id="joined_date" name="joined_date"
                value="{{ request('joined_date') }}" placeholder="@lang('suppliers.add-date')"> --}}
                <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' autocomplete="off"
                class="form-control custom-placeholder" id="joined_date" name="joined_date"
                placeholder="{{__('users.joined_date')}}" value="{{ request('joined_date') }}"/>
            <div class="custom-down"><i class="fas fa-chevron-down"></i></div>
        </div>
        <div class="col-md-4 res-status">
            <select class="form-control form-control-sm aiz-selectpicker mb-2 mb-md-0" id="status" name="status">
                <option value="">@lang('suppliers.status')</option>
                <option value="1"
                    {{ request('status') == "1" ? 'selected' : '' }}>
                    @lang('suppliers.active')</option>
                <option value="2"
                    {{ request('status') == "2" ? 'selected' : '' }}>
                    @lang('suppliers.inactive')</option>
            </select>

        </div>

        <div class="col-md all-btn-search">
            <button type="submit" class="pl-0 pr-0 btn btn-info w-32 mr-2 d-flex  btn-responsive justify-content-center">
                <i class="fas fa-search img-redo mr-2"></i>
                <span>@lang('suppliers.search')</span>
            </button>
            <a href="{{ route('suppliers.index') }}"
                class="pl-0 pr-0 w-32 btn btn-info ml-2 mr-2 d-flex btn-responsive justify-content-center">
                <i class="fas fa-redo-alt img-redo mr-2"></i>
                <span>@lang('suppliers.reset')</span>
            </a>
            <?php
                $request = request()->all();
                $newRequest = http_build_query($request);
            ?>
            <a href="{{ route('suppliers.export') . '?' . $newRequest }}"
                class="pl-0 pr-0 btn btn-info w-34 ml-2 d-flex  btn-responsive justify-content-center">
                <i class="fas fa-cloud-download-alt img-redo mr-2"></i>
                <span>@lang('suppliers.excel')</span>
            </a>
        </div>

    </div>
</form>

<div class="card">
    <div class="card-body-table">
        <table class="table-collection aiz-table mb-0">
            <thead>
                <tr>
                    <th>@lang('suppliers.id')</th>
                    <th>@lang('suppliers.name')</th>
                    <th>@lang('suppliers.email')</th>
                    <th>@lang('suppliers.location')</th>
                    <th>@lang('suppliers.j-date')</th>
                    <th>@lang('suppliers.products')</th>
                    <th>@lang('suppliers.status')</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $item)
                    <tr>
                        <th>#{{ $item->id }}</th>
                        <td>
                            @php
                                $images = $item->images;
                            @endphp
                            @if(!empty($images) && !empty($images[0]))
                            <img class="imgtable mr-2" src="{{ $images[0] }}" alt="">
                            {{ $item->name }}
                            {{-- @if(!empty($images))
                                <img class="imgtable mr-2" src="{{ $images }}"
                                    alt="">{{ $item->name }} --}}
                            @else
                                <div class="d-flex">
                                    <div class=" image-avatar-supplier-index mr-2">
                                        <h1 class="text-image-avatar-supplier-index">
                                            {{ $item->name[0] }}
                                        </h1>
                                    </div>
                                    {{ $item->name }}
                                </div>
                            @endif
                        </td>
                        <td>{{ $item->email }}</td>
                        <td>{{ $item->location }}</td>
                        <td>{{ $item->created_at->format('Y/m/d') }}</td>
                        <td>{{ count($item->products) }}</td>
                        <td>{{ $item->statusText() }}</td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-drop" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-h"></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item "
                                        href="{{ route('suppliers.detail', ['id'=>$item->id]) }}">@lang('suppliers.view')</a>
                                    <div>
                                        <a href="{{ route('suppliers.updatestatus',$item) }}"
                                            data-active="{{ $item->isActive() }}" class="toggle-status dropdown-item">
                                            {{ $item->isActive() ? __('suppliers.inactive') : __('suppliers.active') }}
                                        </a>
                                    </div>
                                    <a class="dropdown-item suppliers-delete" href="javascript:void(0)"
                                        data-delete="{{ $item->id }}">@lang('users.del')</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>

    </div>
</div>

    <div class="aiz-pagination paginationOrder">
        {{ $data->appends(request()->input())->links() }}
    </div>




{{-- <div class="modal fade" id="confirm-ban">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title h6">{{ translate('Confirmation') }}</h5>
<button type="button" class="close" data-dismiss="modal"></button>
</div>
<div class="modal-body">
    <p>{{ translate('Do you really want to ban this Customer?') }}</p>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-light"
        data-dismiss="modal">{{ translate('Cancel') }}</button>
    <a type="button" id="confirmation" class="btn btn-primary">{{ translate('Proceed!') }}</a>
</div>
</div>
</div>
</div> --}}

{{-- <div class="modal fade" id="confirm-unban">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title h6">{{ translate('Confirmation') }}</h5>
<button type="button" class="close" data-dismiss="modal"></button>
</div>
<div class="modal-body">
    <p>{{ translate('Do you really want to unban this Customer?') }}</p>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-light"
        data-dismiss="modal">{{ translate('Cancel') }}</button>
    <a type="button" id="confirmationunban" class="btn btn-primary">{{ translate('Proceed!') }}</a>
</div>
</div>
</div>
</div> --}}
@endsection

@section('modal')
@include('modals.delete_modal')
@endsection


@section('script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script>
    function deleteItem(id) {
        let url = "{{ route('suppliers.destroy', 'id_replace') }}";
        url = url.replace('id_replace', id);
        $.ajax({
            url,
            method: "DELETE",
            dataType: "JSON",
            success: function (response) {
                console.log(response);
                if (response) {
                    location.reload();
                    
                } else {
                    Swal.fire(
                        '@lang('suppliers.delete_false')',
                        '',
                        'warning'
                    );
                }
            },
            error: function () {
                Swal.fire(
                    '@lang('suppliers.delete_false')',
                    '',
                    'warning'
                );
            }
        });
    }
    $(document).on('click', 'a.dropdown-item.suppliers-delete', function () {
        let delete_id = $(this).attr('data-delete');

        Swal.fire({
            title: '@lang('suppliers.delete')',
            text: '@lang('categories.continue')',
            confirmButtonText: '@lang('categories.yes')',
            cancelButtonText: '@lang('categories.no')',
            showCancelButton: true,
            showCloseButton: true,
        }).then((result) => {
            if (result.isConfirmed) {
                deleteItem(delete_id);
            }
        })
    });

    $(document).on('click', '.toggle-status', function (e) {
        e.preventDefault();
        const active = this.dataset.active;
        Swal.fire({
            title: active ? '@lang('suppliers.ques_inactive')' : '@lang('suppliers.ques_active')',
            text: '@lang('categories.continue')',
            confirmButtonText: '@lang('categories.yes')',
            cancelButtonText: '@lang('categories.no')',
            showCancelButton: true,
            showCloseButton: true,
        })
        .then((result) => {
            if (result.isConfirmed)
                $.post(this.href).then(() => {
                    location.reload();
                }).catch((e) => {
                    console.log('eerror ', e)
                })

        })
    });

    $('#joined_date').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        autoUpdateInput: false,
        minDate: '1921/01/01',
        locale: {
            format: 'YYYY/MM/DD',
            applyLabel: "대다",
            cancelLabel: "취소",
                "monthNames":[
                    "일월" , // Tháng 1
                    "이월" , // Tháng 2
                    "삼월" , // Tháng 3
                    "사월" , // Tháng 4
                    "오월" , // Tháng 5
                    "유월" , // Tháng 6
                    "칠월" , // Tháng 7
                    "팔월" , // Tháng 8
                    "구월" , // Tháng 9
                    "시월" , // Tháng 10
                    "십일월" , // Tháng 11
                    "십이월" , // Tháng 12
                ],
                daysOfWeek:[
                    "월요일" ,// Thứ 2
                    "화요일" ,// Thứ 3
                    "수요일" ,// Thứ 4
                    "목요일" ,// Thứ 5
                    "금요일" ,// Thứ 6
                    "토요일" ,// Thứ 7
                    "일요일" ,//  Chủ nhật.
                ]
        }
    });
    $('#joined_date').on('apply.daterangepicker', function(ev, picker){
        console.log(picker.startDate.format('YYYY/MM/DD'));

        $('#joined_date').val(picker.startDate.format('YYYY/MM/DD'));

    })
    @if(request('joined_date'))
    $('#joined_date').value("{{request('joined_date')}}");
    @endif

</script>
@endsection
