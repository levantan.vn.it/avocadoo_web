@extends('backend.layouts.app')
@section('title')
@lang('suppliers.supplier_detail')
@endsection
@section('content')
<div class="mb-4">
    <div class="backpage mb-5">
        <a href="{{url()->previous()}}" class="back btn"><i class="fas fa-chevron-left"></i></a>
    </div>
</div>
<div class="row m-0 ">
    <div>
        @if(!empty($images) && !empty($images[0]))
            <img class="avatar-supplier-detail mr-2" src="{{ $images[0] }}" alt="">
            @else
            <div class=" image-avatar-supplier-detail">
                <h1 class="text-image-avatar-supplier-detail">
                    {{$data->name[0]}}
                </h1>
            </div>
            @endif

    </div>
    <div class="avt-detail ml-1">
        <h6 class="mb-2 font-weight-800  ">#{{ $data->id }}</h6>
        <h5  class="mb-2 font-weight-800 ">{{ $data->name }}</h5>
        @if ($data->status==1)
            <label  class=" bg-success text-white lb-status-user" style="border-radius: 100px">@lang('suppliers.active')</label>
        @else
            <label  class=" bg-danger text-white lb-status-user" style="border-radius: 100px">@lang('suppliers.inactive')</label>
        @endif
    </div>
   <div class="icon-lock col-md">
        {{-- <form action="{{ route('suppliers.updatestatus',$data->id)}}" id="form-active-supplier" method="POST" class="mr-2">
            @csrf
            <input type="hidden" name="status" value="{{ $data->status == 1? '2':'1'}}">
            @if ($data->status==1)
            <button type="button" class="btn btn-info" id="InActive-supplier">
            @else --}}
            <button type="button" class="btn btn-info toggle-status" data-href="{{ route('suppliers.updatestatus', $data) }}">
                @if ($data->isActive())
                <i class="fas fa-lock img-redo"></i>
                @else
                <i class="fas fa-unlock-alt"></i>
                @endif
            </button>
            <!-- <button type="submit" class="btn btn-info "><i class="fas fa-lock img-redo "></i></button> -->
        {{-- </form> --}}
   </div>
</div>
<div class="mt-3 row ml-0 mr-0">
    <div class="col-md-3 detail-content1">
        <a href="javascript:void(0)" class="content1-img suppliered-information">
            <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail" alt="">
        </a>
        <div class="all-content1">
            <div>
                <label class="font-weight-bold">@lang('suppliers.id')</label>
                <p>#{{ $data->id }}</p>
            </div>
            <div>
                <label class="font-weight-bold">@lang('suppliers.sup-name')</label>
                <p>{{ $data->name }}</p>
            </div>
            <div>
                <label class="font-weight-bold">@lang('suppliers.sup-address')</label>
                <p> {{ $data->address }}</p>
            </div>
            <div>
                <label class="font-weight-bold">@lang('suppliers.location')</label>
                <p>{{ $data->location }}</p>
            </div>
            <div>
                <label class="font-weight-bold">@lang('suppliers.sup-phone')</label>
                <p>
                <?php
                        $number = $data->phone;
                        echo (substr($number, 0,3) . '-') . (substr($number, 3,4) . '-') . (substr($number, 7,4));
                ?>
                </p>
            </div>
            <div>
                <label class="font-weight-bold">@lang('suppliers.email')</label>
                <p>{{ $data->email }}</p>
            </div>
            <div>
                <label class="font-weight-bold">@lang('suppliers.sup-rep')</label>
                <p>{{ $data->representative }}</p>
            </div>
            <div>
                <label class="font-weight-bold">@lang('suppliers.j-date')</label>
                <p>{{ $data->created_at->format('Y/m/d') }}</p>
            </div>
        </div>
    </div>
    <div class="col-md-3 detail-content1">
        <a href="javascript:void(0)" class="content1-img suppliered-image">
            <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail" alt="">
        </a>
        <div class="all-content1">
            <div>
                <p class="font-weight-800">@lang('suppliers.img-sup')</p>
                {{-- <img src="{{ static_asset($data->image->file_name)}}" class="img-item1" alt="Search"> --}}
            </div>
            <div class="all-ImgContent mt-3 row">

            @if(count($images ?? []))
                @foreach($images as $image)
                @if ($loop->first)
                    <img src="{{ $image }}" class="img-supplier-detail-1 ml-2 mb-2" >
                @else
                <div>
                    <img src="{{ $image }}" class="img-supplier-detail-2 " >
                </div>
                @endif
                @endforeach
            @endif
            </div>
            {{-- <div>
                <p class="font-weight-800">@lang('suppliers.img-sup')</p>
                <img src="/public/assets/img/avatar-place.png" class="img-item1" alt="Search">
            </div>
            <div class="all-ImgContent mt-3">
            <img src="/public/assets/img/avatar-place.png" class="img-item2" alt="Search">
            </div> --}}
            {{-- @endif --}}
        </div>
    </div>

    <div class="col-md detail-content2">
        <div class="all-content1">
            <div>
                <p class="font-weight-800">@lang('suppliers.products')</p>
            </div>
            <div class="overflow-auto">
                @foreach ($data->products as $item)
                <table>
                    <tbody>
                        <tr>
                            <td rowspan="3">
                                @if($item->images)
                                @php
                                    $pro_img = explode(',',$item->images);
                                @endphp
                                <img class="img-table mr-4" src="{{my_asset($pro_img[0])}}" alt="img" >
                                @else
                                <img class="img-table mr-4" src="{{my_asset('/assets/img/placeholder.jpg')}}" alt="img" >
                                @endif
                            </td>
                            <td> SKU: <b>{{  $item->min_price_sku }}</b></td>
                        </tr>
                        <tr>
                            <td>{{ $item->name }} </td>
                        </tr>
                        <tr>
                            @if (count($item->skus))
                            <td>
                                <?php
                                $number =  $item->min_price;
                                echo( format_price( $number).' 원');
                                ?>
                                 {{-- {{ $item->min_price }} 원 --}}
                            </td>
                        @endif
                        </tr>
                    </tbody>
                </table>

                {{-- <div class="d-flex">
                    <div>
                        <img class="img-table mr-4" src="{{ optional($item->images->first())->file_url}}" alt="img" >
                    </div>
                    <div>
                        <p>
                            SKU: <b>{{  $item->min_price_sku }}</b>
                        </p>
                        <p>
                            {{ $item->name }}
                        </p>
                        <p>
                            @if (count($item->skus))
                                 {{ $item->min_price }} 원
                            @endif
                        </p>
                    </div>
                </div> --}}

                @endforeach


            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
    @include('templates.popup.supplier-information-popup')
    @include('templates.popup.supplier-images-popup')
@endsection

@section('script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script type="text/javascript">
        // $('#').submit().click(function () {
        //     $('#').show()
        //  })

        $('.img-ava2').click(function(){
            $('div .img-ava2').addClass('test')
            console.log('fa')
        })
        $('.suppliered-information').click(function() {
            let url = $(this).attr('href');
            $('.supplier-information').addClass('show');
        });
        $('.suppliered-image').click(function() {
            let url = $(this).attr('href');
            $('.supplier-images').addClass('show');
        });

        $(document).ready(function(){
            @error('SupplierRequest')
                document.querySelector('.supplier-information').classList.add('show');
            @enderror
	 	});

         $("#maskpopupinfor").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
        var curchr = this.value.length;
        var curval = $(this).val();
            if (curchr == 11 ) {
                $(this).val(curval);
            }
        $(this).attr('maxlength', '11');
    });

// Active Supplier

$(document).on('click', '.toggle-status', function(e) {
        const active = $(this).find('.fa-lock')?.length ?? true;
        Swal.fire({
            title: active ? '@lang('suppliers.ques_inactive')' : '@lang('suppliers.ques_active')',
            text: '@lang('categories.continue')',
            confirmButtonText: '@lang('categories.yes')',
            cancelButtonText: '@lang('categories.no')',
            showCancelButton: true,
            showCloseButton: true,
        }).then((result) => {
            if (result.isConfirmed){
                $.post(this.dataset.href).then(()=>{
                    // location.href = "{{ route('suppliers.detail', 0) }}";
                    location.reload();

                }).catch(err=>{
                    console.log('Er: ', err)
                })
            }
        })
    });



</script>

@endsection
