@extends('backend.layouts.app')
@section('title')
@lang('suppliers.add')
@endsection
@section('content')

<div class="mb-4">
    <a href="{{ route('suppliers.index') }}">
        <button class="btn-back">
            <img src="{{ static_asset('assets/img/icons/back-admin.svg') }}"
                class="img-search mr-2" alt="">
        </button>
    </a>
</div>
<div class="row m-0 ">
    <div class="icon-lock col-md">
        <button type="submit" class="btn-info btn btn-create-now">@lang('suppliers.create')</button>
    </div>
</div>
<div class=" row mt-3">
    <div class="col-md-3 px-2 box-supplier">
        <div class="bg-white">
        <button type="button" style="height: 300px" class="btn w-100 border-0 suppliered-add-information">
            <i class="fas fa-plus-circle d-block supplier-check"></i>
            <span class="mt-3 d-block">@lang('suppliers.add-sup-info')</span>
        </button>
        <div class="d-none ml-4" id="info">
            <a href="javascript:void(0)" class="content1-img suppliered-show-information mr-2">
                <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail" alt="">
            </a>
            <button type="button" style="height: 300px" class="btn w-100 border-0">
                <i class="fas fa-check text-success d-block supplier-check"></i>
                <span class="mt-3 d-block">@lang('suppliers.add-sup-img')</span>
            </button>
        </div>

        </div>
    </div>
    <div class="col-md-3 px-2 box-supplier">
        <div class="bg-white validate-success">
            <button type="button" style="height: 300px" class="btn w-100  border-none suppliered-add-image">
                <i class="fas d-block fa-plus-circle supplier-check"></i>
                <span class="mt-3 d-block">@lang('suppliers.add-sup-img')</span>
            </button>
        </div>
        <div id="image" class="d-none bg-white editimg">
            <a href="javascript:void(0)" class="content1-img suppliered-show-image  mr-2">
                <img src="{{ static_asset('assets/img/icons/edit.png') }}" class="img-EditDetail" alt="">
            </a>
            <button type="button" style="height: 300px" class="btn w-100 border-0">
                <i class="fas fa-check text-success d-block supplier-check"></i>
                <span class="mt-3 d-block">@lang('suppliers.add-sup-info')</span>
            </button>
        </div>
    </div>
    <div class="col-md-6 px-2 box-supplier">
        <div style="height: 300px" class="bg-white">
        <p class="font-weight-800 pt-3 pl-3"> @lang('suppliers.product')</p>
        </div>
    </div>
</div>
@endsection

@section('modal')
<form name="create" class="create_suppliers" action="{{ route('suppliers.createinfo') }}" method="POST">
    @csrf
    @include('templates.popup.supplier-add-information')
    @include('templates.popup.supplier-add-img')
</form>
@endsection

@section('script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script type="text/javascript">
    // upload image success
    $(document).on('click','.click-upload-images',function(e){
        // $('#image').removeClass('d-none');
        // $('.validate-success').addClass('d-none')

        $('.text-danger-image').remove();
        let noErr = true;
        const msgErr = `<div class='error mt-3 text-danger-image'>@lang('suppliers.required-image')</div>`;
        const msgErr2= `<div class='error mt-3 text-danger-image'>@lang('suppliers.required-main-image')</div>`;
        if( !$('.supplier-add-imgs .children_image_supplier_1 .file-preview-item').length ){
            $('.children_image_supplier_1').append(msgErr);

            noErr = false;
        }else{
            $('#image').removeClass('d-none');
            $('.children_image_supplier_1 .validate-success').addClass('d-none');
            $('.children_image_supplier_1 .supplier-add-imgs').show();
            $('#image').removeClass('d-none');
            $('.validate-success').addClass('d-none')
        $('.error.mt-1').remove();

        }
        // if( !$('.supplier-add-imgs .children_image_supplier_2 .file-preview-item').length ){
        //     $('.children_image_supplier_2').append(msgErr2);

        //     noErr = false;
        // }else{
        //     $('#image').removeClass('d-none');
        //     $('.children_image_supplier_2 .validate-success').addClass('d-none');
        //     $('.children_image_supplier_2 .supplier-add-imgs').show();
        //     $('#image').removeClass('d-none');
        //     $('.validate-success').addClass('d-none')
        // $('.error.mt-1').remove();

        // }
        return noErr;
    })
    var checkInfor =false;
    //ajax validate info
    $(document).on('click','.click-validateDelivery',function(){
            var url = $(this).data('action');
            var name = $('#name').val();
            var address = $('#address').val();
            var location = $('#location').val();
            var representative=$('#representative').val();
            var phone=$('#phone').val();
            var email=$('#email').val();
            data = {
                "token" : "{{csrf_token()}}",
                "name": name,
                "address": address,
                "location" : location,
                "representative" : representative,
                "phone": phone,
                'email': email,
            };
            $.ajax({
                url: url,
                data: data,
                type: "POST",
                beforeSend: function(){
                    $('.supplier-add-information .error').remove();
                    $('.supplier-add-information .invalid-feedback').remove();
                    //  $('.form-control').removeClass('is-invalid');
                },
                success: function(res){
                $('#info').removeClass('d-none');
                $('.suppliered-add-information').addClass('d-none');
                $('.supplier-add-information').removeClass('show');
                checkInfor=true;
                },
                error : function(err) {
                    const {errors} = err.responseJSON;
                    console.log(err);
                    Object.keys(errors).forEach(item=>{
                        let errorElement = $(` <p class="error mt-1"> ${errors[item]}</p>`);
                        $(`[name="${item}"]`).parent().append(errorElement);
                    });
                }
            })

        });

    $('.suppliered-add-information, .suppliered-show-information').click(function () {
        let url = $(this).attr('href');
        $('.supplier-add-information').addClass('show');
    });
    $(document).on('click','.suppliered-add-image',function(){
        $('.supplier-add-imgs').addClass('show');

    })
    $(document).on('click','.btn-create-now',function(){
        $('.create_suppliers').submit();


    });
    $(document).on('click','.suppliered-add-image, .suppliered-show-image',function () {
        $('.supplier-add-imgs').addClass('show');
    });


    $(document).ready(function(){

        @error('AddSupplierRequest')
            document.querySelector('.supplier-add-information').classList.add('show');
        @enderror
    });
    @if($errors->any() && !$errors->first('AddSupplierRequest'))
    $('#info').removeClass('d-none');
    $('.suppliered-add-information').addClass('d-none');
    $('.supplier-add-information').removeClass('show');
    checkInfor=true;
    @endif

    @if($errors->has('id_files'))
            $('.btn.w-100.border-none.suppliered-add-image').click();
    @elseif ($errors->any() )
    $('#image').removeClass('d-none');
    $('.children_image_supplier_1 .validate-success').addClass('d-none');
    $('.children_image_supplier_1 .supplier-add-imgs').show();
    $('#image').removeClass('d-none');
    $('.validate-success').addClass('d-none')
    @endif
</script>
@endsection
