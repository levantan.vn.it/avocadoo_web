<div class="add-child-category popup-db">
    <div class="popup-products-edit popup-add-child">
        <form action="{{ route('categories.createchild') }}" method="post" class="redirect-popup d-inline-block w-100">
            @csrf
            <div class="popup-header">
                <a href="javascript:document.querySelector('.add-child-category').classList.remove('show')"
                    class="text-yl close-model">
                    <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">

                </a>
            </div>
            <div class="row m-0 all-form-productid mb-3 ">
                <div class="w-48">
                    <p class="font-weight-800 d-flex">@lang('categories.title-cate') &nbsp; <span class="text-danger" style="margin-top: -3px">*</span></p>
                    <input type="text" name="title" class="form-control @error('title') is-invalid  @enderror" placeholder="@lang('categories.title-cate')">

                @error('title')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
                </div>
            </div>
            <div class="row m-0 all-form-productid mb-3">
                <div class="w-48">
                    <p class="font-weight-800">@lang('categories.type-cate')</p>
                    <input type="text" class="form-control" readonly placeholder="@lang('categories.child-cate')">
                </div>
                <div class="w-48">
                    <p class="font-weight-800">@lang('categories.pri-cate')</p>
                    <select class="form-control form-control-sm aiz-selectpicker mb-2 mb-md-0"
                        name="parent_id">
                        @foreach ($categories as $category)
                        <option value="{{ $category->id  }}" placeholder="">{{ $category->title }}</option>
                    @endforeach
                    </select>
                </div>
            </div>
            <div class="w-96 mb-3 img_child_cate">
                <p class="font-weight-800">@lang('categories.img-cate') &nbsp; <span class="text-danger" style="margin-top: -3px">*</span></p>
                <div class="banner-img-title mt-3">
                    <div class="kt-img row">
                        @include('backend.inc.upload_file',['values_file'=>null,'type'=>'single'])
                    </div>
                </div>
            </div>
            <div class="all-btn-group-popup">
                <a href="javascript:document.querySelector('.add-child-category').classList.remove('show')"
                    class="mr-3 btn  br-10">@lang('categories.cancel')</a>
                    <button type="button" href="" class="btn btn-info br-10 check-form">@lang('categories.save')</button>
            </div>
        </form>
    </div>
</div>

