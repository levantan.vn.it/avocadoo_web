<div class="mark-transactions popup-db">
   <div class="popup-products-edit br-10">
      <div class="popup-header">
         <a href="javascript:document.querySelector('.mark-transactions').classList.remove('show')"
            class="text-yl close-model">
            <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
         </a>
      </div>
      <div class="overflow-auto show-like">
         <table class="table">
            <thead>
               <tr>
                  <th scope="col">@lang('users.transaction_id')</th>
                  <th scope="col">@lang('users.date')</th>
                  <th scope="col">@lang('users.money')</th>
                  <th scope="col">@lang('users.method')</th>
                  <th scope="col">@lang('users.status')</th>
               </tr>
            </thead>
            <tbody>
               @if (count($customers->transaction))
                  @foreach ($customers->transaction as $transaction)
                     <tr>
                        <th scope="row">#{{$transaction->id}}</th>
                        <td> {{$transaction->created_at->format('Y/m/d')}} </td>
                        <td>
                           {{format_price($transaction->money)}} @lang('users.pri')
                        </td>
                        <td> {{$transaction->getCardBrand()}} **** **** ****
                           <?php 
                              echo substr($transaction->method, -4, 4); 
                           ?>
                        </td>
                        <td> {{$transaction->statusTran()}}</td>
                     </tr>
                  @endforeach
               @else
                  <tr>
                     <td class="text-center pb-0" colspan="5">
                           <h5 class="mb-0 pt-3">@lang('users.nodata_transaction')</h5>
                     </td>
                  </tr>
               @endif
            </tbody>
         </table>
      </div>
   </div>
</div>
