<div class="mark-member-img popup-db z-index-9">
    <div class="popup-products-edit">
        @if($edit)
        <form action="{{route('upload.update-image')}}" method="post" id="form-mark-deceased" class="redirect-popup">
            @csrf
        @endif
            <input type="hidden" name="id" value="{{$edit ? $member->id : ''}}">
            <input type="hidden" name="entity" value="member">
            <input type="hidden" name="route" value="members.edit">
            <div class="popup-header">
                <a href="javascript:document.querySelector('.mark-member-img').classList.remove('show')"
                  class="text-yl close-model">
                  <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
                </a>
            </div>

            <div>
                <b>@lang('banners.bn_img') </b>
            </div>
            <div class="banner-img-title mt-3">
                {{-- @dump(optional($member)->id_images) --}}
                <div class="kt-img row">
                    @include('backend.inc.upload_file',['values_file'=> $edit ? optional($member)->id_images : "",'type'=>'single'])
                </div>
                @error('id_files')
                <p class="error mt-1">{{ $message}}</p>
                @enderror
            </div>
            <div class="all-btn-group-popup">
                <a href="javascript:document.querySelector('.mark-member-img').classList.remove('show')"
                  class="mr-3 btn btn-info br-10 btn_cancel">@lang('members.cancel')</a>
                @if($edit)
                <a href="javascript:void(0)" class="btn btn-info br-10 btn-saveimg" data-action="{{route('members.validate_image')}}">
                    @lang('members.save')</a>
                @else 
                <a href="javascript:void(0)"
                  class="mr-3 btn btn-info br-10 btn-saveimg" data-action="{{route('members.validate_image')}}">@lang('members.save')</a>
                  @endif
            </div>
        @if($edit)
    </form>
    @endif
    </div>
</div>
