<form action="{{route('orders.updateInfoOrder',$order->id)}}" class="click-update-info" method="POST">
    @csrf
    <div class="click-change-status-order popup-db">
        <div class="popup-products-edit">
            <div class="popup-header">
                <a href="javascript:document.querySelector('.click-change-status-order').classList.remove('show')" class="text-yl close-model">
                    <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
                </a>
            </div>
            <div class="row m-0 all-form-productid mb-3">
                <div class="w-48">
                    <p class="font-weight-800">@lang('orders.user_name') <span class="text-danger">*</span></p>
                    <input type="text" autocomplete="off" class="form-control custom-placeholder" name="displayname" id="displayname" value="{{optional($order)->displayname}}">
                    @error('displayname')
                    <p class="error mt-1">{{ $message}}</p>
                    @enderror
                </div>
                <div class="w-48">
                    <p class="font-weight-800">@lang('orders.phone') <span class="text-danger">*</span></p>
                    <input type="number" autocomplete="off" class="form-control custom-placeholder" name="phone" id="phone" value="{{optional($order)->phone}}">
                    @error('phone')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
                </div>
            </div>
            <div class="row m-0 all-form-productid mb-3">
                <div class="w-48">
                    <p class="font-weight-800">@lang('orders.email') <span class="text-danger">*</span></p>
                    <input type="text" autocomplete="off" class="form-control custom-placeholder" name="email" id="email" value="@if(!empty($order->email)) {{$order->email}} @else{{optional($order->user)->email}}@endif">
                    @error('email')
                    <p class="error mt-1">{{ $message}}</p>
                    @enderror
                </div>
                <div class="w-48">
                    <p class="font-weight-800">@lang('orders.store_name') <span class="text-danger">*</span></p>
                    <input type="text" autocomplete="off" class="form-control custom-placeholder" name="store_name" id="store_name" value="{{optional($order)->store_name}}">
                    @error('store_name')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
                </div>
            </div>
            <div class="row m-0 all-form-productid mb-3">
                <div class="w-48">
                    <p class="font-weight-800">@lang('orders.delivery') <span class="text-danger">*</span></p>
                    <input type="text" autocomplete="off" class="form-control custom-placeholder" name="delivery_address" id="delivery_address" value="{{optional($order)->delivery_address}}">
                    @error('delivery_address')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
                </div>
            </div>
  
            <div class="all-btn-group-popup">
                <a href="javascript:document.querySelector('.click-change-status-order').classList.remove('show')" class="mr-3 btn  br-10">@lang('orders.cancel')</a>
                <button type="submit" class="mr-3 btn click-validateDelivery btn-info br-10" ">@lang('orders.save')</button>
            </div>
        </div>
    </div>
</form>