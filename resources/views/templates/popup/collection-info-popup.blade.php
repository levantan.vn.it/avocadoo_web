<div class="edit-info-collection popup-db">
    <div class="popup-products-edit">
        <div class="popup-header">
            <a href="javascript:document.querySelector('.edit-info-collection').classList.remove('show')"
                class="text-yl close-model">
                <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">

            </a>
        </div>
        @if($edit)
        <form id="form-mark-deceased" class="redirect-popup">
            @csrf
        @endif
        <div class="row m-0 all-form-productid mb-2 ">
            @if($edit)
            <div class="w-48 mb-3">
                <p class="font-weight-800">@lang('collections.id')</p>
                <input type="text" class="form-control" disabled value="#{{optional($data)->id}}" name="" >
            </div>
            @endif
            <div class="w-48 mb-3">
                <p class="font-weight-800">@lang('collections.title')<span class="required-icon"> *</span></p>
                <input type="text" id="name" class="form-control" value="{{$edit ? optional($data)->name : old('name')}}" name="name" >
                      <p class="error name_err mt-1"></p>
                      @error('name')
                      <p class="error mt-1">{{ $message}}</p>
                        @enderror
            </div>
            <div class="w-48 mb-3">
                <p class="font-weight-800">@lang('collections.section')<span class="required-icon"> *</span></p>
                @if($edit)
                <select class="form-control form-control-sm aiz-selectpicker mb-2 mb-md-0" id="section" name="section">
                    <option value="">{{ __('collections.select_section') }}</option>
                    <option value="1" {{optional($data)->section == 1 ? 'selected' : ''}}>{{ __('collections.hot_deal') }}</option>
                    <option value="2" {{optional($data)->section == 2 ? 'selected' : ''}}>{{ __('collections.special_events') }}</option>
                    <option value="3" {{optional($data)->section == 3 ? 'selected' : ''}}>{{ __('collections.end_season_sale') }}</option>
                </select>
                      <p class="error mt-1"></p>
                      @error('section')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
                @else
                <select class="form-control form-control-sm aiz-selectpicker mb-2 mb-md-0" id="section" name="section">
                    <option value="">{{ __('collections.select_section') }}</option>
                    <option value="1" {{request('section') == 1 ? 'selected' : ''}} >{{ __('collections.hot_deal') }}</option>
                    <option value="2" {{request('section') == 2 ? 'selected' : ''}}>{{ __('collections.special_events') }}</option>
                    <option value="3" {{request('section') == 3 ? 'selected' : ''}}>{{ __('collections.end_season_sale') }}</option>
                </select>
                      <p class="error section_err mt-1"></p>
                      @error('section')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
                @endif

            </div>
            <div class="w-48 mb-3">
                <p class="font-weight-800">@lang('collections.sort_descript') <span class="required-icon"> *</span></p>
                <input type="text" id="short_description"  class="form-control" value="{{$edit ? optional($data)->short_description : old('short_description')}}"  name="short_description" >
                <p class="error short_description_err  mt-1"></p>
                @error('short_description')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
           </div>

           <div class="w-48 mb-3">
            <p class="font-weight-800">@lang('collections.start_date') <span class="required-icon"> *</span></p>
            <input type="text" id="start_date"  class="form-control"
            value="{{$edit ? date('Y-m-d',strtotime(optional($data)->start_date)) : old('start_date')}}"  name="start_date">
                <p class="error start_date_err mt-1"></p>
                @error('start_date')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
            </div>
            <div class="w-48 mb-3">
                <p class="font-weight-800">@lang('collections.end_date') <span class="required-icon"> *</span></p>
                <input type="text" id="end_date" class="form-control"
                 value="{{$edit ? date('Y-m-d',strtotime(optional($data)->end_date)) : old('end_date')}}"  name="end_date" >
                <p class="error end_date_err mt-1"></p>
                @error('end_date')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
            </div>
            <div class="w-48 mb-3">
                <p class="font-weight-800">@lang('collections.brand_id')</p>
                @if($edit)
                @php
                    $alls = $data->allBrands() ? json_decode($data->allBrands()) : [];
                @endphp
                <select class="form-control form-control-sm aiz-selectpicker mb-2 mb-md-0 brand_product" id="brand_id" name="brand_id">
                    <option value="">{{ __('collections.select_brand') }}</option>
                    @if(count($alls) && !empty($alls))
                    @foreach(($alls ?? []) as $all)
                    <option value="{{optional($all)->id}}" {{ optional($all)->id == optional($brand_name)->id ? 'selected' : ''}}>{{optional($all)->name}}</option>
                    @endforeach
                    @endif
                </select>
                @else
                @php
                    $alls = $collection->allBrands() ? json_decode($collection->allBrands()) : [];
                @endphp
                <select class="form-control form-control-sm aiz-selectpicker mb-2 mb-md-0" id="brand_id" name="brand_id">
                    <option value="">{{ __('collections.select_brand') }}</option>
                    @if(count($alls) && !empty($alls))
                    @foreach(($alls ?? []) as $all)
                    <option value="{{optional($all)->id}}">{{optional($all)->name}}</option>
                    @endforeach
                    @endif
                </select>
                @endif

            </div>
            @if($edit)
            <div class="w-48 mb-3">
                 <p class="font-weight-800">@lang('collections.created-date')</p>
                 <input type="text" id="created_at" disabled  class="form-control" 
                  value="{{optional($data)->created_at}}"  name="created_at" >
                 <p class="error created_at_err  mt-1"></p>
                 @error('created_at')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
            </div>
            @endif

        </div>

        @if($edit)
        <div class="all-btn-group-popup">
            <a href="javascript:document.querySelector('.edit-info-collection').classList.remove('show')"
                class="mr-3 btn br-10">@lang('collections.cancel')</a>
            <a href="javascript:void(0)" class="btn btn-info br-10 btn-updateinfo" data-action="{{route('collections.info',optional($data)->id)}}" >@lang('collections.save')</a>
        </div>
        </form>
        @else
        <div class="all-btn-group-popup">
            <a href="javascript:document.querySelector('.edit-info-collection').classList.remove('show')"
                class="mr-3 btn br-10 btn-cancel-create">@lang('collections.cancel')</a>
            <a href="javascript:void(0)" data-action="{{route('collections.validate')}}" data-text="data-info" class="btn btn-info br-10 btn-save-create">@lang('collections.save')</a>
        </div>
    @endif
    </div>
</div>
