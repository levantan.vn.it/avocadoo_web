<form action="{{route ('banners.updateCollection',$banners)}}" method="POST">
    @csrf
    <div class="collectionsEdit popup-db">
        <div class="popup-products-edit">
            <div class="popup-header">
                <a href="javascript:document.querySelector('.collectionsEdit').classList.remove('show')"
                    class="text-yl close-model">
                    <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
    
                </a>
            </div>
            <p class="font-weight-800">@lang('banners.link') <span class="text-danger">*</span></p>
            <div class="row m-0 all-form-productid mb-3 ">
                <div class="w-48">
                    <select class="form-control form-control-sm aiz-selectpicker mb-2 mb-md-0" id="collection_id" name="collection_id">
                        <option value="0"></option>
                        @foreach ($collections as $item)
                            <option value="{{$item->id}}" @if ($banners->collection_id == $item->id) selected @endif data-title="">#{{$item->id}}</option>
                        @endforeach
                        @error('collection_id')
                            <p class="error mt-1">{{$message}}</p>
                        @enderror
                        @if($errors->has('collection_id'))
                        <span class="errors">
                            <strong class="text-errors">{{ $errors->first('collection_id') }}</strong>
                        </span>
                        @endif
                    </select>
                </div>
                <div class="w-48">
                    <input type="text" class="form-control" id="joined_date131"  name="title" value="{{optional($banners)->title??old('title')}}">
                    @if($errors->has('title'))
                    <span class="errors">
                        <strong class="text-errors">{{ $errors->first('title') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="all-btn-group-popup">
                    <a href="javascript:document.querySelector('.collectionsEdit').classList.remove('show')" class="mr-3 btn">@lang('banners.cancel')</a>
                    <button type="submit" class="btn btn-info">@lang('banners.save')</button>
            </div>
        </div>
    </div>
</form>
