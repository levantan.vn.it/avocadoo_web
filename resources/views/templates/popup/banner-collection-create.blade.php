<div class="collectionsEdit2 z-index-9 popup-db">
    <div class="popup-products-edit">
        <div class="popup-header">
            <a href="javascript:document.querySelector('.collectionsEdit2').classList.remove('show')" class="text-yl close-model">
                <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">

            </a>
        </div>
        <p class="font-weight-800">@lang('banners.link') <span class="text-danger">*</span></p>
        <div class="row m-0 all-form-productid mb-3 ">
            <div class="w-48">
                <select class="form-control form-control-sm aiz-selectpicker mb-2 mb-md-0" id="status514" name="collection_id" form="form-banner">
                    <option value="" selected>@lang('banners.nothing')</option>
                    @foreach ($collections as $item)
                    <option value="{{$item->id}}">#{{$item->id}}</option>
                    @endforeach

                </select>
                @error('collection_id')
                <p class="error mt-1">{{ $message}}</p>
                @enderror
            </div>

            <div class="w-48">
                <input type="text" class="form-control" name="title" value="" id="joined_date411" form="form-banner">
                @error('title')
                <p class="error mt-1">{{ $message}}</p>
                @enderror
            </div>
        </div>

        <div class="all-btn-group-popup">
            <a href="javascript:document.querySelector('.collectionsEdit2').classList.remove('show')" class="mr-3 btn">@lang('banners.cancel')</a>
            <a href="javascript:void(0)" data-action="{{route('banners.validateBannerCollection')}}" class="mr-3 btn btn-info clickAddCollection">@lang('banners.save')</a>
        </div>
    </div>
</div>