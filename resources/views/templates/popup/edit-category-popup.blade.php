<div class="edit-category popup-db">
    <div class="popup-products-edit popup-edit-category">
        <form method="post" class="redirect-popup d-inline-block w-100 edit-primary">
             @csrf
            <div class="popup-header">
            <a href="javascript:document.querySelector('.edit-category').classList.remove('show')"
                class="text-yl close-model">
                <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
            </a>
        </div>
        <div class="row m-0 all-form-productid mb-3">
            <div class="w-100">
                <p class="font-weight-800 d-flex">@lang('categories.title-cate')&nbsp; <span class="text-danger" style="margin-top: -3px">*</span></p>
                <input type="text" name="title" class="form-control @error('title') is-invalid  @enderror" >
                @error('title')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="row m-0 all-form-productid mb-3">
            <div class="w-100">
                <p class="font-weight-800">@lang('categories.type-cate')</p>
                <input type="text" class="form-control" readonly placeholder="@lang('categories.pri-cate')">
            </div>
        </div>


        <div class="w-100 mb-3">
            <p class="font-weight-800">@lang('categories.img-cate')&nbsp; <span class="text-danger" style="margin-top: -3px">*</span></p>
            <div class="banner-img-title mt-3">
                <div class="kt-img row custom-image">
                    @include('backend.inc.upload_file',['values_file'=>null,'type'=>'single'])
                </div>
            </div>
         </div>
        <div class="all-btn-group-popup">
                <a href="javascript:document.querySelector('.edit-category').classList.remove('show')"
                    class="mr-3 btn  br-10">@lang('categories.cancel')</a>
                    <button type="button" href="" class="btn btn-info br-10 check-form">@lang('categories.save')</button>
                </div>
            </form>
    </div>
</div>
