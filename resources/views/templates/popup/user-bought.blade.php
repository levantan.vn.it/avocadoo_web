<div class="mark-boughts popup-db">
    <div class="popup-products-edit br-10">
        <div class="popup-header">
            <a href="javascript:document.querySelector('.mark-boughts').classList.remove('show')"
                class="text-yl close-model">
                <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
            </a>
        </div>
        <div class="overflow-auto show-like">
            <div class="">
                @if (count($orders))
                    @foreach ( ($orders ?? []) as $order)
                        @foreach( ($order->products ?? []) as $product)
                        <?php $item = $product->minSkuItem; 
                            $photo = optional($product->photos)[0];
                        ?>
                            <div class="d-flex mt-2 ">
                                <div class="mb-1">
                                    {{-- <img class="img-table mr-4" src="{{ $product->file_url }}" alt="{{ $product->name }}"> --}}
                                    <img class="img-table mr-4" src="{{ $photo ?? my_asset('assets/img/placeholder.jpg')}}" alt="">
                                </div>
                                <div>
                                    <p class="mb-1">
                                        @lang('users.sku') <b>{{ $product->minSkuItem ? $product->minSkuItem->sku :null  }}</b>
                                    </p>
                                    <p class="mb-1">
                                        {{ $product->name }}
                                    </p>
                                    <p class="mb-1">
                                        <p>
                                            {{format_price($product->minPrice)}} @lang('users.pri')
                                        </p>
                                        <?php
                                        // $number = $product->minPrice;
                                        // echo $number==0 ? '0 원' : number_format($number, 0, ',', ',') . ' 원';
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <hr>
                        @endforeach
                    @endforeach
                @else
                    <h5 class="mb-0 py-3 text-center">@lang('users.nodata_bought')</h5>
                @endif
            </div>
        </div>
    </div>
</div>
