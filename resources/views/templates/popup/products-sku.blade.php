<form action="{{route ('products.updateSkus',$products)}}" method="post" class="redirect-popup d-inline-block">
    @csrf
    <div class="form-skus popup-db">
        <div class="popup-products-edit popup-products-transform">
            <div class="popup-header">
                <a href="javascript:document.querySelector('.form-skus').classList.remove('show')" class="text-yl close-model">
                    <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
                </a>
            </div>
            <div class="custom-productHeight">
                <div class="mb-3">
                    <p class="font-weight-800 fs-16">@lang('products.pr-var')</p>
                </div>
                @if(!empty($product_sku) && count($product_sku))
                @foreach($product_sku as $index => $sku)
                <div class="mb-3 ">
                    <div class=" d-flex {{!empty($sku->sku) && $sku->status == 2?'opacity-5':''}}">
                        <p class="font-weight-800 fs-16">{{$sku->title??($product_sku_title[$index]??'')}} <span class="text-danger">*</span></p>
                        <input type="hidden" name="title[]" value="{{$sku->title??($product_sku_title[$index]??'')}}">
                        <input type="hidden" name="variant[]" value="{{$variant_sku[$index]??$sku->variant}}">
                    </div>
                    <div class="row m-0 all-form-productid">
                        <div class="width-productId">
                            <input type="text" readonly class="fs-16 form-control" name="sku[]" value="{{!empty($sku->sku)?($sku->sku?? ''):($sku?? '')}}">
                        </div>
                        <input type="hidden" class="status" name="status[]" value="{{!empty($sku->sku) && $sku->status == 2?2:1}}">
                        <div class="width-inputSku">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Kg</span>
                                </div>
                                <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' {{!empty($sku->sku) && $sku->status == 2?'readonly':''}} class="form-control remove-readonly" name="weight[]" value="{{!empty($sku->sku)?$sku->weight:''}}">
                            </div>
                            @error("weight.{$index}")
                            <p class="error mt-1">{{ $message}}</p>
                            @enderror
                        </div>
                        <div class="width-inputSku">
                            <div class="input-group input-PriSale">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">@lang('orders.pri')</span>
                                    </div>
                                    <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' {{!empty($sku->sku) && $sku->status == 2?'readonly':''}} class=" form-control remove-readonly" name="price[]" value="{{!empty($sku->sku)?($sku->price):''}}">
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">%</span>
                                    </div>
                                    <input type="number" {{!empty($sku->sku) && $sku->status == 2?'readonly':''}} class=" form-control remove-readonly" name="sale[]" value="{{!empty($sku->sku)?$sku->sale:''}}">
                                </div>
                                @error("price.{$index}")
                                <p class="error mt-1">{{ $message}}</p>
                                @enderror
                                @error("sale.{$index}")
                                <p class="error mt-1">{{ $message}}</p>
                                @enderror
                            </div>
                        </div>
                        <div class="width-inputSku">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">@lang('products.quantity')</span>
                                </div>
                                <input type="number" {{!empty($sku->sku) && $sku->status == 2?'readonly':''}} class="form-control remove-readonly" name="qty[]" value="{{!empty($sku->sku)?$sku->qty:''}}">
                            </div>
                            @error("qty.{$index}")
                            <p class="error mt-1">{{ $message}}</p>
                            @enderror
                        </div>
                        <a href="javascript:void(0);" class="all-push {{!empty($sku->sku) && $sku->status == 2?'':'d-none'}}">
                            <img src="{{ static_asset('assets/img/icons/add.png') }}" class="img-remove-push mr-2 " alt="Add">
                        </a>
                        <a href="javascript:void(0);" class="all-push2 {{!empty($sku->sku) && $sku->status == 2?'d-none':''}}">
                            <img src="{{ static_asset('assets/img/icons/minus.png') }}" class="img-remove-push mr-2 " alt="Minus">
                        </a>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
            <div class="all-btn-group-popup">
                <a href="javascript:document.querySelector('.form-skus').classList.remove('show')" class="mr-3 btn">@lang('banners.cancel')</a>
                <button type="submit" data-action="{{route('products.validateSku')}}" class="btn btn-info click-validateSku">@lang('banners.save')</button>
            </div>
        </div>
    </div>
</form>