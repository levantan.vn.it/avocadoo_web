<div class="supplier-information popup-db">
    <div class="popup-products-edit">
        <form action="{{ route('suppliers.update', $data->id) }}" method="post" class="redirect-popup d-inline-block w-100">
        <div class="popup-header">
            <a href="javascript:document.querySelector('.supplier-information').classList.remove('show')"
                class="text-yl close-model">
                <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">

            </a>
        </div>
        <div class="row m-0 all-form-productid mb-3 ">
            <div class="w-48">
                <label class="font-weight-800"> @lang('suppliers.id')</label>
                <input type="text" class="form-control" readonly value="#{{ $data->id }}">
            </div>
            <div class="w-48">
                <label class="font-weight-800 d-flex"> @lang('suppliers.sup-name')&nbsp; <span class="text-danger" style="margin-top: -3px">*</span></label>
                <input type="text" class="form-control @error('name') is-invalid  @enderror" maxlength="255" name="name" value="{{ $data->name }}">
                @error('name')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
            </div>
        </div>

        <div class="row m-0 all-form-productid mb-3 ">
            <div class="w-48">
                <label class="font-weight-800 d-flex">  @lang('suppliers.sup-address')&nbsp; <span class="text-danger" style="margin-top: -3px">*</span></label>
                <input type="text" class="form-control @error('address') is-invalid  @enderror" maxlength="255" name="address" value="{{ $data->address }}">
                @error('address')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
            </div>
            <div class="w-48">
                <label class="font-weight-800 d-flex">  @lang('suppliers.location')&nbsp; <span class="text-danger" style="margin-top: -3px">*</span></label>
                <input type="text" class="form-control @error('location') is-invalid  @enderror"  maxlength="255" name="location" value="{{ $data->location }}">
                @error('location')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="row m-0 all-form-productid mb-3 ">
            <div class="w-48">
                <label class="font-weight-800 d-flex">  @lang('suppliers.email')&nbsp; <span class="text-danger" style="margin-top: -3px">*</span></label>
                <input type="email" class="form-control @error('email') is-invalid  @enderror"  name="email" value="{{ $data->email }}">
                @error('email')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
            </div>
            <div class="w-48">
                <p class="font-weight-800 d-flex">  @lang('suppliers.sup-phone')&nbsp; <span class="text-danger" style="margin-top: -3px">*</span></p>
                <input type="tel" id='maskpopupinfor' class="form-control @error('phone') is-invalid  @enderror" maxlength="11" name="phone" value="{{ $data->phone }}">
                @error('phone')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
            </div>
        </div>

        <div class="row m-0 all-form-productid mb-3 ">
            <div class="w-48">
                <label class="font-weight-800 d-flex"> @lang('suppliers.sup-rep')&nbsp; <span class="text-danger" style="margin-top: -3px">*</span></label>
                <input type="text" class="form-control @error('representative') is-invalid  @enderror" maxlength="255" name="representative" value="{{ $data->representative }}">
                @error('representative')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
            </div>
            <div class="w-48">
                <label class="font-weight-800"> @lang('suppliers.j-date')</label>
                <input type="text" class="form-control" readonly value="{{ $data->created_at->format('Y/m/d') }}">
            </div>
        </div>

        <div class="all-btn-group-popup">
                <a href="javascript:document.querySelector('.supplier-information').classList.remove('show')"
                    class="mr-3 btn  br-10">@lang('suppliers.cancel')</a>
                 @csrf
                 <button type="submit" href="javascripts:void(0)" class="btn btn-info br-10">@lang('suppliers.save')</button>
        </div>
        </form>
        </div>
    </div>
</div>
