<form action="{{route('orders.updateDelivery',$order->id)}}" method="post"  class="redirect-popup d-inline-block">
    @csrf
    <div class="mark-oder-infors popup-db">
        <div class="popup-products-edit br-10">
            <div class="popup-header">
                <a href="javascript:document.querySelector('.mark-oder-infors').classList.remove('show')" class="text-yl close-model">
                    <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
                </a>
            </div>
            <div class="row m-0 all-form-productid mb-3 ">
                <div class="w-48">
                    <p class="font-weight-800">@lang('orders.delivery_transaction_id') <span class="text-danger">*</span></p>
                    <input type="text" class="form-control" name="delivery_id" @if ($order->delivery_id) value="{{ $order->delivery_id }}" @else value="" @endif>
                    @if($errors->has('delivery_id'))
                        <span class="errors">
                            <strong class="text-errors">{{ $errors->first('delivery_id') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="w-48">
                    <p class="font-weight-800">@lang('orders.shipper_name') <span class="text-danger">*</span> </p>
                    <input type="text" class="form-control" name="shipper_name" value="{{ $order->shipper_name }}">
                    @if($errors->has('shipper_name'))
                        <span class="errors">
                            <strong class="text-errors">{{ $errors->first('shipper_name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="row m-0 all-form-productid mb-3">
                <div class="w-48">
                    <p class="font-weight-800">@lang('orders.driver_number') <span class="text-danger">*</span></p>
                    <input type="number" class="form-control" name="shipper_phone" value="{{ $order->shipper_phone }}">
                    @if($errors->has('shipper_phone'))
                        <span class="errors">
                            <strong class="text-errors">{{ $errors->first('shipper_phone') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="w-48">
                    <p class="font-weight-800">@lang('orders.delivery_date') <span class="text-danger">*</span></p>
                    <input type="text" autocomplete="off" class="form-control custom-placeholder"  name="delivery_date" id="click_dateInfomation"  @if($order->delivery_date) value="{{ Date('Y-m-d',strtotime($order->delivery_date)) }}" @else value="" @endif>
                    @if($errors->has('delivery_date'))
                        <span class="errors">
                            <strong class="text-errors">{{ $errors->first('delivery_date') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="all-btn-group-popup">
                <a href="javascript:document.querySelector('.mark-oder-infors').classList.remove('show')" class="mr-3 btn  br-10">@lang('orders.cancel')</a>
                <button type="submit" class="btn btn-info br-10 ">@lang('orders.save')</button>
            </div>
        </div>
    </div>
</form>