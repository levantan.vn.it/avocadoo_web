<div class="supplier-add-images popup-db">
    <div class="popup-products-edit">
        <form action="{{route('upload.update-image')}}" method="post" id="form-mark-deceased" class="redirect-popup">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="entity" value="supplier">
            <input type="hidden" name="route" value="suppliers.create">
            <div class="popup-header">
                <a href="javascript:document.querySelector('.supplier-add-images').classList.remove('show')"
                    class="text-yl close-model">
                    <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">

                </a>
            </div>
            <div>
                <div class="w-96 mb-3">
                    <b>@lang('suppliers.img-sup')</b>
                </div>
                <div class="w-96 mb-3 col-12">
                    <div class="row">
                        @include('backend.inc.upload_file',['values_file'=>$data->id_images])
                    </div>

                </div>
                <div class="all-btn-group-popup">
                    <a href="javascript:document.querySelector('.supplier-add-images').classList.remove('show')"
                        class="mr-3 btn  br-10">@lang('suppliers.cancel')</a>
                    <button type="submit" class="btn btn-info br-10">@lang('suppliers.save')</button>
                </div>
            </div>
        </form>
    </div>

</div>
