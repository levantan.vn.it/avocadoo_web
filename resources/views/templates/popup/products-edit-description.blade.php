<div class="mark-edit-descriptions popup-db">
   <div class="popup-products-edit br-10">
      <form action="{{route('products.updateDesc',['id'=>$products->id])}}" method="post"  class="redirect-popup">
         @csrf
         <div class="popup-header">
            <a href="javascript:document.querySelector('.mark-edit-descriptions').classList.remove('show')"
               class="text-yl close-model">
               <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
            </a>
         </div> 
         <div class="w-96 mb-3 row m-0 all-form-productid">
            <b>@lang('products.pr-des') <span class="text-danger">*</span></b>
         </div>
         <div class="bg-popups">
            <div class="bg-popup">
               <textarea class="aiz-text-editor" name="description">{!! optional($products)->description!!}</textarea>
            </div>
            @if($errors->has('description'))
               <p class="error mt-1">{{$errors->first('description')}}</p>
            @endif
         </div>
         <div class="all-btn-group-popup">
            <a href="javascript:document.querySelector('.mark-edit-descriptions').classList.remove('show')"
               class="mr-3 btn br-10">@lang('suppliers.cancel')</a>
            <button type="submit" class="btn btn-info br-10">@lang('suppliers.save')</button>
         </div>
      </form>
   </div>
</div>
