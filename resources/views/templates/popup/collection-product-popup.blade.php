<div class="edit-product-collection popup-db">
    <div class="popup-products-edit">
        <div class="popup-header">
            <a href="javascript:document.querySelector('.edit-product-collection').classList.remove('show')"
                class="text-yl close-model close-model-product">
                <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">

            </a>
        </div>
        <div class="modal-product">
            <div class="search-bar">
                <input type="text" class="form-control " id="search-product" name="search" placeholder="@lang('collections.search-pro')">
                @if($edit)
                <button type="button" class="btn-search" data-action="{{route('collections.information',$data->id)}}">
                    <img width="20px" src="{{static_asset('assets/img/icons/search_icon.svg')}}" alt="">
                </button>
                @else 
                <button type="button" class="btn-search" data-action="{{route('collections.create')}}">
                    <img width="20px" src="{{static_asset('assets/img/icons/search_icon.svg')}}" alt="">
                </button>
                @endif
            </div>
            @if($edit)
            <div class="product-collection">
                @if(count($products) && !empty($products))
                @foreach ($products as $product_collect)
                    <div class="overflow-auto mt-2">
                        <div class="d-flex bd-highlight align-items-center">
                                <div class="p-2 bd-highlight">
                                    @if($product_collect->images)
                                        @php
                                            $pro_img = explode(',',$product_collect->images);
                                        @endphp
                                        <img class="img-table mr-2" src="{{static_asset($pro_img[0])}}" alt="img" >
                                    @else 
                                        <img class="img-table mr-2" src="{{static_asset('/assets/img/placeholder.jpg')}}" alt="img" >
                                    @endif
                                </div>
                                <div class="p-2 bd-highlight">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>SKU: <b>{{$product_collect->minPriceSku}}</b></td>
                                            </tr>
                                            <tr>
                                                <td>{{$product_collect->name}}</td>
                                            </tr>
                                            @php
                                            $min_price = collect($product_collect->skus)->min('price');
                                            @endphp
                                            <tr>
                                                <td>{{format_price($min_price)}}원</td>
                                            </tr>
                                        </tbody>
                                        {{-- @dump($product_collect->id) --}}
                                    </table>
                                </div>
                                <div class="ml-auto p-2 bd-highlight">
                                    <form action="{{route('collections.producDel',$data->id)}}" method="post" class="form_delete">
                                        @csrf
                                        <input type="hidden" name="delete_product" value="{{$product_collect->id}}" >
                                        <a href="javascript:void(0)" class="a-delete-products"> <i class="fas fa-times"></i>
                                        </a>
                                    </form>

                                </div>
                        </div>
                    </div>
                @endforeach
                @endif
            </div>
            <div class="all-product">
                <form action="{{route('collections.product',$data->id)}}" method="post" >
                    @csrf
                    <div id="product-list">

                    </div>
                </form>
            </div>
            @else 
            <div class="all-product">
                <form action="" method="post" >
                    @csrf
                    <div id="product-list">
                    </div>
                </form>
            </div>
            @endif
            
        </div>

    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
