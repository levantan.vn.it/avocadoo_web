<div class="members-roles popup-db z-index-99">
    <div class="popup-products-edit">
        <div class="popup-header">
            <a href="javascript:document.querySelector('.members-roles').classList.remove('show')"
                class="text-yl close-model">
                <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">

            </a>
        </div>
        {{-- @dump($edit) --}}
       @if($edit) <form class="form-role">
            @csrf            
            @endif
            <div class="row m-0 all-form-productid mb-3 ">
                <div class="w-48 res-member">
                    <div class="wrapper">
                        <p class="font-weight-800">@lang('members.role') <span class="required-icon"> *</span></p>
                        @php
                        $roles = $edit ? $member->allRoles() : $members->allRoles();
                        @endphp
                        <select class="form-control form-control-sm aiz-selectpicker mb-2 mb-md-0 select_role" id="role_id" name="role_id">
                            <option value="" >{{__('members.select_roles')}}</option>
                            @foreach($roles as $index => $role)
                            <option value="{{$role->id}}" {{($edit ? $role->name == optional($member->role)->name :  '') ? 'selected': ''}}>{{$role->name?? ''}}</option>
                            @endforeach
                        </select>
                        @error('role_id')
                        <p class="error mt-1">{{ $message}}</p>
                        @enderror
                        <p class="error role_id_err"></p>
                    </div>
                </div>
            </div>
            <div class="row m-0 all-form-productid mb-3 access_show">
                <div class="w-100">
                    <p class="font-weight-800">@lang('members.noti-access') </p>
                    @if($edit)
                    @php
                        $pers = json_decode(optional($member->role)->permissions)?? [];
                        $arr = [];
                        foreach($pers as $per) {
                            $arr[] = $trans[$per];
                        }
                        if(count($arr) == 1)
                        $access_edit = implode("",$arr);
                        elseif(count($arr) >= 2)
                        $access_edit = implode(", ",$arr);
                        else
                        $access_edit = '';
                        // $access_edit = implode(", ",$pers);
                    @endphp
                    {{-- <p class="access_edit">{{$access_edit}}</p> --}}
                    @endif
                    @foreach($roles as $index => $role)
                    @php
                        $js = json_decode($role->permissions)?? [];
                        $arr = [];
                        foreach($js as $j) {
                            $arr[] = $trans[$j];
                        }
                        if(count($arr) == 1)
                        $access = implode("",$arr);
                        elseif(count($arr) >= 2)
                        $access = implode(", ",$arr);
                        else
                        $access = '';
                    @endphp
                    {{-- @dump($js) --}}
                    @if($edit)
                    <p class="access {{$role->name == optional($member->role)->name ? 'show' : ''}}" id = {{$role->id}}>{{$access}}</p>
                    @else
                    <p class="access" id = {{$role->id}}>{{$access}}</p>
                    @endif
                    @endforeach
                    
                </div>
            </div>
            <div class="row m-0 all-form-productid mb-3 ">
                @if($edit)
                <div class="w-48 res-member">
                    <p class="font-weight-800">@lang('members.joined') </p>
                     <input type="text" autocomplete="off" class="form-control" placeholder="YY-MM-DD" disabled
                     name="created_at" id="created_at"  value="{{date('Y-m-d', strtotime(optional($member)->created_at))}}">
                    @error('created_at')
                      <p class="error mt-1">{{ $message}}</p>
                    @enderror
                    <p class="error created_at_err"></p>
                </div>
                @endif
            </div>
            <div class="all-btn-group-popup">
                    @if($edit)
                    <a href="javascript:document.querySelector('.members-roles').classList.remove('show')"
                        class="mr-3 btn">@lang('members.cancel')</a>
                    <button type="button" data-action="{{route('members.updateRole',optional($member)->id)}}"
                         class="btn btn-info btn-update-roles">@lang('members.save')</button>
                    @else
                    {{-- <a href="javascript:void(0)" class="btn-cancel-role mr-3 btn">Cancel</a> --}}
                    <input type="reset" id="role-reset" class="btn-cancel-role mr-3 btn " value="@lang('members.cancel')">
                    <a href="javascript:void(0)" class="btn-create-role btn btn-info" data-text="data-role" data-action="{{route('members.validate-roles')}}">@lang('members.save')</a>
                    @endif
            </div>
        @if($edit)</form> @endif

    </div>
</div>
