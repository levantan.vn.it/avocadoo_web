<div class="mark-banners-img popup-db">
    <div class="popup-products-edit">
        <form action="{{route('banners.updateImageBanner',$banners->id)}}" method="post" class="redirect-popup">
            @csrf
            <input type="hidden" name="id" value="{{$banners->id}}">
            <input type="hidden" name="entity" value="banner">
            <input type="hidden" name="route" value="banners.detail">
            <div class="popup-header">
                <a href="javascript:document.querySelector('.mark-banners-img').classList.remove('show')" class="text-yl close-model">
                    <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
                </a>
            </div>

            <div>
                <b>@lang('banners.bn_img') </b>
            </div>
            <div class="banner-img-title mt-3">
                <div class="kt-img row">
                    @include('backend.inc.upload_file',['values_file'=>$banners->id_images,'type'=>'single'])
                </div>
            </div>
            @error('id_files')
            <p class="error mt-1">{{$message}}</p>
            @enderror
            <div class="all-btn-group-popup">
                <a href="javascript:document.querySelector('.mark-banners-img').classList.remove('show')" class="mr-3 btn br-10">@lang('banners.cancel')</a>
                <button type="submit" class="btn btn-info br-10">@lang('banners.save')</button>
            </div>
        </form>
    </div>
</div>