<div class="mark-notifications-img popup-db z-index-99">
    <div class="popup-products-edit">
            <input type="hidden" name="id" value="">
            <input type="hidden" name="entity" value="notification">
            <input type="hidden" name="route" value="notifications.index">
            <div class="popup-header">
                <a href="javascript:document.querySelector('.mark-notifications-img').classList.remove('show')"
                    class="text-yl close-model">
                    <img src="{{ static_asset('assets/img/icons/x.svg') }}" class="" alt="X">
                </a>
            </div>
            <div>
                <div class="w-96 mb-3">
                  <p class="font-weight-bold">@lang('notifications.noti-img') </p>
                </div>
                <div class="w-96 mb-3 col-12">
                    <div class="row">
                        @include('backend.inc.upload_file',['values_file'=> '','type' => 'single'])
                    </div>
                </div>
                <div class="all-btn-group-popup">
                  <a href="javascript:document.querySelector('.mark-notifications-img').classList.remove('show')"
                      class="mr-3 btn btn_cancel br-10">@lang('notifications.btn_cancel')</a>
                  <a href="javascript:void(0)" data-text="data-image" class="btn btn-info br-10 btn_addimg" data-action="{{route('notifications.validate_image')}}"
                  >@lang('notifications.btn_save')</a>
                </div>
  
            </div>
    </div>
  </div>