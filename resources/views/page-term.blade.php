<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ static_asset('assets/css/custom-style.css?v='.time()) }}">

    <title>@lang('orders.terms')</title>
</head>
<body>
<div class="custom-title-policy">
    <p>@lang('orders.terms')</p>
</div>
<div class="custom-content-policy">
    {!! $page->value !!}
</div>
</body>
</html>
