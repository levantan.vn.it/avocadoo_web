
<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'Dashboard' => '대시보드',
    'Users' => '회원관리',
    'Orders' => '주문관리',
    'Suppliers' => '브랜드관리',
    'Products' => '상품관리',
    'Categories' => '카테고리 관리',
    'Collections' => '컬렉션 관리',
    'Banners' => '배너관리',
    'Notifications' => '알림관리',
    'Messages' => '메시지 관리',
    'Team-Members' => '어드민 관리',
    'Settings' => '설정',

    'search-menu'=>'메뉴 검색',
    'email' => '이메일',
    'pass' => '비밀번호',
    'sign-up' => '로그인',
    'forgotPassword' => '비밀번호 찾기?',
    'profile'=>'프로필',
    'logout'=>'로그 아웃',
    'welcome-to'=>'안녕하세요',
    'login'=>"로그인 해주세요.",
    'uploaded-files'    =>  "Upload file management"






];

