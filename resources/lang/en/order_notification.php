<?php

return [
    'unpaid' => [
        'title' => 'Your order :ORDER_ID have created successfully',
        'content' => 'Your order :ORDER_ID created success fully. Please check your order detail for more informations.'
    ],
    'paid' => [
        'title' => 'Your order :ORDER_ID was paid successfully',
        'content' => 'Your order :ORDER_ID was paid success fully. Please check your order detail for more informations.'
    ],
    'delivering' => [
        'title' => 'Your order :ORDER_ID is delivering',
        'content' => 'Your order :ORDER_ID was transfered to :DELIVERY_NAME with delivery ID :DELIVERY_ID to delivery. Please check your order detail for more informations.'
    ],
    'arrived' => [
        'title' => 'Your order :ORDER_ID is arrived',
        'content' => 'Your order :ORDER_ID was deliveried by :DELIVERY_NAME with delivery ID :DELIVERY_ID is arrived. Please check your order detail for more informations.'
    ],
    'cancelled' => [
        'title' => 'Your order :ORDER_ID was cancelled',
        'content' => 'We are really sorry for your cancled order :ORDER_ID. Because of this product is out of stocks, or many other reasons so it cannot be deliveried to you. So this order gonna be cancled.\n We going to make refund money process to your wallet, and its gonna take a few days.'
    ],
    'refund' => [
        'title' => 'Your order :ORDER_ID was refunded',
        'content' => 'Your order :ORDER_ID was refunded'
    ]
];
