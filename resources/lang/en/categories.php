<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
// categories-index
    'cate' => 'CATEGORIES',
    'search-cate' => 'Search Category',
    'pri-cate'  =>  "Primary Category",
    'child-cate'  =>  "Child Category",
    'title-cate'  =>  " Category Title",
    'type-cate'  =>  " Category Type",
    'img-cate'  =>  "Category Images",
    'lock' => "Lock",
    'unlock' => "UnLock",
    'edit' => "Edit",
    'lock' => "Lock",
    'del' => 'Delete',
    'cancel' => 'Cancel',
    'save' => 'Save',

    'noti_lock'=> 'The primary category has been locked.',
    'noti_unlock'=> 'The primary category has been unlocked.',
    'noti_updatechild'=> 'The category has been updated successfully.',
    'noti_updatepri'=> 'The category has been updated successfully.',
    'noti_createchild'=> 'The child category has been created.',
    'noti_createpri'=> 'The primary category has been created.',
    'noti_delete_pri'=>'The primary category has been deleted.',
    'noti_delete_pri_false'=>'You cannot delete a category that contains products.',
    'noti_delete_child'=>'The child category has been deleted.',

    'nothing_found' => 'Nothing Found',

    'lock_pri'=> 'Do you want to lock the primary category?',
    'unlock_pri'=> 'Do you want to unlock the primary category?',
    'delete_pri'=> 'Do you want to delete the child category?',
    'delete_child' =>'Do you want to delete the primary category?',
    'continue'=> 'Do you want to continue?',
    'yes'=>'Yes',
    'no'=>'No'










];
