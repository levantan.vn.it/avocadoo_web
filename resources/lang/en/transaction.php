<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'transaction' => 'TRANSACTIONS',
    'tran_search' => "Search Transaction ID / Order ID",

    'id' => "Transaction ID",
    'date' => "Date",
    'us_id' => "User ID",
    'od_id' => "Order ID",
    'money' => "Money",
    'method'  =>  "Method",
    'status'  =>  "Status",
    'view'  =>  "View",
    'active'  =>  "Active",
    'del'  =>  "Delete",



];
