<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Settings Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'settings' => 'Settings',
    // Company Information
    "title_company" => "Company Information",
    "title_infor" => "Information",
    "address" => "Address",
    "register_number" => "Business Registration Numbers",
    "email" => "Email",
    "hotline" => "Hotline",
    "description" => "Description",
    "btn_save" => "Save",
    "msg_company_success" => "Update Company Information Success",
    "company_name" => 'Company Name',
    "represent_name" => 'Representative Name',
    // FAQ
    "faq" => "FAQ",
    "btn_add_faq" => "Add new question",
    "answer" => "Answer",
    "placeholder_question" => "Question title here",
    "msg_question_success" => "Create Question Success",
    "msg_answer_success" => "Update FAQ Content Success",
    "msg_validate_space" => "You cannot enter spaces.",
    'btn_delete' => 'Delete',
    'msg_deletefaq_success' =>  'Remove FAQ Success',
    'msg_deleterole_success' => "Remove Roles success",
    'faq_content_required' => 'Content spaces cannot be entered.',

    // Privacy Policies
    "privacy_title" => "Privacy Policies",
    "msg_policy_success" => "Update Privacy and Policies success",

    //Terms of Use
    "terms_title" => "Terms of Use",
    "msg_terms_success" => "Update Terms of Use Success",

    //Roles

    "role_title" => "Role Management",
    "btn_add_role" => "Add new role",
    "placeholder_roletitle" => "Type Role Title Here",
    "module_listing" => "Module Listing",
    "msg_module_success" => "Update Module Item Success",
    "msg_title_role" => "Create Role Title Success",
    "role_name_required" => 'The name field is required.',

    // Delivery Management
    "delivery_title" => "Delivery Management",
    "deli_fee" => 'Delivery Fee',
    "weight" => 'Weight',
    "distance" => "Distance(In City)",
    "interpro" => "Interprovincial",
    "weight_formu" => "Weight Formula",
    "distance_formu" => "Distance Formula",
    "inter_deli" => "Interprovincial Delivery",
    "weight_title" => "Weight base on Weight",
    "distance_title" => "Weight base on Distance",
    "kg" => "kg",
    "price" => "원",
    "add_weight" => "Add weight range",
    "first" => "First",
    "next" => "Next",
    "msg_required" => "This field cannot be empty.",
    "km" => "Km",
    "kilo" => "Kilometers (km)",
    "fixed_area" => "Fixed Area",
    "add_distance" => "Add distance range",
    "area" => "Area",
    "add_area" => "Add new area",
    "by_car" => "By Car",
    "by_airplane" => "By Airplane",
    "city" => "City",
    "add_city" => "Add new city/province",
    "msg_weight_sucess" => "Update weight range success",
    "msg_distance_sucess" => "Update distance range success",
    "msg_inter_success" => "Update Interprovincial Delivery success",

    'popup_delete' => 'Delete this row ?',
    'confirm_popup' => 'Do you want to continue ?',
    'yes' => 'Yes',
    'no' => 'No',
    'msg_row_delete' => 'Delete success',

    'chooses_city'  =>  'Choose City',
    'district'  =>  'District',
    'ward'  =>  'Ward',
    'chooses_district'  =>  'Choose District',
    'chooses_ward'  =>  'Choose Ward',
    'delivery_company'  =>  "Delivery Company",
    'company_shipping_name'  =>  "Shipping Company Name",
    'company_logo'  =>  "Company Logo",
    'company_delivery_time' =>  "Estimated delivery period ",
    'please_choose_logo'    =>  "Choose your company logo",
    'special_delivery'  =>  "Special Delivery",
    'delivery_condition'    =>  "Delivery Condition",
    'add_delivery_condition'    =>  "Add Delivery Condition",
    'fixed_price'   =>  "Fixed Price",
    'tax_code'  =>  "Tax Code"

];
