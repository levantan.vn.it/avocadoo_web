<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
// collection-index
    'collection' => 'COLLECTIONS',
    'search-collection' => 'Search Collection ID / Collection Title',
    'add'  =>  "ADD NEW COLLECTION",
    'search'  =>  "SEARCH",
    'reset'  =>  "RESET",
    'excel'  =>  "EXCEL",
    'created-date'  =>  "Created Date",
    'status'  =>  "Status",
    'active'  =>  "Active",
    'inactive'  =>  "InActive",
    'lock'  =>  "Lock",
    'id'  =>  "Collection ID",
    'img'  =>  "Images",
    'title'  =>  "Collection Title",
    'section'  =>  "Section",
    'products'  =>  "Products",
    'view'  =>  "View",
    'del'  =>  "Delete",
    'sort_descript' => 'Short Description',
    'start_date' => 'Start Date',
    'end_date' => 'End Date',
    'brand_id' => 'Brand Linked',
    'product_management' => 'Product management',
    'status_change' => 'Do you want to change collection status?',
    'delete_coll' => 'Delete Collection ',

// collection-create
    'create'  =>  "CREATE COLLECTION",
    'add-cl-info'  =>  "Add Collection Information",
    'add-cl-img'  =>  "Add Collection Images",
    'add-product'  =>  "Add Products",
    'select_section'=> "Select Section",
    'select_brand' => "Select Brand Link",

// collection information
    'collection_detail' => 'Collection Information',
    'img-cl'  =>  "Collection Images",
    'save' => 'Save',
    'cancel' => 'Cancel',
    'search-pro' => 'Search Products',
    'hot_deal'  =>  'Hot Deal',
    'special_events'    =>  "Special Events",
    'end_season_sale'    =>  "End of Season Sale",

    'no_pro'=> 'No product',
    'no_image' =>'No collection image',
    'noti_create'=>'The collection has been created.',



    'msg_infor_success' => "Update Infor Collection Success",
    'msg_add_product' => "Add Product Success",
    'msg_delete_product' => "Delete Product Success",
    'msg_delete_collection' => "Delete Collection Success",
    'msg_status_success' => "Change Status Success",
    'msg_image_success' => "Update Image Success",
    'popup_status' => 'Change status collections',
    'confirm_popup' => 'Do you want to continue',
    'yes' => 'Yes',
    'no' => 'No',
    "no_data_product" => "No results found."

];
