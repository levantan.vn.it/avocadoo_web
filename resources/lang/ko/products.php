
<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'products' => '상품관리',
    'add' => '상품 추가',
    'add-date' => '가입일',
    'id' => '상품 ID',
    'title' => '상품명',
    'skus' => 'SKUs',
    'var' => '종류',
    'vars' => '변종',
    'price' => '가격',
    'lasted' => '마지막 수정일',
    'quantity' => '수량',
    'status' => '상태',
    'in' => '재고 중',
    'out' => '재고있음',
    'created' => '생성일',
    'pr-img' => '상품 이미지',
    'pr-des' => '제품 설명',
    'pr-var' => 'SKU 정보',
    "description" => "설명",
    // 'pr-varss'=>'',
    'pr-skus' => 'SK우스 정보',
    'sku' => 'SKU',
    'pr-consign' => '위탁 ID',
    'cate' => '카테고리',
    'sup' => '브랜드',
    'ori' => '원산지',
    'size' => '사이즈',
    'place-search' => '상품 ID / 상품명',
    'delete-pro' => "상품 삭제",
    'sku-format' => "SKU 판",
    'var-title' => "변형 제목",
    'add-value' => "필드 추가",
    'add-variant' => "종류 추가",
    'value' => "값",
    'add-info' => '상품 정보 추가',
    'add-img' => '상품 이미지 추가',
    'add-skus' => '상품 SKU & 수량 추가',
    'create-product' => '상품 추가',
    'please_add_product_before_sku' =>  "SKU를 추가하기 전에 상품을 저장해 주세요.",
    'nothing' => "검색된 결과가 없습니다.",
    'update-info-success' => '상품정보를 업데이트 했습니다.',
    'update-sku-success' => '상품 SKU를 업데이트 했습니다.',
    'delete-success' => '상품이 삭제되었습니다.',
    'change-status-success' => '상품의 상태가 변경되었습니다.',
    'create-success' => '상품 추가 완료',
    'description-success' => '상품 상세설명을 업데이트 했습니다.',
    'product-detail' => '상품 관리 상세',
    'required-image'=>'상품 이미지 필드는 필수입니다.',
    'update-image-success'=>'상품 이미지를 업데이트 했습니다',








];
