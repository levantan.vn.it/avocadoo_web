<?php

return [
  'make_primary' => '주 배송지 설정에 실패했습니다.',
  'add_failed' => '배송지 추가에 실패했습니다.',
  'update_failed' => '배송지 수정에 실패했습니다.',
  'delete_failed' => '배송지 삭제에 실패했습니다.'
];
