<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
// categories-index
    'cate' => '카테고리 관리',
    'search-cate' => ' 카테고리',
    'pri-cate'  =>  "주요 카테고리",
    'child-cate'  =>  "하위 카테고리",
    'title-cate'  =>  " 카테고리 제목",
    'type-cate'  =>  " 카테고리 유형",
    'img-cate'  =>  "카테고리 이미지",
    'lock' => "자물쇠",
    'unlock' => "터놓다",
    'edit' => "편집하다",
    'lock' => "자물쇠",
    'del' => '삭제',
    'cancel' => '취소',
    'save' => '저장',

    'noti_lock'=> '기본 카테고리가 잠겼습니다.',
    'noti_unlock'=> '기본 카테고리가 잠금 해제되었습니다.',
    'noti_updatechild'=> '카테고리가 성공적으로 업데이트되었습니다.',
    'noti_updatepri'=> '카테고리가 성공적으로 업데이트되었습니다.',
    'noti_createchild'=> '하위 카테고리가 생성되었습니다.',
    'noti_createpri'=> '기본 카테고리가 생성되었습니다.',
    'noti_delete_pri'=>'기본 카테고리가 삭제되었습니다.',
    'noti_delete_pri_false'=>'상품이 포함된 카테고리는 삭제할 수 없습니다.',
    'noti_delete_child'=>'하위 카테고리가 삭제되었습니다.',

    'nothing_found' => '검색된 결과가 없습니다.',

    'lock_pri'=> '기본 카테고리를 잠그시겠습니까?',
    'unlock_pri'=> '기본 카테고리를 잠금 해제 하시겠습니까?',
    'delete_pri'=> '기본 카테고리를 삭제 하시겠습니까?',
    'delete_child' =>'하위 카테고리를 삭제 하시겠습니까?',
    'continue'=> '계속 하시겠습니까?',
    'yes'=>'예',
    'no'=>'아니오',
    'main-img'=>'카테고리 메인 이미지.',















];
