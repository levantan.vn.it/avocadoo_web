<?php

return [

    //index
    'users' => '회원관리',
    'users_detail' => '회원 상세',
    'search_id' => '회원 ID / 화면에 표시될 이름 / 폰번호 / Email',
    'add'  =>  "ADD NEW USER",
    'search'  =>  "검색",
    'reset'  =>  "초기화",
    'excel'  =>  "EXCEL",
    'state'  =>  "State",
    'id'  =>  "ID",
    'display'  =>  "화면에 표시될 이름",
    'email'  =>  "Email",
    'phone'  =>  "폰번호",
    'business_name'  =>  "상호명",
    'joined_date'  => "가입일",
    'status'  =>  "상태",
    'view'  =>  "조회",
    'del'  =>  "삭제",
    //detail
    'us_id'  =>  "회원 ID",
    'us_name'  =>  "이름",
    'tax_invoice'  =>  "세금 계산서",
    'representative'  =>  "대표명",
    'business_registration_document'  =>  "사업자 등록증",
    'business_registration_number'  =>  "등록번호",
    'store_name'  =>  "세금계산서 항목",
    'store_address'  =>  "배송수령지",
    'delivery_address'  =>  "주소",
    'analytics'  =>  "통계분석",
    'transaction'  =>  "거래내역",
    'delivery_book'  =>  "배송지",
    'payment_method'  =>  "결제수단",
    'like_product'  =>  "찜한 상품",
    'bought_product'  =>  "구매한 상품",
    'sku'  =>  "SKU:",
    //users detail edit business
    'cancel'  =>  "취소",
    'save'  =>  "저장",
    'business_name_id'  =>  "업체명 ID",
    'transaction_id'  =>  "거래 ID",
    'date'  =>  "날짜",
    'money'  =>  "금액",
    'method'  =>  "결제수단",
    //user detail delivery
    'location_name'  =>  "배송지명",
    'receiver'  =>  "수신인",
    'type'  =>  "배송지 유형",
    //user detail payment
    'value'  =>  "결제정보",
    'connected'  =>  "연결 여부",
    'active'  =>  "액티브",
    'deactive'  =>  "비활성",
    'block'  =>  "차단",
    'success'  =>  "성공",
    'failed'  =>  "실패했습니다",
    'primary'  =>  "주배송지",
    'normal'  =>  "일반",
    //popup
    'active_user'  =>  "활동중인 회원?",
    'inactive_user'  =>  "비활동 회원?",
    'del_user'  =>  "삭제된 회원?",
    'block_user'  =>  "차단된 회원?",
    'unblock_user'  =>  "차단해제된 회원?",
    'continue'  =>  "계속 진행하시겠습니까?",
    'deleted'  =>  "삭제!",
    'deleted_er'  =>  "변경 사항은 저장되지 않습니다!",
    
    'yes'  =>  "예",
    'no'  =>  "아니오",
    'pri' => '원',
    //messenger
    'messenger_del_user' => '관리자 멤버가 삭제되었습니다.',
    'messenger_status' => '사용자의 상태가 변경되었습니다',
    'messenger_business' => '사용자 비즈니스가 업데이트되었습니다',
    'messenger_information' => '사용자의 정보가 변경되었습니다',
    'nodata_transaction' => '거래 내역이 없습니다.',
    'nodata_delivery' => '등록된 배송지가 없습니다.',
    'nodata_payment' => '등록된 결제수단이 없습니다.',
    'nodata_like' => '찜한 상품이 없습니다.',
    'nodata_bought' => '구매한 상품이 없습니다.',
    //card payment
    'unknown'  =>  "알수없음",
    'maestro'  =>  "Maestro",
    'discover'  =>  "Discover",
    'masterCard'  =>  "MasterCard",
    'visa_Card'  =>  "Visa Card",
    'diners_Club'  =>  "Diners Club",
    'american_Express'  =>  "American Express",
    'JCB'  =>  "JCB",


    // 'custom' => [
    //     'email' => [
    //         'required' => 'We need to know your email address!',
    //         'max' => 'Your email address is too long!'
    //     ],
    // ],
    
    

];
