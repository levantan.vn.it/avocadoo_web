<?php

return [

    'order' => '주문관리',
    'order_detail' => '주문관리 상세',
    'search_id' => ' 주문 ID',
    'add'  =>  "주문 생성",
    'search'  =>  "검색",
    'reset'  =>  "초기화",
    'excel'  =>  "EXCEL",
    'od_id'  =>  "주문 ID",
    'delivery_id'  =>  "배송 ID",
    'od_date'  =>  "주문일",
    'pri' => '원',
    'us_id'  =>  "회원 ID",
    'email'  =>  "Email",
    'products' => '제품',
    'product'  =>  "상품 ",
    'price'  =>  "총 금액",
    'od_state'  =>  "상태",
    'active_or'  =>  "주문 재생성",
    'od_inacive'  =>  "비활성",
    'od_lock'  =>  "자물쇠",
    'date'  =>  "Date",
    'view'  =>  "조회",
    'active'  =>  "액티브",
    'del'  =>  "삭제",
    'unpaid'  =>  "결제전 주문",
    'completed'  =>  "주문완료",
    'cancel_order' => "취소된 주문",
    'reason_cancel' => "취소 사유",
    // 'delivered'=>"배송중",
    'cancel_l' => '주문취소',
    'cancel' => '취소',
    'save' => '저장',
    'delete-status'=>'삭제',
    'delivered'=> '배송완료',
    // detail
    'processing' => '배송준비중',
    'call_delivery' => '전화 배달',
    'display_name'  =>  "이름 표시하기",
    'full_name'  =>  "성명",
    'phone'  =>  "폰번호",
    'joined_date'  =>  "Joined Date",
    'delivery'  =>  "배송 주소",
    'note'  =>  "기타 요청",
    'billed'  =>  "청구서 정보",
    'delivery_method'  =>  "배송수단",
    'delivery_flashship'  =>  "배송수단",
    'payment_method'  =>  "결제수단",
    'or_sub'        => "부분합계",
    'or_fee'        => "배송비",
    'or_total'        => "총금액",
    'or_visa'        => "Visa Card",
    'or_transaction'        => "거래내역",
    'canceled' => '취소 된',
    //detail popup information
    'delivery_transaction_id' => '배송 ID',
    'shipper_name' => '배송기사',
    'shipper_phone' => '배송기사 전화번호',
    'driver_number' => '배송기사 전화번호',
    'delivery_date' => '배송일',
    //detail2
    'delivering'  =>  "배송중",
    'Complete'  =>  "배송완료",
    'make'=> "반송 요청",
    'Customer'  =>  "고객 확인",
    //detail popup add
    'supplier_img'  =>  "공급 업체 이미지",
    //create
    'new' => '새로운',
    'processing_od' => '주문 처리',
    'create_or' => '주문 생성',
    'add_od_information' => '주문 정보 추가',
    'add_delivery_payment_method' => '배송 및 결제 방법 추가',
    'add_products' => '제품 추가',
    //create order add 회원 ID . 배송 주소록 . 회원 이름. 수신인 이름.전화번호
    'delivery_bookk' => '배송 주소록',
    'user_namee' => '회원 이름',
    'receiverr' => '수신인 이름',
    'phonee' => '전화번호',


    'delivery_book' => '배달 책',
    'user_name' => '회원이름 ',
    'receiver' => '수화기',
    //create order delivery
    'delivery_fee' => '배송비',
    //billed-popup
    'delivery_billed' => '송장',
    // API
    'failed_in_add_product' => '장바구니에 제품을 추가하지 못했습니다.',
    'quantity'=>'수량',
    'day-delivery'=> '일 배달',
    'sub-total'=> '부분합계',
    'add_delivery_book' =>  "주문하시기 전에 배송지를 추가해 주세요.",
    'address_not_support_shipping'  =>  "귀하의 주소는 배송을 지원하지 않습니다",
    'cart_empty'    =>  "장바구니가 비어 있습니다.",
    'delivery-detail'=>'플래시 쉽',
    'create-order'=>'주문 생성',
    'add-info'=>'주문 정보 추가',
    'add-delivery'=>"배송 및 결제수단 추가",
    'add-products'=>'상품 추가',
    'yes' => '예',
    'no' => '아니오',
    'failed_payment'    =>  '결제 실패',
    'success_payment'    =>  '결제 성공',
    'not_found' =>  "주문을 찾을 수 없음",
    'title_confirm_refund'  =>  "환불 하시겠습니까?",
    'refund'    =>  "환불 금액",
    'status_refund' =>  "취소됨 - 환불된 금액 ",
    'no_payment'    =>  "미지급",
    'refund_faild'  =>  "환불 실패",
    'refund_success'  =>  "환불 완료 되었습니다.",
    'store_name'=>'매장명',
    'sample'=>'샘플 텍스트 입니다. 텍스트를 작성해 주세요.',
    'status-success'=> '주문 상태가 변경되었습니다.',
    'info-success'=>'주문 정보가 업데이트 되었습니다.',
    'delete-success'=>'주문이 삭제되었습니다.',
    'img-success'=>'주문의 상품 이미지가 업데이트 되었습니다.',
    'reason-success'=>'주문 취소 사유가 업데이트 되었습니다.',
    'img-invoice-success'=>'주문의 인보이스 이미지가 업데이트 되었습니다.',
    'change-delivery-success'=>'배송 상태를 배송완료로 변경 하시겠습니까?',
    'invoice'=>'송장',
    'create-success'=>'주문 생성 완료.',
    'delete-order'=>'주문 삭제?',
    'back-order'=>'주문 재생성?',
    'text-img'=>'송장',
    'text-img-invoice'=>'세금계산서',
    'tax-invoice'=>'세금계산서',
    'img-tax-invoice'=>'세금계산서 이미지를 업데이트 하였습니다.',
    'delivery-success'=>'배송단위 정보가 업데이트 되었습니다.',
    'change-status-orders'=>'주문 상태를 변경하시겠습니까?',
    'cancel-order-status'=>'주문취소?',
    'shipping'=>'배송 송장',
    'nothing'=>'선택 안됨',
    'file-pdf-success'=>'세금계산서 파일과 배송송장 파일을 업로드 하였습니다.',
    'order-detail'=>'주문상세',
    'not_deliveried'    =>  "회원님이 주문하신 상품은 아직 배송되지 않았습니다.",
    'validate-product'=>'상품 필드는 필수입니다.',
    'unpaid'=>'미결제',
    'PROCESSING'=>'배송준비중',
    'update-pdf-success'=>'배송완료 - 송장 전송 완료',
    'active-order'=>'진행중인 주문',

    //policy and terms
    'policy'=>'웹사이트 정책',
    'terms'=>'이용약관',
    'confirm-admin'=>'주문 완료 확인?',




    


];
