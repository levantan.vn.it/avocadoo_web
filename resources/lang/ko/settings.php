<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Settings Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'settings' => '설정',
    // Company Information
    "title_company" => "회사 정보",
    "title_infor" => "정보",
    "address" => "주소",
    "register_number" => "사업자 등록 번호",
    "email" => "Email",
    "hotline" => "연락처",
    "description" => "설명",
    "btn_save" => "저장",
    "msg_company_success" => "회사 정보 업데이트가 완료되었습니다.",
    "company_name" => '회사명',
    "represent_name" => '대표자명',
    // FAQ
    "faq" => "FAQ",
    "btn_add_faq" => "질문 추가",
    "answer" => "답변 ",
    "placeholder_question" => "질문 제목",
    "msg_question_success" => "질문이 추가되었습니다.",
    "msg_answer_success" => "FAQ 내용이 업데이트 되었습니다.",
    "msg_validate_space" => "공백을 입력할 수 없습니다.",
    'btn_delete' => '지우다',
    'msg_deletefaq_success' =>  'FAQ 성공 제거',
    'msg_deleterole_success' => "역할(Role) 성공 제거",
    'faq_content_required' => '함유량 공백을 입력할 수 없습니다.',

    // Privacy Policies
    "privacy_title" => "개인정보 보호정책",
    "msg_policy_success" => "개인정보보호정책이 업데이트 되었습니다.",

    //Terms of Use
    "terms_title" => "약관 관리",
    "msg_terms_success" => "이용약관 업데이트가 완료되었습니다.",

    //Roles

    "role_title" => "Role(역할) 관리",
    "btn_add_role" => "새 역할(Role) 추가 ",
    "placeholder_roletitle" => "역할 제목을 입력하세요.",
    "module_listing" => "접근할 수 있는 메뉴",
    "msg_module_success" => "관리자 역할의 접근 권한 메뉴가 업데이트 되었습니다.",
    "msg_title_role" => "새 ROLE(역할) 항목이 생성되었습니다.",
    "role_name_required" => "이름 항목은 필수입력 입니다.",

    // Delivery Management
    "delivery_title" => "배송관리",
    "deli_fee" => '배송료',
    "weight" => '무게',
    "distance" => "거리(In City)",
    "interpro" => "Interprovincial",
    "weight_formu" => "무게 공식",
    "distance_formu" => "거리 공식",
    "inter_deli" => "Interprovincial Delivery",
    "weight_title" => "무게 기준 배송",
    "distance_title" => "거리 기준 배송",
    "kg" => "kg",
    "price" => "원",
    "add_weight" => "무게 범위 추가",
    "first" => "처음",
    "next" => "다음",
    "msg_required" => "이 필드는 비워 둘 수 없습니다.",
    "km" => "Km",
    "kilo" => "킬로미터(km)",
    "fixed_area" => "고정된 지역(영역)",
    "add_distance" => "거리 범위 추가",
    "area" => "지역(영역)",
    "add_area" => "새 지역 추가",
    "by_car" => "차량 운송",
    "by_airplane" => "항공 운송",
    "city" => "도시",
    "add_city" => "새 시/도 추가",
    "msg_weight_sucess" => "무게 범위 업데이트 완료",
    "msg_distance_sucess" => "거리 범위 업데이트 완료",
    "msg_inter_success" => "Update Interprovincial Delivery success",

    'popup_delete' => '이 행 삭제',
    'confirm_popup' => '계속 하시겠습니까?',
    'yes' => '예',
    'no' => '아니',
    'msg_row_delete' => '삭제가 완료되었습니다.',

    'city'  =>  '시티',
    'chooses_city'  =>  '도시 선택',
    'district'  =>  '지구',
    'ward'  =>  '구',
    'chooses_district'  =>  '지구 선택',
    'chooses_ward'  =>  '와드 선택',
    'delivery_company'  =>  "배송업체 정보",
    'company_shipping_name'  =>  "배송업체명",
    'company_logo'  =>  "배송업체 로고",
    'company_delivery_time' =>  "예상 배송기간 ",
    'please_choose_logo'    =>  "배송업체 로고를 선택하세요.",
    'special_delivery'  =>  "특별 배송",
    'delivery_condition'    =>  "배송 조건",
    'add_delivery_condition'    =>  "배송 조건 추가",
    'fixed_price'   =>  "고정 가격",
    'tax_code'  =>  "세금 코드"

];
